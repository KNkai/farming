import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/loading_task.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';
import 'package:mi_smart/src/share/widget/load_more.dart';

import 'base_list_bloc.dart';

class BaseListPage<T> extends StatefulWidget {
  final Widget Function(T each) item;
  final String title;
  final GetListUseCase getListUseCase;
  final LoadMoreBaseListUseCase loadMoreBaseListUseCase;
  final SearchBaseListUseCase searchBaseListUseCase;
  final void Function(BuildContext context, T item) onTap;
  final bool onBack;
  final Widget Function(BuildContext, int) separatorBuilder;

  const BaseListPage(this.item,
      {this.onTap,
      this.title,
      this.onBack = true,
      this.separatorBuilder,
      this.getListUseCase,
      this.loadMoreBaseListUseCase,
      this.searchBaseListUseCase});

  @override
  _BaseListPageState createState() => _BaseListPageState<T>();
}

class _BaseListPageState<T> extends State<BaseListPage<T>> {
  BaseListBloc _bloc;

  @override
  void initState() {
    _bloc = BaseListBloc<T>(
        getListProtocol: widget.getListUseCase,
        searchProtocol: widget.searchBaseListUseCase,
        loadMoreProtocol: widget.loadMoreBaseListUseCase);
    _bloc.add(InitEvent());
    super.initState();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      onSearch: widget.searchBaseListUseCase != null
          ? (search) {
              _bloc.add(SearchEvent(search));
            }
          : null,
      title: widget.title,
      onBack: widget.onBack,
      child: LoadingTask(
        bloc: _bloc,
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: StreamBuilder<ListState>(
                stream: _bloc.stateList.stream,
                builder: (_, ss) {
                  final state = ss?.data;
                  if (state != null) {
                    if (state is ListEmptyState) {
                      return const Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            'Dữ liệu trống',
                            style: TextStyle(fontSize: 20),
                          ));
                    } else if (state is ListLoadState<T>) {
                      return LoadMoreScrollView(
                        bloc: _bloc,
                        separatorBuilder: widget.separatorBuilder,
                        padding: const EdgeInsets.only(bottom: 60),
                        itemCount: state.data.length,
                        itemBuilder: (_, index) => GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              if (widget.onTap != null) {
                                widget.onTap(context, state.data[index]);
                              }
                            },
                            child: widget.item(state.data[index])),
                      );
                    }
                  }

                  return const SizedBox.shrink();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
