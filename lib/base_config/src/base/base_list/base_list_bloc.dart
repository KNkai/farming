import 'package:mi_smart/src/share/widget/load_more.dart';
import 'package:rxdart/subjects.dart';

import '../base_bloc.dart';
import '../base_event.dart';

class BaseListBloc<T> extends BaseBloc {
  final stateList = BehaviorSubject<ListState>();
  final GetListUseCase getListProtocol;
  final LoadMoreBaseListUseCase loadMoreProtocol;
  final SearchBaseListUseCase searchProtocol;

  BaseListBloc({this.getListProtocol, this.loadMoreProtocol, this.searchProtocol});

  bool _lockLoadMore = false;
  int _totalDataList;
  int _page = 1;

  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case InitEvent:
        _eventInit();
        break;
      case LoadMoreEvent:
        _eventLoadMore();
        break;
      case SearchEvent:
        _eventSearch(event);
        break;
    }
  }

  void _eventSearch(SearchEvent event) {
    if (event.text != null && searchProtocol != null) {
      _page = 1;
      loadingSink.add(true);
      searchProtocol.search(search: event.text).then((value) {
        try {
          if (value != null && value is ResultSuccess<T>) {
            _totalDataList = value.total;
            stateList.add(ListLoadState(value.dataList));
          } else {
            stateList.add(ListEmptyState());
          }
        } finally {
          loadingSink.add(false);
        }
      });
    }
  }

  void _eventLoadMore() {
    if (_totalDataList != null && stateList?.value != null && loadMoreProtocol != null) {
      final state = stateList.value;
      if (state is ListLoadState && !_lockLoadMore && state.data != null && state.data.length < _totalDataList) {
        _lockLoadMore = true;
        loadMoreProtocol.loadMore(page: _page + 1).then((value) {
          try {
            if (value != null && value is ResultSuccess<T>) {
              _page++;
              stateList.value = (state..data.addAll(value.dataList));
            }
          } finally {
            _lockLoadMore = false;
          }
        });
      }
    }
  }

  void _eventInit() {
    if (getListProtocol != null) {
      loadingSink.add(true);
      _page = 1;
      getListProtocol.getList().then((value) {
        try {
          if (value != null && value is ResultSuccess<T>) {
            _totalDataList = value.total;
            stateList.add(ListLoadState<T>(value.dataList));
          } else {
            stateList.add(ListEmptyState());
          }
        } finally {
          loadingSink.add(false);
        }
      });
    }
  }

  @override
  void dispose() {
    stateList.close();
    super.dispose();
  }
}

class InitEvent extends BaseEvent {}

class SearchEvent extends BaseEvent {
  final String text;
  SearchEvent(this.text);
}

abstract class ListState {}

class ListLoadState<T> extends ListState {
  List<T> data;
  ListLoadState(this.data);
}

class ListEmptyState extends ListState {}

abstract class GetListUseCase {
  Future getList();
}

abstract class LoadMoreBaseListUseCase {
  Future loadMore({int page});
}

abstract class SearchBaseListUseCase {
  Future search({String search});
}

class ResultSuccess<T> {
  final int total;
  final List<T> dataList;
  ResultSuccess(this.total, this.dataList);
}
