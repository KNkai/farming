
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'base_event.dart';
import 'base_state.dart';

abstract class BaseBloc {
  final BehaviorSubject<BaseState> _processStateSubject =
      BehaviorSubject<BaseState>();

  final StreamController<bool> _loadingStreamController =
      BehaviorSubject<bool>();

  final StreamController<bool> _endScrollStreamController =
      BehaviorSubject<bool>();

  final StreamController<BaseEvent> _eventStreamController =
      BehaviorSubject<BaseEvent>();

  Sink<BaseEvent> get event => _eventStreamController.sink;

  Stream<bool> get loadingStream => _loadingStreamController.stream;
  Sink<bool> get loadingSink => _loadingStreamController.sink;

  Stream<bool> get endScrollStream => _endScrollStreamController.stream;
  Sink<bool> get endScrollSink => _endScrollStreamController.sink;

  Stream<BaseState> get processStateStream => _processStateSubject.stream;
  Sink<BaseState> get processStateSink => _processStateSubject.sink;

  BaseBloc() {
    _eventStreamController.stream.listen((event) {
      if (event is! BaseEvent) {
        throw Exception("Invalid event");
      }

      dispatchEvent(event);
    });
  }

  void add(BaseEvent event) {
    _eventStreamController.sink.add(event);
  }


  void dispatchEvent(BaseEvent event);

  @mustCallSuper
  void dispose() {
    _eventStreamController.close();
    _loadingStreamController.close();
    _processStateSubject.close();
    _endScrollStreamController.close();
  }
}
