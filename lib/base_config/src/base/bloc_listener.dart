import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/base/base_state.dart';

import 'base_bloc.dart';

class BlocListener<T extends BaseBloc> extends StatelessWidget {
  final Widget child;
  final Function(BaseState state) listener;
  final BaseBloc bloc;

  const BlocListener({
    @required this.child,
    @required this.listener,
    @required this.bloc,
  });
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BaseState>(
      stream: bloc.processStateStream,
      initialData: null,
      builder: (context, snapshot) {
        if (snapshot.hasData) listener(snapshot.data);
        return child;
      },
    );
  }
}
