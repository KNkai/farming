import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/modules/notification/notification_page.dart';

import '../../base_config.dart';

class FirebaseService {
  FirebaseService._();

  static final FirebaseService _instance = FirebaseService._();

  static FirebaseService get instance => _instance;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  void init() {
    _firebaseMessaging
      ..configure(
        onLaunch: _onLaunch,
        onResume: _onResume,
      )
      ..requestNotificationPermissions();
  }

  Future<void> _onResume(Map<String, dynamic> message) async {
    _fcmClick(message: message);
  }

  Future<void> _onLaunch(Map<String, dynamic> message) async {
    _fcmClick(message: message);
  }

  Future<String> getDeviceToken() {
    return _firebaseMessaging.getToken();
  }

  void _fcmClick({Map<String, dynamic> message}) {
    if (message != null) {
      SPref.instance.get(CustomString.KEY_TOKEN).then((value) {
        NotificationPage.go(context: navigatorKey.currentContext, onBack: () {});
      });
    }
  }
}
