class CustomString {
  static const KEY_TOKEN = 'key_token';
  static const KEY_NAME = 'key_name';
  static const KEY_ID = 'agency_id';
  static const PREVALENT_ERROR_MSG = 'Có lỗi xảy ra';
  static const EMPTY_ERROR_MSG = "Dữ liệu trống";
}

class ValidationMessage {
  static const unAuthenticatedVn = 'Xác thực thất bại! Vui lòng nhập lại.';
  static const expiredVerifyCodeVn = 'Mã xác thực hết hạn! Vui lòng nhập lại.';
}
