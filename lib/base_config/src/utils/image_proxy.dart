import 'dart:convert';
import 'package:crypto/crypto.dart';

String resizeImageLink(String url,
    {int w, int h, String resizingType = "fill", String gravity = "sm", int enlarge = 0, int quality}) {
  String host = "https://img.mcom.app";
  // String key = "14cabb3323a2bdfa9508";
  // String salt = "2a718a29ac4389ed2976";
  String base64Url = base64String(url);
  String input = "$base64Url";
  if (w != null) input = "w:$w/$input";
  if (h != null) input = "h:$h/$input";
  if (resizingType != null) input = "rt:$resizingType/$input";
  if (enlarge != null) input = "el:$enlarge/$input";
  if (gravity != null) input = "g:$gravity/$input";
  if (quality != null) input = "q:$quality/$input";

  // print('url $url');
  // print('base64Url $base64Url');

  // print('encode salt $salt/$input');
  // print('base64 salt ${base64String("$salt/$input")}');
  // String signedUrl = hmac("$salt/$input", key);
  // print('signedUrl $signedUrl');
  String resizeLink = "$host/c/$input";
  return resizeLink;
}

String hmac(String input, String key) {
  var bytes = utf8.encode(input);
  var hmacSha256 = new Hmac(sha256, utf8.encode(key)); // HMAC-SHA256
  var digest = hmacSha256.convert(bytes);
  return base64.encode(digest.bytes);
}

String base64String(String input) {
  return base64.encode(utf8.encode(input));
}
