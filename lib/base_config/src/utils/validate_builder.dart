class ValidateRole {
  final String type;
  final Function validate;
  final Function transform;

  ValidateRole(this.type, {this.validate, this.transform});
}

class ValidateBuilder {
  List<ValidateRole> roles = [];
  String value(value) {
    for (var role in roles) {
      if (role.type == "validate") {
        String errMsg = role.validate(value);
        if (errMsg != null) return errMsg;
      }
      if (role.type == "transform") {
        value = role.transform(value);
      }
    }
    return null;
  }

  Function build() {
    return value;
  }

  ValidateBuilder empty({String errMsg = "Bắt buộc nhập", bool skip = false}) {
    if (skip) return this;
    roles.add(ValidateRole("validate", validate: (value) {
      if (value == null) return errMsg;
      if (value is List && value.length == 0) return errMsg;
      if (value.isEmpty) return errMsg;
      return null;
    }));
    return this;
  }

  ValidateBuilder phone({String errMsg = "Yêu cầu nhập điện thoại", bool skip = false}) {
    if (skip) return this;
    roles.add(ValidateRole("valudate", validate: (value) {
      RegExp phoneRex = new RegExp(r"^(?:\d{10}|\d{11})$");
      if (!phoneRex.hasMatch(value)) return errMsg;
      return null;
    }));
    return this;
  }

  ValidateBuilder minLenght({String errMsg, int length, bool skip = false}) {
    if (skip) return this;
    roles.add(ValidateRole("validate", validate: (value) {
      if (value.length < length) return errMsg ?? "Yêu cầu nhập tối thiểu $length ký tự";
      return null;
    }));
    return this;
  }

  ValidateBuilder maxLenght({String errMsg, int length, bool skip = false}) {
    if (skip) return this;
    roles.add(ValidateRole("validate", validate: (value) {
      if (value.length > length) return errMsg ?? "Yêu cầu nhập không quá $length ký tự";
      return null;
    }));
    return this;
  }

  ValidateBuilder range({String errMsg, int min, int max, bool skip = false}) {
    if (skip) return this;
    roles.add(ValidateRole("validate", validate: (value) {
      if (min != null && (num.tryParse(value) ?? 0) < min) return errMsg ?? "Không được nhập ít hơn $min";
      if (max != null && (num.tryParse(value) ?? 0) > max) return errMsg ?? "Không được nhập nhiều hơn $max";
      return null;
    }));
    return this;
  }

  ValidateBuilder validate(Function validate, {bool skip = false}) {
    if (skip) return this;
    roles.add(ValidateRole("validate", validate: validate));
    return this;
  }

  ValidateBuilder transform(Function transform) {
    roles.add(ValidateRole('transform', transform: transform));
    return this;
  }

  ValidateBuilder parseNum() {
    roles.add(ValidateRole('transform', transform: (value) => num.parse(value)));
    return this;
  }
}
