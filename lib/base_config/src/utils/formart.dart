import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

import '../../base_config.dart';
import 'package:timeago/timeago.dart' as timeago;

class Format {
  static String timeAgo(DateTime date) {
    return timeago.format(date, locale: 'vi');
  }

  static String ddsmmsyyyy(DateTime dateTime) => dateTime == null ? '' : DateFormat('dd/MM/yyyy').format(dateTime);
  static String ddsmmsyyyyhhmm(DateTime dateTime) =>
      dateTime == null ? '' : DateFormat('dd/MM/yyyy HH:mm').format(dateTime);
  static String hhmm(DateTime dateTime) => dateTime == null ? '' : DateFormat('HH:mm').format(dateTime);

  static String formatErrFirebaseLoginToString(String err) {
    String message = "";
    switch (err) {
      case "ERROR_ARGUMENT_ERROR":
        message = "Vui lòng nhập đầy đủ dữ liệu";
        break;
      case "ERROR_OPERATION_NOT_ALLOWED":
        message = "Phương thức đăng nhập này chưa được cho phép";
        break;
      case "ERROR_USER_DISABLED":
        message = "Tài khoản này đã bị khoá";
        break;
      case "ERROR_INVALID_EMAIL":
        message = "Định dạng Email không đúng";
        break;
      case "ERROR_USER_NOT_FOUND":
        message = "Tài khoản không tồn tại";
        break;
      case "ERROR_WRONG_PASSWORD":
        message = "Sai mật khẩu, vui lòng nhập lại";
        break;
      case "ERROR_TOO_MANY_REQUESTS":
        message = "Quá giới hạn số lần đăng nhập, xin hãy thử lại sau vài phút";
        break;
      default:
        print(err);
        message = "Lỗi Đăng Nhập";
    }
    return message;
  }
}

class ResultError<T> {
  final String msg;
  final Exception exception;
  final T data;
  ResultError({this.msg, this.exception, this.data});
}

class EmptyError {}

class Formart {
  static toVNDCurency(int value) {
    final formatter = NumberFormat.currency(locale: "vi_VN");
    String newValue = formatter.format(value);
    return newValue;
  }

  static toVNDCurency2(int value) {
    final formatter = NumberFormat.currency(locale: "vi_VN");
    String newValue = formatter.format(value);
    newValue = newValue.substring(0, newValue.length - 3);
    newValue = newValue + 'đ';
    return newValue;
  }

  static String formatNumber(double number) {
    final numberFormater = NumberFormat("#,##0.00", "en_US");
    return numberFormater.format(number);
  }

  static String formatTime(String time) {
    if (time == null || DateTime.tryParse(time) == null) return null;
    var result = DateTime.tryParse(time);
    result = result.toLocal();
    if (result == null) return null;
    return '${result.hour}:${result.minute < 10 ? ('0' + result.minute.toString()) : result.minute}';
  }

  static String formatToDate(String time) {
    if (time == null || DateTime.tryParse(time) == null) return null;
    var result = DateTime.tryParse(time);
    result = result.toLocal();
    if (result == null) return null;
    return '${result.day}/${result.month}/${result.year}';
  }

  static T getMapElement<T>(Map map, List<String> keys, {T defaultValue}) {
    if (map == null) return null;
    dynamic mapChildFlag = map;
    for (int i = 0; i < keys.length; i++) {
      mapChildFlag = mapChildFlag[keys[i]];
      if (mapChildFlag == null) return defaultValue;
      if (i == keys.length - 1) {
        if (T == double) {
          return double.tryParse(mapChildFlag.toString()) as T;
        }
        if (T == int) {
          return int.tryParse(mapChildFlag.toString()) as T;
        }
        return mapChildFlag as T;
      }
    }
    return defaultValue;
  }

  static String formatShortDate(String time) {
    if (time == null || DateTime.tryParse(time) == null) return null;
    var result = DateTime.tryParse(time).toLocal();
    if (result == null) return null;
    return '${result.day}/${result.month}';
  }

  static String formatToDateTime(String time) {
    if (time == null) return null;
    var result = DateTime.tryParse(time).toLocal();
    if (result == null) return null;
    return '${result.day}/${result.month}/${result.year} ${result.hour}:${result.minute < 10 ? ('0' + result.minute.toString()) : result.minute}';
  }

  static DateTime vnDateStringToDateTime(String date) {
    int day = int.parse(date.substring(0, 2));
    int month = int.parse(date.substring(3, 5));
    int year = int.parse(date.substring(6, 10));
    DateTime dateTime = DateTime(year, month, day);
    return dateTime;
  }

  static String formatErrFirebaseLoginToString(String err) {
    String message = "";
    switch (err) {
      case "ERROR_ARGUMENT_ERROR":
        message = "Vui lòng nhập đầy đủ dữ liệu";
        break;
      case "ERROR_OPERATION_NOT_ALLOWED":
        message = "Phương thức đăng nhập này chưa được cho phép";
        break;
      case "ERROR_USER_DISABLED":
        message = "Tài khoản này đã bị khoá";
        break;
      case "ERROR_INVALID_EMAIL":
        message = "Định dạng Email không đúng";
        break;
      case "ERROR_USER_NOT_FOUND":
        message = "Tài khoản không tồn tại";
        break;
      case "ERROR_WRONG_PASSWORD":
        message = "Sai mật khẩu, vui lòng nhập lại";
        break;
      case "ERROR_EMAIL_ALREADY_IN_USE":
        message = "Email này đã được sử dụng. Vui lòng đăng nhập";
        break;
      case "ERROR_TOO_MANY_REQUESTS":
        message = "Quá giới hạn số lần yêu cầu, xin hãy thử lại sau";
        break;
      case "ERROR_INVALID_VERIFICATION_CODE":
        message = "Mã xác nhận không chính xác";
        break;
      case 'ERROR_SESSION_EXPIRED':
        message = "Mã xác nhận đã hết hạn, nhấn gửi lại.";
        break;
      default:
        print(err);
        message = "Lỗi Đăng Nhập";
    }
    return message;
  }

  static String mapToGraphqlString(Map<String, dynamic> map) {
    String result = "";
    map.forEach((key, value) {
      if (value is String) {
        result = result + key + " : " + '"' + value + '",';
      }
      if (value is bool || value is int || value is double) {
        result = result + key + " : " + value.toString() + ' ,';
      }
    });
    return result;
  }

  static String listStringToGraphqlString(List list) {
    if (list == null) return '[]';
    String result = "[";
    list.forEach((value) {
      result += '"${value.toString()}",';
    });
    result += "]";
    return result;
  }

  static String listMapToGraphqlString(List list) {
    if (list == null) return '[]';
    String result = "[";
    list.forEach((value) {
      result += '${mapToGraphqlString(value)},';
    });
    result += "]";
    return result;
  }

  static String dynamicToGraphqlString(dynamic val) {
    //input: dynamic
    //output: return String in grapql base on it's type
    if (val == null) return null;
    if (val is String) return '"$val"';
    if (val is bool || val is int || val is double) return '$val';
    if (val is Map) return mapToGraphqlString(val);
    if (val is List<String>) return listStringToGraphqlString(val);
    if (val is List<Map>) return listMapToGraphqlString(val);
    return null;
  }
}

void showWarningDialog(BuildContext context, String content, String btnText, {String title, Function onCloseDialog}) {
  Widget alert;

  Widget _titleWidget({TextAlign textAlign = TextAlign.start}) => title != null
      ? Text(
          title,
          textAlign: textAlign,
        )
      : null;

  if (content != null && content.isNotEmpty) {
    if (Platform.isAndroid) {
      // If it has plenty of buttons, it will stack them on vertical way.
      // If title isn't supplied, height of this alert will smaller than the one has title.
      alert = AlertDialog(
        title: _titleWidget(),
        content: Text(
          content,
          textAlign: TextAlign.start,
        ),
        actions: [
          FlatButton(
            child: Text(btnText),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    } else {
      // Almost similiar with Cupertino style.
      // If title isn't supplied, height of this alert will smaller than the one has title.
      alert = CupertinoAlertDialog(
        title: Padding(
          padding: const EdgeInsets.only(
            bottom: 10,
          ),
          child: _titleWidget(textAlign: TextAlign.center),
        ),
        content: Text(
          content,
          textAlign: TextAlign.start,
        ),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              btnText,
            ),
          )
        ],
      );
    }

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    ).then((value) {
      if (onCloseDialog != null) {
        onCloseDialog();
      }
    });
  }
}
