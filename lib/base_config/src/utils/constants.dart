import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mi_smart/src/share/utils/color.dart';

import 'size_config.dart';

Color ptPrimaryColor(BuildContext context) => Theme.of(context).primaryColor;
double deviceWidth(BuildContext context) => MediaQuery.of(context).size.width;
double scaleWidth(BuildContext context) => MediaQuery.of(context).size.width / 375;
double deviceHeight(BuildContext context) => MediaQuery.of(context).size.height;

///TextTheme
// TextStyle ptDisplay4(BuildContext context) => Theme.of(context).textTheme.display4;
// TextStyle ptDisplay3(BuildContext context) => Theme.of(context).textTheme.display3;
// TextStyle ptDisplay2(BuildContext context) => Theme.of(context).textTheme.display2;
// TextStyle ptDisplay1(BuildContext context) => Theme.of(context).textTheme.display1;
TextStyle ptHeadline(BuildContext context) => Theme.of(context).textTheme.headline1;
// TextStyle ptTitle(BuildContext context) => Theme.of(context).textTheme.title;
TextStyle ptSubhead(BuildContext context) => Theme.of(context).textTheme.bodyText2;
TextStyle ptBody2(BuildContext context) => Theme.of(context).textTheme.bodyText2;
TextStyle ptBody1(BuildContext context) => Theme.of(context).textTheme.bodyText1;
TextStyle ptCaption(BuildContext context) => Theme.of(context).textTheme.caption;
TextStyle ptButton(BuildContext context) => Theme.of(context).textTheme.button;
TextStyle ptSubtitle(BuildContext context) => Theme.of(context).textTheme.subtitle1;
TextStyle ptOverline(BuildContext context) => Theme.of(context).textTheme.overline;

///HeaderStyle
TextStyle ptHeadStyleText(BuildContext context) => Theme.of(context).textTheme.headline2.copyWith(fontSize: 20);

///FontStyle
class FONT {
  static const String Bold = "OpenSans-Bold";
  static const String BoldItalic = "OpenSans-BoldItalic";
  static const String ExtraBold = "OpenSans-ExtraBold";
  static const String ExtraBoldItalic = "OpenSans-ExtraBoldItalic";
  static const String Italic = "OpenSans-Italic";
  static const String Light = "OpenSans-Light";
  static const String LightItalic = "OpenSans-LightItalic";
  static const String Regular = "OpenSans-Regular";
  static const String SemiBold = "OpenSans-SemiBold";
  static const String SemiBoldItalic = "OpenSans-SemiBoldItalic";
}

Center kLoadingSpinner = const Center(
  child: SpinKitCircle(
    color: AppColor.primary,
    size: 50.0,
  ),
);
Function kLoadingBuilder = (BuildContext context, Widget child, ImageChunkEvent loadingProgress) =>
    loadingProgress != null ? kLoadingSpinner : child;
Duration kDuration = const Duration(microseconds: 200);
TextStyle kTextTitle = const TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black);
TextStyle kErrorText = const TextStyle(fontSize: 11, color: Colors.red, fontWeight: FontWeight.w500);
Color kBackgroundColor = const Color(0xFFF5F5F5);
InputDecoration kDefaultInputDecoration = InputDecoration(
    alignLabelWithHint: true,
    fillColor: Colors.white,
    focusColor: Colors.white,
    floatingLabelBehavior: FloatingLabelBehavior.always,
    contentPadding: const EdgeInsets.symmetric(horizontal: 20),
    enabledBorder: kOutlineInputBorder,
    disabledBorder: kOutlineInputBorder,
    border: kOutlineInputBorder,
    errorBorder: kOutlineInputBorder.copyWith(borderSide: const BorderSide(color: Colors.red)),
    focusedErrorBorder: kOutlineInputBorder.copyWith(borderSide: const BorderSide(color: Colors.red)),
    focusedBorder: kOutlineInputBorder.copyWith(borderSide: const BorderSide(color: AppColor.primary)),
    filled: true,
    labelStyle: const TextStyle(height: -1));
OutlineInputBorder kOutlineInputBorder = OutlineInputBorder(
  borderSide: const BorderSide(width: 0.8, style: BorderStyle.solid, color: AppColor.grey),
  borderRadius: BorderRadius.circular(5),
  gapPadding: 10,
);

SizedBox kFieldSpacing = SizedBox(height: SizeConfig.getScreenHeight(12));

AppBar buildSheetBar(BuildContext context, {Function onPress, @required String title, String btnTitle}) {
  return AppBar(
    backgroundColor: AppColor.primary,
    title: Text(title),
    leading: InkWell(
      onTap: () => Navigator.of(context).pop(),
      child: const Icon(Icons.close),
    ),
    actions: [
      if (btnTitle != null)
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 12),
          child: InkWell(
            onTap: onPress,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(4), border: Border.all(color: Colors.white)),
              child: Text(
                btnTitle,
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
              ),
            ),
          ),
        )
    ],
  );
}

AppBar buildPageBar(BuildContext context, String title, {Function callBackData}) {
  return AppBar(
    backgroundColor: AppColor.primary,
    title: Text(title),
    leading: InkWell(
      onTap: () => Navigator.of(context).pop(callBackData != null ? callBackData() : null),
      child: const Padding(
        padding: EdgeInsets.only(left: 5),
        child: Icon(
          Icons.arrow_back_ios,
          color: Colors.white,
          size: 20,
        ),
      ),
    ),
  );
}
