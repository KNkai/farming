class Assets {
  static const home_icon = 'assets/images/logo.png';
  static const google_icon = 'assets/images/google.png';
  static const bg_splash = 'assets/bg_splash.jpg';
  static const mcom = 'assets/mcom.png';
  static const bg_listbook = 'assets/images/bg_listbook.jpg';
  static const covid = 'assets/images/covid.jpg';
  static const bg_bookdetail = 'assets/images/bg-baby.png';
  static const medical2 = 'assets/images/medical_2.png';
  static const bg_doctor = 'assets/images/bg_doctor.png';
  static const momo = 'assets/images/momo.png';
  static const atm = 'assets/images/atm.png';
  static const vietcombank = 'assets/images/vietcombank.png';
  static const viettinbank = 'assets/images/viettinbank.png';
  static const ocb = 'assets/images/ocb.png';
  static const techcombank = 'assets/images/techcombank.png';
  static const banner = 'assets/banner.png';
  static const pin = 'assets/image/app_pin.png';
}
