import 'dart:convert';

import 'package:flutter/material.dart';
import '../base_config/src/base/base_graphql.dart';
import 'package:graphql/client.dart';
import '../src/modules/select_area/model/map_model.dart';

class GraphRepository<T> {
  final String shortFragment = "id";
  final String fullFragment = "id";
  final String apiName = "";

  T fromJson(Map<String, dynamic> json) => {} as T;

  Future<GetListData<T>> getAll({String fragment, QueryInput query, bool cache = true}) async {
    fragment = fragment ?? shortFragment;
    String api = 'getAll${apiName}';
    QueryOptions options = QueryOptions(documentNode: gql('''
        query GetAll(\$q: QueryGetListInput!) {
          $api(q: \$q) { data { $fragment } pagination { limit page total } }
        }
      '''), variables: {"q": query == null ? {} : query.toJson()}, fetchPolicy: cache ? null : FetchPolicy.noCache);

    QueryResult result = await GraphQL.instance.client.query(options);
    ;
    if (result.hasException) {
      throw result.exception.graphqlErrors[0].message.toString();
    }
    return GetListData<T>(List<T>.from(result.data[api]["data"].map((x) => fromJson(x))),
        Pagination.fromJson(result.data[api]["pagination"]));
  }

  Future<T> getOne({@required String id, String fragment, bool cache = true}) async {
    fragment = fragment ?? fullFragment;
    String api = 'getOne${apiName}';
    QueryOptions options = QueryOptions(documentNode: gql('''
        query GetOne {
          $api(id: "$id") {  $fragment  }
        }
      '''), fetchPolicy: cache ? null : FetchPolicy.noCache);

    QueryResult result = await GraphQL.instance.client.query(options);
    ;
    if (result.hasException) {
      throw result.exception.graphqlErrors[0].message.toString();
    }
    return fromJson(result.data[api]);
  }

  Future<T> create({@required Map<String, dynamic> data, String fragment}) async {
    fragment = fragment ?? fullFragment;
    String api = 'create$apiName';
    MutationOptions options = MutationOptions(documentNode: gql('''
        mutation Create(\$d: Create${apiName}Input!) {
          $api(data: \$d) {  $fragment  }
        }
      '''), fetchPolicy: FetchPolicy.noCache, variables: {"d": data});

    QueryResult result = await GraphQL.instance.client.mutate(options);
    ;
    if (result.hasException) {
      throw result.exception.graphqlErrors[0].message.toString();
    }
    return fromJson(result.data[api]);
  }

  Future<T> update({@required String id, @required Map<String, dynamic> data, String fragment}) async {
    fragment = fragment ?? fullFragment;
    String api = 'update$apiName';
    MutationOptions options = MutationOptions(documentNode: gql('''
        mutation Update(\$d: Update${apiName}Input!) {
          $api(id: "$id", data: \$d) {  $fragment  }
        }
      '''), fetchPolicy: FetchPolicy.noCache, variables: {"d": data});

    QueryResult result = await GraphQL.instance.client.mutate(options);
    ;
    if (result.hasException) {
      throw result.exception.graphqlErrors[0].message.toString();
    }
    return fromJson(result.data[api]);
  }

  Future<T> delete({@required String id, @required Map<String, dynamic> data, String fragment}) async {
    fragment = fragment ?? shortFragment;
    String api = 'deleteOne$apiName';
    MutationOptions options = MutationOptions(documentNode: gql('''
        mutation Delete {
          $api(id: "$id") { $fragment  }
        }
      '''), fetchPolicy: FetchPolicy.noCache);

    QueryResult result = await GraphQL.instance.client.mutate(options);
    ;
    if (result.hasException) {
      throw result.exception.graphqlErrors[0].message.toString();
    }
    return fromJson(result.data[api]);
  }
}

class GetListData<T> {
  final List<T> data;
  final Pagination pagination;

  GetListData(this.data, this.pagination);
}

class QueryInput {
  QueryInput({
    this.limit = 20,
    this.page = 1,
    this.search,
    this.order,
    this.filter,
  });

  int limit;
  int page;
  String search;
  dynamic order;
  dynamic filter;

  factory QueryInput.fromRawJson(String str) => QueryInput.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory QueryInput.fromJson(Map<String, dynamic> json) => QueryInput(
        limit: json["limit"] == null ? null : json["limit"],
        page: json["page"] == null ? null : json["page"],
        search: json["search"] == null ? null : json["search"],
        order: json["order"],
        filter: json["filter"],
      );

  Map<String, dynamic> toJson() => {
        "limit": limit == null ? null : limit,
        "page": page == null ? null : page,
        "search": search == null ? null : search,
        "order": order,
        "filter": filter,
      };
}
