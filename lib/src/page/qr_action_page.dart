import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';
import 'package:mi_smart/src/page/new_diary/new_diary_page.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QrActionPage extends StatelessWidget {
  final bool fromDashboard;
  final bool toCallAction;
  QrActionPage._(this.fromDashboard, this.toCallAction);
  static void goFromDashboard(BuildContext context) {
    onCustomPersionRequest(
        permission: Permission.camera,
        onGranted: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) => QrActionPage._(true, false)));
        });
  }

  static void goToCallAction(BuildContext context, {@required void Function(dynamic) onOpenAction}) {
    onCustomPersionRequest(
        permission: Permission.camera,
        onGranted: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) => QrActionPage._(false, true))).then((value) {
            if (value != null) {
              onOpenAction(value);
            }
          });
        });
  }

  final qrKey = GlobalKey(debugLabel: 'QR');

  @override
  Widget build(BuildContext context) {
    bool lock = false;
    return Scaffold(
      body: Stack(
        children: [
          QRView(
            key: qrKey,
            onQRViewCreated: (controller) {
              controller.scannedDataStream.listen((scanData) {
                if (!lock) {
                  lock = true;
                  var scanContent;
                  try {
                    scanContent = jsonDecode(scanData.code);
                    if (HiveService.instnace.boxRegions.values.isNotEmpty) {
                      if (scanContent['type'] == 'region' && fromDashboard) {
                        try {
                          HiveService.instnace.boxRegions
                            ..values.first.data.firstWhere((element) => element['id'] == scanContent['regionId']);
                          NewDiaryPage.goReplacement(context: context, id: scanContent['regionId']);
                        } catch (_) {
                          WarningDialog.show(context, 'Không có dữ liệu vùng', 'OK', title: 'Thông báo');
                        }
                      } else if (scanContent['type'] == 'phaseLog') {
                        try {
                          HiveService.instnace.boxRegions
                            ..values.first.data.firstWhere((element) => element['id'] == scanContent['regionId']);
                          if (HiveService.instnace.boxDiaries.values.isNotEmpty) {
                            try {
                              final diary = HiveService.instnace.boxDiaries.values.first.data.firstWhere((element) {
                                return element['status'] == 'OPENING';
                              });
                              try {
                                final phase =
                                    (diary['phases'] as List).firstWhere((element) => element['status'] == 'OPENING');
                                final attributes = (phase['attributes'] as List);
                                var contain;
                                for (int c = 0; c < attributes.length; c++) {
                                  if (attributes[c]['key'] == scanContent['attributeKey']) {
                                    contain = (attributes[c]);
                                    break;
                                  }
                                }
                                if (contain == null) {
                                  WarningDialog.show(context, 'Dữ liệu nghiệp vụ không hợp lệ', 'OK',
                                      title: 'Thông báo');
                                } else {
                                  if (fromDashboard) {
                                    NewDiaryPage.goReplacementAndAction(
                                        context: context,
                                        id: scanContent['regionId'],
                                        actionData: contain..putIfAbsent('phaseId', () => phase['id']));
                                  } else if (toCallAction) {
                                    Navigator.of(context).pop(contain..putIfAbsent('phaseId', () => phase['id']));
                                  }
                                }
                              } catch (_) {
                                WarningDialog.show(context, 'Dữ liệu mùa vụ không hợp lệ', 'OK', title: 'Thông báo');
                              }
                            } catch (_) {
                              WarningDialog.show(context, 'Dữ liệu mùa vụ không hợp lệ', 'OK', title: 'Thông báo');
                            }
                          } else {
                            WarningDialog.show(context, 'Không có dữ mùa vụ', 'OK', title: 'Thông báo');
                          }
                        } catch (_) {
                          WarningDialog.show(context, 'Không có dữ liệu vùng', 'OK', title: 'Thông báo');
                        }
                      }
                    } else {
                      WarningDialog.show(context, 'Không có dữ liệu vùng', 'OK', title: 'Thông báo');
                    }
                    Future.delayed(Duration(seconds: 2)).then((value) {
                      lock = false;
                    });
                  } catch (_) {
                    Future.delayed(Duration(seconds: 2)).then((value) {
                      lock = false;
                    });
                    WarningDialog.show(context, 'QR code không hợp lệ', 'OK', title: 'Thông báo');
                  }
                }
              });
            },
            overlay: QrScannerOverlayShape(
              borderColor: Colors.red,
              borderRadius: 10,
              borderLength: 30,
              borderWidth: 0,
              cutOutSize: 300,
            ),
          ),
          Positioned(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                _back(context);
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 30,
              ),
            ),
            left: 20,
            top: 34,
          ),
          Positioned(
              bottom: 60,
              left: 20,
              right: 20,
              child: Text(
                'Di chuyển camera đến vùng chứa mã QR để quét.',
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
    );
  }

  void _back(BuildContext context) {
    Navigator.of(context).pop();
  }
}
