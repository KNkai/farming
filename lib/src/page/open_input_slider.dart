import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';

void openInputSlider(
    {@required BuildContext context,
    @required String label,
    @required String hint,
    bool note = false,
    String noteValue,
    String value,
    void Function(String, String) onReturn}) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    builder: (contextX) {
      final textController = TextEditingController(text: value ?? '');
      final noteController = TextEditingController(text: noteValue ?? '');
      return Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.of(contextX).viewInsets.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            ColoredBox(
              color: AppColor.primary,
              child: Padding(
                padding: EdgeInsets.only(
                    left: SizeConfig.setWidth(25), top: SizeConfig.setHeight(16), bottom: SizeConfig.setHeight(16)),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.close,
                        size: 24,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: SizeConfig.setWidth(16),
                    ),
                    Text(label, style: TextStyle(fontSize: 18, color: Colors.white))
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: SizeConfig.setWidth(18), top: SizeConfig.setHeight(26)),
              child: Text(
                hint,
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: SizeConfig.setWidth(16),
                right: SizeConfig.setWidth(16),
                top: SizeConfig.setHeight(2),
                bottom: note ? 0 : SizeConfig.setHeight(28),
              ),
              child: TextField(
                autofocus: true,
                keyboardType: TextInputType.number,
                controller: textController,
                onSubmitted: (value) {
                  if (onReturn != null) {
                    onReturn(textController.text, noteController.text);
                  }
                  ;
                  Navigator.of(context).pop();
                },
                scrollPadding: EdgeInsets.zero,
                cursorColor: Colors.grey,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(left: SizeConfig.setWidth(16)),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.grey[300], width: 1),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.grey[300], width: 1),
                  ),
                ),
              ),
            ),
            if (note)
              Padding(
                padding: EdgeInsets.only(left: SizeConfig.setWidth(18)),
                child: Text(
                  'Ghi chú',
                  style: TextStyle(fontSize: 12, color: Colors.grey),
                ),
              ),
            if (note)
              Padding(
                padding: EdgeInsets.only(
                  left: SizeConfig.setWidth(16),
                  right: SizeConfig.setWidth(16),
                  top: SizeConfig.setHeight(2),
                  bottom: SizeConfig.setHeight(28),
                ),
                child: TextField(
                  autofocus: true,
                  controller: noteController,
                  scrollPadding: EdgeInsets.zero,
                  maxLines: 2,
                  cursorColor: Colors.grey,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: SizeConfig.setWidth(16)),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.grey[300], width: 1),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.grey[300], width: 1),
                    ),
                  ),
                ),
              ),
            PhysicalModel(
              elevation: 8.0,
              color: Colors.white,
              child: Padding(
                  child: SizedBox(
                    width: double.infinity,
                    child: AppBtn(
                      'Xác nhận',
                      onPressed: () {
                        if (onReturn != null) onReturn(textController.text, noteController.text);
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  padding:
                      EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16), vertical: SizeConfig.setHeight(14))),
            )
          ],
        ),
      );
    },
  );
}
