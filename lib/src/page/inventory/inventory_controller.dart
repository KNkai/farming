import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:mi_smart/src/page/hive_service/diaries_regions_logs.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';

class InventoryController {
  final stateExpenseExpect = ValueNotifier<double>(DateTime.now().day.toDouble() * 1000);
  final stateEdit = ValueNotifier<bool>(true);
  final stateEditStatus = ValueNotifier<int>(0);
  final stateItems = StreamController<StateItem>();
  final _cloneItems = List<RegionSupplyByDate>();
  final _mapUpdate = HashMap<String, int>();
  int _index = 0;
  String _regionId;

  InventoryController(String regionId) {
    _regionId = regionId;
    _init();
  }

  void _init() {
    _cloneItems.clear();
    HiveService.instnace.boxRegionSupplyDate.values.forEach((e) {
      var list = List();
      e.data.forEach((element) {
        if (element['regionId'] == _regionId) {
          final map = HashMap();
          (element as Map).forEach((key, value) {
            map.putIfAbsent(key, () => value);
          });
          list.add(map);
        }
      });
      _cloneItems.add(RegionSupplyByDate(list, e.time));
    });
  }

  void changeIndex(int index) {
    if (_cloneItems.elementAt(index).data.isEmpty) {
      stateEditStatus.value = 0;
      stateEdit.value = false;
      stateItems.add(ItemEmtpy());
    } else {
      _index = index;
      stateEdit.value = _cloneItems.elementAt(index).time.isSameDate(DateTime.now());
      stateItems.add(ItemLoaded(_cloneItems.elementAt(index).data));
    }
  }

  void updateExpense(double expense) {
    stateExpenseExpect.value = expense;
  }

  void increase(String supplieId, int value, int indexSupply) {
    _cloneItems.elementAt(_index).data[indexSupply]['qtyInStock'] = value + 1;
    _mapUpdate[_cloneItems.elementAt(_index).data[indexSupply]['id']] = value + 1;
    stateItems.add(ItemLoaded(_cloneItems.elementAt(_index).data));
  }

  void decrease(String supplieId, int value, int indexSupply) {
    if (value > -1) {
      _cloneItems.elementAt(_index).data[indexSupply]['qtyInStock'] = value - 1;
      _mapUpdate[_cloneItems.elementAt(_index).data[indexSupply]['id']] = value - 1;
      stateItems.add(ItemLoaded(_cloneItems.elementAt(_index).data));
    }
  }

  void openChange() {
    stateEditStatus.value = 1;
  }

  void cancelChange() {
    stateEditStatus.value = 0;
    _init();
    _mapUpdate.clear();
    stateItems.add(ItemLoaded(_cloneItems.elementAt(_index).data));
  }

  void confirmChange() {
    _mapUpdate.forEach((key, value) {
      HiveService.instnace.updateRegionSupply(key, value);
    });
    stateEditStatus.value = 0;
  }
}

extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return this.year == other.year && this.month == other.month && this.day == other.day;
  }
}

abstract class StateItem {}

class ItemLoaded extends StateItem {
  final List<dynamic> data;
  ItemLoaded(this.data);
}

class ItemEmtpy extends StateItem {}
