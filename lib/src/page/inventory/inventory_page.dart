import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/inventory/inventory_controller.dart';
import 'package:mi_smart/src/page/new_diary/component/time_widget.dart';
import 'package:mi_smart/src/page/new_diary/component/white_cover.dart';
import 'package:mi_smart/src/page/open_input_slider.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

class InventoryPage extends StatelessWidget {
  final String regionId;
  InventoryPage(this.regionId);
  @override
  Widget build(BuildContext context) {
    final _controller = InventoryController(this.regionId);
    _controller.changeIndex(0);
    return AppScafold(
        title: 'Vật tư',
        bgColor: AppColor.tertiary,
        padding: EdgeInsets.zero,
        child: SafeArea(
            child: ValueListenableBuilder<int>(
                valueListenable: _controller.stateEditStatus,
                builder: (_, editStatus, __) {
                  return ListView(
                    padding:
                        EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16), vertical: SizeConfig.setHeight(24)),
                    children: [
                      TimeWidget(
                        freezing: editStatus == 1,
                        onChangeDay: (index) {
                          _controller.changeIndex(index);
                        },
                      ),
                      SizedBox(
                        height: SizeConfig.setHeight(23),
                      ),
                      IgnorePointer(
                        ignoring: editStatus == 0,
                        child: GestureDetector(
                          onTap: () {
                            openInputSlider(
                                context: context,
                                label: 'Chi phí vật tư dự kiến',
                                hint: 'Chi phí vật tư dự kiến',
                                onReturn: (value, _) {
                                  _controller.updateExpense(double.parse(value));
                                });
                          },
                          child: ValueListenableBuilder<double>(
                            builder: (_, value, __) {
                              return WhiteCover(
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Chi phí vật tư dự kiến',
                                            style: TextStyle(fontWeight: FontWeight.w600),
                                          ),
                                          Text('${appCurrency(value)}'),
                                        ],
                                      ),
                                    ),
                                    editStatus == 1
                                        ? Icon(
                                            Icons.edit,
                                            color: AppColor.primary,
                                            size: 28,
                                          )
                                        : SizedBox.shrink()
                                  ],
                                ),
                              );
                            },
                            valueListenable: _controller.stateExpenseExpect,
                          ),
                        ),
                      ),
                      Padding(
                          child: Text(
                            'Danh sách vật tư',
                            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(24))),
                      Padding(
                        padding: EdgeInsets.only(bottom: SizeConfig.setHeight(14)),
                        child: StreamBuilder<StateItem>(
                            stream: _controller.stateItems.stream,
                            builder: (context, snapshot) {
                              if (snapshot?.data == null || snapshot.data is ItemEmtpy) return SizedBox.shrink();

                              final list = (snapshot.data as ItemLoaded).data;
                              return Column(
                                children: [
                                  for (int c = 0; c < list.length; c++)
                                    Padding(
                                      padding: EdgeInsets.only(bottom: SizeConfig.setHeight(10)),
                                      child: WhiteCover(
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    list[c]['supplies']['name'],
                                                    style: TextStyle(fontWeight: FontWeight.w600),
                                                  ),
                                                  SizedBox(
                                                    height: SizeConfig.setHeight(7),
                                                  ),
                                                  Text(
                                                    list[c]['suppliesCode'],
                                                    style: TextStyle(fontSize: 12, color: Colors.grey),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                if (editStatus == 1) {
                                                  _controller.increase(list[c]['suppliesId'], list[c]['qtyInStock'], c);
                                                }
                                              },
                                              child: DecoratedBox(
                                                decoration: BoxDecoration(
                                                    color: editStatus == 1 ? AppColor.primary : Colors.white,
                                                    shape: BoxShape.circle),
                                                child: Icon(
                                                  Icons.add,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            Column(
                                              children: [
                                                SizedBox(
                                                  width: 50,
                                                  child: Text(
                                                    list[c]['qtyInStock'].toString(),
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  list[c]['supplies']['unitName'],
                                                  style: TextStyle(fontSize: 12, color: Colors.grey),
                                                ),
                                              ],
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                if (editStatus == 1) {
                                                  _controller.decrease(list[c]['suppliesId'], list[c]['qtyInStock'], c);
                                                }
                                              },
                                              child: DecoratedBox(
                                                decoration: BoxDecoration(
                                                    color: editStatus == 1 ? AppColor.primary : Colors.white,
                                                    shape: BoxShape.circle),
                                                child: Icon(
                                                  Icons.remove,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                ],
                              );
                            }),
                      ),
                      ValueListenableBuilder(
                        valueListenable: _controller.stateEdit,
                        builder: (_, edit, __) {
                          if (edit && editStatus == 0)
                            return AppBtn('Thay đổi vật tư', onPressed: () {
                              _controller.openChange();
                            });
                          if (edit && editStatus == 1)
                            return Row(
                              children: [
                                Expanded(
                                  child: AppBtn(
                                    'Huỷ thay đổi',
                                    onPressed: () {
                                      _controller.cancelChange();
                                    },
                                    paddingBtn: EdgeInsets.zero,
                                    textColor: Colors.black,
                                    color: Colors.white,
                                    borderColor: AppColor.primary,
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                  child: AppBtn(
                                    'Xác nhận',
                                    paddingBtn: EdgeInsets.zero,
                                    onPressed: () {
                                      _controller.confirmChange();
                                    },
                                  ),
                                ),
                              ],
                            );

                          return SizedBox.shrink();
                        },
                      )
                    ],
                  );
                })));
  }
}
