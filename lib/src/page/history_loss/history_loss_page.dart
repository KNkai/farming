import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';
import 'package:mi_smart/src/page/open_input_slider.dart';
import 'package:mi_smart/src/page/refresh.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

import '../new_diary/component/white_cover.dart';
import 'history_loss_controller.dart';

class HistoryLossPage extends StatefulWidget {
  static void go({@required BuildContext context, @required String id, @required VoidCallback refresh}) =>
      Navigator.of(context).push(MaterialPageRoute(builder: (_) => HistoryLossPage(id))).then((value) {
        refresh();
      });
  final String id;

  HistoryLossPage(this.id);

  @override
  _HistoryLossPageState createState() => _HistoryLossPageState();
}

class _HistoryLossPageState extends State<HistoryLossPage> with RouteAware, WidgetsBindingObserver, Refresh {
  final _controller = HistoryLossController();

  @override
  void pleaseRefresh() {
    _controller.init(widget.id);
  }

  @override
  void initState() {
    super.initState();
    _controller.init(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      title: 'Lịch sử hao hụt',
      padding: EdgeInsets.zero,
      child: ValueListenableBuilder<HashMap<String, List<dynamic>>>(
          valueListenable: _controller.stateLosses,
          builder: (_, losses, __) {
            if (losses == null) return SizedBox.shrink();

            final diary =
                HiveService.instnace.boxDiaries.values.first.data.firstWhere((element) => element['id'] == widget.id);
            final totalLoss = diary['totalOutputLoss'];
            final list = losses.keys.toList()
              ..sort((a, b) {
                var splitA = a.split('/');
                var splitB = b.split('/');
                return DateTime(
                  int.parse(splitB[2]),
                  int.parse(splitB[1]),
                  int.parse(splitB[0]),
                ).compareTo(DateTime(
                  int.parse(splitA[2]),
                  int.parse(splitA[1]),
                  int.parse(splitA[0]),
                ));
              });

            return Padding(
              padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(24)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16)),
                    child: SizedBox(
                      width: double.infinity,
                      child: WhiteCover(
                        bgColor: AppColor.primary,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Tổng sản lượng hao hụt:',
                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '$totalLoss ${diary['unit']}',
                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: SizeConfig.setHeight(24), horizontal: SizeConfig.setWidth(16)),
                    child: Text(
                      'Lịch sử',
                      style: TextStyle(
                        color: AppColor.primary,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      children: list.map((e) {
                        return Container(
                          width: double.infinity,
                          color: Colors.white,
                          margin: EdgeInsets.only(bottom: 10),
                          padding: EdgeInsets.symmetric(vertical: 20, horizontal: SizeConfig.setWidth(16)),
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      e.split('/')[0].toString(),
                                      style: TextStyle(
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    const SizedBox(width: 10),
                                    Text(
                                      'Tháng ${e.split('/')[1].toString()},\n2021',
                                      style: TextStyle(
                                        fontSize: 13,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              ...generate(context, losses, e, diary['unit'])
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            );
          }),
    );
  }

  List<Widget> generate(context, losses, e, unit) {
    final data = List<Widget>();
    (losses[e] as List)
      ..sort((a, b) {
        return (DateTime.parse(b['createdAt'])
            .millisecondsSinceEpoch
            .compareTo(DateTime.parse(a['createdAt']).millisecondsSinceEpoch));
      });
    for (int i = 0; i < losses[e].length; i++)
      data.add(Padding(
        padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(10)),
        child: GestureDetector(
          onLongPress: () {
            openInputSlider(
                onReturn: (value, _) {
                  _controller.updateHistoryLog(losses[e][i]['id'], int.parse(value), losses[e][i]['value']);
                },
                context: context,
                value: losses[e][i]['value'].toString(),
                label: 'Hao hụt',
                hint: '');
          },
          child: _ActionItem(
            bgAction: Color(0xffF1F1F1),
            whoCreated: 'Quản lý',
            label: 'Hao hụt ' + removeDecimalZeroFormat(losses[e][i]['value']) + ' $unit',
            time: DateFormat('h:mm a').format(DateTime.parse(losses[e][i]['createdAt']).toLocal()),
          ),
        ),
      ));

    return data;
  }
}

class _ActionItem extends StatelessWidget {
  final String label;
  final String content;
  final Widget trailingWidget;
  final Color bgAction;
  final String time;
  final String whoCreated;
  _ActionItem({@required this.label, this.content, this.trailingWidget, this.bgAction, this.time, this.whoCreated});
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: Color(0xfff1f1f1),
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: Stack(
        children: [
          Positioned(
            left: 0,
            top: 0,
            bottom: 0,
            width: SizeConfig.setWidth(76),
            child: DecoratedBox(
              decoration: BoxDecoration(
                  color: Color(0xff4B554C),
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(5))),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16), vertical: SizeConfig.setHeight(10)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      time == null ? '' : time.split(' ')[0],
                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                    Text(
                      time == null
                          ? ''
                          : time.split(' ')[1] == 'AM'
                              ? 'Sáng'
                              : 'Chiều',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: SizeConfig.setWidth(86), top: SizeConfig.setHeight(10), bottom: SizeConfig.setHeight(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
                    trailingWidget ?? const SizedBox(),
                  ],
                ),
                if (content != null)
                  SizedBox(
                    height: SizeConfig.setHeight(4),
                  ),
                if (content != null)
                  Text(
                    content ?? '',
                    style: TextStyle(fontSize: 13, color: Color(0xff0D1904)),
                  ),
                if (whoCreated != null)
                  Text(
                    'Người thực hiện: $whoCreated',
                    style: TextStyle(fontSize: 12, color: Colors.black.withOpacity(.6)),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
