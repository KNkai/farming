import 'dart:collection';

import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';
import 'package:mi_smart/src/share/utils/util.dart';

class HistoryLossController {
  final stateLosses = ValueNotifier<HashMap<String, List<dynamic>>>(null);
  String _farmingDiaryId;
  void init(String id) {
    final map = HashMap<String, List<dynamic>>();
    if (HiveService.instnace.boxLosses.values.isNotEmpty) {
      HiveService.instnace.boxLosses.values.first.data.forEach((element) {
        if (element['farmingDiaryId'] == id) {
          _farmingDiaryId = id;
          var date = dateFormat(DateTime.parse(element['createdAt']));
          if (map[date] == null) {
            map.putIfAbsent(date, () => <dynamic>[element]);
          } else {
            map[date].add(element);
          }
        }
      });
      stateLosses.value = map;
    }
  }

  void updateHistoryLog(String id, int value, int oldValue) {
    HiveService.instnace.updateHistoryLog(_farmingDiaryId, id, value - oldValue, value, () {
      init(_farmingDiaryId);
    });
  }
}
