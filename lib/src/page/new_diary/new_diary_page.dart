import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/diary_tab/diary_tab.dart';
import 'package:mi_smart/src/page/input_action/input_action_page.dart';
import 'package:mi_smart/src/page/inventory/inventory_page.dart';
import 'package:mi_smart/src/page/pre_quantity_tab/pre_quantity_tab.dart';
import 'package:mi_smart/src/share/app_font.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class NewDiaryPage extends StatelessWidget {
  static void go({@required BuildContext context, @required String id}) =>
      Navigator.of(context).push(MaterialPageRoute(builder: (_) => NewDiaryPage(id: id)));
  static void goReplacement({@required BuildContext context, @required String id}) =>
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => NewDiaryPage(id: id)));
  static void goReplacementAndAction(
          {@required BuildContext context, @required String id, @required dynamic actionData}) =>
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (_) => NewDiaryPage(id: id, actionData: actionData)));
  final String id;
  final dynamic actionData;
  NewDiaryPage({this.id, this.actionData});
  @override
  Widget build(BuildContext context) {
    final _navigatorController = PageController();
    final _stateIndex = ValueNotifier(0);

    return Scaffold(
      body: SafeArea(
        child: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _navigatorController,
          children: [
            InputActionPage(
              id,
              actionData: actionData,
            ),
            DiaryTab(id),
            PreQuantityTab(id),
            InventoryPage(id),
          ],
        ),
      ),
      bottomNavigationBar: ValueListenableBuilder<int>(
        valueListenable: _stateIndex,
        builder: (_, index, ___) {
          return BottomNavigationBar(
            currentIndex: index,
            backgroundColor: Colors.white,
            type: BottomNavigationBarType.fixed,
            items: [
              _bottomIcon(icon: AppFont.inputAction, label: 'Nhập tác vụ'),
              _bottomIcon(icon: AppFont.showDiary, label: 'Nhật ký'),
              _bottomIcon(icon: AppFont.propersalData, label: 'Sản lượng\ndự kiến'),
              _bottomIcon(icon: AppFont.inventory, label: 'Vật tư')
            ],
            onTap: (index) {
              _stateIndex.value = index;
              _navigatorController.jumpToPage(index);
            },
          );
        },
      ),
    );
  }

  BottomNavigationBarItem _bottomIcon({@required IconData icon, @required String label}) => BottomNavigationBarItem(
        activeIcon: Padding(
          padding: EdgeInsets.only(bottom: SizeConfig.setHeight(8)),
          child: Icon(
            icon,
            color: AppColor.primary,
          ),
        ),
        icon: Padding(
          padding: EdgeInsets.only(bottom: SizeConfig.setHeight(8)),
          child: Icon(
            icon,
          ),
        ),
        label: label,
      );
}
