import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class TimeWidget extends StatelessWidget {
  final void Function(int) onChangeDay;
  final bool freezing;
  TimeWidget({@required this.onChangeDay, this.freezing = false});
  final reversed = HiveService.instnace.boxRegionSupplyDate.values.toList().reversed.toList();
  final _pageController =
      PageController(viewportFraction: 0.2, initialPage: HiveService.instnace.boxRegionSupplyDate.values.length - 1);
  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: AppColor.tertiary,
      child: SizedBox(
        height: SizeConfig.setHeight(80),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16)),
          child: Stack(
            children: [
              Positioned(
                left: 0,
                right: 0,
                bottom: 0,
                child: Center(
                  child: Text(
                    'Chọn ngày',
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ),
              Positioned.fill(
                child: IgnorePointer(
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SizedBox(
                      height: SizeConfig.setWidth(50),
                      width: SizeConfig.setWidth(50),
                      child: DecoratedBox(
                          decoration: BoxDecoration(color: AppColor.primary, borderRadius: BorderRadius.circular(8))),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0,
                bottom: 0,
                right: 0,
                top: SizeConfig.setHeight(7),
                child: Center(
                  child: FractionallySizedBox(
                    widthFactor: 0.8,
                    child: PageView.builder(
                        physics: freezing ? NeverScrollableScrollPhysics() : null,
                        onPageChanged: (index) {
                          onChangeDay(HiveService.instnace.boxRegionSupplyDate.values.length - index - 1);
                        },
                        controller: _pageController,
                        itemCount: HiveService.instnace.boxRegionSupplyDate.values.length,
                        itemBuilder: (_, index) {
                          return Center(
                            child: AnimatedBuilder(
                              animation: _pageController,
                              builder: (_, __) {
                                double page;
                                try {
                                  page = _pageController.page;
                                } catch (_) {
                                  page = HiveService.instnace.boxRegionSupplyDate.values.length - 1.0;
                                }
                                final colorLerp = Color.lerp(AppColor.tertiary, AppColor.primary, (index - page).abs());
                                final time = HiveService.instnace.boxRegionSupplyDate.values
                                    .toList()
                                    .reversed
                                    .elementAt(index)
                                    .time;
                                return Column(
                                  children: [
                                    Text(
                                      time.day.toString(),
                                      style: TextStyle(color: colorLerp, fontSize: 16, fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      'Tháng ${time.month}',
                                      style: TextStyle(color: colorLerp, fontSize: 7),
                                    ),
                                  ],
                                );
                              },
                            ),
                          );
                        }),
                  ),
                ),
              ),
              Positioned.fill(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Transform.rotate(
                      angle: pi,
                      child: Icon(
                        Icons.arrow_right_alt,
                        size: 30,
                        color: Color(0xffBDC1BB),
                      ),
                    ),
                    DecoratedBox(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(stops: [
                        0,
                        .5,
                        1
                      ], colors: [
                        // Colors.red,
                        // Colors.red.withOpacity(0.9),
                        // Colors.red.withOpacity(0.3),
                        AppColor.tertiary,
                        AppColor.tertiary.withOpacity(0.9),
                        AppColor.tertiary.withOpacity(0.3)
                      ])),
                      child: SizedBox(
                        width: SizeConfig.setWidth(46),
                        height: SizeConfig.setHeight(40),
                      ),
                    ),
                    Expanded(child: SizedBox.shrink()),
                    Transform.rotate(
                      angle: pi,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(stops: [
                          0,
                          .5,
                          1
                        ], colors: [
                          AppColor.tertiary,
                          AppColor.tertiary.withOpacity(0.9),
                          AppColor.tertiary.withOpacity(0.3)
                        ])),
                        child: SizedBox(
                          width: SizeConfig.setWidth(46),
                          height: SizeConfig.setHeight(40),
                        ),
                      ),
                    ),
                    Icon(
                      Icons.arrow_right_alt,
                      size: 30,
                      color: Color(0xffBDC1BB),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
