import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/diary_tab/diary_controller.dart';
import 'package:mi_smart/src/page/open_input_slider.dart';
import 'package:mi_smart/src/share/utils/util.dart';

class BannerListActionInput extends StatelessWidget {
  final dynamic data;
  final String day;
  final String month;
  final DiaryController diaryController;
  const BannerListActionInput({Key key, @required this.diaryController, @required this.data, this.day, this.month})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: SizeConfig.setWidth(16)),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  day,
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(width: 10),
                Text(
                  'Tháng $month,\n2021',
                  style: TextStyle(
                    fontSize: 13,
                  ),
                ),
              ],
            ),
          ),
          for (int i = 0; i < data.length; i++)
            Padding(
              padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(10)),
              child: GestureDetector(
                onLongPress: () {
                  openInputSlider(
                      onReturn: (value, _) {
                        diaryController?.updatePhaseLog(data[i]['phase_log_id_custom'], data[i], value);
                      },
                      context: context,
                      label: data[i]['display'].toString() ?? '',
                      hint: data[i]['display'].toString() ?? '',
                      value: data[i]['value'] == '' ? '' : removeDecimalZeroFormat(data[i]['value']));
                },
                child: _ActionItem(
                  whoCreated: data[i]['who_created_custom'],
                  bgAction: Color(0xffF1F1F1),
                  time: data[i]['time_custom'],
                  label: data[i]['display'].toString() ?? '',
                  content: data[i]['value'] == '' ? '' : removeDecimalZeroFormat(data[i]['value']),
                ),
              ),
            ),
        ],
      ),
    );
  }
}

class _ActionItem extends StatelessWidget {
  final String label;
  final String content;
  final Widget trailingWidget;
  final Color bgAction;
  final String time;
  final String whoCreated;
  _ActionItem({@required this.label, this.content, this.trailingWidget, this.bgAction, this.time, this.whoCreated});
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: Color(0xfff1f1f1),
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: Stack(
        children: [
          Positioned(
            left: 0,
            top: 0,
            bottom: 0,
            width: SizeConfig.setWidth(76),
            child: DecoratedBox(
              decoration: BoxDecoration(
                  color: Color(0xff4B554C),
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(5))),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16), vertical: SizeConfig.setHeight(10)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      time == null ? '' : time.split(' ')[0],
                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                    Text(
                      time == null
                          ? ''
                          : time.split(' ')[1] == 'AM'
                              ? 'Sáng'
                              : 'Chiều',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: SizeConfig.setWidth(86), top: SizeConfig.setHeight(10), bottom: SizeConfig.setHeight(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
                    trailingWidget ?? const SizedBox(),
                  ],
                ),
                if (content != null)
                  SizedBox(
                    height: SizeConfig.setHeight(4),
                  ),
                if (content != null)
                  Text(
                    content ?? '',
                    style: TextStyle(fontSize: 13, color: Color(0xff0D1904)),
                  ),
                if (whoCreated != null)
                  Text(
                    'Người thực hiện: $whoCreated',
                    style: TextStyle(fontSize: 12, color: Colors.black.withOpacity(.6)),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
