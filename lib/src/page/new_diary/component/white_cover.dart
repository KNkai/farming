import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';

class WhiteCover extends StatelessWidget {
  final Widget child;
  final Color bgColor;
  WhiteCover({@required this.child, this.bgColor});
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), color: bgColor ?? Colors.white),
      child: Padding(
          padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(16), horizontal: SizeConfig.setWidth(16)),
          child: child),
    );
  }
}
