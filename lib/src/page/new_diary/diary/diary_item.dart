import 'package:flutter/material.dart';
import 'package:mi_smart/src/page/new_diary/component/white_cover.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';

class ItemDiary extends StatelessWidget {
  final String title, empName, total;
  final DateTime startDiary, endDiary;
  final Function isOpenFunc;
  const ItemDiary({
    Key key,
    @required this.title,
    @required this.empName,
    @required this.total,
    @required this.startDiary,
    @required this.endDiary,
    this.isOpenFunc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: WhiteCover(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                title ?? '',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Icon(
                        Icons.person_outline_rounded,
                        color: AppColor.primary,
                        size: 15,
                      ),
                    ),
                    TextSpan(
                      text: ' Người tạo: ',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                    TextSpan(
                      text: '${empName ?? ''}',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Icon(
                        Icons.access_time_rounded,
                        color: AppColor.primary,
                        size: 15,
                      ),
                    ),
                    TextSpan(
                      text: ' Bắt đầu: ',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                    TextSpan(
                      text:
                          '${dateFormat(startDiary ?? DateTime.now())} đến ${dateFormat(endDiary ?? DateTime.now().add(Duration(days: 180)))} ',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Icon(
                        Icons.list_rounded,
                        color: AppColor.primary,
                        size: 15,
                      ),
                    ),
                    TextSpan(
                      text: ' Sản lượng: ',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                    TextSpan(
                      text: '${total ?? ''}',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            AppBtn(
              'Đang mở',
              onPressed: isOpenFunc != null ? () => isOpenFunc() : null,
              width: 100,
              paddingBtn: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              borderRadius: BorderRadius.circular(50),
            ),
          ],
        ),
      ),
    );
  }
}
