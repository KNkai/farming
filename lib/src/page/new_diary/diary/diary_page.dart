import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/new_diary/diary/diary_detail.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

import 'diary_item.dart';

class DiaryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppScafold(
      title: 'Nhật ký',
      padding: EdgeInsets.zero,
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(24), horizontal: SizeConfig.setWidth(16)),
        children: [
          AppBtn(
            'Thêm nhật ký',
            onPressed: () {},
          ),
          for (int i = 0; i <= 10; i++)
            ItemDiary(
              title: 'Cho tôm giống ăn lần ${i}',
              empName: 'Nguyễn Văn A',
              total: '${i + Random.secure().nextInt(100)} tấn',
              startDiary: DateTime.now().add(Duration(days: 10)),
              endDiary: DateTime.now().add(Duration(days: 190)),
              isOpenFunc: i % 2 == 1
                  ? () {
                      DiaryDetail.go(context: context);
                    }
                  : null,
            ),
        ],
      ),
    );
  }
}
