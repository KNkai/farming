import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/new_diary/component/white_cover.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

class DiaryDetail extends StatelessWidget {
  static void go({@required BuildContext context}) =>
      Navigator.of(context).push(MaterialPageRoute(builder: (_) => DiaryDetail()));
  @override
  Widget build(BuildContext context) {
    int i = 0;
    return AppScafold(
      title: 'Nhật ký',
      padding: EdgeInsets.zero,
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(24), horizontal: SizeConfig.setWidth(16)),
        children: [
          BannerDiary(
            title: 'Cho tôm giống ăn lần ${i}',
            empName: 'Nguyễn Văn A',
            total: '${i + Random.secure().nextInt(100)} tấn',
            startDiary: DateTime.now().add(Duration(days: 10)),
            endDiary: DateTime.now().add(Duration(days: 190)),
            isOpenFunc: i % 2 == 1
                ? () {
                    print(i);
                  }
                : null,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'GIAI ĐOẠN CANH TÁC',
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Đang ở giai đoạn: Giai đoạn cho tôm giống ăn lần 2',
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class BannerDiary extends StatelessWidget {
  final String title, empName, total;
  final DateTime startDiary, endDiary;
  final Function isOpenFunc;
  final bool isOpen;
  const BannerDiary({
    Key key,
    @required this.title,
    @required this.empName,
    @required this.total,
    @required this.startDiary,
    @required this.endDiary,
    this.isOpenFunc,
    this.isOpen = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: WhiteCover(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                title ?? '',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Icon(
                        Icons.person_outline_rounded,
                        color: AppColor.primary,
                        size: 15,
                      ),
                    ),
                    TextSpan(
                      text: ' Người tạo: ',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                    TextSpan(
                      text: '${empName ?? ''}',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Icon(
                        Icons.access_time_rounded,
                        color: AppColor.primary,
                        size: 15,
                      ),
                    ),
                    TextSpan(
                      text: ' Bắt đầu: ',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                    TextSpan(
                      text:
                          '${dateFormat(startDiary ?? DateTime.now())} đến ${dateFormat(endDiary ?? DateTime.now().add(Duration(days: 180)))} ',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Icon(
                        Icons.list_rounded,
                        color: AppColor.primary,
                        size: 15,
                      ),
                    ),
                    TextSpan(
                      text: ' Sản lượng: ',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                    TextSpan(
                      text: '${total ?? ''}',
                      style: TextStyle(
                        color: AppColor.grey,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                color: isOpen ? AppColor.primary : AppColor.grey,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  '${isOpen ? 'Đang mở' : 'Đã đóng'}',
                  style: TextStyle(
                    color: isOpen ? AppColor.grey : Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
            ),
            AppBtn(
              'Chỉnh sửa',
              onPressed: isOpenFunc != null ? () => isOpenFunc() : null,
              borderRadius: BorderRadius.circular(10),
            ),
          ],
        ),
      ),
    );
  }
}
