import 'package:hive/hive.dart';

class InputActionModel {}

@HiveType(typeId: 0)
class TextSearchModel extends HiveObject {
  @HiveField(0)
  final String result;
  @HiveField(1)
  final String searchType;
  @HiveField(2)
  final DateTime timeSearch;

  TextSearchModel({this.result, this.searchType, this.timeSearch});

  static addItem(TextSearchModel item) async {
    var box = await Hive.openBox<TextSearchModel>('TextSearchModel');

    box.add(item);
  }

  static deleteItem() async {
    var box = await Hive.openBox<TextSearchModel>('TextSearchModel');
    box.deleteAll(box.keys);
  }
}
