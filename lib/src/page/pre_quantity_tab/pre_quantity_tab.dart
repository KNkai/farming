import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/history_loss/history_loss_page.dart';
import 'package:mi_smart/src/page/open_input_slider.dart';
import 'package:mi_smart/src/page/pre_quantity_tab/pre_quantity_controller.dart';
import 'package:mi_smart/src/page/refresh.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

import '../new_diary/component/white_cover.dart';

class PreQuantityTab extends StatefulWidget {
  final String id;
  PreQuantityTab(this.id);

  @override
  _PreQuantityTabState createState() => _PreQuantityTabState();
}

class _PreQuantityTabState extends State<PreQuantityTab> with RouteAware, WidgetsBindingObserver, Refresh {
  final _controller = PrequantityController();

  final _textController = TextEditingController();
  final _unitController = TextEditingController();

  @override
  void initState() {
    _controller.initDiary(widget.id);
    super.initState();
  }

  @override
  void pleaseRefresh() {
    _controller.initDiary(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      bgColor: AppColor.tertiary,
      title: 'Sản lượng dự kiến',
      padding: EdgeInsets.zero,
      child: ValueListenableBuilder<StateDiary>(
        valueListenable: _controller.stateDiary,
        builder: (_, raw, __) {
          final diary = raw == null ? null : (raw as StateDiaryLoaded).data;
          if (diary == null) return Center(child: Text('Không có mùa vụ'));
          return ListView(
            padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(24)),
            children: [
              SizedBox(
                height: SizeConfig.setHeight(23),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16)),
                child: WhiteCover(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: SizeConfig.setHeight(30),
                      ),
                      GestureDetector(
                        onTap: () {
                          showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(1995, 12),
                                  lastDate: DateTime(2022, 12))
                              .then((value) {
                            _controller.changeDate(value);
                          });
                        },
                        child: _BorderFieldWithLabel(
                          label: 'Thời hạn đến',
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                  diary == null ||
                                          ['expectedHarvestDate'] == "null" ||
                                          diary['expectedHarvestDate'] == null
                                      ? ''
                                      : dateFormat(DateTime.parse(diary['expectedHarvestDate'])),
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                              Icon(Icons.calendar_today)
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: SizeConfig.setHeight(24),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: _BorderFieldWithLabel(
                                isPadding: false,
                                child: TextFormField(
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                  keyboardType: TextInputType.number,
                                  onChanged: (value) {
                                    _controller.changeProdution(value);
                                  },
                                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                  controller: diary == null || diary['production'] == null
                                      ? null
                                      : (_textController
                                        ..text = diary['production'].toString()
                                        ..selection = TextSelection.fromPosition(
                                            TextPosition(offset: diary['production'].toString().length))),
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(left: 20), border: InputBorder.none),
                                ),
                                label: 'Sản lượng dự kiến'),
                          ),
                          SizedBox(
                            width: SizeConfig.setWidth(22),
                          ),
                          Expanded(
                            child: _BorderFieldWithLabel(
                                isPadding: false,
                                child: TextFormField(
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                  onChanged: (value) {
                                    _controller.changeUnit(value);
                                  },
                                  controller: diary == null || diary['unit'] == null
                                      ? null
                                      : (_unitController
                                        ..text = diary['unit'].toString()
                                        ..selection = TextSelection.fromPosition(
                                            TextPosition(offset: diary['unit'].toString().length))),
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(left: 20), border: InputBorder.none),
                                ),
                                label: 'Đơn vị thu hoạch'),
                          )
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.setHeight(24),
                      ),
                      AppBtn(
                        'Nhập hao hụt',
                        borderRadius: BorderRadius.circular(10),
                        onPressed: () {
                          openInputSlider(
                              onReturn: (value, _) {
                                _controller.inputLoss(diary['id'], int.parse(value));
                              },
                              context: context,
                              label: 'Nhập hao hụt',
                              hint: 'Nhập hao hụt');
                        },
                      ),
                      SizedBox(
                        height: SizeConfig.setHeight(24),
                      ),
                      AppBtn(
                        'Xem lịch sử hao hụt',
                        onPressed: () => HistoryLossPage.go(
                            context: context,
                            id: diary['id'],
                            refresh: () {
                              _controller.initDiary(widget.id);
                            }),
                        color: Colors.white,
                        textColor: AppColor.primary,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.setHeight(32),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16)),
                child: WhiteCover(
                  bgColor: AppColor.primary,
                  child: Text(
                    'Tổng sản lượng hao hụt:\n${diary['totalOutputLoss'].toInt()} ${diary['unit']}',
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

class _BorderFieldWithLabel extends StatelessWidget {
  final Widget child;
  final String label;
  final bool isPadding;
  const _BorderFieldWithLabel({
    Key key,
    @required this.child,
    @required this.label,
    this.isPadding = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(fontSize: 12),
        ),
        DecoratedBox(
            child: Padding(
                padding: isPadding
                    ? EdgeInsets.symmetric(vertical: SizeConfig.setHeight(14), horizontal: SizeConfig.setWidth(16))
                    : EdgeInsets.zero,
                child: child),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1, color: Colors.grey[300]))),
      ],
    );
  }
}
