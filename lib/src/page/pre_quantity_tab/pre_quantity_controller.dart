import 'package:flutter/cupertino.dart';
import 'package:mi_smart/src/page/hive_service/diaries_regions_logs.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';

class PrequantityController {
  final stateDiary = ValueNotifier<StateDiary>(null);
  Diaries _diaries;

  void initDiary(String id) {
    _diaries = HiveService.instnace.boxDiaries.values.first;
    try {
      stateDiary.value = StateDiaryLoaded(
          _diaries.data.firstWhere((element) => element['regionId'] == id && element['status'] == 'OPENING'));
    } catch (_) {
      stateDiary.value = StateDiaryLoaded(null);
    }
  }

  void inputLoss(String farmingDiaryId, int value) {
    HiveService.instnace.inputLosses(farmingDiaryId, value);
    stateDiary.value = StateDiaryLoaded(
        HiveService.instnace.boxDiaries.values.first.data.firstWhere((element) => element['id'] == farmingDiaryId));
  }

  void changeProdution(dynamic value) async {
    if (value == null) return;
    (stateDiary.value as StateDiaryLoaded).data['production'] = int.parse(value);
    stateDiary.value = stateDiary.value;
    await _diaries.save();
    stateDiary.notifyListeners();
    HiveService.instnace.updateFarmingDiary(
        (stateDiary.value as StateDiaryLoaded).data['id'],
        Map<String, dynamic>()
          ..putIfAbsent("productionUnitId", () => (stateDiary.value as StateDiaryLoaded).data['productionUnitId'])
          ..putIfAbsent("expectedHarvestDate", () => (stateDiary.value as StateDiaryLoaded).data['expectedHarvestDate'])
          ..putIfAbsent("production", () => value));
  }

  void changeUnit(String unit) async {
    if (unit == null) return;
    (stateDiary.value as StateDiaryLoaded).data['unit'] = unit;
    stateDiary.value = stateDiary.value;
    await _diaries.save();
    stateDiary.notifyListeners();
    HiveService.instnace.updateFarmingDiary(
        (stateDiary.value as StateDiaryLoaded).data['id'],
        Map<String, dynamic>()
          ..putIfAbsent("unit", () => unit)
          ..putIfAbsent("expectedHarvestDate", () => (stateDiary.value as StateDiaryLoaded).data['expectedHarvestDate'])
          ..putIfAbsent("production", () => (stateDiary.value as StateDiaryLoaded).data['production']));
  }

  void changeDate(DateTime time) async {
    if (stateDiary.value != null && time != null) {
      (stateDiary.value as StateDiaryLoaded).data['expectedHarvestDate'] = time.toString();
      stateDiary.value = stateDiary.value;
      await _diaries.save();
      stateDiary.notifyListeners();
      HiveService.instnace.updateFarmingDiary(
          (stateDiary.value as StateDiaryLoaded).data['id'],
          Map<String, dynamic>()
            ..putIfAbsent("productionUnitId", () => (stateDiary.value as StateDiaryLoaded).data['productionUnitId'])
            ..putIfAbsent("expectedHarvestDate", () => time.toString())
            ..putIfAbsent("production", () => (stateDiary.value as StateDiaryLoaded).data['production']));
    }
  }
}

abstract class StateDiary {}

class StateDiaryLoaded extends StateDiary {
  final dynamic data;
  StateDiaryLoaded(this.data);

  @override
  bool operator ==(Object other) => false;
}
