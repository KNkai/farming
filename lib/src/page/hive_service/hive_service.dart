import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/page/hive_service/diaries_regions_logs.dart';
import 'package:mi_smart/src/page/inventory/inventory_controller.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';
import 'package:path_provider/path_provider.dart';

class HiveService {
  static HiveService instnace = HiveService._();
  HiveService._();

  Box<Diaries> boxDiaries;
  Box<Regions> boxRegions;
  Box<Logs> boxLogs;
  Box<Losses> boxLosses;
  Box<UpdateFarmingDiary> boxUpdateDiaries;
  Box<InputLosses> boxInputLosses;
  Box<InputAction> boxInputAction;
  Box<FinishPhase> boxFinishPhase;
  Box<RegionSupplyByDate> boxRegionSupplyDate;
  Box<UpdateRegionSupply> boxUpdateRegionSupply;
  Box<UpdateFarmingDiaryPhaseLog> boxUpdateFarmingPhaseLog;
  Box<UpdateHistoryLog> boxUpdateHistoryLog;
  bool releaseTheKraken = false;

  init() async {
    WidgetsFlutterBinding.ensureInitialized();
    Directory directory = await getApplicationDocumentsDirectory();
    Hive.init(directory.path);
    Hive.registerAdapter(DiariesAdapter());
    Hive.registerAdapter(RegionsAdapter());
    Hive.registerAdapter(LogsAdapter());
    Hive.registerAdapter(UnitsAdapter());
    Hive.registerAdapter(LossesAdapter());
    Hive.registerAdapter(UpdateFarmingDiaryAdapter());
    Hive.registerAdapter(InputLossesAdapter());
    Hive.registerAdapter(InputActionAdapter());
    Hive.registerAdapter(FinishPhaseAdapter());
    Hive.registerAdapter(RegionSupplyByDateAdapter());
    Hive.registerAdapter(UpdateRegionSupplyAdapter());
    Hive.registerAdapter(UpdateFarmingDiaryPhaseLogAdapter());
    Hive.registerAdapter(UpdateHistoryLogAdapter());
    boxDiaries = await Hive.openBox<Diaries>('Diaries');
    boxRegions = await Hive.openBox<Regions>('Regions');
    boxLogs = await Hive.openBox<Logs>('Logs');
    boxLosses = await Hive.openBox<Losses>('Losses');
    boxUpdateDiaries = await Hive.openBox<UpdateFarmingDiary>('UpdateFarmingDiary');
    boxInputLosses = await Hive.openBox<InputLosses>('InputLosses');
    boxInputAction = await Hive.openBox<InputAction>('InputAction');
    boxFinishPhase = await Hive.openBox<FinishPhase>('FinishPhase');
    boxRegionSupplyDate = await Hive.openBox<RegionSupplyByDate>('RegionSupplyByDate');
    boxUpdateRegionSupply = await Hive.openBox<UpdateRegionSupply>('UpdateRegionSupply');
    boxUpdateFarmingPhaseLog = await Hive.openBox<UpdateFarmingDiaryPhaseLog>('UpdateFarmingDiaryPhaseLog');
    boxUpdateHistoryLog = await Hive.openBox<UpdateHistoryLog>('UpdateHistoryLog');
  }

  Future<void> installData() async {
    var results = await Future.wait([
      BaseApi.instance.rawBodyRawDataExecute(_allRegions),
      BaseApi.instance.rawBodyRawDataExecute(_allDiaries),
      BaseApi.instance.rawBodyRawDataExecute(_allPhasingLogs),
      BaseApi.instance.rawBodyRawDataExecute(_allOutputLoss),
    ]);
    var inventories = await Future.wait([
      for (int c = 0; c < 31; c++)
        BaseApi.instance.rawBodyRawDataExecute(_allRegionSupplyByDate(DateTime.now().subtract(Duration(days: c))))
    ]);

    await HiveService.instnace.boxRegions.clear();
    await HiveService.instnace.boxDiaries.clear();
    await HiveService.instnace.boxLogs.clear();
    await HiveService.instnace.boxLosses.clear();
    await HiveService.instnace.boxRegionSupplyDate.clear();
    HiveService.instnace.boxRegions.add(Regions(data: results[0].data['getAllRegion']['data']));
    HiveService.instnace.boxDiaries.add(Diaries(data: results[1].data['getAllFarmingDiary']['data']));
    HiveService.instnace.boxLogs.add(Logs(data: results[2].data['getAllFarmingDiaryPhaseLog']['data']));
    HiveService.instnace.boxLosses.add(Losses(
        data: results[3].data['getAllOutputLossLog']['data'] == null
            ? []
            : (results[3].data['getAllOutputLossLog']['data'] as List)));
    int d = 0;
    inventories.forEach((element) {
      HiveService.instnace.boxRegionSupplyDate.add(RegionSupplyByDate(
          element.data['getAllRegionSuppliesByDate']['data'], DateTime.now().subtract(Duration(days: d++))));
    });
  }

  runService() async {
    Future.doWhile(() {
      return Future.delayed(Duration(seconds: 2)).then((_) async {
        /// Sync Diary
        for (int c = 0; c < boxUpdateDiaries.values.length; c++) {
          final element = boxUpdateDiaries.values.elementAt(c);
          try {
            try {
              final result = await InternetAddress.lookup('google.com');
              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                BaseApi.instance.rawBodyRawDataExecute('''
                mutation updateFarmingDiary(\$q: UpdateFarmingDiaryInput!) {
                    updateFarmingDiary(id: "${element.id}", data: \$q) {
                    id
                  }
                }
                ''', variables: {"q": element.data});
                element.delete();
              }
            } on SocketException catch (_) {}
          } catch (e) {
            element.delete();
          }
        }

        /// Sync Input Losses
        for (int c = 0; c < boxInputLosses.values.length; c++) {
          final element = boxInputLosses.values.elementAt(c);
          try {
            try {
              final result = await InternetAddress.lookup('google.com');
              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                for (int d = 0; d < element.value.length; d++) {
                  BaseApi.instance.rawBodyRawDataExecute(
                    '''
                mutation {
                createOutputLossLog(data: {
                  farmingDiaryId: "${element.key}",
                  value: ${element.value[d]}
                  }) { id }
                }
                ''',
                  );
                }
                element.delete();
              }
            } on SocketException catch (_) {}
          } catch (e) {
            element.delete();
          }
        }

        /// Sync Input
        for (int c = 0; c < boxInputAction.values.length; c++) {
          final element = boxInputAction.values.elementAt(c);
          try {
            try {
              final result = await InternetAddress.lookup('google.com');
              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                BaseApi.instance.rawBodyRawDataExecute('''
                mutation mutationName(\$q: CreateFarmingDiaryPhaseLogInput!) {
                  createFarmingDiaryPhaseLog(data: \$q) {
                    id
                  }
                }
                ''', variables: {
                  "q": {
                    "phaseId": element.phaseId,
                    "time": element.time,
                    "attributes": [
                      {"display": element.display, "key": element.k, "value": element.value, "type": "NUMBER"}
                    ]
                  }
                });
                element.delete();
              }
            } on SocketException catch (_) {}
          } catch (e) {
            element.delete();
          }
        }

        /// Sync Finish Phase
        for (int c = 0; c < boxFinishPhase.values.length; c++) {
          final element = boxFinishPhase.values.elementAt(c);
          try {
            try {
              final result = await InternetAddress.lookup('google.com');
              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                BaseApi.instance.rawBodyRawDataExecute('''
                mutation {
                  updateFarmingDiaryPhase(id: "${element.phaseId}", data:{
                    status: "RESOLVED"
                  }) { id }
                }
                ''');
                element.delete();
              }
            } on SocketException catch (_) {}
          } catch (e) {
            element.delete();
          }
        }

        /// Sync Update Region Supply
        for (int c = 0; c < boxUpdateRegionSupply.values.length; c++) {
          final element = boxUpdateRegionSupply.values.elementAt(c);
          try {
            try {
              final result = await InternetAddress.lookup('google.com');
              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                BaseApi.instance.rawBodyRawDataExecute(
                    'mutation { updateRegionSupplies(id: "${element.key}", data: { qtyInStock:${element.qtyInStock} }) { id } }');
                element.delete();
              }
            } on SocketException catch (_) {}
          } catch (e) {
            element.delete();
          }
        }

        /// Sync Update Phase Log
        for (int c = 0; c < boxUpdateFarmingPhaseLog.values.length; c++) {
          final element = boxUpdateFarmingPhaseLog.values.elementAt(c);
          try {
            try {
              final result = await InternetAddress.lookup('google.com');
              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                BaseApi.instance.rawBodyRawDataExecute('''
                mutation mutationName(\$data: UpdateFarmingDiaryPhaseLogInput!) {
                  updateFarmingDiaryPhaseLog(id: "${element.key}", data: \$data) {
                    id
                  }
                }
                ''', variables: {
                  "data": Map<String, dynamic>()
                    ..putIfAbsent(
                        'attributes',
                        () => [
                              Map<String, dynamic>()
                                ..putIfAbsent('display', () => element.data['attributes'][0]['display'])
                                ..putIfAbsent('key', () => element.data['attributes'][0]['key'])
                                ..putIfAbsent('value', () => element.data['attributes'][0]['value'])
                                ..putIfAbsent('type', () => 'NUMBER')
                            ])
                });
                element.delete();
              }
            } on SocketException catch (_) {}
          } catch (e) {
            element.delete();
          }
        }

        /// Sync Output Loss
        for (int c = 0; c < boxUpdateHistoryLog.values.length; c++) {
          final element = boxUpdateHistoryLog.values.elementAt(c);
          try {
            try {
              final result = await InternetAddress.lookup('google.com');
              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                BaseApi.instance.rawBodyRawDataExecute('''
                mutation updateOutputLossLog(\$q: UpdateOutputLossLogInput!) {
                    updateOutputLossLog(id: "${element.key}", data: \$q) {
                            id
                    }
                }
                ''', variables: {
                  "q": {"value": element.loss.toInt()}
                });
                element.delete();
              }
            } on SocketException catch (_) {}
          } catch (e) {
            element.delete();
          }
        }

        releaseTheKraken = true;
        return true;
      });
    });
  }

  void updateHistoryLog(String farmingDiaryId, String lossId, int adjustValue, int data, VoidCallback finish) {
    boxUpdateHistoryLog.put(lossId, UpdateHistoryLog(data));
    boxUpdateHistoryLog.get(lossId).save();
    if (adjustValue != null) {
      boxDiaries.values.first.data.firstWhere((element) => element['id'] == farmingDiaryId)['totalOutputLoss'] +=
          adjustValue;
    }
    boxDiaries.values.first.save();
    for (int c = 0; c < boxLosses.values.first.data.length; c++) {
      if (boxLosses.values.first.data.elementAt(c)['id'] == lossId) {
        final element = boxLosses.values.first.data.elementAt(c);
        element['value'] = data.toInt();
        boxLosses.values.first.save();
        break;
      }
    }
    finish();
  }

  void updateFarmingPhaselog(String phaseLogId, Map data, VoidCallback onFinish) {
    boxUpdateFarmingPhaseLog.put(phaseLogId, UpdateFarmingDiaryPhaseLog(data));
    boxUpdateFarmingPhaseLog.get(phaseLogId).save();
    for (int c = 0; c < boxLogs.values.first.data.length; c++) {
      if (boxLogs.values.first.data.elementAt(c)['id'] == phaseLogId) {
        final element = boxLogs.values.first.data.elementAt(c);
        element['attributes'] = data['attributes'];
        boxLogs.values.first.save();
        break;
      }
    }
    onFinish();
  }

  void updateRegionSupply(String regionSupplyId, int qtyInStock) {
    final list = HiveService.instnace.boxRegionSupplyDate.values;
    for (int c = 0; c < list.length; c++) {
      if (list.elementAt(c).time.isSameDate(DateTime.now())) {
        for (int d = 0; d < list.elementAt(c).data.length; d++) {
          if (list.elementAt(c).data.elementAt(d)['id'] == regionSupplyId) {
            list.elementAt(c).data.elementAt(d)['qtyInStock'] = qtyInStock;
            list.elementAt(c).save();
            break;
          }
        }
        break;
      }
    }

    boxUpdateRegionSupply.put(regionSupplyId, UpdateRegionSupply(qtyInStock));
    boxUpdateRegionSupply.get(regionSupplyId).save();
  }

  void finishPhase(String phaseId, String regionId) {
    boxFinishPhase.add(FinishPhase(phaseId)).then((value) {
      boxFinishPhase.get(value).save();
    });
    (boxDiaries.values.first.data
                .firstWhere((element) => element['status'] == 'OPENING' && element['regionId'] == regionId)['phases']
            as List)
        .firstWhere((element) => element['status'] == 'OPENING')['status'] = 'RESOLVED';
    boxDiaries.values.first.save();
  }

  void updateFarmingDiary(String id, Map data) {
    boxUpdateDiaries.put(id, UpdateFarmingDiary(id, data));
    boxUpdateDiaries.get(id).save();
  }

  void addInputAction(String phaseId, String key, String display, double value) {
    boxInputAction.add(InputAction(DateTime.now().toUtc().toString(), display, key, value, phaseId)).then((value) {
      boxInputAction.get(value).save();
    });
    if (boxLogs.values.isNotEmpty) {
      SPref.instance.get(CustomString.KEY_NAME).then((name) {
        boxLogs.values.first.data.add({
          "phase": {"status": "OPENING"},
          "phaseId": phaseId,
          "value": value,
          "user": {"name": name},
          "time": DateTime.now().toUtc().toString(),
          "updatedAt": DateTime.now().toUtc().toString(),
          "attributes": [
            {
              "key": key,
              "display": display,
              "value": value,
            }
          ]
        });
        boxLosses.values.first.save();
      });
    }
  }

  void inputLosses(String farmingDiaryId, int value) {
    final farmingDiary = boxInputLosses.get(farmingDiaryId);
    if (farmingDiary != null) {
      boxDiaries.values.first.data.firstWhere((element) => element['id'] == farmingDiaryId)['totalOutputLoss'] += value;
      boxDiaries.values.first.save();
      farmingDiary.value.add(value);
      farmingDiary.save();
    } else {
      boxInputLosses.put(farmingDiaryId, InputLosses(List<int>()..add(value)));
      boxInputLosses.get(farmingDiaryId).save();
      boxDiaries.values.first.data.firstWhere((element) => element['id'] == farmingDiaryId)['totalOutputLoss'] += value;
      boxDiaries.values.first.save();
    }
    if (boxLosses.values.isNotEmpty) {
      boxLosses.values.first.data.add({
        "id": DateTime.now().millisecondsSinceEpoch.toString(),
        "farmingDiaryId": farmingDiaryId,
        "value": value,
        "createdAt": DateTime.now().toString()
      });
      boxLosses.values.first.save();
    }
  }
}

const _allRegions =
    'query GetAllRegion{getAllRegion(q:{limit: 9999, page: 1, offset: null, filter: {}, search: "" , order: {area: -1} }){data{id name province provinceId district districtId ward wardId locationLat locationLng area updatedAt zone{ name } farmingTypeId farmingType{ name listImages } materialId material{ name listImages } listImages } }}';
const _allDiaries =
    '{ getAllFarmingDiary(q: {limit: 9999} ) { data { expectedHarvestDate totalOutputLoss productionUnitId production id name phaseIds phases { id name status startAt endAt attributes { key display type options min max required } } materialId regionId startAt endAt createrName production unit status qrcodeIds createdAt updatedAt material { id name } qrCodes { id name } region { id name } } } }';

const _allPhasingLogs =
    '{ getAllFarmingDiaryPhaseLog(q: { limit: 9999}) { data { id name phaseId attributes { display key value type } time createdBy phase { status } user { name } createdAt updatedAt } } } ';

const _allOutputLoss =
    'query { getAllOutputLossLog(q: { limit: 9999}) { data { id farmingDiaryId value createdAt } } }';

String _allRegionSupplyByDate(DateTime date) {
  return '{ getAllRegionSuppliesByDate(date:"${DateFormat('yyyy-MM-dd').format(date)}", q: {limit: 10000}) { data { createdAt id regionId suppliesId suppliesCode qtyInStock supplies { name unitName } } } } ';
}
