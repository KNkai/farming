// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'diaries_regions_logs.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DiariesAdapter extends TypeAdapter<Diaries> {
  @override
  final int typeId = 20;

  @override
  Diaries read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Diaries(
      data: (fields[0] as List)?.cast<dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, Diaries obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is DiariesAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

class RegionsAdapter extends TypeAdapter<Regions> {
  @override
  final int typeId = 21;

  @override
  Regions read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Regions(
      data: (fields[0] as List)?.cast<dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, Regions obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is RegionsAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

class UnitsAdapter extends TypeAdapter<Units> {
  @override
  final int typeId = 23;

  @override
  Units read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Units(
      data: (fields[0] as List)?.cast<dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, Units obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is UnitsAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

class LogsAdapter extends TypeAdapter<Logs> {
  @override
  final int typeId = 22;

  @override
  Logs read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Logs(
      data: (fields[0] as List)?.cast<dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, Logs obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is LogsAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

class LossesAdapter extends TypeAdapter<Losses> {
  @override
  final int typeId = 24;

  @override
  Losses read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Losses(
      data: (fields[0] as List)?.cast<dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, Losses obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is LossesAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

class UpdateFarmingDiaryAdapter extends TypeAdapter<UpdateFarmingDiary> {
  @override
  final int typeId = 25;

  @override
  UpdateFarmingDiary read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UpdateFarmingDiary(
      fields[0] as String,
      (fields[1] as Map)?.cast<dynamic, dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, UpdateFarmingDiary obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UpdateFarmingDiaryAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class InputLossesAdapter extends TypeAdapter<InputLosses> {
  @override
  final int typeId = 26;

  @override
  InputLosses read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return InputLosses(
      (fields[0] as List)?.cast<int>(),
    );
  }

  @override
  void write(BinaryWriter writer, InputLosses obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.value);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is InputLossesAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class InputActionAdapter extends TypeAdapter<InputAction> {
  @override
  final int typeId = 27;

  @override
  InputAction read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return InputAction(
      fields[0] as String,
      fields[1] as String,
      fields[2] as String,
      fields[3] as double,
      fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, InputAction obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.time)
      ..writeByte(1)
      ..write(obj.display)
      ..writeByte(2)
      ..write(obj.k)
      ..writeByte(3)
      ..write(obj.value)
      ..writeByte(4)
      ..write(obj.phaseId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is InputActionAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FinishPhaseAdapter extends TypeAdapter<FinishPhase> {
  @override
  final int typeId = 28;

  @override
  FinishPhase read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FinishPhase(
      fields[0] as String,
    );
  }

  @override
  void write(BinaryWriter writer, FinishPhase obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.phaseId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FinishPhaseAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RegionSupplyByDateAdapter extends TypeAdapter<RegionSupplyByDate> {
  @override
  final int typeId = 29;

  @override
  RegionSupplyByDate read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RegionSupplyByDate(
      (fields[0] as List)?.cast<dynamic>(),
      fields[1] as DateTime,
    );
  }

  @override
  void write(BinaryWriter writer, RegionSupplyByDate obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.data)
      ..writeByte(1)
      ..write(obj.time);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RegionSupplyByDateAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UpdateRegionSupplyAdapter extends TypeAdapter<UpdateRegionSupply> {
  @override
  final int typeId = 30;

  @override
  UpdateRegionSupply read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UpdateRegionSupply(
      fields[0] as int,
    );
  }

  @override
  void write(BinaryWriter writer, UpdateRegionSupply obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.qtyInStock);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UpdateRegionSupplyAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UpdateFarmingDiaryPhaseLogAdapter extends TypeAdapter<UpdateFarmingDiaryPhaseLog> {
  @override
  final int typeId = 31;

  @override
  UpdateFarmingDiaryPhaseLog read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UpdateFarmingDiaryPhaseLog(
      (fields[0] as Map)?.cast<dynamic, dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, UpdateFarmingDiaryPhaseLog obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UpdateFarmingDiaryPhaseLogAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UpdateHistoryLogAdapter extends TypeAdapter<UpdateHistoryLog> {
  @override
  final int typeId = 32;

  @override
  UpdateHistoryLog read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UpdateHistoryLog(
      fields[0] as int,
    );
  }

  @override
  void write(BinaryWriter writer, UpdateHistoryLog obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.loss);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UpdateHistoryLogAdapter && runtimeType == other.runtimeType && typeId == other.typeId;
}
