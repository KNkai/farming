import 'package:hive/hive.dart';

part 'task_model.g.dart';

@HiveType(typeId: 0)
class TaskModel extends HiveObject {
  @HiveField(0)
  final String collection;
  @HiveField(1)
  final Map package;

  TaskModel({this.collection, this.package});
}
