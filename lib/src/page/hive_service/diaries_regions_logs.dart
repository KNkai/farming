import 'package:hive/hive.dart';

part 'diaries_regions_logs.g.dart';

@HiveType(typeId: 20)
class Diaries extends HiveObject {
  @HiveField(0)
  final List data;

  Diaries({this.data});
}

@HiveType(typeId: 21)
class Regions extends HiveObject {
  @HiveField(0)
  final List data;

  Regions({this.data});
}

@HiveType(typeId: 22)
class Logs extends HiveObject {
  @HiveField(0)
  final List data;

  Logs({this.data});
}

@HiveType(typeId: 23)
class Units extends HiveObject {
  @HiveField(0)
  final List data;

  Units({this.data});
}

@HiveType(typeId: 24)
class Losses extends HiveObject {
  @HiveField(0)
  final List data;

  Losses({this.data});
}

///

@HiveType(typeId: 25)
class UpdateFarmingDiary extends HiveObject {
  @HiveField(0)
  final String id;

  @HiveField(1)
  final Map data;

  UpdateFarmingDiary(this.id, this.data);
}

@HiveType(typeId: 26)
class InputLosses extends HiveObject {
  @HiveField(0)
  final List<int> value;

  InputLosses(this.value);
}

//
@HiveType(typeId: 27)
class InputAction extends HiveObject {
  @HiveField(0)
  final String time;

  @HiveField(1)
  final String display;

  @HiveField(2)
  final String k;

  @HiveField(3)
  final double value;

  @HiveField(4)
  final String phaseId;

  InputAction(this.time, this.display, this.k, this.value, this.phaseId);
}

//
@HiveType(typeId: 28)
class FinishPhase extends HiveObject {
  @HiveField(0)
  final String phaseId;

  FinishPhase(this.phaseId);
}

//
@HiveType(typeId: 29)
class RegionSupplyByDate extends HiveObject {
  @HiveField(0)
  final List<dynamic> data;

  @HiveField(1)
  final DateTime time;

  RegionSupplyByDate(this.data, this.time);
}

//
//
@HiveType(typeId: 30)
class UpdateRegionSupply extends HiveObject {
  @HiveField(0)
  final int qtyInStock;

  UpdateRegionSupply(this.qtyInStock);
}

//
//
@HiveType(typeId: 31)
class UpdateFarmingDiaryPhaseLog extends HiveObject {
  @HiveField(0)
  final Map data;

  UpdateFarmingDiaryPhaseLog(this.data);
}

@HiveType(typeId: 32)
class UpdateHistoryLog extends HiveObject {
  @HiveField(0)
  final int loss;

  UpdateHistoryLog(this.loss);
}
