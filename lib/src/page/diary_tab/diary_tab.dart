import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/new_diary/component/banner_list_action_input.dart';
import 'package:mi_smart/src/page/new_diary/component/white_cover.dart';
import 'package:mi_smart/src/page/refresh.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

import 'diary_controller.dart';

class DiaryTab extends StatefulWidget {
  final String id;
  DiaryTab(this.id);

  @override
  _DiaryTabState createState() => _DiaryTabState();
}

class _DiaryTabState extends State<DiaryTab> with RouteAware, WidgetsBindingObserver, Refresh {
  final _controller = DiaryController();

  @override
  void initState() {
    _controller.init(id: widget.id);
    super.initState();
  }

  @override
  void pleaseRefresh() {
    _controller.init(id: widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      title: 'Nhật ký',
      bgColor: AppColor.tertiary,
      padding: EdgeInsets.zero,
      child: ValueListenableBuilder(
        valueListenable: _controller.stateDiary,
        builder: (context, state, empty) {
          if (state == null) return SizedBox.shrink();

          if (state is DiaryEmpty) return empty;

          final data = (state as DiaryData);
          return ListView(
            padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(24)),
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.setWidth(16)),
                child: WhiteCover(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Mùa vụ: ${data.data == null ? '' : data.data["name"]}',
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                    SizedBox(
                      height: SizeConfig.setHeight(8),
                    ),
                    Text(
                      data.data == null
                          ? 'Thời gian:'
                          : 'Thời gian: ${dateFormat(DateTime.parse(data.data["startAt"]))} đến ${dateFormat(DateTime.parse(data.data["endAt"]))}',
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                    SizedBox(
                      height: SizeConfig.setHeight(8),
                    ),
                    Text(
                      '${data.data == null ? '' : phaseName(data)}',
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                  ],
                )),
              ),
              SizedBox(
                height: SizeConfig.setHeight(27),
              ),
              ...generates(data)
            ],
          );
        },
        child: Center(
          child: Text("Không lấy được dữ liệu"),
        ),
      ),
    );
  }

  String phaseName(data) {
    try {
      return data.data["phases"].firstWhere((element) => element['status'] == 'OPENING')['name'];
    } catch (_) {
      return 'Không có giai đoạn';
    }
  }

  List<Widget> generates(DiaryData data) {
    final list = List<Widget>();
    data.logs.keys.forEach((element) {
      final split = element.split('/');
      list.add(BannerListActionInput(
        diaryController: _controller,
        data: data.logs[element],
        day: split[0],
        month: split[1],
      ));
    });
    return list;
  }
}
