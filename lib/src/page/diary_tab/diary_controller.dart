import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';

class DiaryController {
  final stateDiary = ValueNotifier<DiaryState>(null);
  String _regionId;
  void init({@required String id}) {
    _regionId = id;
    var e;
    try {
      e = HiveService.instnace.boxDiaries.values.first.data.firstWhere((element) {
        return element['regionId'] == id && element['status'] == 'OPENING';
      });
    } catch (_) {}
    var phasseID;

    try {
      phasseID = (e['phases'] as List).firstWhere((element) => element['status'] == 'OPENING')['id'];
    } catch (_) {}
    final logs = LinkedHashMap<String, List<dynamic>>();
    if (phasseID != null) {
      HiveService.instnace.boxLogs.values.first.data
          .where((element) => element['phaseId'] == phasseID && element['phase']['status'] == 'OPENING')
          .toList()
            ..sort((a, b) => DateTime.parse(b['time']).compareTo(DateTime.parse(a['time'])))
            ..forEach((element) {
              final key = DateFormat('dd/MM/yyyy').format(DateTime.parse(element['time']));
              if (logs.containsKey(key)) {
                (element['attributes'] as List).forEach((map) {
                  logs[key].add((map as Map)
                    ..putIfAbsent('phase_log_id_custom', () => element['id'])
                    ..putIfAbsent('who_created_custom', () => element['user']['name'])
                    ..putIfAbsent(
                        'time_custom', () => DateFormat("h:mm a").format(DateTime.parse(element['time']).toLocal())));
                });
              } else {
                final list = List();
                (element['attributes'] as List).forEach((map) {
                  list.add((map as Map)
                    ..putIfAbsent('phase_log_id_custom', () => element['id'])
                    ..putIfAbsent('who_created_custom', () => element['user']['name'])
                    ..putIfAbsent(
                        'time_custom', () => DateFormat("h:mm a").format(DateTime.parse(element['time']).toLocal())));
                });
                logs[key] = list;
              }
            });
    }

    stateDiary.value = DiaryData(e, logs);
  }

  void updatePhaseLog(String phaseLogId, dynamic data, dynamic value) {
    HiveService.instnace.updateFarmingPhaselog(
        phaseLogId,
        Map<String, dynamic>()
          ..putIfAbsent(
              'attributes',
              () => [
                    Map<String, dynamic>()
                      ..putIfAbsent('display', () => data['display'])
                      ..putIfAbsent('key', () => data['key'])
                      ..putIfAbsent('value', () => double.parse(value))
                      ..putIfAbsent('type', () => 'NUMBER')
                  ]), () {
      init(id: _regionId);
    });
  }
}

abstract class DiaryState {}

class DiaryEmpty extends DiaryState {}

class DiaryData extends DiaryState {
  final dynamic data;
  final LinkedHashMap<String, List<dynamic>> logs;
  DiaryData(this.data, this.logs);
}
