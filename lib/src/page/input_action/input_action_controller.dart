import 'package:flutter/material.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';
import 'package:provider/provider.dart';

class InputActionController {
  final stateFertilization = ValueNotifier<String>('|');

  static InputActionController of(BuildContext context) {
    return Provider.of<InputActionController>(context, listen: false);
  }

  void addPhaseLog(String phaseId, String key, String display, double value) {
    HiveService.instnace.addInputAction(phaseId, key, display, value);
  }

  void finishPhase(String phaseId, String regionId, VoidCallback onCallback) {
    HiveService.instnace.finishPhase(phaseId, regionId);
    onCallback();
  }
}
