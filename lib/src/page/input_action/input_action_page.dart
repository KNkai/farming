import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';
import 'package:mi_smart/src/page/input_action/input_action_controller.dart';
import 'package:mi_smart/src/page/open_input_slider.dart';
import 'package:mi_smart/src/page/qr_action_page.dart';
import 'package:mi_smart/src/page/refresh.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

import '../new_diary/component/white_cover.dart';

class InputActionPage extends StatefulWidget {
  final String regionId;
  final dynamic actionData;
  InputActionPage(this.regionId, {this.actionData});

  @override
  _InputActionPageState createState() => _InputActionPageState();
}

class _InputActionPageState extends State<InputActionPage> with RouteAware, WidgetsBindingObserver, Refresh {
  final _controller = InputActionController();
  bool _autoOpenAction = true;

  @override
  void pleaseRefresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var region;
    try {
      region =
          HiveService.instnace.boxRegions.values.first.data.firstWhere((element) => element['id'] == widget.regionId);
    } catch (_) {}
    var diary;
    try {
      diary = HiveService.instnace.boxDiaries.values.first.data
          .firstWhere((element) => element['regionId'] == widget.regionId && element['status'] == 'OPENING');
    } catch (_) {}
    var latest;
    try {
      latest = (diary['phases'] as List).firstWhere((element) => element['status'] == 'OPENING');
    } catch (_) {}
    var attributes;
    try {
      attributes = (latest['attributes'] as List);
    } catch (_) {}

    if (widget.actionData != null && _autoOpenAction) {
      _autoOpenAction = false;
      Future.delayed(Duration(seconds: 1)).then((_) {
        openInputSlider(
            onReturn: (value, _) {
              _controller.addPhaseLog(widget.actionData['phaseId'], widget.actionData['key'],
                  widget.actionData['display'], double.parse(value));
              Future.delayed(Duration(seconds: 1)).then((_) {
                WarningDialog.show(context, 'Bạn vừa mới "${widget.actionData['display']}" với giá trị "$value"', "OK",
                    title: 'Thêm tác vụ thành công');
              });
            },
            context: context,
            hint: widget.actionData['display'],
            label: widget.actionData['display']);
      });
    }
    return AppScafold(
      onQr: () {
        QrActionPage.goToCallAction(context, onOpenAction: (actionData) {
          Future.delayed(Duration(seconds: 1)).then((_) {
            openInputSlider(
                onReturn: (value, _) {
                  var display = actionData['display'];
                  _controller.addPhaseLog(
                      actionData['phaseId'], actionData['key'], actionData['display'], double.parse(value));
                  Future.delayed(Duration(seconds: 1)).then((_) {
                    WarningDialog.show(context, 'Bạn vừa mới "${display}" với giá trị "$value"', "OK",
                        title: 'Thêm tác vụ thành công');
                  });
                },
                context: context,
                hint: actionData['display'],
                label: actionData['display']);
          });
        });
      },
      bgColor: AppColor.tertiary,
      title: 'Nhập tác vụ',
      padding: EdgeInsets.zero,
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: SizeConfig.setHeight(24), horizontal: SizeConfig.setWidth(16)),
        children: [
          WhiteCover(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(region == null ? 'Chưa có mùa vụ' : region['name'], style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(
                height: SizeConfig.setHeight(8),
              ),
              Text(DateFormat('dd/MM/yyyy hh:mm a').format(DateTime.now()),
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(
                height: SizeConfig.setHeight(8),
              ),
              Text(
                '${diary == null ? 'Chưa có giai đoạn' : diary['name']}',
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
              SizedBox(
                height: SizeConfig.setHeight(8),
              ),
              Text(
                diary == null
                    ? 'Thời gian:'
                    : 'Thời gian:  ${dateFormat(DateTime.parse(diary["startAt"]))} đến ${dateFormat(DateTime.parse(diary["endAt"]))}',
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
              SizedBox(
                height: SizeConfig.setHeight(8),
              ),
              Text(
                '${latest == null ? 'Hết giai đoạn trong mùa vụ' : latest['name']}',
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
              if (latest != null)
                SizedBox(
                  height: SizeConfig.setHeight(15),
                ),
              if (latest != null)
                AppBtn(
                  'Kết thúc giai đoạn',
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(
                              "Thông báo",
                              style: TextStyle(color: Colors.green),
                            ),
                            content: Text(
                              "Bạn chắc chắn muốn kết thúc giai đoạn?",
                            ),
                            actions: <Widget>[
                              RawMaterialButton(
                                onPressed: () {},
                                child: Text(
                                  "Đóng",
                                  style: TextStyle(color: AppColor.primary),
                                ),
                              ),
                              RawMaterialButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  _controller.finishPhase(latest['id'], widget.regionId, () {
                                    setState(() {});
                                  });
                                },
                                child: Text(
                                  "Kết thúc giai đoạn",
                                  style: TextStyle(color: AppColor.primary),
                                ),
                              )
                            ],
                          );
                        });
                  },
                )
            ],
          )),
          SizedBox(
            height: SizeConfig.setHeight(27),
          ),
          Text(
            'Danh sách hành động',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: SizeConfig.setHeight(8),
          ),
          if (attributes != null)
            ...attributes
                .map((e) => Padding(
                      padding: EdgeInsets.only(bottom: SizeConfig.setHeight(16)),
                      child: _ActionItem(
                        label: e['display'],
                        onReturn: (value, _) {
                          _controller.addPhaseLog(latest['id'], e['key'], e['display'], double.parse(value));
                          Future.delayed(Duration(seconds: 1)).then((_) {
                            WarningDialog.show(context, 'Bạn vừa mới "${e['display']}" với giá trị "$value"', "OK",
                                title: 'Thêm tác vụ thành công');
                          });
                        },
                      ),
                    ))
                .toList(),
        ],
      ),
    );
  }
}

class _ActionItem extends StatelessWidget {
  final String label;
  final String content;
  final String note;
  final void Function(String, String) onReturn;
  _ActionItem({@required this.label, this.content, this.note, this.onReturn});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        openInputSlider(
            onReturn: onReturn,
            context: context,
            label: label,
            hint: label,
            value: content,
            noteValue: note,
            note: note != null);
      },
      child: DecoratedBox(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              stops: [0.014, 0.014],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [AppColor.primary, Colors.white]),
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: SizeConfig.setHeight(14),
            horizontal: SizeConfig.setWidth(13),
          ),
          child: Row(
            children: [
              Expanded(
                  child: content == null
                      ? Text(label, style: TextStyle(fontWeight: FontWeight.w600))
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
                            SizedBox(
                              height: SizeConfig.setHeight(4),
                            ),
                            Text(
                              content,
                              style: TextStyle(fontSize: 12, color: Colors.black.withOpacity(.6)),
                            ),
                          ],
                        )),
              SizedBox(
                width: SizeConfig.setWidth(10),
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 16,
              )
            ],
          ),
        ),
      ),
    );
  }
}
