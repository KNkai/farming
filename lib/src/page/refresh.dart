import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/main.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';

mixin Refresh<T extends StatefulWidget> on State<T>, RouteAware, WidgetsBindingObserver {
  void showLoadingOverlay(BuildContext context) {
    final overlayState = Overlay.of(context);
    final loadingEntry = OverlayEntry(builder: (ctx) {
      return Positioned.fill(
        child: ColoredBox(
          color: Colors.white.withOpacity(0.5),
          child: Center(child: kLoadingSpinner),
        ),
      );
    });
    overlayState.insert(loadingEntry);
    HiveService.instnace.releaseTheKraken = false;
    Future.doWhile(() async {
      await Future.delayed(Duration(seconds: 1));
      if (HiveService.instnace.releaseTheKraken) {
        InternetAddress.lookup('google.com').catchError((_) {
          loadingEntry.remove();
        }).then((result) async {
          if (result != null && result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            await HiveService.instnace.installData();
            loadingEntry.remove();
            pleaseRefresh();
          }
        }).catchError((_) {
          loadingEntry.remove();
        });
        return false;
      }
      return true;
    });
  }

  void pleaseRefresh();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      showLoadingOverlay(context);
    }
  }

  @override
  void didPushNext() {
    WidgetsBinding.instance.removeObserver(this);
    super.didPushNext();
  }

  @override
  void didPopNext() {
    WidgetsBinding.instance.addObserver(this);
    super.didPopNext();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    routeObserver.subscribe(this, ModalRoute.of(context));
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    routeObserver.unsubscribe(this);
    super.dispose();
  }
}
