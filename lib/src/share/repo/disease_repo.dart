import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/model/pagination.dart';

class DiseaseSituationSrv extends BaseService {
  DiseaseSituationSrv() : super(module: 'DiseaseSituation', fragment: ''' 
id: String
createdAt: DateTime
updatedAt: DateTime
title: String
description: String
listImages: [String]order: '{createdAt: -1}
regionId: ID
reporterId: ID
reportedAt: DateTime
reporterName: String
reporterAvatar: String
reportedBy: String
diseaseId: ID
region {
  name
  district
  ward
  province
}
disease {
  id
  name
}
status: String
commentCount: Int
user {
  id
  name
  avatar
}
staff {
  id
  name
  avatar
}
  ''');
}

class DiseaseSrv extends BaseService {
  DiseaseSrv() : super(module: 'Disease', fragment: ''' 
id: String
name: String
createdAt: DateTime
updatedAt: DateTime
description: String
htmlContent: String
listImages: [String]
  ''');
}

class DiseaseRepo {
  Future<List> getListDisease(String idRegion, {Pagination paging, Function(Pagination) onPaging}) async {
    paging = paging ?? Pagination(limit: 10, page: 1);
    final res = await DiseaseSituationSrv()
        .getList(filter: 'regionId: "$idRegion"', limit: paging.limit, order: '{createdAt: -1}', page: paging.page);
    if (onPaging != null) onPaging(Pagination.fromJson(res["pagination"]));
    return res["data"];
  }

  Future getOne(String id) async {
    final res = await DiseaseSituationSrv().getItem(id);
    return res;
  }

  Future<List<dynamic>> getList(
      {int limit = 10, String filter, String search, String order, int page = 1, int offset}) async {
    try {
      final res = await DiseaseSituationSrv()
          .getList(limit: limit, filter: filter, search: search, offset: offset, order: order, page: page);
      return res["data"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List> getSortOptions() async {
    try {
      final res = await DiseaseSrv().queryEnum("getDiseaseSituationOrder");
      return res["getDiseaseSituationOrder"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List> getListDiseaseType() async {
    final res = await DiseaseSrv().getList(limit: 20, order: '{createdAt: -1}');
    return res["data"];
  }

  Future createDiseaseSituation(String title, String description, List<String> images, String regionId,
      String reportedAt, String diseaseId) async {
    final res = await DiseaseSituationSrv().add('''
  title: """$title"""
  description: """$description"""
  listImages: ${Formart.listStringToGraphqlString(images)}
  regionId: "$regionId"
  reportedAt: "$reportedAt"
  diseaseId: "$diseaseId"
    ''');
    return res;
  }

  Future editDiseaseSituation(
      String id, String title, String description, List<String> images, String diseaseId) async {
    final res = await DiseaseSituationSrv().update(id: id, data: '''
  title: """$title"""
  description: """$description"""
  listImages: ${Formart.listStringToGraphqlString(images)}
  diseaseId: "$diseaseId"
    ''');
    return res;
  }

  Future editSituationStatus(String id, String status, DateTime reportedAt, String title) async {
    final res = await DiseaseSituationSrv().update(id: id, data: '''
    status: "$status"
    reportedAt: "${reportedAt.toString()}",
    title: """$title"""
      ''');
    return res;
  }

  remove(String id) async {
    final res = await DiseaseSituationSrv().delete(id);
    return res;
  }
}
