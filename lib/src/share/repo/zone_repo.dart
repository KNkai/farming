import 'package:mi_smart/base_config/base_config.dart';

class ZoneRepo {
  Future<List> getAllZone() async {
    try {
      final res = await ZoneSrv().getList(limit: 100);
      return res["data"];
    } catch (e) {
      throw Exception(e);
    }
  }
}

class ZoneSrv extends BaseService {
  ZoneSrv() : super(module: 'Zone', fragment: ''' 
id: String
name: String
locationLat: Float
locationLng: Float
createdAt: DateTime
updatedAt: DateTime
regionCount: Int
  ''');
}
