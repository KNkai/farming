import 'package:mi_smart/base_config/base_config.dart';

class DiaryPharseRepo {
  Future<List<dynamic>> getList(
      {int limit = 10,
      String filter,
      String search,
      String order = '{createdAt: -1}',
      int page = 1,
      int offset}) async {
    try {
      final res = await DiaryPharseSrv()
          .getList(limit: limit, filter: filter, search: search, offset: offset, order: order, page: page);
      return res["data"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<dynamic> getOne(String id) async {
    try {
      final res = await DiaryPharseSrv().getItem(id);
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List<dynamic>> getListByDiaryiId(String id) async {
    try {
      final res = await DiaryPharseSrv().getList(filter: 'farmingDiaryId: "$id"', order: '{createdAt: 1}');
      return res["data"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future updatePhase(String id, String name, String status, String startAt, String endAt) async {
    try {
      final res = await DiaryPharseSrv().update(id: id, data: '''
      name: "$name"
      status: "$status"
      startAt: "$startAt"
      endAt: "$endAt"
      ''');
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}

class DiaryPharseSrv extends BaseService {
  DiaryPharseSrv() : super(module: 'FarmingDiaryPhase', fragment: ''' 
id: String
name: String
status: String
startAt: DateTime
endAt: DateTime
updatedAt: DateTime
attributes {
    key
    type
    display
    options
    min
    max
    required
    supplies {
      id
      name
      unit {
        id
        name
      }
    }
  }
  ''');
}
