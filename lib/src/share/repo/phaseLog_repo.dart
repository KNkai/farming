import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/model/pagination.dart';

class PhaseLogRepo {
  Future<List<dynamic>> getFarmingDiaryPhaseLog(String id, {Pagination paging, Function(Pagination) onPaging}) async {
    paging = paging ?? Pagination(limit: 5, page: 1);
    final res = await PhaseLogSrv()
        .getList(filter: 'phaseId: "$id"', order: '{createdAt: -1}', limit: paging.limit, page: paging.page);
    if (onPaging != null) onPaging(Pagination.fromJson(res["pagination"]));
    return res["data"];
  }

  Future<dynamic> updatePhaseLog(String id, String time, String attributes) async {
    try {
      final res = await PhaseLogSrv().update(id: id, data: '''
      time: "$time"
      attributes: $attributes
      ''');
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future createPhaseLog(String phaseId, String time, String attributes) async {
    try {
      final res = await PhaseLogSrv().add('''
      phaseId: "$phaseId"
      time: "$time"
      attributes: $attributes
      ''');
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future deletePhaseLog(String id) async {
    try {
      final res = await PhaseLogSrv().delete(id);
      return res;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}

class PhaseLogSrv extends BaseService {
  PhaseLogSrv() : super(module: 'FarmingDiaryPhaseLog', fragment: ''' 
id
name
staffId
phaseId
attributes {
    key
    type
    display
    value
    supplies {
      id
      name
      unit {
        id
        name
      }
    }
}
color
time
createdBy
createrId
createrName
createrAvatar
createdAt
updatedAt
staff {
    id
    name
    avatar
}
user {
    id
    name
    avatar
}
  ''');
}
