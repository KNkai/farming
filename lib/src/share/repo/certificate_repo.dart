import 'package:mi_smart/base_config/base_config.dart';

class CertificateRepo {
  // Future<List> addCertificate({
  //     String title, String description, List<String> listImgs, DateTime effectiveDate, DateTime expiredDate}) async {
  //   try {
  //     final res = await CertificateSrv().add('''
  //       data: {
  //         title: "$title"
  //         description: "$description"
  //         listImages: ${Formart.listStringToGraphqlString(listImgs)}
  //         effectiveDate: "${effectiveDate.toString()}"
  //         expiredDate: "${expiredDate.toString()}"
  //       }
  //     ''');
  //     return res["data"];
  //   } catch (e) {
  //     throw Exception(e);
  //   }
  // }
}

class CertificateSrv extends BaseService {
  CertificateSrv() : super(module: 'Certificate', fragment: ''' 
title: String
description: String
listImages: [String]
effectiveDate: DateTime
expiredDate: DateTime
  ''');
}
