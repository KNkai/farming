import 'package:mi_smart/base_config/base_config.dart';

class FarmingTypeRepo {
  Future<List> getAllFarmingType() async {
    try {
      final res = await FarmingTypeSrv().getList(limit: 100);
      return res["data"];
    } catch (e) {
      throw Exception(e);
    }
  }
}

class FarmingTypeSrv extends BaseService {
  FarmingTypeSrv() : super(module: 'FarmingType', fragment: ''' 
id: String
name: String
description: String
listImages: [String]
createdAt: DateTime
updatedAt: DateTime
regionCount: Int
  ''');
}
