import 'package:mi_smart/base_config/base_config.dart';

class MaterialRepo {
  Future<List> getAllMaterial() async {
    try {
      final res = await MaterialSrv().getList(limit: 100);
      return res["data"];
    } catch (e) {
      throw Exception(e);
    }
  }
}

class MaterialSrv extends BaseService {
  MaterialSrv() : super(module: 'Material', fragment: ''' 
id: String
name: String
description: String
htmlContent: String
listImages: [String]
thumbnail: String
createdAt: DateTime
updatedAt: DateTime
regionCount: Int
  ''');
}
