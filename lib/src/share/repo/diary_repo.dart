import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/model/pagination.dart';

class DiaryRepo {
  Future getDiary(String diaryId) async {
    return await DiarySrv().getItem(diaryId);
  }

  Future<List> getListDiary(String idRegion, {Pagination paging, Function(Pagination) onPaging}) async {
    paging = paging ?? Pagination(limit: 5, page: 1);
    final res = await DiarySrv().getList(
      filter: 'regionId: "$idRegion"',
      limit: paging.limit,
      order: '{createdAt: -1}',
      page: paging.page,
    );
    if (onPaging != null) onPaging(Pagination.fromJson(res["pagination"]));
    return res["data"];
  }

  Future<List<dynamic>> getList(
      {int limit = 10, String filter, String search, String order, int page = 1, int offset}) async {
    try {
      final res = await DiarySrv()
          .getList(limit: limit, filter: filter, search: search, offset: offset, order: order, page: page);
      return res["data"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List> getSortOptions() async {
    try {
      final res = await DiarySrv().queryEnum("getFarmingDiaryOrder");
      return res["getFarmingDiaryOrder"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<dynamic> createDiary({
    String name,
    String startAt,
    String endAt,
    int production,
    String unit,
    String regionId,
    String materialId,
  }) async {
    final res = await DiarySrv().add('''
name: "$name"
startAt: "$startAt"
endAt: "$endAt"
production: $production
unit: "$unit"
regionId: "$regionId"
''');
    return res;
  }

  Future<dynamic> updateDiary({
    String id,
    String name,
    String startAt,
    String endAt,
    int production,
    String unit,
    String status,
  }) async {
    try {
      final res = await DiarySrv().update(
        id: id,
        data: '''
name: "$name"
startAt: "$startAt"
endAt: "$endAt"
production: $production
unit: "$unit"
status: "$status"
''',
      );
      return res;
    } catch (e) {
      throw Exception(e);
    }
  }
}

class DiarySrv extends BaseService {
  DiarySrv() : super(module: 'FarmingDiary', fragment: ''' 
id: String
name: String
phaseIds: [ID]
materialId: ID
regionId: ID
startAt: DateTime
endAt: DateTime
createdById: ID
production: Int
unit
status: String
qrcodeIds: [ID]
createdAt: DateTime
updatedAt: DateTime
createrName
material {
  id
  name
}
region {
  id
  name
  zone {
    id
    name
  }
}

  ''');
}
