import 'package:mi_smart/base_config/base_config.dart';

class DiseaseCommentSrv extends BaseService {
  DiseaseCommentSrv() : super(module: 'DiseaseComment', fragment: ''' 
id
situationId
createdAt
message
listImages
commenterName
commenterAvatar
commenterId 
user {
  id
  name
  avatar
}
staff {
  id
  name
  avatar
}
  ''');
}

class DiseaseCommentRepo {
  getList(String situationId) async {
    final res =
        await DiseaseCommentSrv().getList(filter: 'situationId: "$situationId"', limit: 100, order: '{createdAt: -1}');
    return res["data"];
  }

  createComment(String situationId, String message, List<String> images, {String fragment}) async {
    final res = await DiseaseCommentSrv().add('''
    message: """$message"""
    listImages: ${Formart.listStringToGraphqlString(images)}
    situationId: "$situationId"
      ''', fragment: fragment);
    return res;
  }

  updateComment(String commentId, {String message, List<String> listImages}) async {
    return await DiseaseCommentSrv().update(id: commentId, data: '''
    message: """$message"""
    listImages: ${Formart.listStringToGraphqlString(listImages)}
    ''');
  }

  Future removeComment(String id) async {
    final res = await DiseaseCommentSrv().delete(id);
    return res;
  }
}
