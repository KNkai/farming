import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/map_with_polygon.dart';

class RegionSrv extends BaseService {
  RegionSrv() : super(module: 'Region', fragment: ''' 
      id
      name
      province
      provinceId
      district
      districtId
      ward
      wardId
      locationLat
      locationLng
      area
      updatedAt
      ownerName
      polygon {
          paths {
              lat
              lng
          }
          strokeColor
          strokeOpacity
          strokeWeight
          fillColor
          fillOpacity
      }
      ownerPhone
      ownerAddress
      name
      note
      certificates {
        title: String
        description: String
        listImages: [String]
        effectiveDate: DateTime
        expiredDate: DateTime
      }
      assignees {
        id: String
        phone: String
        email: String
        name: String
        avatar: String
        uid: String
        isBlock: Boolean
        address: String
        province: String
        district: String
        ward: String
        provinceId: String
        districtId: String
        wardId: String
        createdAt: DateTime
        updatedAt: DateTime
      }
      zoneId
      zone {
        name
      }
      farmingTypeId
      farmingType {
        id
        name
        listImages
      }
      materialId
      material {
        id
        name
        listImages
      }
      listImages
      openingDiary {
        farmingDiaryId
        phaseId
        farmingDiary {
            id
            name
            status
        }
        phase {
            id
            name
            status
        }
      }
  ''');
}

class RegionRepo {
  Future getRegionInfo(String idRegion) async {
    final res = await RegionSrv().getItem(idRegion);
    return res;
  }

  Future<dynamic> addRegionCertificate(String id, String idCertificate) async {
    final res = await RegionSrv().update(id: id, data: '''
certificates: "s$idCertificate"
      ''');
    return res;
  }

  Future<dynamic> addCertificateToRegion(
      {String idRegion,
      String title,
      String description,
      List<String> listImgs,
      String effectiveDate,
      String expiredDate}) async {
    final res = await RegionSrv().mutate(
        'addCertificateToRegion',
        '''
    regionId: "$idRegion"
data: {
  title: "$title"
  description: "$description"
  listImages: ${Formart.listStringToGraphqlString(listImgs)}
  effectiveDate: "$effectiveDate"
  expiredDate: "$expiredDate"
}
      ''',
        fragment: 'id');
    return res;
  }

  Future<dynamic> updateCertificateToRegion({@required String id, List<dynamic> certificates}) async {
    final res = await RegionSrv().update(id: id, data: '''
certificates: [${parseJsonToFilter(certificates)}]
      ''');
    return res;
  }

  Future<dynamic> updateRegion(
      {@required String id,
      String name,
      String provinceId,
      String districtId,
      String wardId,
      double area,
      String ownerName,
      String ownerPhone,
      String ownerAddress,
      String farmingTypeId,
      PolygonData polygon,
      String note,
      List<String> images}) async {
    final res = await RegionSrv().update(id: id, data: '''
name: "$name"
provinceId: "$provinceId"
districtId: "$districtId"
wardId: "$wardId"
area: ${area ?? 0}
ownerName: "$ownerName"
ownerPhone: "$ownerPhone"
ownerAddress: "$ownerAddress"
farmingTypeId: "$farmingTypeId"
 ${polygon == null ? '' : '''
      polygon: {
        strokeColor: "${toHex(polygon.strokeColor)}"
        strokeOpacity: ${polygon.strokeOpacity}
        strokeWeight: ${polygon.strokeWeight}
        fillColor: "${toHex(polygon.fillColor)}"
        fillOpacity: ${polygon.fillOpacity}
  paths: ${jsonEncode(polygon.paths.map((e) => {"lat": e.latitude, "lng": e.longitude}).toList()).replaceAll('"', '')}
}
      '''}
note: """$note"""
listImages: ${Formart.listStringToGraphqlString(images)}
      ''');
    return res;
  }

  Future deleteRegion({@required String id}) async {
    final res = await RegionSrv().delete(id);
    return res;
  }

  Future<dynamic> assignStaffToRegion({@required String regionId, @required String phone, @required String name}) {
    return RegionSrv().mutate(
        "assignStaffToRegion",
        '''
      regionId: "$regionId", phone: "$phone", name: "$name"
    ''',
        fragment: "id name phone");
  }
}
