import 'package:mi_smart/base_config/base_config.dart';

class StaffRepo {
  Future deleteStaff(String id) async {
    final res = await StaffSrv().delete(id);
    return res;
  }
}

class StaffSrv extends BaseService {
  StaffSrv() : super(module: 'Staff', fragment: ''' 
id: String
phone: String
email: String
name: String
avatar: String
uid: String
isBlock: Boolean
address: String
province: String
district: String
ward: String
provinceId: String
districtId: String
wardId: String
createdAt: DateTime
updatedAt: DateTime
  ''');
}
