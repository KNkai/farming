import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';

class ErrorImgWidget extends StatelessWidget {
  final String text;
  const ErrorImgWidget({this.text});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        SizedBox(
          height: 100,
          child: Image.asset('assets/error.png'),
        ),
        const SpacingBox(
          height: 3,
        ),
        Text(
          text ?? 'Có lỗi xảy ra.',
          style: ptButton(context),
        ),
      ]),
    );
  }
}
