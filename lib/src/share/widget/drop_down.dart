import 'package:flutter/material.dart';

import '../../../base_config/base_config.dart';

class CustomStringDropDown extends StatefulWidget {
  final List<String> values;
  final String title;
  final Function(String) onSelect;
  final bool hasBorder;
  final bool isDense;
  final bool enable;
  final String hint;
  final String initialValue;
  final Map<String, String> mapDataEnum;
  final bool notEmptyValidator;
  const CustomStringDropDown(
      {@required this.values,
      this.mapDataEnum,
      this.title,
      this.hint,
      this.isDense = false,
      this.hasBorder = true,
      this.notEmptyValidator = false,
      @required this.onSelect,
      this.enable = true,
      this.initialValue});

  @override
  _CustomStringDropDownState createState() => _CustomStringDropDownState();
}

class _CustomStringDropDownState extends State<CustomStringDropDown> {
  String value;
  @override
  Widget build(
    BuildContext context,
  ) {
    final items = <DropdownMenuItem<String>>[];
    widget.values.forEach((value) {
      items.add(DropdownMenuItem(
        value: value,
        child: Text(
          widget.mapDataEnum != null ? widget.mapDataEnum[value] : value.toString(),
        ),
      ));
    });
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.title != null)
          Padding(
            padding: const EdgeInsets.all(5),
            child: Text(
              widget.title,
              style: ptCaption(context),
            ),
          ),
        Container(
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                side: widget.hasBorder
                    ? const BorderSide(width: 0.5, style: BorderStyle.solid, color: Colors.black54)
                    : BorderSide.none,
                borderRadius: const BorderRadius.all(Radius.circular(4.0)),
              ),
            ),
            child: widget.enable
                ? DropdownButtonFormField(
                    value: value ?? widget.initialValue,
                    isExpanded: true,
                    items: items,
                    validator: (value) {
                      if (widget.notEmptyValidator) {
                        if (value == null || value.toString().trim() == '') {
                          return 'Bắt buộc chọn';
                        }
                        return null;
                      }
                      return null;
                    },
                    onChanged: widget.enable
                        ? (val) {
                            widget.onSelect(val as String);
                            setState(() {
                              value = val;
                            });
                          }
                        : null,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      enabledBorder: const UnderlineInputBorder(borderSide: BorderSide.none),
                      isDense: widget.isDense,
                      labelText: widget.hint,
                      contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 13),
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                    ),
                  )
                : Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 15, top: 15, bottom: 15),
                          child: Text(widget.initialValue.toString()),
                        ),
                      ),
                    ],
                  )),
      ],
    );
  }
}

class CustomMapDropDown extends StatefulWidget {
  final Map<String, dynamic> values;
  final Function(dynamic) onSelect;
  final dynamic initialValue;
  final bool enable;
  final String hint;

  const CustomMapDropDown({@required this.values, @required this.onSelect, this.hint, this.enable, this.initialValue});

  @override
  _CustomMapDropDownState createState() => _CustomMapDropDownState();
}

class _CustomMapDropDownState extends State<CustomMapDropDown> {
  dynamic value;

  @override
  Widget build(
    BuildContext context,
  ) {
    final items = <DropdownMenuItem<String>>[];
    widget.values.forEach((key, value) {
      items.add(DropdownMenuItem(
        value: value,
        child: Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: Text(key),
        ),
      ));
    });
    return Container(
      decoration: const ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1.0, style: BorderStyle.solid, color: Colors.black54),
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
        ),
      ),
      child: widget.enable
          ? DropdownButtonFormField(
              value: value ?? widget.initialValue,
              isExpanded: true,
              items: items,
              onChanged: widget.enable
                  ? (val) {
                      widget.onSelect(val);
                      setState(() {
                        value = val;
                      });
                    }
                  : null,
              decoration: InputDecoration(
                fillColor: Colors.white,
                enabledBorder: const UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
                labelText: widget.hint,
                contentPadding: const EdgeInsets.all(5),
                floatingLabelBehavior: FloatingLabelBehavior.always,
              ),
            )
          : Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15, top: 15, bottom: 15),
                    child: Text(widget.initialValue.toString()),
                  ),
                ),
              ],
            ),
    );
  }
}

class CustomListDropDown extends StatefulWidget {
  final List values;
  final Function(dynamic) onSelect;
  final dynamic initialValue;
  final bool enable;
  final String title;
  final String fieldDisplay;
  final String fieldValue;
  final bool isDense;
  final String leadingValue;
  final Function(String) getLeading;
  final String hint;
  final bool hasborder;
  final String placeholder;

  const CustomListDropDown(
      {@required this.values,
      @required this.onSelect,
      this.fieldDisplay,
      this.fieldValue,
      this.hint,
      this.title,
      this.hasborder = false,
      this.leadingValue,
      this.isDense = false,
      this.getLeading,
      this.enable = true,
      this.initialValue,
      this.placeholder});

  @override
  _CustomListDropDownState createState() => _CustomListDropDownState();
}

class _CustomListDropDownState extends State<CustomListDropDown> {
  dynamic value;

  @override
  void initState() {
    value = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(
    BuildContext context,
  ) {
    final items = <DropdownMenuItem>[];
    for (final val in widget.values) {
      items.add(DropdownMenuItem(
        value: widget.fieldValue != null ? val[widget.fieldValue] : val,
        child: Text(widget.fieldDisplay != null ? val[widget.fieldDisplay] : val),
      ));
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.title != null)
          Padding(
            padding: const EdgeInsets.all(5),
            child: Text(
              widget.title,
              style: ptCaption(context),
            ),
          ),
        Container(
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              side: widget.hasborder
                  ? const BorderSide(width: 0.5, style: BorderStyle.solid, color: Colors.grey)
                  : BorderSide.none,
              borderRadius: const BorderRadius.all(Radius.circular(4.0)),
            ),
          ),
          child: (widget.enable != null && widget.enable)
              ? DropdownButtonFormField(
                  isExpanded: true,
                  value:
                      value, //?? (widget.fieldValue != null ? widget.values[0][widget.fieldValue] : widget.values[0]),
                  items: items,

                  onChanged: widget.enable
                      ? (val) {
                          widget.onSelect(val);
                          setState(() {
                            value = val;
                          });
                        }
                      : null,
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    enabledBorder: const UnderlineInputBorder(borderSide: BorderSide.none),
                    isDense: widget.isDense,
                    labelText: widget.hint,
                    hintText: widget.placeholder,
                    contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 13),
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                  ),
                )
              : Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 5, top: 15, bottom: 15),
                        child: Text(widget.fieldDisplay != null && widget.fieldValue != null
                            ? widget.values.firstWhere(
                                (element) => element[widget.fieldValue] == widget.initialValue)[widget.fieldDisplay]
                            : widget.initialValue.toString()),
                      ),
                    ),
                  ],
                ),
        ),
      ],
    );
  }
}
