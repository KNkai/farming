import 'dart:math';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';

import 'image_view.dart';

class ImageSlider extends StatelessWidget {
  final List<String> images;
  final BorderRadius borderRadius;
  final double heightScale;

  const ImageSlider({Key key, this.images, this.borderRadius, this.heightScale}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        height: SizeConfig.setHeight(232),
        viewportFraction: 1,
        autoPlay: true,
        autoPlayInterval: const Duration(seconds: 10),
        autoPlayAnimationDuration: const Duration(milliseconds: 1200),
      ),
      items: _buildListImage(images),
    );
  }

  _buildListImage(List<dynamic> list) {
    return list
        .map<Widget>(
          (item) => Builder(builder: (context) {
            return ClipRRect(
              borderRadius: borderRadius ??
                  const BorderRadius.only(bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
              child: ImageView(
                tag: 'img' + Random().nextInt(100000000).toString(), // this need a unique id
                url: item,
              ),
            );
          }),
        )
        .toList();
  }
}
