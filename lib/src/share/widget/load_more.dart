import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:provider/provider.dart';

typedef IndexedWidgetBuilder = Widget Function(BuildContext context, int index);

class LoadMoreEvent extends BaseEvent {
  LoadMoreEvent();
}

class LoadMoreScrollView extends StatelessWidget {
  final BaseBloc bloc;
  final int itemCount;
  final IndexedWidgetBuilder itemBuilder;
  final ScrollPhysics physics;
  final Widget Function(BuildContext, int) separatorBuilder;
  final EdgeInsets padding;
  LoadMoreScrollView(
      {@required this.bloc,
      @required this.itemCount,
      this.padding,
      @required this.itemBuilder,
      this.separatorBuilder,
      this.physics});

  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return StreamProvider<bool>.value(
      initialData: false,
      value: bloc.endScrollStream,
      child: NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scrollInfo) {
          if (scrollInfo is ScrollEndNotification && _scrollController.position.extentAfter == 0) {
            bloc.event.add(LoadMoreEvent());
          }
          if (scrollInfo.metrics.pixels < scrollInfo.metrics.maxScrollExtent) {
            bloc.endScrollSink.add(false);
          }
          return true;
        },
        child: separatorBuilder != null
            ? ListView.separated(
                padding: this.padding,
                shrinkWrap: true,
                itemBuilder: itemBuilder,
                controller: _scrollController,
                physics: physics,
                separatorBuilder: separatorBuilder,
                itemCount: itemCount)
            : ListView.builder(
                physics: physics,
                padding: this.padding,
                shrinkWrap: true,
                controller: _scrollController,
                itemBuilder: itemBuilder,
                itemCount: itemCount,
              ),
      ),
    );
  }
}
