import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/utils/color.dart';

enum TextFieldType { Email, Phone, Password, Text }

class TextInput extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final Widget icon;
  final TextInputType keyboardType;
  final FocusNode focusNode;
  final bool hinden;
  final bool shouldValidate;
  final Function(String) onChange;
  final TextFieldType type;
  final List<TextInputFormatter> inputFormatters;
  const TextInput(
      {this.focusNode,
      this.hint,
      this.controller,
      this.icon,
      this.onChange,
      this.shouldValidate = true,
      this.type = TextFieldType.Text,
      this.keyboardType,
      this.hinden = false,
      this.inputFormatters});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onSaved: (text) {},
      validator: (value) {
        if (shouldValidate) {
          if (value.trim() == '') return 'Cần điền';
          if (type == TextFieldType.Phone) {
            if (!Validator.isPhoneNumber(value)) return 'Số điện thoại không hợp lệ';
          }
          if (type == TextFieldType.Email) {
            if (!Validator.isEmail(value)) return 'Email không hợp lệ';
          }
          if (type == TextFieldType.Password) {
            if (!Validator.isPassword(value)) return 'Password không đúng';
          }
        }
        return null;
      },
      controller: controller,
      focusNode: focusNode,
      keyboardType: keyboardType ?? TextInputType.text,
      obscureText: hinden,
      onChanged: onChange,
      showCursor: false,
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
        fillColor: Colors.white,
        filled: true,
        contentPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        isDense: true,
        hintText: hint ?? '',
        labelStyle: const TextStyle(height: -1),
        prefixIcon: Padding(
          padding: const EdgeInsets.all(8),
          child: Container(
              height: 30,
              width: 30,
              margin: const EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                color: AppColor.grey.withOpacity(0.2),
                borderRadius: BorderRadius.circular(5),
              ),
              child: icon),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: const BorderSide(
            width: 0,
            style: BorderStyle.none,
          ),
        ),
      ),
    );
  }
}
