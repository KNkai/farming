import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/share/utils/color.dart';

FilterSheet filterSheet;

showFilterSheet(List<FilterSheetModel> list, BuildContext context, Function(String) callBack) {
  if (filterSheet == null) {
    filterSheet = FilterSheet(list, callBack);
  }
  showModalBottomSheet(context: context, isScrollControlled: true, builder: (context) => FilterSheet(list, callBack));
}

class FilterSheetModel {
  String name;
  String title;
  List items;
  int colCount;
  FilterSheetModel({this.name, this.title, this.items, this.colCount = 1});
}

class FilterSheet extends StatefulWidget {
  final Function(String) callBack;
  final List<FilterSheetModel> filters;

  const FilterSheet(this.filters, this.callBack);
  @override
  _FilterSheetState createState() => _FilterSheetState();
}

class _FilterSheetState extends State<FilterSheet> with AutomaticKeepAliveClientMixin {
  List<String> result = [];

  @override
  void initState() {
    super.initState();
    // filters
  }

  @override
  bool get wantKeepAlive => true;

  _onChangeFilters(List<String> items, String name) {
    String replacementStr = '';
    items.forEach((element) {
      replacementStr += '"$element",';
    });
    replacementStr = '$name:{__in:[$replacementStr]}';

    int index = result.indexWhere((element) => element.contains(name));
    if (index == -1) {
      result.add(replacementStr);
    } else {
      result[index] = replacementStr;
    }
  }

  _onSubmit() {
    String res = '';
    result.forEach((element) {
      res += element;
      res += ' ';
    });
    widget.callBack('$res');
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: deviceHeight(context) - 85),
      child: Container(
        color: Colors.white,
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 65, left: 15, bottom: 15, right: 15),
                child: Column(
                    children: widget.filters
                        .map<Widget>((e) => FilterObject(e, (items) => _onChangeFilters(items, e.name)))
                        .toList()),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              height: 50,
              width: deviceWidth(context),
              decoration: const BoxDecoration(
                color: AppColor.primary,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () => navigatorKey.currentState.maybePop(),
                    child: const Padding(
                      padding: EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                      child: Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const Text(
                    'Bộ lọc',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                  ),
                  const Spacer(),
                  InkWell(
                    onTap: () {
                      navigatorKey.currentState.maybePop();
                      _onSubmit();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4), border: Border.all(color: Colors.white)),
                      child: const Text(
                        'Áp dụng',
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FilterObject extends StatefulWidget {
  final FilterSheetModel model;
  final Function(List<String>) callBackOptions;
  const FilterObject(this.model, this.callBackOptions);
  @override
  _FilterObjectState createState() => _FilterObjectState();
}

class _FilterObjectState extends State<FilterObject> with AutomaticKeepAliveClientMixin {
  List<String> selectedItems = [];

  @override
  void initState() {
    widget.model.items.forEach((element) {
      selectedItems.add(element["id"]);
    });
    widget.callBackOptions(selectedItems);
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            widget.model.title ?? '',
            style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
        ),
        StaggeredGridView.count(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: widget.model.colCount,
          staggeredTiles: widget.model.items.map<StaggeredTile>((_) => const StaggeredTile.fit(1)).toList(),
          children: widget.model.items
              .map<Widget>(
                (e) => CheckBoxItem(e["name"], e["id"], (flagCheck) {
                  if (flagCheck) {
                    selectedItems.add(e["id"]);
                  } else {
                    selectedItems.remove(e["id"]);
                  }
                  widget.callBackOptions(selectedItems);
                }),
              )
              .toList(),
        ),
      ],
    );
  }
}

class CheckBoxItem extends StatefulWidget {
  final String label;
  final String value;
  final Function(bool) onCheck;
  const CheckBoxItem(this.label, this.value, this.onCheck);
  @override
  _CheckBoxItemState createState() => _CheckBoxItemState();
}

class _CheckBoxItemState extends State<CheckBoxItem> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  bool value = true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Checkbox(
            value: value,
            onChanged: (val) {
              setState(() {
                value = val;
              });
              widget.onCheck(val);
            },
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap),
        Text(
          widget.label,
          style: ptCaption(context)
              .copyWith(fontSize: 14, color: Colors.black87.withOpacity(0.7), fontWeight: FontWeight.w500),
        ),
      ],
    );
  }
}
