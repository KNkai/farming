import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';

class EmptyWidget extends StatelessWidget {
  final String text;
  const EmptyWidget({this.text});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        SizedBox(
          height: 100,
          child: Image.asset('assets/list.png'),
        ),
        const SpacingBox(
          height: 3,
        ),
        Text(text ?? 'Danh sách trống', style: ptButton(context).copyWith(color: Colors.black54)),
      ]),
    );
  }
}
