import 'package:flutter/material.dart';

class SearchField extends StatefulWidget {
  final Function(String) onFieldSubmitted;
  final Function(String) onChange;
  final String hint;
  const SearchField({this.onFieldSubmitted, this.onChange, this.hint});

  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController();
    return Center(
      child: Container(
        height: 36,
        padding: const EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(35), color: Colors.grey[100]),
        child: TextFormField(
          onFieldSubmitted: widget.onFieldSubmitted,
          onChanged: widget.onChange,
          controller: controller,
          decoration: InputDecoration(
              hintText: widget.hint ?? 'Tìm kiếm',
              hintStyle: const TextStyle(fontSize: 14),
              contentPadding: const EdgeInsets.only(bottom: 2),
              border: InputBorder.none,
              prefixIcon: const Icon(Icons.search),
              prefixIconConstraints: const BoxConstraints(maxHeight: 18, minWidth: 40),
              suffixIcon: InkWell(
                  onTap: () {
                    controller.clear();
                    if (widget.onChange != null) widget.onChange('');
                    if (widget.onFieldSubmitted != null) widget.onFieldSubmitted('');
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(left: 5, top: 5, right: 5),
                    child: Icon(
                      Icons.close,
                      size: 16,
                    ),
                  ))),
        ),
      ),
    );
  }
}
