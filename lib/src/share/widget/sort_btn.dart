import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class SortBtn extends StatefulWidget {
  final Function(String) onSubmit;
  final List values; // values of sort selection
  const SortBtn(this.onSubmit, this.values);

  @override
  _SortBtnState createState() => _SortBtnState();
}

class _SortBtnState extends State<SortBtn> {
  String selectedValue;
  String currentValue;

  @override
  void initState() {
    if (widget.values.length > 0) {
      currentValue = widget.values[0]["value"].toString();
      selectedValue = widget.values[0]["value"].toString();
      widget.onSubmit(selectedValue);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String selectedSortName;
    if (currentValue != null) {
      final item = widget.values.firstWhere((element) => element["value"].toString() == currentValue);
      if (item != null) {
        selectedSortName = item['name'];
      }
    }
    return GestureDetector(
      onTap: () => _showBotomSheet((val) => setState(() {
            currentValue = val;
          })),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            'Sắp xếp: ',
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
          ),
          Text(selectedSortName ?? '',
              style: const TextStyle(color: AppColor.primary, fontSize: 13.3, fontWeight: FontWeight.bold)),
          const Icon(
            Icons.arrow_drop_down,
            color: Colors.black38,
          ),
        ],
      ),
    );
  }

  _buildItemSelect(String key, String value, Function(String) onChange) {
    return Row(
      children: [
        Radio(
            value: key,
            groupValue: selectedValue,
            onChanged: onChange,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap),
        Text(
          value,
          style: ptCaption(context).copyWith(
              fontSize: 14, color: Colors.black87.withOpacity(0.7), letterSpacing: 0.3, fontWeight: FontWeight.w600),
        )
      ],
    );
  }

  _showBotomSheet(Function setStateCallback) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) => ConstrainedBox(
          constraints: BoxConstraints(maxHeight: deviceHeight(context) - 85),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                height: 50,
                width: deviceWidth(context),
                decoration: const BoxDecoration(
                  color: AppColor.primary,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () => navigatorKey.currentState.maybePop(),
                      child: const Padding(
                        padding: EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    const Text(
                      'Sắp xếp',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                    ),
                    const Spacer(),
                    InkWell(
                      onTap: () {
                        navigatorKey.currentState.maybePop();
                        widget.onSubmit(selectedValue);
                        setStateCallback(selectedValue);
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4), border: Border.all(color: Colors.white)),
                        child: const Text(
                          'Áp dụng',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: Colors.white,
                padding: const EdgeInsets.all(9),
                child: ListView.builder(
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: widget.values.length,
                    itemBuilder: (context, index) {
                      final item = widget.values[index];
                      return _buildItemSelect(item["value"].toString(), item["name"], (val) {
                        setState(() {
                          selectedValue = val;
                        });
                      });
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
