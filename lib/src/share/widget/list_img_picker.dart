// INPUT :
// *********  A LIST OF STRING - IMAGE URL FROM IMGUR

// OUTPUT :
// *********  A FUCNTION THAT HANDLE A LIST OF
// **** CURRENT SELECTED IMAGE URL ****
// WHEN IMAGE URL IS UPLOAD OR REMOVE - IMPLEMENT FROM FATHER WIDGET

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/src/dialog.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/share/utils/color.dart';

import 'image_picker.dart';
import 'image_view.dart';

class ImageRowPicker extends StatefulWidget {
  final List<String> listImg;
  final bool canRemove;
  final Function(List<String>) onUpdateListImg;
  final Function reloadParent;
  const ImageRowPicker(this.listImg, {this.onUpdateListImg, this.canRemove = true, this.reloadParent});
  @override
  _ImageRowPickerState createState() => _ImageRowPickerState();
}

class _ImageRowPickerState extends State<ImageRowPicker> with AutomaticKeepAliveClientMixin {
  @override
  get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView.separated(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      itemCount: (widget.listImg?.length ?? 0) + 1,
      separatorBuilder: (context, index) => const SpacingBox(width: 2),
      itemBuilder: (context, index) {
        if (index != (widget.listImg?.length ?? 0)) {
          return Stack(
            alignment: AlignmentDirectional.bottomStart,
            children: [
              SizedBox(
                height: 60,
                width: 73,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    height: 60,
                    width: 65,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: widget.listImg[index] != "loading"
                          ? ImageView(url: widget.listImg[index], w: 73, h: 60)
                          : kLoadingSpinner,
                    ),
                  ),
                ),
              ),
              if (widget.canRemove != null && widget.canRemove)
                Positioned(
                  top: 0,
                  right: 0,
                  child: InkWell(
                    onTap: () {
                      showConfirmDialog(context, 'Xác nhận xóa hình ảnh này?', navigatorKey: navigatorKey,
                          confirmTap: () async {
                        widget.listImg.removeAt(index);
                        setState(() {});
                        //await navigatorKey.currentState.maybePop();

                        if (widget.onUpdateListImg != null) widget.onUpdateListImg(widget.listImg);
                        if (widget.reloadParent != null) widget.reloadParent();
                      });
                    },
                    child: Container(
                      height: 20,
                      width: 20,
                      decoration: const BoxDecoration(
                        color: AppColor.primary,
                        shape: BoxShape.circle,
                      ),
                      child: const Center(
                          child: Icon(
                        Icons.close,
                        color: Colors.white,
                        size: 16,
                      )),
                    ),
                  ),
                ),
            ],
          );
        }
        if (widget.onUpdateListImg != null)
          return Align(
            alignment: Alignment.bottomCenter,
            child: InkWell(
              onTap: () => imagePicker(context, _onImagePick, _onImagePick),
              child: Container(
                height: 60,
                width: 65,
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(4),
                ),
                child: Image.asset(
                  'assets/add_img.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
          );
        return Container();
      },
    );
  }

  String loadingImage = 'https://www.bis.org/img/uploading.gif';
  _onImagePick(String path) async {
    try {
      setState(() {
        widget.listImg.add("loading");
      });
      File imgFile = File(path);
      String imageUrl = await ImageUtil.upload(imgFile);
      setState(() {
        widget.listImg.remove("loading");
        widget.listImg.add(imageUrl);
      });
      if (widget.reloadParent != null) widget.reloadParent();
    } catch (e) {
      showAlertDialog(context, 'Xảy ra lỗi khi upload ảnh, xin vui lòng thử lại', navigatorKey: navigatorKey);
    } finally {
      widget.listImg.remove(loadingImage);
      setState(() {});
      if (widget.onUpdateListImg != null) widget.onUpdateListImg(widget.listImg);
    }
  }
}
