import 'package:flutter/material.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/share/app_font.dart';
import 'package:mi_smart/src/share/utils/color.dart';

import './search_field.dart';
import 'app_btn.dart';

class AppScafold extends StatelessWidget {
  final Widget child;
  final Widget bottomWidget;
  final Function(String) onSearch;
  final Function onFilter;
  final VoidCallback onQr;
  final String title;
  final Color bgColor;
  final Color appBarColor;
  final Color appBarContentColor;
  final String titleBottom;
  final bool onBack;
  final EdgeInsets padding;
  final Function onBackTap;
  final Function onBottomTap;
  const AppScafold(
      {@required this.child,
      this.title,
      this.onSearch,
      this.titleBottom = '',
      this.bgColor,
      this.bottomWidget,
      this.onQr,
      this.appBarContentColor,
      this.appBarColor,
      this.onFilter,
      this.padding = const EdgeInsets.only(top: 23, left: 23, right: 23),
      this.onBottomTap,
      this.onBack = true,
      this.onAdd,
      this.onBackTap});
  final Function onAdd;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        titleSpacing: 0,
        backgroundColor: appBarColor ?? Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0,
        brightness: Brightness.light,
        leading: onBack && navigatorKey.currentState.canPop()
            ? GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  if (onBackTap == null)
                    Navigator.of(context).pop();
                  else
                    onBackTap();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 5),
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: appBarContentColor ?? Colors.black,
                    size: 20,
                  ),
                ),
              )
            : null,
        title: title != null
            ? Text(title,
                style: TextStyle(fontSize: 18, color: appBarContentColor ?? Colors.black, fontWeight: FontWeight.w500))
            : onSearch != null
                ? Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: SearchField(onFieldSubmitted: onSearch, hint: 'Tìm kiếm ...'),
                  )
                : null,
        actions: [
          onQr != null
              ? GestureDetector(
                  onTap: onQr,
                  child: const Padding(
                    padding: EdgeInsets.only(right: 15),
                    child: Icon(
                      AppFont.actionQr,
                      color: AppColor.primary,
                    ),
                  ),
                )
              : onFilter != null
                  ? GestureDetector(
                      onTap: onFilter,
                      child: const Padding(
                        padding: EdgeInsets.only(right: 15),
                        child: Icon(
                          Icons.filter_list,
                          color: Colors.black54,
                        ),
                      ),
                    )
                  : Container(),
        ],
      ),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Container(
                padding: this.padding,
                constraints: const BoxConstraints.expand(),
                color: bgColor ?? Colors.grey[200],
                child: child,
              ),
            ),
            if (bottomWidget != null) bottomWidget,
            if (onBottomTap != null)
              Container(
                  padding: EdgeInsets.only(
                      left: 22,
                      right: 22,
                      top: 20,
                      bottom: MediaQuery.of(context).padding.bottom == 0 ? 20 : MediaQuery.of(context).padding.bottom),
                  decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.black38,
                        offset: Offset(
                          0, // horizontal
                          -1, // vertical
                        ),
                        blurRadius: 2,
                        spreadRadius: -1)
                  ]),
                  child: AppBtn(
                    titleBottom,
                    onPressed: onBottomTap,
                  ))
          ],
        ),
      ),
      floatingActionButton: onAdd != null
          ? FloatingActionButton(
              elevation: 0,
              mini: true,
              onPressed: () {
                onAdd();
              },
              child: const Icon(
                Icons.add,
                color: Colors.white,
              ),
            )
          : null,
    );
  }
}
