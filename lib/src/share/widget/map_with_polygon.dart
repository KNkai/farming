import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';

class MapWithPolygon extends StatefulWidget {
  final PolygonData polygon;
  final void Function(MapWithPolygonDelegate) onDelegate;
  const MapWithPolygon({@required this.polygon, this.onDelegate});

  @override
  _MapWithPolygonState createState() => _MapWithPolygonState();
}

class _MapWithPolygonState extends State<MapWithPolygon> with MapWithPolygonDelegate {
  MapType _type = MapType.none;
  GoogleMapController _controller;

  @override
  Widget build(Object context) {
    return GoogleMap(
        mapToolbarEnabled: false,
        polygons: {
          Polygon(
              geodesic: true,
              strokeWidth: widget.polygon?.strokeWeight ?? 3,
              strokeColor: widget.polygon?.strokeColor?.withOpacity(widget?.polygon?.strokeOpacity?.toDouble()) ??
                  AppColor.primary,
              fillColor: widget?.polygon?.fillColor?.withOpacity(widget.polygon?.fillOpacity ?? 0.3) ??
                  AppColor.primary.withOpacity(0.3),
              polygonId: PolygonId('1'),
              points: widget?.polygon?.paths)
        },
        myLocationButtonEnabled: false,
        myLocationEnabled: true,
        tiltGesturesEnabled: false,
        mapType: _type,
        onMapCreated: (controller) {
          _controller = controller;
          _callbackDelegate(this);
          Future.delayed(const Duration(milliseconds: 500)).then((_) {
            setState(() {
              _type = MapType.normal;
            });
            _moveCamera(controller, widget.polygon.paths);
          });
        },
        gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
          Factory<OneSequenceGestureRecognizer>(
            () => EagerGestureRecognizer(),
          ),
        ].toSet(),
        initialCameraPosition: const CameraPosition(zoom: 20, target: LatLng(10.774296, 106.696959))); // Dst 1
  }

  void _callbackDelegate(MapWithPolygonDelegate delegate) {
    if (widget.onDelegate != null) {
      widget.onDelegate(delegate);
    }
  }

  void _moveCamera(GoogleMapController controller, List<LatLng> paths) {
    controller.moveCamera(CameraUpdate.newLatLngBounds(bounds(paths), 50));
  }

  @override
  void updatePolygonPath(List<LatLng> newPolygon) {
    _moveCamera(_controller, newPolygon);
  }
}

abstract class MapWithPolygonDelegate {
  void updatePolygonPath(List<LatLng> newPolygon);
}

class PolygonData {
  List<LatLng> paths;
  final Color strokeColor;
  final double strokeOpacity;
  final int strokeWeight;
  final Color fillColor;
  final double fillOpacity;
  PolygonData(
      {@required this.paths,
      @required this.strokeColor,
      @required this.strokeOpacity,
      @required this.strokeWeight,
      @required this.fillColor,
      @required this.fillOpacity});

  factory PolygonData.fromJson(Map<String, dynamic> json) {
    return PolygonData(
        fillOpacity: (json['fillOpacity'] as num)?.toDouble(),
        fillColor: fromHex(json['fillColor'] as String),
        strokeWeight: json['strokeWeight'] as int,
        strokeOpacity: (json['strokeOpacity'] as num)?.toDouble(),
        strokeColor: fromHex((json['strokeColor'] as String)),
        paths: (json['paths'] as List)
            ?.map((e) => e == null ? null : LatLng((e['lat'] as num)?.toDouble(), (e['lng'] as num)?.toDouble()))
            ?.toList());
  }

  PolygonData copyWithNewPolygon(List<LatLng> newPath) {
    return PolygonData(
        fillColor: this.fillColor,
        strokeColor: this.strokeColor,
        strokeOpacity: this.strokeOpacity,
        strokeWeight: this.strokeWeight,
        paths: newPath,
        fillOpacity: this.fillOpacity);
  }
}
