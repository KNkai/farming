import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/utils/color.dart';

enum TextFieldType { Email, Phone, Password, Text }

class TextForm extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final String suffixText;
  final int maxLines;
  final EdgeInsets padding;
  final bool enabled;
  final Widget icon;
  final String initialValue;
  final TextInputType keyboardType;
  final FocusNode focusNode;
  final bool hinden;
  final bool shouldValidate;
  final bool showCursor;
  final Function(String) onChange;
  final TextFieldType type;
  final bool hasBorder;
  final String placeholder;
  final List<TextInputFormatter> inputFormatters;

  const TextForm(
      {this.focusNode,
      this.hint,
      this.padding,
      this.hasBorder = false,
      this.controller,
      this.icon,
      this.showCursor,
      this.enabled = true,
      this.maxLines,
      this.onChange,
      this.suffixText,
      this.inputFormatters,
      this.initialValue,
      this.shouldValidate = true,
      this.type = TextFieldType.Text,
      this.keyboardType,
      this.hinden = false,
      this.placeholder});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: this.padding ?? const EdgeInsets.symmetric(horizontal: 8.0, vertical: 15),
      child: TextFormField(
        onSaved: (text) {},
        maxLines: maxLines,
        validator: (value) {
          if (shouldValidate) {
            if (value.trim() == '') return 'Bắt buộc nhập';
            if (type == TextFieldType.Phone) {
              if (!Validator.isPhoneNumber(value)) return 'Số điện thoại không hợp lệ';
            }
            if (type == TextFieldType.Email) {
              if (!Validator.isEmail(value)) return 'Email không hợp lệ';
            }
            if (type == TextFieldType.Password) {
              if (!Validator.isPassword(value)) return 'Password không đúng';
            }
          }
          return null;
        },
        inputFormatters: inputFormatters,
        controller: controller,
        showCursor: this.showCursor,
        focusNode: focusNode,
        keyboardType: keyboardType ?? TextInputType.text,
        obscureText: hinden,
        onChanged: onChange,
        style: const TextStyle(fontSize: 15),
        initialValue: initialValue,
        enabled: enabled,
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          isDense: true,
          labelText: hint ?? '',
          hintText: placeholder,
          suffixText: suffixText,
          contentPadding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
          labelStyle: const TextStyle(height: -1),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? AppColor.grey : Colors.white,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? AppColor.grey : Colors.white,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? Colors.red : Colors.white,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? Colors.red : Colors.white,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              width: 2,
              style: BorderStyle.solid,
              color: AppColor.primary,
            ),
          ),
        ),
      ),
    );
  }
}

class TextDateForm extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final String suffixText;
  final int maxLines;
  final Widget icon;
  final DateTime initialValue;
  final FocusNode focusNode;
  final bool hinden;
  final bool shouldValidate;
  final Function(DateTime) onChange;
  final bool hasBorder;
  final Function(DateTime) onSelected;
  final String placeholder;
  final Widget suffixWidget;
  const TextDateForm(
      {this.focusNode,
      this.hint,
      this.hasBorder = false,
      this.controller,
      this.icon,
      this.maxLines,
      this.onChange,
      this.suffixText,
      this.initialValue,
      @required this.onSelected,
      this.shouldValidate = true,
      this.hinden = false,
      this.placeholder,
      this.suffixWidget});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: DateTimeField(
        onSaved: (text) {},
        maxLines: maxLines,
        validator: (value) {
          if (shouldValidate) {
            if (value == null) return 'Bắt buộc nhập';
          }
          return null;
        },
        resetIcon: null,
        style: const TextStyle(fontSize: 15),
        format: DateFormat("dd/MM/yyyy"),
        onShowPicker: (context, currentValue) {
          return showDatePicker(
            context: context,
            firstDate: DateTime(2020),
            initialDate: currentValue ?? DateTime.now(),
            lastDate: DateTime(2030),
            builder: (context, child) => Localizations.override(
              context: context,
              locale: const Locale('vi'),
              child: child,
            ),
          ).then((value) => onSelected(value));
        },
        controller: controller,
        focusNode: focusNode,
        obscureText: hinden,
        onChanged: onChange,
        initialValue: initialValue,
        decoration: InputDecoration(
          labelStyle: const TextStyle(height: -1),
          fillColor: Colors.white,
          filled: true,
          isDense: true,
          labelText: hint ?? '',
          hintText: placeholder,
          suffixText: suffixText,
          suffix: suffixWidget,
          contentPadding: const EdgeInsets.all(15),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? AppColor.grey : Colors.white,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? Colors.red : Colors.white,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? Colors.red : Colors.white,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              width: 2,
              style: BorderStyle.solid,
              color: AppColor.primary,
            ),
          ),
        ),
      ),
    );
  }
}

class TextTimeForm extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final String suffixText;
  final int maxLines;
  final Widget icon;
  final DateTime initialValue;
  final FocusNode focusNode;
  final bool hinden;
  final bool shouldValidate;
  final Function(DateTime) onChange;
  final bool hasBorder;
  final Function(DateTime) onSelected;
  const TextTimeForm(
      {this.focusNode,
      this.hint,
      this.hasBorder = false,
      this.controller,
      this.icon,
      this.maxLines,
      this.onChange,
      this.suffixText,
      this.initialValue,
      @required this.onSelected,
      this.shouldValidate = true,
      this.hinden = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: DateTimeField(
        resetIcon: null,
        onSaved: (text) {},
        maxLines: maxLines,
        validator: (value) {
          if (shouldValidate) {
            if (value == null) return 'Bắt buộc nhập';
          }
          return null;
        },
        style: const TextStyle(fontSize: 15),
        format: DateFormat("HH:mm"),
        onShowPicker: (context, currentValue) {
          return showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
            builder: (context, child) => Localizations.override(
              context: context,
              locale: const Locale('vi'),
              child: child,
            ),
          ).then((value) => value == null ? onSelected(null) : onSelected(DateTimeField.convert(value)));
        },
        controller: controller,
        focusNode: focusNode,
        obscureText: hinden,
        onChanged: onChange,
        initialValue: initialValue,
        decoration: InputDecoration(
          labelStyle: const TextStyle(height: -1),
          fillColor: Colors.white,
          filled: true,
          isDense: true,
          labelText: hint ?? '',
          suffixText: suffixText,
          contentPadding: const EdgeInsets.all(15),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? AppColor.grey : Colors.white,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? Colors.red : Colors.white,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              width: 0.8,
              style: BorderStyle.solid,
              color: hasBorder ? Colors.red : Colors.white,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              width: 2,
              style: BorderStyle.solid,
              color: AppColor.primary,
            ),
          ),
        ),
      ),
    );
  }
}
