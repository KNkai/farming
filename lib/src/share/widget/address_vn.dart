import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/src/modules/profile/model/get_district/get_district.graphql.dart';
import 'package:mi_smart/src/modules/profile/model/get_province/get_province.graphql.dart';
import 'package:mi_smart/src/modules/profile/model/get_ward/get_ward.graphql.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class AddressVn extends StatefulWidget {
  final ValueNotifier<AddressVnValues> controller;
  final String defaultDetail;
  final void Function(String detail, Province p, District d, Ward w) onData;
  final bool hasAddressText;

  const AddressVn({this.defaultDetail, this.controller, this.hasAddressText = true, @required this.onData});

  @override
  _AddressVnState createState() => _AddressVnState();
}

class _AddressVnState extends State<AddressVn> {
  _Bloc _bloc;
  TextEditingController _ctl;
  @override
  void initState() {
    _bloc = _Bloc(widget?.controller?.value?.province, widget?.controller?.value?.district,
        widget?.controller?.value?.ward, widget?.controller?.value?.address);
    _bloc.eventInit();
    _ctl = TextEditingController(text: widget.defaultDetail);
    widget.controller?.addListener(_valuesListeners);
    _bloc.stateSide.stream.listen((onData) {
      if (onData is _StateSideAlert) {
        showWarningDialog(context, onData.msg, 'OK', title: 'Thông báo');
      } else if (onData is _StateSideOpenList) {
        _showDialogl1(onData.type, onData.list);
      } else if (onData is _StateSideCallbackData) {
        widget.onData(onData.detail, onData.province, onData.district, onData.ward);
      } else if (onData is _StateSideUpdateDetailView) {
        _ctl.text = onData.detail;
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _bloc.dispose();
    widget.controller?.removeListener(_valuesListeners);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: _bloc.stateLoading,
      builder: (_, ss, ___) {
        if (ss) return const LinearProgressIndicator();

        return Column(
          children: <Widget>[
            if (widget.hasAddressText)
              Row(
                children: <Widget>[
                  const Text('Địa chỉ:'),
                  const SizedBox(width: 5),
                  Expanded(
                    child: TextFormField(
                      controller: _ctl,
                      onChanged: (value) {
                        _bloc.eventUpdateDetail(value);
                      },
                      decoration: const InputDecoration(border: InputBorder.none),
                    ),
                  )
                ],
              ),
            Row(
              children: <Widget>[
                const Text('Tỉnh: '),
                ValueListenableBuilder<Province>(
                    valueListenable: _bloc.stateSelectProvine,
                    builder: (_, ss, __) {
                      if (ss?.name == null) return const Text('Chưa chọn');
                      return Text(ss.name);
                    }),
                const Spacer(),
                IconButton(
                  color: AppColor.primary,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  onPressed: () {
                    _bloc.eventOpenProvines();
                  },
                ),
              ],
            ),
            Row(
              children: <Widget>[
                const Text('Quận: '),
                ValueListenableBuilder<District>(
                    valueListenable: _bloc.stateSelectDistrict,
                    builder: (_, ss, __) {
                      if (ss?.name == null) return const Text('Chưa chọn');
                      return Text(ss.name);
                    }),
                const Spacer(),
                IconButton(
                  color: AppColor.primary,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  onPressed: () {
                    _bloc.eventOpenDistricts();
                  },
                ),
              ],
            ),
            Row(
              children: <Widget>[
                const Text(
                  'Phường: ',
                ),
                ValueListenableBuilder<Ward>(
                    valueListenable: _bloc.stateSelectWard,
                    builder: (_, ss, __) {
                      if (ss?.name == null) return const Text('Chưa chọn');
                      return Text(ss.name);
                    }),
                const Spacer(),
                IconButton(
                  color: AppColor.primary,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  onPressed: () {
                    _bloc.eventOpenWards();
                  },
                ),
              ],
            )
          ],
        );
      },
    );
  }

  String _textType(OpenType type) {
    switch (type) {
      case OpenType.provine:
        return 'Chọn tỉnh';
      case OpenType.district:
        return 'Chọn quận';
      case OpenType.ward:
        return 'Chọn phường';
    }

    return '';
  }

  void _valuesListeners() {
    final fullAddress = widget.controller.value;
    _bloc.eventInitAgain(fullAddress.province, fullAddress.district, fullAddress.ward, fullAddress.address);
  }

  void _showDialogl1(OpenType type, List<dynamic> list) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          title: new Text(_textType(type)),
          content: SizedBox(
            height: MediaQuery.of(context).size.height * 0.7,
            width: MediaQuery.of(context).size.width * 0.7,
            child: ListView.builder(
                itemCount: list.length,
                itemBuilder: (_, index) {
                  return ListTile(
                    onTap: () {
                      if (type == OpenType.provine) {
                        _bloc.eventSelectProvine(list[index]);
                      } else if (type == OpenType.district) {
                        _bloc.eventSelectDistrict(list[index]);
                      } else {
                        _bloc.eventSelectWard(list[index]);
                      }
                      Navigator.pop(context);
                    },
                    title: Text(
                      list[index].name,
                    ),
                  );
                }),
          ),
        );
      },
    );
  }

  void showWarningDialog(BuildContext context, String content, String cancelText,
      {String title, Function onCloseDialog, String onActionText, Function onTapAction}) {
    Widget alert;

    Widget _titleWidget({TextAlign textAlign = TextAlign.start}) => title != null
        ? Text(
            title,
            textAlign: textAlign,
          )
        : null;
    if (content != null && content.isNotEmpty) {
      if (Platform.isAndroid) {
        // If it has plenty of buttons, it will stack them on vertical way.
        // If title isn't supplied, height of this alert will smaller than the one has title.
        alert = AlertDialog(
          title: _titleWidget(),
          content: Text(
            content,
            textAlign: TextAlign.start,
          ),
          actions: [
            FlatButton(
              child: Text(cancelText),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            onActionText != null && onTapAction != null
                ? FlatButton(
                    child: Text(onActionText),
                    onPressed: () {
                      onTapAction();
                    },
                  )
                : const SizedBox.shrink()
          ],
        );
      } else {
        // Almost similiar with Cupertino style.
        // If title isn't supplied, height of this alert will smaller than the one has title.
        var listAction = <Widget>[];
        listAction.add(CupertinoDialogAction(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            cancelText,
          ),
        ));
        if (onActionText != null && onTapAction != null)
          listAction.add(CupertinoDialogAction(
            onPressed: () {
              onTapAction();
            },
            child: Text(
              onActionText,
            ),
          ));

        alert = CupertinoAlertDialog(
          title: Padding(
            padding: const EdgeInsets.only(
              bottom: 10,
            ),
            child: _titleWidget(textAlign: TextAlign.center),
          ),
          content: Text(
            content,
            textAlign: TextAlign.start,
          ),
          actions: listAction,
        );
      }

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      ).then((value) {
        if (onCloseDialog != null) {
          onCloseDialog();
        }
      });
    }
  }
}

class _Bloc {
  final stateLoading = ValueNotifier<bool>(false);
  final stateNumber = ValueNotifier<String>('');
  final stateSide = StreamController<_StateSide>();
  final ValueNotifier<Province> stateSelectProvine;
  final ValueNotifier<District> stateSelectDistrict;
  final ValueNotifier<Ward> stateSelectWard;
  final _repo = ProvinceVnRepo();

  List<Province> _listProvince;
  List<District> _listDistricts;
  List<Ward> _listWard;
  String _detail;

  _Bloc(Province defaultProvince, District defaultDistrict, Ward defaultWard, String detail)
      : this.stateSelectProvine = ValueNotifier<Province>(defaultProvince),
        this.stateSelectDistrict = ValueNotifier<District>(defaultDistrict),
        this._detail = detail,
        this.stateSelectWard = ValueNotifier<Ward>(defaultWard);

  void eventInitAgain(Province defaultProvince, District defaultDistrict, Ward defaultWard, String detail) {
    stateSelectProvine.value = defaultProvince;
    stateSelectDistrict.value = defaultDistrict;
    stateSelectWard.value = defaultWard;
    stateSide.add(_StateSideUpdateDetailView(detail));
    _init();
  }

  void eventInit() async {
    stateSide.add(_StateSideUpdateDetailView(_detail));
    _init();
  }

  void eventUpdateDetail(String detail) {
    _detail = detail;
    _callbackData();
  }

  void _init() async {
    stateLoading.value = true;
    try {
      final provinces = await _repo.getAllProvince();
      _listProvince = (provinces);

      if (stateSelectProvine.value != null) {
        for (int c = 0; c < provinces.length; c++) {
          if (provinces[c].name == stateSelectProvine.value.name) {
            _listDistricts = await _repo.getDistrictByProvince(provinces[c].id);
            break;
          }
        }
      }

      if (stateSelectDistrict.value?.name != null && _listDistricts != null && _listDistricts.isNotEmpty) {
        for (int c = 0; c < _listDistricts.length; c++) {
          if (_listDistricts[c].name == stateSelectDistrict.value.name) {
            _listWard = await _repo.getWardByDistrict(_listDistricts[c].id);
            break;
          }
        }
      }
    } finally {
      stateLoading.value = false;
    }
  }

  void _callbackData() {
    stateSide.add(
        _StateSideCallbackData(_detail, stateSelectProvine.value, stateSelectDistrict.value, stateSelectWard.value));
  }

  void eventOpenProvines() {
    _openList(_listProvince, OpenType.provine);
  }

  void eventOpenDistricts() {
    _openList(_listDistricts, OpenType.district);
  }

  void eventOpenWards() {
    _openList(_listWard, OpenType.ward);
  }

  void _openList(List<dynamic> list, OpenType type) {
    if (list == null || list.isEmpty) {
      if (type == OpenType.provine)
        stateSide.add(_StateSideAlert('Dữ liệu trống'));
      else if (type == OpenType.district)
        stateSide.add(_StateSideAlert('Hãy chọn tỉnh trước'));
      else
        stateSide.add(_StateSideAlert('Hãy chọn quận trước'));
    } else {
      stateSide.add(_StateSideOpenList(list, type));
    }
  }

  void eventSelectProvine(Province province) {
    stateSelectProvine.value = province;

    _listDistricts = null;
    _listWard = null;
    stateSelectDistrict.value = null;
    stateSelectWard.value = null;
    _callbackData();

    _repo.getDistrictByProvince(province.id).then((onValue) {
      _listDistricts = (onValue);
    });
  }

  void eventSelectDistrict(District district) {
    stateSelectDistrict.value = district;
    _listWard = null;
    stateSelectWard.value = null;
    _callbackData();

    _repo.getWardByDistrict(district.id).then((onValue) {
      _listWard = (onValue);
    });
  }

  void eventSelectWard(Ward ward) {
    stateSelectWard.value = ward;
    _callbackData();
  }

  dispose() {
    stateSelectProvine.dispose();
    stateSide.close();
    stateLoading.dispose();
    stateNumber.dispose();
  }
}

enum OpenType { provine, district, ward }

class _StateSide {}

class _StateSideAlert extends _StateSide {
  final String msg;
  _StateSideAlert(this.msg);
}

class _StateSideOpenList extends _StateSide {
  final List<dynamic> list;
  final OpenType type;
  _StateSideOpenList(this.list, this.type);
}

class _StateSideUpdateDetailView extends _StateSide {
  final String detail;
  _StateSideUpdateDetailView(this.detail);
}

class _StateSideCallbackData extends _StateSide {
  final String detail;
  final Province province;
  final District district;
  final Ward ward;
  _StateSideCallbackData(this.detail, this.province, this.district, this.ward);
}

class ProvinceVnRepo {
  Future<Province> getProvince(String provinceId) async {
    return BaseApi.instance.rawBodyRawDataExecute('''
    query {
        getAllAddress(q:{filter:{provinceId:"$provinceId"},limit:1}) {
          data { province provinceId }
        }
    }
    ''').then((value) {
      var res = value.data["getAllAddress"]["data"][0];
      return Province(id: res["provinceId"], name: res["province"]);
    });
  }

  Future<District> getDistrict(String districtId) async {
    return BaseApi.instance.rawBodyRawDataExecute('''
    query {
        getAllAddress(q:{filter:{districtId:"$districtId"},limit:1}) {
          data { district districtId }
        }
    }
    ''').then((value) {
      var res = value.data["getAllAddress"]["data"][0];
      return District(id: res["districtId"], name: res["district"]);
    });
  }

  Future<Ward> getWard(String wardId) async {
    return BaseApi.instance.rawBodyRawDataExecute('''
    query {
        getAllAddress(q:{filter:{wardId:"$wardId"},limit:1}) {
          data { ward wardId }
        }
    }
    ''').then((value) {
      var res = value.data["getAllAddress"]["data"][0];
      return Ward(id: res["wardId"], name: res["ward"]);
    });
  }

  Future<List<Province>> getAllProvince() async {
    return BaseApi.instance.rawBodyRawDataExecute('''
    query getProvince {
        getProvince {
            id
            province
        }
    }
    ''').then((value) {
      return GetProvince$Query.fromJson(value.data).getProvince;
    });
  }

  Future<List<District>> getDistrictByProvince(String pId) async {
    return BaseApi.instance.rawBodyRawDataExecute('''
query getDistrict(\$p: String!) {
    getDistrict(provinceId: \$p) {
        id
        district
    }
}
    ''', variables: {'p': pId}).then((value) {
      return GetDistrict$Query.fromJson(value.data).getDistrict;
    });
  }

  Future<List<Ward>> getWardByDistrict(String dId) async {
    return BaseApi.instance.rawBodyRawDataExecute('''
query getWard(\$d: String!) {
    getWard(districtId: \$d) {
        id
        ward
    }
}
    ''', variables: {'d': dId}).then((value) {
      return GetWard$Query.fromJson(value.data).getWard;
    });
  }
}

class AddressVnValues {
  final String address;
  final Province province;
  final District district;
  final Ward ward;
  AddressVnValues(this.address, this.province, this.district, this.ward);
}
