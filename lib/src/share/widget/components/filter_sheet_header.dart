import 'package:flutter/material.dart';

import '../../../../base_config/src/utils/constants.dart';
import '../../../../navigator.dart';
import '../../utils/color.dart';

class FilterSheetHeader extends StatelessWidget {
  final Function onSubmit;
  const FilterSheetHeader({Key key, this.onSubmit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      height: 50,
      width: deviceWidth(context),
      decoration: const BoxDecoration(
        color: AppColor.primary,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          InkWell(
            onTap: () => navigatorKey.currentState.maybePop(),
            child: const Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
              child: Icon(
                Icons.close,
                color: Colors.white,
              ),
            ),
          ),
          const Text(
            'Bộ lọc',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
          ),
          const Spacer(),
          InkWell(
            onTap: () {
              this.onSubmit();
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(4), border: Border.all(color: Colors.white)),
              child: const Text(
                'Áp dụng',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
