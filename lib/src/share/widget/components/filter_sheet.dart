import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../../../base_config/src/utils/constants.dart';
import '../../../../navigator.dart';
import 'check_box_item.dart';
import 'filter_sheet_header.dart';

class FilterSheet extends StatefulWidget {
  final List<FilterGroup> groups;
  final Function onSubmit;
  const FilterSheet({Key key, this.groups, this.onSubmit}) : super(key: key);

  @override
  _FilterSheetState createState() => _FilterSheetState();
}

class FilterGroup {
  String label;
  List<FilterItem> items;
  int colCount;
  FilterGroup({this.label, this.items, this.colCount});
  List<dynamic> selectedValues() {
    return this.items.where((e) => e.selected).map((e) => e.value).toList();
  }
}

class FilterItem {
  String label;
  dynamic value;
  bool selected;
  FilterItem({this.label, this.value, this.selected});
}

class _FilterSheetState extends State<FilterSheet> {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: deviceHeight(context) - 85),
      child: Container(
        color: Colors.white,
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 65, left: 15, bottom: 15, right: 15),
                child: Column(
                  children: List.generate(
                    widget.groups.length,
                    (index) => buildFilterGroup(widget.groups[index]),
                  ),
                ),
              ),
            ),
            FilterSheetHeader(onSubmit: () {
              navigatorKey.currentState.maybePop();
              widget.onSubmit(widget.groups);
            }),
          ],
        ),
      ),
    );
  }

  Column buildFilterGroup(FilterGroup group) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            group.label ?? '',
            style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
        ),
        StaggeredGridView.count(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: group.colCount,
          staggeredTiles: group.items.map((_) => const StaggeredTile.fit(1)).toList(),
          children: List.generate(group.items.length, (index) {
            FilterItem e = group.items[index];
            return CheckBoxItem(
              label: e.label,
              value: e.selected,
              onChanged: (value) {
                setState(() {
                  e.selected = value;
                });
              },
            );
          }),
        ),
      ],
    );
  }
}
