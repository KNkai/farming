import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';

class CheckBoxItem extends StatelessWidget {
  final bool value;
  final String label;
  final Function onChanged;
  const CheckBoxItem({Key key, this.value, this.label, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Checkbox(
          value: value,
          onChanged: onChanged,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        ),
        Expanded(
          child: Text(
            label,
            style: ptCaption(context).copyWith(
              fontSize: 14,
              color: Colors.black87.withOpacity(0.7),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ],
    );
  }
}
