import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:permission_handler/permission_handler.dart';

void imagePicker(BuildContext context, Function(String path) onCameraPick, Function(String path) onGalleryPick,
    {String title}) {
  showModalBottomSheet(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      elevation: 0,
      context: context,
      builder: (_) {
        return Material(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 4),
            width: double.infinity,
            height: 150,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title ?? 'Chọn ảnh từ điện thoại',
                  style: const TextStyle(fontWeight: FontWeight.bold, color: Color(0xff696969)),
                ),
                const SizedBox(
                  height: 12,
                ),
                const Divider(height: 1, color: Colors.grey),
                const SizedBox(
                  height: 16,
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    onCustomPersionRequest(
                        permission: Permission.camera,
                        onGranted: () {
                          // close showModalBottomSheet
                          Navigator.of(context).pop();
                          ImagePicker().getImage(source: ImageSource.camera).then((value) {
                            onGalleryPick(value.path);
                          });
                        });
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: const [
                      Icon(
                        Icons.add_a_photo,
                        color: Color(0xff696969),
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        'Camera',
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    onCustomPersionRequest(
                        permission: Permission.photos,
                        onGranted: () {
                          // close showModalBottomSheet
                          Navigator.of(context).pop();

                          ImagePicker().getImage(source: ImageSource.gallery).then((value) {
                            onCameraPick(value.path);
                          });
                        });
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: const [
                      Icon(
                        Icons.apps,
                        color: Color(0xff696969),
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        'Thư viện',
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                )
              ],
            ),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topRight: Radius.circular(8), topLeft: Radius.circular(8))),
            padding: const EdgeInsets.all(15),
          ),
        );
      });
}
