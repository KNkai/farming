import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/utils/image_proxy.dart';
import 'package:mi_smart/navigator.dart';
import 'package:photo_view/photo_view.dart';

class ImageView extends StatelessWidget {
  final String url;
  final String tag;
  final int w, h;
  const ImageView({@required this.url, this.tag, this.w, this.h});
  @override
  Widget build(BuildContext context) {
    String genTag = tag ?? url + Random().nextInt(10000000).toString();
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (_) {
          return DetailImageScreen(
            url,
            tag: genTag,
            scaleW: w,
            scaleH: h,
          );
        }));
      },
      child: Hero(
          tag: genTag,
          child: Image.network(
            (w != null && h != null) ? resizeImageLink(url, w: w, h: h) : url,
            fit: BoxFit.cover,
            loadingBuilder: kLoadingBuilder,
          )),
    );
  }
}

class DetailImageScreen extends StatelessWidget {
  final String url;
  final String tag;
  final int scaleW, scaleH;
  const DetailImageScreen(this.url, {this.tag, this.scaleW, this.scaleH});

  @override
  Widget build(BuildContext context) {
    String genTag = url + Random().nextInt(10000000).toString();
    return Scaffold(
      body: Stack(fit: StackFit.expand, children: [
        Center(
          child: Hero(
            tag: tag ?? genTag,
            child: PhotoView(
              backgroundDecoration: const BoxDecoration(color: Colors.black87),
              imageProvider: NetworkImage(
                url,
              ),
              loadingBuilder: (context, event) => PhotoView(
                backgroundDecoration: const BoxDecoration(color: Colors.black87),
                imageProvider: NetworkImage(
                  resizeImageLink(url, w: scaleW ?? deviceWidth(context), h: scaleH),
                ),
                loadingBuilder: (context, event) => Center(
                  child: kLoadingSpinner,
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: 50,
          right: 10,
          child: InkWell(
            onTap: () => navigatorKey.currentState.maybePop(),
            child: Container(
              decoration: BoxDecoration(color: Colors.white24, borderRadius: BorderRadius.circular(20)),
              width: 40,
              height: 40,
              child: const Icon(Icons.close),
            ),
          ),
        ),
      ]),
    );
  }
}
