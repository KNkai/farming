import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppForm extends StatelessWidget {
  final Widget rawChild;
  final Widget child;
  final String title;
  final EdgeInsets titlePadding;
  final EdgeInsets padding;
  final Function onTap;
  final Color colorBg;

  static double fontSizeContentRecommend = 20;

  const AppForm(
      {this.title,
      this.onTap,
      this.titlePadding,
      this.child,
      this.rawChild,
      this.padding,
      this.colorBg});

  @override
  Widget build(BuildContext context) {
    if (this.title != null && this.title.isNotEmpty)
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: titlePadding ?? const EdgeInsets.all(0),
            child: Text(
              this.title,
              style: const TextStyle(color: Color(0xff707070)),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(onTap: onTap, child: _whiteWrap())
        ],
      );

    return InkWell(onTap: onTap, child: _whiteWrap());
  }

  Widget _whiteWrap() => this.child != null
      ? Container(
          padding:
              padding ?? const EdgeInsets.symmetric(vertical: 12, horizontal: 15),
          decoration: BoxDecoration(
              color: colorBg ?? Colors.white,
              borderRadius: const BorderRadius.all(Radius.circular(9))),
          child: child,
        )
      : rawChild ?? const SizedBox.shrink();
}

class AppFormIcon extends StatelessWidget {
  final EdgeInsets padding;
  final Widget trailingHint;
  final String title;
  final bool isExpandChild;
  final Widget child;
  final Function onTap;
  const AppFormIcon(this.child,
      {this.title,
      this.trailingHint,
      this.padding,
      this.isExpandChild = true,
      this.onTap});

  Widget build(BuildContext context) {
    return AppForm(
      padding: padding,
      onTap: onTap,
      title: title,
      child: trailingHint == null
          ? child
          : Row(
              children: <Widget>[
                isExpandChild
                    ? Expanded(
                        child: child,
                      )
                    : child,
                trailingHint,
              ],
            ),
    );
  }
}
