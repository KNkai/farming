import 'package:flutter/material.dart';

const String _kFontFamily = 'mismart_icon_font';

class AppFont {
  AppFont._();

  static const IconData inputAction =
      IconData(0xe900, fontFamily: _kFontFamily);

  static const IconData inventory = IconData(0xe901, fontFamily: _kFontFamily);

  static const IconData showDiary = IconData(0xe902, fontFamily: _kFontFamily);

  static const IconData propersalData =
      IconData(0xe903, fontFamily: _kFontFamily);

  static const IconData actionQr = IconData(0xe904, fontFamily: _kFontFamily);
}
