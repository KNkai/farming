import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';

import 'select_list_sheet.dart';

class SelectListInput extends StatefulWidget {
  const SelectListInput({
    Key key,
    @required this.labelText,
    @required this.sheetItems,
    this.sheetTitle,
    this.hintText,
    this.onSaved,
    this.onSelect,
    this.controller,
    this.selectedId,
    this.disabled = false,
    this.validator,
  }) : super(key: key);
  final String sheetTitle, labelText, hintText;
  final dynamic sheetItems;
  final Function(SelectListSheetItem) onSelect;
  final Function(dynamic) onSaved;
  final TextEditingController controller;
  final dynamic selectedId;
  final bool disabled;
  final Function(String) validator;

  @override
  _SelectListInputState createState() => _SelectListInputState();
}

class _SelectListInputState extends State<SelectListInput> {
  TextEditingController controller;
  SelectListSheetItem selectedItem;
  @override
  void initState() {
    super.initState();
    controller = widget.controller ?? new TextEditingController();
    if (widget.selectedId != null) {
      if (widget.sheetItems is List<SelectListSheetItem>) {
        setSelectedItem(widget.sheetItems);
      } else {
        (widget.sheetItems as Future<List<SelectListSheetItem>>).then((value) {
          setSelectedItem(value);
          return value;
        });
      }
    }
  }

  void setSelectedItem(List<SelectListSheetItem> items) {
    selectedItem = items.firstWhere((e) => e.id == widget.selectedId, orElse: () => null);
    controller.text = selectedItem?.display ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: widget.disabled
            ? null
            : () {
                showModalBottomSheet(
                  context: context,
                  builder: (context) {
                    return widget.sheetItems is List<SelectListSheetItem>
                        ? buildSelectListSheet(widget.sheetItems)
                        : FutureBuilder<List<SelectListSheetItem>>(
                            initialData: const [],
                            future: widget.sheetItems,
                            builder: (_, snapshot) {
                              if (snapshot.hasData) {
                                return buildSelectListSheet(snapshot.data);
                              } else {
                                return Center(child: kLoadingSpinner);
                              }
                            },
                          );
                  },
                ).then((value) => FocusScope.of(context).requestFocus(FocusNode()));
              },
        child: IgnorePointer(
          child: TextFormField(
            style: const TextStyle(fontSize: 16),
            // enabled: true,
            onSaved: (value) {
              if (widget.onSaved != null && this.selectedItem != null) widget.onSaved(this.selectedItem.id);
            },
            validator: widget.validator != null
                ? (value) {
                    String errMsg = widget.validator(value);
                    if (errMsg != null) {
                      Scrollable.ensureVisible(context, duration: const Duration(milliseconds: 800));
                      return errMsg;
                    }
                    return null;
                  }
                : (value) => null,
            controller: controller,
            showCursor: false,
            decoration: kDefaultInputDecoration.copyWith(
              fillColor: widget.disabled ? Colors.grey[100] : Colors.white,
              labelText: widget.labelText,
              hintText: widget.hintText,
              suffixIcon: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: SvgPicture.asset('assets/icons/arrow_down.svg'),
              ),
            ),
          ),
        ),
      ),
    );
  }

  SelectListSheet buildSelectListSheet(List<SelectListSheetItem> items) {
    return SelectListSheet(
      title: widget.sheetTitle ?? widget.hintText ?? widget.labelText,
      items: items,
      onSelect: onSelectedSheetItem,
    );
  }

  onSelectedSheetItem(e) {
    selectedItem = e;
    controller.text = e.display;
    if (widget.onSelect != null) widget.onSelect(e);
  }
}
