import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class AnimatedDot extends StatelessWidget {
  const AnimatedDot({
    Key key,
    @required this.actived,
  }) : super(key: key);

  final bool actived;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: actived ? 16 : 8,
      height: 8,
      margin: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 5.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: actived ? AppColor.primary : const Color.fromRGBO(0, 0, 0, 0.2),
      ),
    );
  }
}
