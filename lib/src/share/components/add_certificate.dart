import 'package:flutter/material.dart';
import 'package:mi_smart/src/share/widget/list_img_picker.dart';
import 'package:mi_smart/src/share/widget/text_form.dart';

import '../../../base_config/base_config.dart';
import '../../../base_config/src/utils/validate_builder.dart';
import '../../../base_widget/src/spacing_box.dart';
import 'text_input.dart';

class CertificateData {
  String title, description;
  List<String> listImgs;
  DateTime effectiveDate;
  DateTime expiredDate;

  CertificateData({this.title, this.description, this.listImgs, this.effectiveDate, this.expiredDate});
}

class AddCertificateSheet extends StatefulWidget {
  final Function(CertificateData) onAssign;
  const AddCertificateSheet({
    Key key,
    @required this.onAssign,
  }) : super(key: key);

  @override
  _AddCertificateSheetState createState() => _AddCertificateSheetState();
}

class _AddCertificateSheetState extends State<AddCertificateSheet> {
  GlobalKey<FormState> addCertificateForm = new GlobalKey(debugLabel: "addCertificateForm");
  CertificateData certificateData;
  TextEditingController fromDateC = TextEditingController();
  TextEditingController toDateC = TextEditingController();
  List<String> imgs;

  @override
  void initState() {
    super.initState();
    this.certificateData = CertificateData(title: null, description: null);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 2 + MediaQuery.of(context).viewInsets.bottom,
      child: Scaffold(
        appBar: buildSheetBar(context, title: "Thêm chứng nhận", btnTitle: "Tạo", onPress: addCertificate),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: addCertificateForm,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextInput(
                    labelText: "Tên chứng nhận",
                    hintText: "Nhập tên",
                    onSaved: (value) => certificateData.title = value,
                    validator: ValidateBuilder().empty().build(),
                  ),
                  const SpacingBox(
                    height: 2,
                  ),
                  TextInput(
                    labelText: "Mô tả",
                    hintText: "Nhập mô tả",
                    onSaved: (value) => certificateData.description = value,
                    validator: ValidateBuilder().empty().build(),
                  ),
                  const SpacingBox(
                    height: 2,
                  ),
                  SizedBox(
                    height: 70,
                    child: ImageRowPicker(
                      const [],
                      onUpdateListImg: (list) {
                        imgs = list;
                      },
                    ),
                  ),
                  const SpacingBox(
                    height: 2,
                  ),
                  Row(children: [
                    Expanded(
                        child: TextDateForm(
                      onSelected: (date) {
                        certificateData.effectiveDate = date;
                        fromDateC.text = Formart.formatToDate(date.toString());
                      },
                      hint: 'Từ ngày',
                      hasBorder: true,
                      controller: fromDateC,
                    )),
                    const SpacingBox(width: 3),
                    Expanded(
                        child: TextDateForm(
                      hint: 'Đến ngày',
                      onSelected: (date) {
                        certificateData.expiredDate = date;
                        toDateC.text = Formart.formatToDate(date.toString());
                      },
                      hasBorder: true,
                      controller: toDateC,
                    ))
                  ]),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void addCertificate() {
    if (addCertificateForm.currentState.validate()) {
      addCertificateForm.currentState.save();
      widget.onAssign(certificateData);
      Navigator.of(context).pop();
    }
  }
}
