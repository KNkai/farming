import 'package:flutter/material.dart';
import 'package:mi_smart/src/model/disease_situation.model.dart';
import 'package:readmore/readmore.dart';

import '../../../base_widget/src/spacing_box.dart';
import '../../modules/disease/components/situation_footer.dart';
import '../../modules/disease/components/situation_header.dart';
import '../../modules/disease/components/situation_list_image.dart';

class SituationCard extends StatefulWidget {
  final DiseaseSituation situation;
  final Function onTap;
  final Function onUpdate, onUpdateStatus, onDelete;

  const SituationCard(
      {Key key, @required this.situation, this.onTap, this.onUpdate, this.onUpdateStatus, this.onDelete})
      : super(key: key);

  @override
  _SituationCardState createState() => _SituationCardState();
}

class _SituationCardState extends State<SituationCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SituationHeader(
              situation: widget.situation,
              onUpdate: widget.onUpdate,
              onUpdateStatus: widget.onUpdateStatus,
              onDelete: widget.onDelete,
            ),
            const SpacingBox(height: 1),
            ReadMoreText(
              widget.situation.title,
              trimLines: 5,
              colorClickableText: Colors.grey,
              trimMode: TrimMode.Line,
              trimCollapsedText: '... Xem Thêm',
              trimExpandedText: ' Rút gọn',
              style: TextStyle(color: Colors.black87.withOpacity(0.8), fontWeight: FontWeight.w600, fontSize: 14),
            ),
            Text(
              [widget.situation.regionName, widget.situation.ward, widget.situation.district, widget.situation.province]
                  .join(', '),
              style: const TextStyle(fontStyle: FontStyle.italic, fontSize: 13),
            ),
            const SpacingBox(height: 2),
            if (widget.situation.listImages.length > 0) ...[
              SituationListImage(images: widget.situation.listImages),
              const SpacingBox(height: 3),
            ],
            SituationFooter(
              status: widget.situation.status,
              commentCount: widget.situation.commentCount,
              diseaseName: widget.situation.disease.name,
              //nameLabel: widget.situation.regionName,
            )
          ],
        ),
      ),
    );
  }
}
