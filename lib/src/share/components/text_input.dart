import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';

class TextInput extends StatelessWidget {
  const TextInput({
    Key key,
    @required this.labelText,
    this.hintText,
    this.value,
    this.suffix,
    this.onSaved,
    this.validator,
    this.keyboardType,
    this.autofocus = false,
    this.inputFormatters,
    this.onChanged,
    this.enabled = true,
  }) : super(key: key);
  final String labelText, hintText;
  final String value;
  final Widget suffix;
  final Function(String) onSaved, onChanged;
  final Function(String) validator;
  final TextInputType keyboardType;
  final bool autofocus, enabled;
  final List<TextInputFormatter> inputFormatters;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: TextFormField(
        style: const TextStyle(fontSize: 16),
        onSaved: onSaved,
        initialValue: value,
        keyboardType: keyboardType,
        autofocus: autofocus,
        inputFormatters: inputFormatters,
        onChanged: onChanged,
        enabled: enabled,
        decoration: kDefaultInputDecoration.copyWith(
          labelText: labelText,
          hintText: hintText,
          suffix: suffix,
          // isDense: true,
        ),
        validator: validator != null
            ? (value) {
                String errMsg = validator(value);
                if (errMsg != null) {
                  Scrollable.ensureVisible(context, duration: const Duration(milliseconds: 800));
                  return errMsg;
                }
                return null;
              }
            : (value) => null,
      ),
    );
  }
}
