import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/share/widget/address_vn.dart';

import 'select_input.dart';
import 'select_list_sheet.dart';

class AddressChange {
  String provinceId;
  String districtId;
  String wardId;
  AddressChange({this.provinceId, this.districtId, this.wardId});
}

class AddressInput extends StatefulWidget {
  const AddressInput({
    Key key,
    this.provinceId,
    this.districtId,
    this.wardId,
    this.onChange,
    this.onProvinceSaved,
    this.onDistrictSaved,
    this.onWardSaved,
    this.provinceValidator,
    this.districtValidator,
    this.wardValidator,
  }) : super(key: key);
  final String provinceId, districtId, wardId;
  final Function(AddressChange) onChange;
  final Function onProvinceSaved, onDistrictSaved, onWardSaved;
  final Function(String) provinceValidator, districtValidator, wardValidator;
  @override
  _AddressInputState createState() => _AddressInputState();
}

class _AddressInputState extends State<AddressInput> {
  String provinceId, districtId, wardId;
  TextEditingController provinceController = new TextEditingController();
  TextEditingController districtController = new TextEditingController();
  TextEditingController wardController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    provinceId = widget.provinceId;
    districtId = widget.districtId;
    wardId = widget.wardId;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildProvinceField(context),
        const SpacingBox(height: 1),
        buildDistrictField(context),
        const SpacingBox(height: 1),
        buildWardField(context)
      ],
    );
  }

  SelectListInput buildProvinceField(BuildContext context) {
    return SelectListInput(
      sheetTitle: "Chọn tỉnh/thành",
      sheetItems: ProvinceVnRepo().getAllProvince().then((items) {
        return items.map((e) => SelectListSheetItem(display: e.name, id: e.id)).toList();
      }),
      labelText: "Tỉnh/thành",
      hintText: "Chọn tỉnh/thành",
      controller: provinceController,
      selectedId: provinceId,
      validator: widget.provinceValidator ?? (value) => null,
      onSaved: (value) => widget.onProvinceSaved(provinceId),
      onSelect: (item) {
        setState(() {
          provinceId = item.id;
          districtId = null;
          wardId = null;
          districtController.text = '';
          wardController.text = '';
          if (widget.onChange != null) {
            widget.onChange(buildAddressChange());
          }
        });
      },
    );
  }

  SelectListInput buildDistrictField(BuildContext context) {
    // print("buildDistrictField $districtId");
    return SelectListInput(
      sheetTitle: "Chọn quận/huyện",
      disabled: provinceId == null,
      sheetItems: provinceId != null
          ? ProvinceVnRepo().getDistrictByProvince(provinceId).then((items) {
              return items.map((e) => SelectListSheetItem(display: e.name, id: e.id)).toList();
            })
          : [],
      labelText: "Quận/huyện",
      hintText: "Chọn quận/huyện",
      controller: districtController,
      selectedId: districtId,
      onSaved: (value) => widget.onDistrictSaved(districtId),
      validator: widget.districtValidator ?? (value) => null,
      onSelect: (item) {
        setState(() {
          districtId = item.id;
          wardId = null;
          if (widget.onChange != null) {
            widget.onChange(buildAddressChange());
          }
          wardController.text = '';
        });
      },
    );
  }

  SelectListInput buildWardField(BuildContext context) {
    return SelectListInput(
      sheetTitle: "Chọn phường/xã",
      disabled: districtId == null,
      sheetItems: districtId != null
          ? ProvinceVnRepo().getWardByDistrict(districtId).then((items) {
              return items.map((e) => SelectListSheetItem(display: e.name, id: e.id)).toList();
            })
          : [],
      labelText: "Phường/xã",
      hintText: "Chọn phường/xã",
      controller: wardController,
      selectedId: wardId,
      validator: widget.wardValidator ?? (value) => null,
      onSaved: (value) => widget.onWardSaved(wardId),
      onSelect: (item) {
        setState(() {
          wardId = item.id;
          if (widget.onChange != null) {
            widget.onChange(buildAddressChange());
          }
        });
      },
    );
  }

  AddressChange buildAddressChange() =>
      new AddressChange(provinceId: provinceId, districtId: districtId, wardId: wardId);
}
