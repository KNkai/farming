import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../base_config/base_config.dart';
import '../../../base_config/src/utils/constants.dart';

class DatepickerInput extends StatefulWidget {
  final bool disabled;
  final DateTime minTime, maxTime;
  final Function(DateTime) onSelected;
  final DateTime value;
  final Function(DateTime) onSaved;
  final Function(String) validator;
  final TextEditingController controller;
  final String labelText, hintText;
  const DatepickerInput({
    Key key,
    this.disabled = false,
    this.minTime,
    this.maxTime,
    this.onSelected,
    this.value,
    this.onSaved,
    this.validator,
    this.controller,
    @required this.labelText,
    this.hintText,
  }) : super(key: key);

  @override
  _DatepickerInputState createState() => _DatepickerInputState();
}

class _DatepickerInputState extends State<DatepickerInput> {
  DateTime selected;
  TextEditingController controller;
  @override
  void initState() {
    super.initState();
    selected = widget.value;
    controller = widget.controller ?? new TextEditingController();
    if (selected != null) {
      setControllerText(widget.value);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: widget.disabled
            ? null
            : () {
                FocusScope.of(context).requestFocus(FocusNode());
                showDatePicker(
                  context: context,
                  firstDate: widget.minTime ?? DateTime.now().subtract(const Duration(days: 3650)),
                  initialDate: selected ?? DateTime.now(),
                  lastDate: widget.maxTime ?? DateTime.now().add(const Duration(days: 3650)),
                  builder: (context, child) => Localizations.override(
                    context: context,
                    locale: const Locale('vi'),
                    child: child,
                  ),
                ).then((value) {
                  if (value != null) {
                    setState(() {
                      selected = value;
                      if (widget.onSelected != null) {
                        widget.onSelected(value);
                      }
                      setControllerText(selected);
                    });
                  }
                });
              },
        child: IgnorePointer(
          child: TextFormField(
            style: const TextStyle(fontSize: 16),
            // enabled: true,
            onSaved: (value) {
              if (widget.onSaved != null) {
                widget.onSaved(selected);
              }
            },
            validator: widget.validator != null
                ? (value) {
                    String errMsg = widget.validator(selected != null ? selected.toIso8601String() : null);
                    if (errMsg != null) {
                      Scrollable.ensureVisible(context, duration: const Duration(milliseconds: 800));
                      return errMsg;
                    }
                    return null;
                  }
                : (value) => null,
            controller: controller,
            showCursor: false,
            decoration: kDefaultInputDecoration.copyWith(
              fillColor: widget.disabled ? Colors.grey[100] : Colors.white,
              labelText: widget.labelText,
              hintText: widget.hintText,
              suffixIcon: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: SvgPicture.asset('assets/icons/arrow_down.svg'),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void setControllerText(DateTime date) {
    controller.text = Format.ddsmmsyyyy(date.toLocal());
  }
}
