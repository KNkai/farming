import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/formart.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/share/components/diaryPhase_card.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/widget/empty.dart';

class DiaryInfo extends StatelessWidget {
  final List diary;
  final void Function(dynamic) itemTap;

  const DiaryInfo(this.diary, this.itemTap);

  _statusToColor(String status) {
    if (status == 'PENDING') {
      return AppColor.grey;
    }
    if (status == 'DONE') {
      return AppColor.primary;
    }
    if (status == 'DOING') {
      return AppColor.redorange;
    }
    return AppColor.grey;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Giai đoạn canh tác'.toUpperCase(),
            style: TextStyle(color: Colors.black87.withOpacity(0.7), fontSize: 17, fontWeight: FontWeight.w700),
          ),
          // Text(
          //   'Đang ở giai đoạn: cho tôm giống ăn lần 2',
          //   style: TextStyle(color: AppColor.primary, decoration: TextDecoration.underline),
          // ),
          const SpacingBox(height: 3),
          if (diary.length == 0) const EmptyWidget(text: 'Chưa có giai đoạn canh tác nào'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: diary.length,
            itemBuilder: (context, index) {
              final item = diary[index];
              return InkWell(
                onTap: () => itemTap(item),
                child: DiaryPhaseCard(
                  status: item["status"],
                  title: item["name"],
                  startAt: Formart.formatToDate(item["startAt"]) ?? 'Chưa cập nhật',
                  endAt: Formart.formatToDate(item["endAt"]),
                  lastUpdate: Formart.formatToDate(item["updatedAt"]),
                  color: _statusToColor(item["status"]),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
