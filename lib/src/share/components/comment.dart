import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';

class Comment extends StatelessWidget {
  final int count;

  const Comment({Key key, this.count}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        const Icon(
          Icons.chat_bubble,
          color: Colors.black38,
          size: 17,
        ),
        const SpacingBox(width: 1),
        Text("$count Bình luận")
      ],
    );
  }
}
