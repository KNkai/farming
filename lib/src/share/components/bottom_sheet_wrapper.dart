import 'package:flutter/material.dart';

class BottomSheetWrapper extends StatelessWidget {
  final double height;
  final List<Widget> children;
  final AppBar appBar;
  final EdgeInsetsGeometry padding;
  final GlobalKey<FormState> formKey;

  const BottomSheetWrapper(
      {Key key, this.height, @required this.appBar, @required this.children, this.padding, this.formKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    var h = height ?? media.size.height / 2;
    var containerH = h - appBar.preferredSize.height;
    var maxH = media.size.height - 85;
    var bottomH = MediaQuery.of(context).viewInsets.bottom;
    var wrapperH = h + bottomH;
    if (wrapperH > maxH) {
      containerH = maxH - bottomH - appBar.preferredSize.height;
    }
    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: maxH),
      child: SizedBox(
        height: wrapperH,
        child: Scaffold(
          appBar: appBar,
          body: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: containerH,
                  child: Padding(
                    padding: padding,
                    child: Form(
                      key: formKey,
                      child: ListView(
                        children: children,
                      ),
                    ),
                  ),
                ),
                Container(
                  height: bottomH,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  TextField buildTextField() => const TextField(
        decoration: InputDecoration(fillColor: Colors.white, filled: true),
      );
}
