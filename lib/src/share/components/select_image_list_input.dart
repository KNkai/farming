import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/share/widget/list_img_picker.dart';

class SelectImageListInput extends FormField<List<String>> {
  SelectImageListInput({
    FormFieldSetter<List<String>> onSaved,
    FormFieldValidator<List<String>> validator,
    List<String> initialValue = const [],
    AutovalidateMode autovalidateMode = AutovalidateMode.disabled,
    @required String label,
    Function(List<String>) onChanged,
    bool enabled = true,
  }) : super(
            onSaved: onSaved,
            validator: (value) {
              if (value.contains("loading")) return "Hình đang được tải lên";
              if (validator != null) return validator(value);
              return null;
            },
            initialValue: initialValue,
            autovalidateMode: autovalidateMode,
            builder: (FormFieldState<List<String>> state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(label),
                  const SpacingBox(height: 1),
                  SizedBox(
                    height: 70,
                    child: ImageRowPicker(
                      initialValue,
                      onUpdateListImg: (images) {
                        state.didChange(images);
                        if (onChanged != null) onChanged(images);
                      },
                      canRemove: enabled,
                    ),
                  ),
                  ...state.hasError
                      ? [
                          const SpacingBox(height: 1),
                          Padding(
                              padding: const EdgeInsets.only(left: 18),
                              child: Text(
                                state.errorText,
                                style: kErrorText,
                              ))
                        ]
                      : [Container()]
                ],
              );
            });
}
