import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';

class TextareaInput extends StatelessWidget {
  const TextareaInput({
    Key key,
    @required this.labelText,
    this.hintText,
    this.minLines = 3,
    this.maxLines = 5,
    this.value,
    this.onSaved,
    this.validator,
    this.autofocus = false,
  }) : super(key: key);
  final String labelText, hintText;
  final int minLines, maxLines;
  final String value;
  final Function(String) onSaved;
  final Function(String) validator;
  final bool autofocus;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: TextFormField(
        style: const TextStyle(fontSize: 16),
        minLines: minLines,
        maxLines: maxLines,
        initialValue: value,
        onSaved: onSaved,
        showCursor: false,
        autofocus: autofocus,
        validator: validator ?? (value) => null,
        decoration: kDefaultInputDecoration.copyWith(
            labelText: labelText,
            hintText: hintText,
            contentPadding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 16,
            )),
      ),
    );
  }
}
