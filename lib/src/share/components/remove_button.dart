import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/dialog.dart';

import '../../../navigator.dart';

class RemoveButton extends StatelessWidget {
  const RemoveButton({
    Key key,
    this.onRemove,
    @required this.label,
  }) : super(key: key);

  final String label;
  final Function onRemove;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          showConfirmDialog(context, 'Xác nhận xóa?', navigatorKey: navigatorKey, confirmTap: () {
            if (onRemove != null) onRemove();
          });
        },
        child: Text(
          label,
          style: const TextStyle(color: Colors.orange, fontSize: 18, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
