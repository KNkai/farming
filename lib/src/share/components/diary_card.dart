import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';
import 'package:mi_smart/base_config/src/utils/formart.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/share/utils/money_input_formatter.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';

import 'icon_text.dart';

class DiaryCard extends StatelessWidget {
  const DiaryCard({
    Key key,
    @required this.name,
    @required this.createrName,
    this.startDate,
    this.editDiary,
    this.endDate,
    @required this.production,
    @required this.unit,
    @required this.status,
    @required this.data,
    this.press,
  }) : super(key: key);
  final name, createrName, unit, status;
  final DateTime startDate, endDate;
  final double production;
  final GestureTapCallback press;
  final GestureTapCallback editDiary;
  final dynamic data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 12),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.white,
          ),
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                name,
                style: ptHeadline(context).copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.2,
                    color: Colors.black87.withOpacity(0.7)),
              ),
              const SpacingBox(height: 1),
              IconText(icon: Icons.person, text: 'Người tạo: $createrName'),
              const SpacingBox(height: 0.5),
              if (startDate != null)
                IconText(
                    icon: Icons.calendar_today,
                    text: 'Bắt đầu: ${Format.ddsmmsyyyy(startDate)}' +
                        (endDate != null ? ' đến ${Format.ddsmmsyyyy(endDate)}' : '')),
              const SpacingBox(height: 0.5),
              IconText(
                  icon: Icons.list,
                  text: 'Sản lượng: ${MoneyInputFormatter.getMaskedValue(production ?? 0)} ${unit ?? ''}'),
              const SpacingBox(height: 1.5),
              IconText(icon: Icons.location_city, text: 'Địa điểm: ${data['region']['name'] ?? ''}'),
              const SpacingBox(height: 1.5),
              IconText(icon: Icons.map, text: 'Vùng: ${data['region']['zone']['name'] ?? ''}'),
              if (editDiary != null) const SpacingBox(height: 2),
              if (editDiary != null)
                AppBtn(
                  'Chỉnh sửa',
                  onPressed: editDiary,
                )
            ],
          ),
        ),
      ),
    );
  }
}
