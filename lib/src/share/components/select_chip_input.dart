import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';

import '../utils/color.dart';

class SelectChipItem {
  final dynamic id;
  final String display;
  final dynamic data;
  bool selected;

  SelectChipItem({@required this.id, @required this.display, this.data, this.selected = false});
}

class SelectChipInput extends FormField<List<String>> {
  SelectChipInput({
    FormFieldSetter<List<String>> onSaved,
    FormFieldValidator<List<String>> validator,
    AutovalidateMode autovalidateMode = AutovalidateMode.disabled,
    @required String labelText,
    @required List<SelectChipItem> chipItems,
    Function(List<SelectChipItem>) onSelect,
  }) : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: chipItems.where((element) => element.selected).map<String>((e) => e.id).toList(),
            autovalidateMode: autovalidateMode,
            builder: (FormFieldState<List<String>> state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      labelText,
                      style: TextStyle(color: state.hasError ? Colors.red : Colors.black38, fontSize: 11),
                    ),
                  ),
                  Wrap(
                    children: List.generate(
                      chipItems.length,
                      (index) => Padding(
                        padding: const EdgeInsets.all(5),
                        child: InkWell(
                          onTap: () {
                            chipItems[index].selected = !chipItems[index].selected;
                            var selected = chipItems.where((element) => element.selected);
                            state.didChange(selected.map<String>((e) => e.id).toList());
                            if (onSelect != null) onSelect(selected.toList());
                          },
                          child: Chip(
                              backgroundColor: chipItems[index].selected ? AppColor.primary : AppColor.grey,
                              label: Text(
                                chipItems[index].display,
                                style: const TextStyle(color: Colors.white),
                              )),
                        ),
                      ),
                    ),
                  ),
                  state.hasError
                      ? Padding(
                          padding: const EdgeInsets.only(left: 18),
                          child: Text(state.errorText, style: kErrorText),
                        )
                      : Container()
                ],
              );
            });
}
