import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/share/components/status_text.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class DiaryPhaseCard extends StatelessWidget {
  final String status;
  final Color color;
  final String title;
  final String startAt;
  final String endAt;
  final String lastUpdate;

  const DiaryPhaseCard({Key key, this.status, this.color, this.title, this.startAt, this.endAt, this.lastUpdate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    const Map<String, dynamic> statusMap = {
      "OPENING": {"color": AppColor.primary, "display": "Đang mở"},
      "RESOLVED": {"color": AppColor.grey, "display": "Đã hoàn thành"},
      "PENDING": {"color": AppColor.redorange, "display": "Chưa thực hiện"},
    };
    return Stack(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 15,
                  width: 15,
                  decoration: BoxDecoration(shape: BoxShape.circle, color: color ?? Colors.grey[200]),
                ),
                //Flexible(
                // Container(
                //   width: 3,
                //   height: 150,
                //   decoration: BoxDecoration(color: Colors.black26),
                // ),
                //),
              ],
            ),
            const SpacingBox(width: 2),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 12),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white,
                  ),
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        title,
                        style: ptHeadline(context).copyWith(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 0.2,
                            color: Colors.black87.withOpacity(0.7)),
                      ),
                      const SpacingBox(height: 1),
                      Text('Bắt đầu: $startAt' + (endAt != null ? ' đến $endAt' : '')),
                      const SpacingBox(height: 0.5),
                      Text('Cập nhật lần cuối: $lastUpdate'),
                      const SpacingBox(height: 1.5),
                      StatusText(text: statusMap[status]["display"], color: statusMap[status]["color"]),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        Positioned(
          top: 14,
          left: 6,
          bottom: 0,
          child: Container(
            width: 3,
            height: 150,
            decoration: const BoxDecoration(color: Colors.black12),
          ),
        )
      ],
    );
  }
}
