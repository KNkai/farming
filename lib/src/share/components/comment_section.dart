import 'dart:io';

import 'package:flutter/material.dart';

import '../../../base_config/base_config.dart';
import '../../../base_widget/base_widget.dart';
import '../../../base_widget/src/dialog.dart';
import '../../../navigator.dart';
import '../utils/color.dart';
import '../widget/image_picker.dart';
import '../widget/list_img_picker.dart';

class CommentSection extends StatefulWidget {
  final Function(String message, List<String> images) createComment;
  const CommentSection(this.createComment);
  @override
  _CommentSectionState createState() => _CommentSectionState();
}

class _CommentSectionState extends State<CommentSection> {
  TextEditingController _commentController = TextEditingController();
  List<String> imageComment = [];

  _handleSendComment(String text) async {
    if (text == null || text.isEmpty) {
      return showToast("Bắt buộc phải nhập nội dung bình luận", flagColor: false);
    }
    final res = await widget.createComment(text, imageComment);
    if (res == true) {
      setState(() {
        imageComment.clear();
        _commentController.clear();
      });
      FocusScope.of(context).unfocus();
    }
  }

  String loadingImage = 'https://www.bis.org/img/uploading.gif';
  _onImagePick(String path) async {
    try {
      imageComment.add(loadingImage);
      setState(() {});
      File imgFile = File(path);
      String imageUrl = await ImageUtil.upload(imgFile);
      imageComment.add(imageUrl);
      setState(() {});
    } catch (e) {
      showAlertDialog(context, 'Xảy ra lỗi khi upload ảnh, xin vui lòng thử lại', navigatorKey: navigatorKey);
    } finally {
      imageComment.remove(loadingImage);
      setState(() {});
      // widget.onUpdateListImg(widget.listImg);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: deviceWidth(context),
      color: Colors.white,
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: deviceWidth(context) - 46,
                child: TextField(
                  controller: _commentController,
                  onSubmitted: _handleSendComment,
                  onChanged: (text) {
                    setState(() {});
                  },
                  style: Theme.of(context).textTheme.bodyText1,
                  textInputAction: TextInputAction.newline,
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 8,
                  decoration: InputDecoration(
                    suffixIcon: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => imagePicker(context, _onImagePick, _onImagePick),
                          child: const Icon(
                            Icons.image,
                            size: 20,
                            color: AppColor.primary,
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.send,
                            size: 20,
                            color: _commentController.text.isEmpty && imageComment.length == 0
                                ? AppColor.grey
                                : AppColor.primary,
                          ),
                          onPressed: () {
                            _handleSendComment(_commentController.text);
                          },
                        ),
                      ],
                    ),

                    // labelText: "Tìm kiếm... ",
                    hintText: "Viết bình luận",
                    contentPadding: const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
                    hintStyle: Theme.of(context).textTheme.caption.copyWith(fontSize: 15.0),
                    fillColor: Theme.of(context).primaryColor,
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColor.primary, width: 2.0),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColor.grey, width: 2.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          if (imageComment.length > 0)
            Padding(
              padding: const EdgeInsets.only(left: 23, top: 5),
              child: SizedBox(
                height: 70,
                child: ImageRowPicker(
                  imageComment,
                  reloadParent: () {
                    setState(() {});
                  },
                ),
              ),
            ),
        ],
      ),
    );
  }
}
