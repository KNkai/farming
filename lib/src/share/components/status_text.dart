import 'package:flutter/material.dart';

class StatusText extends StatelessWidget {
  const StatusText({
    Key key,
    @required this.text,
    @required this.color,
    this.textColor,
  }) : super(key: key);

  final String text;
  final Color color;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 3),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Text(
        text,
        style: TextStyle(color: textColor ?? Colors.white, fontSize: 12),
      ),
    );
  }
}
