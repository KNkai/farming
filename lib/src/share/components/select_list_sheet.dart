import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';

class SelectListSheetItem {
  final String display;
  final dynamic id;
  final dynamic data;
  SelectListSheetItem({@required this.display, @required this.id, this.data});
}

class SelectListSheet extends StatelessWidget {
  const SelectListSheet({
    Key key,
    @required this.items,
    @required this.title,
    this.onSelect,
    this.emptyText = const Center(child: Text("Chưa có dữ liệu")),
  }) : super(key: key);
  final String title;
  final List<SelectListSheetItem> items;
  final Function(SelectListSheetItem) onSelect;
  final Widget emptyText;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildSheetBar(context, title: title),
      body: items.length == 0
          ? emptyText
          : ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              itemCount: items.length,
              shrinkWrap: true,
              itemBuilder: (context, index) => ListTile(
                title: Text(items[index].display),
                onTap: () {
                  onSelect(items[index]);
                  Navigator.pop(context);
                },
              ),
            ),
    );
  }
}
