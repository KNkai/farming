class AppEnum {
  static Map<String, String> diaryStatusEnum = {
    "OPENING": "Đang mở",
    "CLOSED": "Đã đóng",
  };

  static Map<String, String> pharseStatusEnum = {
    "OPENING": "Đang mở",
    "PENDING": "Chưa thực hiện",
    "RESOLVED": "Đã hoàn thành",
  };

  static Map<String, String> diseaseStatusEnum = {
    "OPENING": "Đang diễn ra",
    "RESOLVED": "Đã kết thúc",
  };

  static Map<String, String> logAttributeTypeEnum = {
    "TEXT": "TEXT",
    "NUMBER": "NUMBER",
    "SELECTBOX": "SELECTBOX",
    "CHECKBOX": "CHECKBOX",
    "IMAGE": "IMAGE"
  };
}
