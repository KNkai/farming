import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:permission_handler/permission_handler.dart';

String appCurrency(num money) {
  if (money == null) return 'đ';
  if (money is double) money = money.toInt();
  if (money == 0) return '0 đ';
  if (money <= 999) return '$money đ';
  return NumberFormat('##,000 đ').format(money);
}

String removeDecimalZeroFormat(dynamic n) {
  n = n.toDouble();
  return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
}

bool phoneNumberMatch(String phone) {
  return RegExp(r"^[0-9]{10}$").hasMatch(phone);
}

bool emailMatch(String email) {
  return RegExp(
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
      .hasMatch(email);
}

String dateFormat(DateTime date) {
  return DateFormat('dd/MM/yyyy').format(date);
}

String parseJsonToFilter(dynamic object) {
  String filter = json.encode(object);
  return filter.substring(1, filter.length - 1).replaceAllMapped(new RegExp(r'\"(\w+)\":'), (match) {
    return '${match.group(1)}:';
  });
}

Color fromHex(String hexString) {
  if (hexString == null || hexString.isEmpty) return null;
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}

String toHex(Color color, {bool leadingHashSign = true}) => color == null
    ? null
    : '${leadingHashSign ? '#' : ''}'
        '${color.alpha.toRadixString(16).padLeft(2, '0')}'
        '${color.red.toRadixString(16).padLeft(2, '0')}'
        '${color.green.toRadixString(16).padLeft(2, '0')}'
        '${color.blue.toRadixString(16).padLeft(2, '0')}';

LatLngBounds bounds(List<LatLng> positions) {
  if (positions == null || positions.isEmpty) return null;
  final southwestLat =
      positions.map((p) => p.latitude).reduce((value, element) => value < element ? value : element); // smallest
  final southwestLon = positions.map((p) => p.longitude).reduce((value, element) => value < element ? value : element);
  final northeastLat =
      positions.map((p) => p.latitude).reduce((value, element) => value > element ? value : element); // biggest
  final northeastLon = positions.map((p) => p.longitude).reduce((value, element) => value > element ? value : element);
  return LatLngBounds(southwest: LatLng(southwestLat, southwestLon), northeast: LatLng(northeastLat, northeastLon));
}

onCustomPersionRequest(
    {@required Permission permission,
    Function onGranted,
    Function onAlreadyDenied,
    Function onJustDeny,
    Function onAndroidPermanentDenied}) {
  permission.status.then((value) {
    if (value.isUndetermined) {
      Permission.camera.request().then((value) {
        if (value.isDenied && onJustDeny != null) {
          onJustDeny();
        } else if (value.isGranted && onGranted != null) {
          onGranted();
        } else if (value.isPermanentlyDenied && onAndroidPermanentDenied != null) {
          onAndroidPermanentDenied();
        }
      });
    } else if (value.isDenied && onAlreadyDenied != null) {
      onAlreadyDenied();
    } else if (value.isGranted && onGranted != null) {
      onGranted();
    }
  });
}

Future<String> getUniqueDeviceId() {
  final infoPlugin = DeviceInfoPlugin();
  if (Platform.isIOS) {
    return infoPlugin.iosInfo.then((info) {
      return info.identifierForVendor;
    });
  } else if (Platform.isAndroid) {
    return infoPlugin.androidInfo.then((info) {
      return info.androidId;
    });
  }

  return null;
}

class WarningDialog {
  static bool _isShow = false;

  static show(BuildContext context, String content, String btnText, {String title, Function onCloseDialog}) {
    if (!_isShow) {
      _isShow = true;
      Widget alert;
      Widget _titleWidget({TextAlign textAlign = TextAlign.start}) => title != null
          ? Text(
              title,
              textAlign: textAlign,
            )
          : null;

      if (content != null && content.isNotEmpty) {
        if (Platform.isAndroid) {
          // If it has plenty of buttons, it will stack them on vertical way.
          // If title isn't supplied, height of this alert will smaller than the one has title.
          alert = AlertDialog(
            title: _titleWidget(),
            content: Text(
              content,
              textAlign: TextAlign.start,
            ),
            actions: [
              FlatButton(
                child: Text(btnText),
                onPressed: () {
                  _isShow = false;
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        } else {
          // Almost similiar with Cupertino style.
          // If title isn't supplied, height of this alert will smaller than the one has title.
          alert = CupertinoAlertDialog(
            title: Padding(
              padding: const EdgeInsets.only(
                bottom: 10,
              ),
              child: _titleWidget(textAlign: TextAlign.center),
            ),
            content: Text(
              content,
              textAlign: TextAlign.start,
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  _isShow = false;
                  Navigator.of(context).pop();
                },
                child: Text(
                  btnText,
                ),
              )
            ],
          );
        }

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        ).then((value) {
          _isShow = false;
          if (onCloseDialog != null) {
            onCloseDialog();
          }
        });
      }
    }
  }
}

onLocationWhenInUsePermissionRequest(
    {Function onGranted, Function onAlreadyDenied, Function onJustDeny, Function onAndroidPermanentDenied}) {
  Permission.locationWhenInUse.status.then((value) {
    if (value.isUndetermined) {
      Permission.locationWhenInUse.request().then((value) {
        if (value.isDenied && onJustDeny != null) {
          onJustDeny();
        } else if (value.isGranted && onGranted != null) {
          onGranted();
        } else if (value.isPermanentlyDenied && onAndroidPermanentDenied != null) {
          onAndroidPermanentDenied();
        }
      });
    } else if (value.isDenied && onAlreadyDenied != null) {
      onAlreadyDenied();
    } else if (value.isGranted && onGranted != null) {
      onGranted();
    }
  });
}

LatLng centroid(List<LatLng> list) {
  double a = 0.0;
  int i1 = 1;
  for (int i = 0; i < list.length; i++) {
    a += list[i].x * list[i1].y - list[i1].x * list[i].y;
    i1 = (i1 + 1) % list.length;
  }
  a *= 0.5;

  double cx = 0.0, cy = 0.0;
  i1 = 1;
  for (int i = 0; i < list.length; i++) {
    double t = list[i].x * list[i1].y - list[i1].x * list[i].y;
    cx += (list[i].x + list[i1].x) * t;
    cy += (list[i].y + list[i1].y) * t;
    i1 = (i1 + 1) % list.length;
  }
  cx = cx / (6.0 * a);
  cy = cy / (6.0 * a);

  return LatLng((cy / 180) - 90, (cx / 360) - 180);
}

extension LatLngCalculation on LatLng {
  double get y {
    return (this.latitude + 90) * 180;
  }

  double get x {
    return (this.longitude + 180) * 360;
  }
}

enum AppRoleEnum { admin, editor, staff, unknown }

class AppRole {
  AppRole._();
  static AppRoleEnum _role = AppRoleEnum.unknown;

  static AppRoleEnum get role => _role;
  static bool get isAdmin => _role == AppRoleEnum.admin;
  static bool get isStaff => _role == AppRoleEnum.staff;
  static bool get isEditor => _role == AppRoleEnum.editor;

  static void setRole(String value) {
    if (value == null || value.isEmpty) return;
    if (value == 'ADMIN') _role = AppRoleEnum.admin;
    if (value == 'EDITOR') _role = AppRoleEnum.editor;
    if (value == 'STAFF') _role = AppRoleEnum.staff;
  }
}
