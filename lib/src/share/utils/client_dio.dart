import 'package:dio/dio.dart';

const GOOGLE_MAP_API_KEY = 'AIzaSyAHY5EcP_p37t5VgOxKeg_vZW2IbrdaTtw';
const HOST_PLACES_API = 'https://maps.googleapis.com/maps/api/place/';

class ClientDio {
  static Dio _dio;

  ClientDio._() {
    BaseOptions _options = BaseOptions(
      connectTimeout: 5000,
    );
    _dio = Dio(_options);
  }

  Future<Response> get(String path, {Map<String, dynamic> queryParameters}) async {
    return _dio.get(path, queryParameters: queryParameters);
  }

  static final ClientDio instance = ClientDio._();
}
