import 'package:flutter/services.dart';
import "package:intl/intl.dart";

class MoneyInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.isEmpty) {
      return newValue;
    }
    int newTextLength = newValue.text.length;
    int selectionIndex = newValue.selection.end;
    String newValueText = newValue.text;
    double parsedNumber = double.parse(MoneyInputFormatter.getUnMaskedValue(newValueText));
    String formatedText = MoneyInputFormatter.getMaskedValue(parsedNumber);
    int formatedTextLength = formatedText.length;
    int movedIndex = formatedTextLength - newTextLength;
    return TextEditingValue(
      text: formatedText,
      selection: TextSelection.collapsed(offset: selectionIndex + movedIndex),
    );
  }

  static String getUnMaskedValue(String value) {
    return value.replaceAll(new RegExp(r','), "");
  }

  static String getMaskedValue(num value) {
    if (value == null) return null;
    return NumberFormat("#,##0").format(value);
  }

  static String getMaskedValueDouble(double value) {
    if (value == null) return null;
    return NumberFormat("#,##0").format(value);
  }
}
