import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/widget/scale_animation.dart';

class LoadingWidget extends StatelessWidget {
  final Widget child;
  final ValueListenable<bool> valueListenable;

  const LoadingWidget({
    @required this.child,
    @required this.valueListenable,
  });

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: valueListenable,
      builder: (context, snapshot, widget) {
        return Stack(
          children: <Widget>[
            IgnorePointer(
              ignoring: snapshot,
              child: child,
            ),
            snapshot
                ? Center(
                    child: ScaleAnimation(
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: const BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.all(
                          Radius.circular(8),
                        ),
                      ),
                      child: SizedBox(
                        height: SizeConfig.setWidth(10),
                        width: SizeConfig.setWidth(10),
                        child: const CircularProgressIndicator(),
                      ),
                    ),
                  ))
                : const SizedBox.shrink()
          ],
        );
      },
    );
  }
}
