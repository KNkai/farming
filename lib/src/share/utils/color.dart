import 'package:flutter/cupertino.dart';

class AppColor {
  static const Color primary = Color(0xff4B554C);
  static const Color purple = Color(0xFF2D3092);
  static const Color secondary = Color(0xFF9BEC79);
  static const Color yellow = Color(0xFFFFDC30);
  static const Color blue = Color(0xFF18B0F1);
  static const Color greenblue = Color(0xFF86DBFF);
  static const Color redorange = Color(0xFFFF6724);
  static const Color grey = Color(0xFFD2D8CF);
  static const Color warning = Color(0xFFFF6716);
  static const Color tertiary = Color(0xfff5f5f5);
}
