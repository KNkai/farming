import 'dart:async';
import 'dart:math';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mi_smart/src/modules/find_regions/find_region_marker_generator.dart';
import 'package:mi_smart/src/modules/search_place/search_place_repo.dart';
import 'package:mi_smart/src/modules/select_area/app_map_repo.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:vector_math/vector_math.dart' as d;

class SelectAreaBloc {
  var _markersPolygons = MarkersAndPolygonPositions([], {});

  final _markerGenerator = FindRegionMarkerGenerator();
  final _repo = AppMapRepo();
  final statePolygonMapView = StreamController<MarkersAndPolygonPositions>();
  final stateSide = StreamController<StateSide>();

  List<LatLng> get polygon => _markersPolygons.positions;

  Future<void> eventMarkPolygon(LatLng position) async {
    _markersPolygons.positions..add(position);

    // sort clockwise
    if (_markersPolygons.positions.length > 3) {
      _markersPolygons.positions = _sortClockWise(polygon);
    }

    final markerNewIndex = (_markersPolygons.markers.length + 1);
    statePolygonMapView.add(MarkersAndPolygonPositions(
        _markersPolygons.positions,
        _markersPolygons.markers
          ..add(Marker(
              icon: BitmapDescriptor.fromBytes((await _markerGenerator.genIndexMarker(markerNewIndex))),
              position: position,
              markerId: MarkerId((markerNewIndex).toString())))));
  }

  Future<void> eventPaintPolygon(List<LatLng> positions) async {
    if (positions.length > 3) {
      positions = _sortClockWise(positions);
    }

    final _setMarker = Set<Marker>();
    for (int c = 0; c < positions.length; c++) {
      _setMarker.add(Marker(
          icon: BitmapDescriptor.fromBytes((await _markerGenerator.genIndexMarker(c + 1))),
          position: positions[c],
          markerId: MarkerId((c).toString())));
    }

    _markersPolygons = MarkersAndPolygonPositions(positions, _setMarker);
    statePolygonMapView.add(_markersPolygons);
  }

  void eventInitPolygon(List<LatLng> positions) {
    if (positions != null && positions.length > 0) {
      eventPaintPolygon(positions);
      stateSide.add(StateSideMoveToBounds(_markersPolygons.positions));
    }
  }

  void eventDeleteAll() {
    _markersPolygons = MarkersAndPolygonPositions([], {});
    statePolygonMapView.add(_markersPolygons);
  }

  void eventMoveToPlace(AutoCompletePrediction prediction) {
    _repo.placeDetail(prediction.placeId).then((value) {
      if (value is LatLng) {
        stateSide.add(StateSideMoveToPlace(value));
      }
    });
  }

  List<LatLng> _sortClockWise(List<LatLng> positions) {
    double x = 0;
    double y = 0;
    positions.forEach((p) {
      x += p.x;
      y += p.y;
    });
    double centerX = x / positions.length;
    double centerY = y / positions.length;

    positions.sort((a, b) {
      double a1 = (d.degrees(atan2(a.x - centerX, a.y - centerY)) + 360) % 360;
      double a2 = (d.degrees(atan2(b.x - centerX, b.y - centerY)) + 360) % 360;
      return (a1 - a2).toInt();
    });
    return positions;
  }

  void dispose() {
    stateSide.close();
    statePolygonMapView.close();
  }
}

abstract class StateSide {}

class StateSideMoveToPlace extends StateSide {
  final LatLng position;
  StateSideMoveToPlace(this.position);
}

class StateSideMoveToBounds extends StateSide {
  final List<LatLng> positions;
  StateSideMoveToBounds(this.positions);
}

class MarkersAndPolygonPositions {
  List<LatLng> positions;
  final Set<Marker> markers;
  MarkersAndPolygonPositions(this.positions, this.markers);
}
