import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mi_smart/src/modules/search_place/search_place_page.dart';
import 'package:mi_smart/src/modules/select_area/select_area_bloc.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';

import 'list_position_page.dart';

class SelectAreaPage extends StatefulWidget {
  static bool _lockGoToMapPage = false;
  static void go(BuildContext context,
      {void Function(List<LatLng>) onPolygon,
      List<LatLng> initialPolygon,
      int strokeWidth,
      Color strokeColor,
      Color fillColor,
      double strokeOpacity,
      double fillOpacity}) {
    if (!_lockGoToMapPage) {
      _lockGoToMapPage = true;
      onLocationWhenInUsePermissionRequest(onGranted: () {
        _lockGoToMapPage = false;
        Navigator.of(context)
            .push(MaterialPageRoute(
                builder: (_) => SelectAreaPage(
                      strokeWidth: strokeWidth,
                      strokeColor: strokeColor,
                      fillColor: fillColor,
                      strokeOpacity: strokeOpacity,
                      fillOpacity: fillOpacity,
                      initialPolygon: initialPolygon,
                    )))
            .then((value) {
          if (value != null && onPolygon != null) onPolygon(value);
        });
      }, onAlreadyDenied: () {
        _lockGoToMapPage = false;
        WarningDialog.show(context, 'Vui lòng mở quyền truy cập địa điểm', 'Đóng');
      }, onAndroidPermanentDenied: () {
        _lockGoToMapPage = false;
        WarningDialog.show(context, 'Vui lòng mở quyền truy cập địa điểm', 'Đóng');
      }, onJustDeny: () {
        _lockGoToMapPage = false;
        WarningDialog.show(context, 'Vui lòng mở quyền truy cập địa điểm', 'Đóng');
      });
    }
  }

  final List<LatLng> initialPolygon;
  final int strokeWidth;
  final Color strokeColor;
  final Color fillColor;
  final double strokeOpacity;
  final double fillOpacity;

  SelectAreaPage(
      {List<LatLng> initialPolygon,
      this.strokeWidth,
      this.strokeColor,
      this.fillColor,
      this.strokeOpacity,
      this.fillOpacity})
      : this.initialPolygon = initialPolygon.toList();

  @override
  _SelectAreaPageState createState() => _SelectAreaPageState();
}

class _SelectAreaPageState extends State<SelectAreaPage> {
  GoogleMapController _mapController;
  Offset _flagScreenCordinate;
  MapType _type = MapType.none;

  final _xFlag = (window.physicalSize.width / (Platform.isAndroid ? 1 : window.devicePixelRatio)) ~/ 2;
  final _yFlag = (window.physicalSize.height / (Platform.isAndroid ? 1 : window.devicePixelRatio)) ~/ 2;

  var _bloc = SelectAreaBloc();

  @override
  void initState() {
    _bloc.stateSide.stream.listen((state) {
      if (state is StateSideMoveToPlace)
        _mapController.moveCamera(CameraUpdate.newLatLng(state.position));
      else if (state is StateSideMoveToBounds)
        Future.delayed(const Duration(seconds: 1)).then((value) {
          // Have to delay to avoid malfunction (freeze)
          _mapController.moveCamera(CameraUpdate.newLatLngBounds(bounds(state.positions), 70.0));
        });
    });
    super.initState();
  }

  @override
  void dispose() {
    _mapController.dispose();
    _bloc = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        StreamBuilder<MarkersAndPolygonPositions>(
          stream: _bloc.statePolygonMapView.stream,
          builder: (context, ss) {
            return GoogleMap(
                mapToolbarEnabled: false,
                polygons: ss?.data?.positions == null || ss.data.positions.isEmpty || ss.data.positions.length < 3
                    ? null
                    : {
                        Polygon(
                            geodesic: true,
                            strokeWidth: widget.strokeWidth ?? 3,
                            strokeColor: widget.strokeColor?.withOpacity(widget?.strokeOpacity?.toDouble() ?? 1) ??
                                AppColor.primary,
                            fillColor: widget?.fillColor?.withOpacity(widget.fillOpacity ?? 0.3) ??
                                AppColor.primary.withOpacity(0.3),
                            polygonId: PolygonId('1'),
                            points: ss.data.positions)
                      },
                markers: ss?.data?.markers,
                mapType: _type,
                myLocationButtonEnabled: false,
                myLocationEnabled: true,
                tiltGesturesEnabled: false,
                onMapCreated: (mapController) {
                  _mapController = mapController;

                  if (widget.initialPolygon != null)
                    _bloc.eventInitPolygon(widget.initialPolygon);
                  else
                    _requestCurrentLocation(_mapController);

                  _visibleMap();
                },
                initialCameraPosition: const CameraPosition(zoom: 14, target: LatLng(10.774296, 106.696959))); // Dist 1
          },
        ),
        SafeArea(
          child: Stack(
            children: [
              if (_flagScreenCordinate == null)
                LayoutBuilder(builder: (context, constraints) {
                  _flagScreenCordinate = Offset(constraints.maxWidth, constraints.maxHeight);
                  return _flagWidget();
                })
              else
                _flagWidget(),
              Positioned(
                top: 6,
                left: 24,
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Column(
                    children: [
                      _wrapShadow(
                          Container(
                            height: 40,
                            width: 40,
                            child: const Icon(Icons.close),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                          ), onTap: () {
                        Navigator.of(context).pop();
                      }),
                      const SizedBox(
                        height: 12,
                      ),
                      _wrapShadow(
                          Container(
                            height: 40,
                            width: 40,
                            child: const Icon(Icons.location_searching),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                          ), onTap: () {
                        _requestCurrentLocation(_mapController);
                      }),
                      const SizedBox(
                        height: 7,
                      ),
                      _wrapShadow(
                          Container(
                            height: 40,
                            width: 40,
                            child: const Icon(Icons.delete_forever),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                          ), onTap: () {
                        _bloc.eventDeleteAll();
                      }),
                    ],
                  ),
                ),
              ),
              Positioned(
                right: 24,
                top: 6,
                child: Row(
                  children: [
                    _wrapShadow(
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: const Text(
                            'Tìm kiếm',
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                        ), onTap: () {
                      SearchPage.goSelect(context, (prediction) {
                        _bloc.eventMoveToPlace(prediction);
                      });
                    }),
                    const SizedBox(
                      width: 10,
                    ),
                    _wrapShadow(
                        Container(
                          height: 40,
                          width: 40,
                          child: const Icon(Icons.assignment),
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                        ), onTap: () {
                      ListPositionPage.go(context, _bloc.polygon, onNewPositions: (positions) async {
                        await _bloc.eventPaintPolygon(positions);
                      });
                    })
                  ],
                ),
              ),
              Positioned(
                bottom: 40,
                left: 10,
                right: 10,
                child: Row(
                  children: [
                    Expanded(
                      child: _wrapShadow(AppBtn(
                        'Đánh dấu',
                        onPressed: () {
                          _markPolylgon(_mapController);
                        },
                        paddingBtn: EdgeInsets.zero,
                      )),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: _wrapShadow(AppBtn(
                        'Lưu',
                        onPressed: () {
                          Navigator.of(context).pop(_bloc.polygon);
                        },
                        paddingBtn: EdgeInsets.zero,
                      )),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _wrapShadow(Widget child, {Function onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey[400],
            spreadRadius: 1,
            blurRadius: 10,
            offset: const Offset(2, 4), // changes position of shadow
          ),
        ]),
        child: child,
      ),
    );
  }

  void _visibleMap() {
    // Delay to avoid drop frames
    Future.delayed(const Duration(milliseconds: 500)).then((_) {
      setState(() {
        _type = MapType.normal;
      });
    });
  }

  Widget _flagWidget() {
    return const Center(
        child: Padding(
      padding: EdgeInsets.only(bottom: 60, left: 23),
      child: Icon(
        Icons.flag,
        size: 40,
      ),
    ));
  }

  void _requestCurrentLocation(GoogleMapController controller) {
    getCurrentPosition().then((value) {
      controller.moveCamera(CameraUpdate.newLatLng(LatLng(value.latitude, value.longitude)));
    });
  }

  void _markPolylgon(GoogleMapController controller) {
    _mapController.getLatLng(ScreenCoordinate(x: _xFlag, y: _yFlag)).then((value) {
      _bloc.eventMarkPolygon(value);
    });
  }
}

class WrapReturnInheritanceSizeOnce extends StatelessWidget {
  final Widget child;
  final void Function(Size) onSize;
  const WrapReturnInheritanceSizeOnce(this.child, this.onSize);
  @override
  Widget build(BuildContext context) {
    onSize((context.findRenderObject() as RenderBox).size);
    return child;
  }
}
