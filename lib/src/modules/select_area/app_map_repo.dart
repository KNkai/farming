import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mi_smart/base_config/src/utils/formart.dart';
import 'package:mi_smart/src/modules/find_regions/model/get_all_region/get_all_region.graphql.dart';
import 'package:mi_smart/src/modules/select_area/model/app_plain_query.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';
import 'package:mi_smart/src/share/utils/client_dio.dart';

import 'model/map_model.dart';

class AppMapRepo {
  Future placeDetail(String placeId) {
    return ClientDio.instance.get(HOST_PLACES_API + 'details/json', queryParameters: {
      'key': GOOGLE_MAP_API_KEY,
      'place_id': placeId,
      'fields': 'geometry',
    }).then((response) {
      if (response?.data == null || response.data['status'] != "OK") return ResultError();

      final location = response.data['result']['geometry']['location'];
      return LatLng(location['lat'], location['lng']);
    }).catchError((_) {
      return ResultError();
    });
  }

  Future getAllRegions({bool isDiseaseModule}) {
    if (isDiseaseModule) {
      return BaseApi.instance.rawBodyRawDataExecute(AppPlainQuery.getAllDiseaseSituationQuery, variables: {
        "q": {"limit": 1000}
      }).then((value) {
        return GetAllDiseaseSituationQuery.fromJson(value?.data)?.getAllDiseaseSituation?.data;
      }).catchError((_) => null);
    }
    return BaseApi.instance
        .execute(GetAllRegionQuery(variables: GetAllRegionArguments(q: GetAllRegion$QueryGetListInput(limit: 1000))))
        .then((value) {
      return value?.data?.getAllRegion?.data;
    }).catchError((onError) {
      return null;
    });
  }

  Future getOneRegion(String id) {
    return BaseApi.instance.rawBodyRawDataExecute(AppPlainQuery.getOneRegionQuery, variables: {"i": id}).then((value) {
      return GetOneRegionQuery.fromJson(value?.data)?.region;
    }).catchError((_) => null);
  }
}
