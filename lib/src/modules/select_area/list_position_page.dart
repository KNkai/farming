import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/widget/app_form.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

class ListPositionPage extends StatefulWidget {
  static void go(BuildContext context, List<LatLng> positions, {void Function(List<LatLng>) onNewPositions}) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => ListPositionPage(positions))).then((value) {
      if (value != null) onNewPositions(value);
    });
  }

  final List<LatLng> positions;

  const ListPositionPage(this.positions);

  @override
  _ListPositionPageState createState() => _ListPositionPageState();
}

class _ListPositionPageState extends State<ListPositionPage> {
  final _pin = Image.asset(
    'images/app_pin.png',
    scale: 2,
  );

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      onBackTap: () {
        Navigator.of(context).pop(widget.positions);
      },
      title: 'Chỉnh sửa toạ độ',
      child: ListView.separated(
          itemBuilder: (_, index) {
            return AppForm(
              padding: const EdgeInsets.only(left: 15, top: 12, right: 0, bottom: 12),
              child: Row(
                children: [
                  _pin,
                  Text(
                    (index + 1).toString(),
                    style: const TextStyle(fontSize: 20),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  const Text(
                    'X',
                    style: TextStyle(color: AppColor.primary),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Text(
                      widget.positions[index].longitude.toStringAsFixed(7),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  const Text(
                    'Y',
                    style: TextStyle(color: AppColor.primary),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Text(
                      widget.positions[index].latitude.toStringAsFixed(7),
                    ),
                  ),
                  IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      widget.positions.removeAt(index);
                      setState(() {});
                    },
                  )
                ],
              ),
            );
          },
          separatorBuilder: (_, index) => const SizedBox(
                height: 15,
              ),
          itemCount: widget.positions.length),
    );
  }
}
