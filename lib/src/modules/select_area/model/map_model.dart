// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';
import 'dart:ui';

import 'package:mi_smart/src/share/utils/util.dart';

class GetOneWeatherQuery {
  GetOneWeatherQuery({
    this.getAllWeather,
  });

  final GetOneWeather getAllWeather;

  factory GetOneWeatherQuery.fromMap(Map<String, dynamic> json) => GetOneWeatherQuery(
        getAllWeather: json["getAllWeather"] == null ? null : GetOneWeather.fromMap(json["getAllWeather"]),
      );
}

class GetOneWeather {
  GetOneWeather({
    this.data,
  });

  final List<GetOneWeatherData> data;

  factory GetOneWeather.fromMap(Map<String, dynamic> json) => GetOneWeather(
        data: json["data"] == null
            ? null
            : List<GetOneWeatherData>.from(json["data"].map((x) => GetOneWeatherData.fromMap(x))),
      );
}

class GetOneWeatherData {
  GetOneWeatherData({
    this.id,
    this.name,
    this.regionId,
    this.state,
    this.base,
    this.current,
    this.hourly,
    this.daily,
    this.createdAt,
    this.updatedAt,
  });

  final String id;
  final String name;
  final String regionId;
  final String state;
  final dynamic base;
  final Current current;
  final List<Current> hourly;
  final List<Daily> daily;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory GetOneWeatherData.fromMap(Map<String, dynamic> json) => GetOneWeatherData(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        regionId: json["regionId"] == null ? null : json["regionId"],
        state: json["state"] == null ? null : json["state"],
        base: json["base"],
        current: json["current"] == null ? null : Current.fromMap(json["current"]),
        hourly: json["hourly"] == null
            ? null
            : List<Current>.from(json["hourly"].map((x) {
                x["sunrise"] = json["current"]["sunrise"];
                x["sunset"] = json["current"]["sunset"];
                x["uvi"] = json["current"]["uvi"];
                return Current.fromMap(x);
              })),
        daily: json["daily"] == null ? null : _list(json),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
      );

  static List<Daily> _list(Map<String, dynamic> json) {
    return List<Daily>.from(json["daily"].map((x) => Daily.fromMap(x))).take(6).toList();
  }
}

class Daily {
  Daily({
    this.dt,
    this.sunrise,
    this.sunset,
    this.temp,
    this.feelsLike,
    this.pressure,
    this.humidity,
    this.dewPoint,
    this.windSpeed,
    this.windDeg,
    this.weather,
    this.clouds,
    this.pop,
    this.rain,
    this.uvi,
  });

  final int dt;
  final int sunrise;
  final int sunset;
  final Temp temp;
  final FeelsLike feelsLike;
  final int pressure;
  final int humidity;
  final double dewPoint;
  final double windSpeed;
  final int windDeg;
  final List<Weather> weather;
  final int clouds;
  final double pop;
  final double rain;
  final double uvi;

  factory Daily.fromMap(Map<String, dynamic> json) => Daily(
        dt: json["dt"] == null ? null : json["dt"],
        sunrise: json["sunrise"] == null ? null : json["sunrise"],
        sunset: json["sunset"] == null ? null : json["sunset"],
        temp: json["temp"] == null ? null : Temp.fromMap(json["temp"]),
        feelsLike: json["feels_like"] == null ? null : FeelsLike.fromMap(json["feels_like"]),
        pressure: json["pressure"] == null ? null : json["pressure"],
        humidity: json["humidity"] == null ? null : json["humidity"],
        dewPoint: json["dew_point"] == null ? null : json["dew_point"].toDouble(),
        windSpeed: json["wind_speed"] == null ? null : json["wind_speed"].toDouble(),
        windDeg: json["wind_deg"] == null ? null : json["wind_deg"],
        weather: json["weather"] == null ? null : List<Weather>.from(json["weather"].map((x) => Weather.fromMap(x))),
        clouds: json["clouds"] == null ? null : json["clouds"],
        pop: json["pop"] == null ? null : json["pop"].toDouble(),
        rain: json["rain"] == null ? null : json["rain"].toDouble(),
        uvi: json["uvi"] == null ? null : json["uvi"].toDouble(),
      );

  Map<String, dynamic> toMap() => {
        "dt": dt == null ? null : dt,
        "sunrise": sunrise == null ? null : sunrise,
        "sunset": sunset == null ? null : sunset,
        "temp": temp == null ? null : temp.toMap(),
        "feels_like": feelsLike == null ? null : feelsLike.toMap(),
        "pressure": pressure == null ? null : pressure,
        "humidity": humidity == null ? null : humidity,
        "dew_point": dewPoint == null ? null : dewPoint,
        "wind_speed": windSpeed == null ? null : windSpeed,
        "wind_deg": windDeg == null ? null : windDeg,
        "weather": weather == null ? null : List<dynamic>.from(weather.map((x) => x.toMap())),
        "clouds": clouds == null ? null : clouds,
        "pop": pop == null ? null : pop,
        "rain": rain == null ? null : rain,
        "uvi": uvi == null ? null : uvi,
      };
}

class FeelsLike {
  FeelsLike({
    this.day,
    this.night,
    this.eve,
    this.morn,
  });

  final double day;
  final double night;
  final double eve;
  final double morn;

  factory FeelsLike.fromMap(Map<String, dynamic> json) => FeelsLike(
        day: json["day"] == null ? null : json["day"].toDouble(),
        night: json["night"] == null ? null : json["night"].toDouble(),
        eve: json["eve"] == null ? null : json["eve"].toDouble(),
        morn: json["morn"] == null ? null : json["morn"].toDouble(),
      );

  Map<String, dynamic> toMap() => {
        "day": day == null ? null : day,
        "night": night == null ? null : night,
        "eve": eve == null ? null : eve,
        "morn": morn == null ? null : morn,
      };
}

class Temp {
  Temp({
    this.day,
    this.min,
    this.max,
    this.night,
    this.eve,
    this.morn,
  });

  final double day;
  final double min;
  final double max;
  final double night;
  final double eve;
  final double morn;

  factory Temp.fromMap(Map<String, dynamic> json) => Temp(
        day: json["day"] == null ? null : json["day"].toDouble(),
        min: json["min"] == null ? null : json["min"].toDouble(),
        max: json["max"] == null ? null : json["max"].toDouble(),
        night: json["night"] == null ? null : json["night"].toDouble(),
        eve: json["eve"] == null ? null : json["eve"].toDouble(),
        morn: json["morn"] == null ? null : json["morn"].toDouble(),
      );

  Map<String, dynamic> toMap() => {
        "day": day == null ? null : day,
        "min": min == null ? null : min,
        "max": max == null ? null : max,
        "night": night == null ? null : night,
        "eve": eve == null ? null : eve,
        "morn": morn == null ? null : morn,
      };
}

class Current {
  Current({
    this.dt,
    this.sunrise,
    this.sunset,
    this.temp,
    this.feelsLike,
    this.pressure,
    this.humidity,
    this.dewPoint,
    this.uvi,
    this.clouds,
    this.visibility,
    this.windSpeed,
    this.windDeg,
    this.weather,
    this.rain,
  });

  final int dt;
  final int sunrise;
  final int sunset;
  final int temp;
  final double feelsLike;
  final int pressure;
  final int humidity;
  final double dewPoint;
  final double uvi;
  final int clouds;
  final int visibility;
  final double windSpeed;
  final int windDeg;
  final List<Weather> weather;
  final Rain rain;

  factory Current.fromMap(Map<String, dynamic> json) => Current(
        dt: json["dt"] == null ? null : json["dt"],
        sunrise: json["sunrise"] == null ? null : json["sunrise"],
        sunset: json["sunset"] == null ? null : json["sunset"],
        temp: json["temp"] == null ? null : json["temp"].toInt(),
        feelsLike: json["feels_like"] == null ? null : json["feels_like"].toDouble(),
        pressure: json["pressure"] == null ? null : json["pressure"],
        humidity: json["humidity"] == null ? null : json["humidity"],
        dewPoint: json["dew_point"] == null ? null : json["dew_point"].toDouble(),
        uvi: json["uvi"] == null ? null : json["uvi"].toDouble(),
        clouds: json["clouds"] == null ? null : json["clouds"],
        visibility: json["visibility"] == null ? null : json["visibility"],
        windSpeed: json["wind_speed"] == null ? null : json["wind_speed"].toDouble(),
        windDeg: json["wind_deg"] == null ? null : json["wind_deg"],
        weather: json["weather"] == null ? null : List<Weather>.from(json["weather"].map((x) => Weather.fromMap(x))),
        rain: json["rain"] == null ? null : Rain.fromMap(json["rain"]),
      );

  Map<String, dynamic> toMap() => {
        "dt": dt == null ? null : dt,
        "sunrise": sunrise == null ? null : sunrise,
        "sunset": sunset == null ? null : sunset,
        "temp": temp == null ? null : temp,
        "feels_like": feelsLike == null ? null : feelsLike,
        "pressure": pressure == null ? null : pressure,
        "humidity": humidity == null ? null : humidity,
        "dew_point": dewPoint == null ? null : dewPoint,
        "uvi": uvi == null ? null : uvi,
        "clouds": clouds == null ? null : clouds,
        "visibility": visibility == null ? null : visibility,
        "wind_speed": windSpeed == null ? null : windSpeed,
        "wind_deg": windDeg == null ? null : windDeg,
        "weather": weather == null ? null : List<dynamic>.from(weather.map((x) => x.toMap())),
        "rain": rain == null ? null : rain.toMap(),
      };

  String tempText() => temp == null ? '' : temp.toStringAsFixed(0).toString() + '°C';
  String pressureText() => pressure == null ? '' : pressure.toString() + 'hPA';
  String windSpeedText() => windSpeed == null ? '' : windSpeed.toString() + 'm/s';
  String windDegText() => windDeg == null ? '' : windDeg.toString() + '°';
  String visibilityText() => visibility == null ? '' : (visibility / 1000).toStringAsFixed(1).toString() + 'km';
  String dewPointText() => dewPoint == null ? '' : dewPoint.toString() + '°C';
  String feelsLikeText() => feelsLike == null ? '' : feelsLike.toString() + '°C';
  String cloudsText() => clouds == null ? '' : clouds.toString() + '%';
}

class Rain {
  Rain({
    this.the1H,
  });

  final double the1H;

  factory Rain.fromMap(Map<String, dynamic> json) => Rain(
        the1H: json["1h"] == null ? null : json["1h"].toDouble(),
      );

  Map<String, dynamic> toMap() => {
        "1h": the1H == null ? null : the1H,
      };
}

class Weather {
  Weather({
    this.id,
    this.main,
    this.description,
    this.icon,
  });

  final int id;
  final String main;
  final String description;
  final String icon;

  factory Weather.fromMap(Map<String, dynamic> json) => Weather(
        id: json["id"] == null ? null : json["id"],
        main: json["main"] == null ? null : json["main"],
        description: json["description"] == null ? null : json["description"],
        icon: json["icon"] == null ? null : json["icon"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "main": main == null ? null : main,
        "description": description == null ? null : description,
        "icon": icon == null ? null : icon,
      };
}

class GetAllDiseaseSituationQuery {
  GetAllDiseaseSituationQuery({
    this.getAllDiseaseSituation,
  });

  final GetAllDiseaseSituation getAllDiseaseSituation;

  factory GetAllDiseaseSituationQuery.fromRawJson(String str) => GetAllDiseaseSituationQuery.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetAllDiseaseSituationQuery.fromJson(Map<String, dynamic> json) => GetAllDiseaseSituationQuery(
        getAllDiseaseSituation: json["getAllDiseaseSituation"] == null
            ? null
            : GetAllDiseaseSituation.fromJson(json["getAllDiseaseSituation"]),
      );

  Map<String, dynamic> toJson() => {
        "getAllDiseaseSituation": getAllDiseaseSituation == null ? null : getAllDiseaseSituation.toJson(),
      };
}

class GetOneRegionQuery {
  GetOneRegionQuery({
    this.region,
  });

  final Region region;

  factory GetOneRegionQuery.fromJson(Map<String, dynamic> json) => GetOneRegionQuery(
        region: json["getOneRegion"] == null ? null : Region.fromJson(json["getOneRegion"]),
      );
}

class GetAllDiseaseSituation {
  GetAllDiseaseSituation({
    this.data,
    this.total,
    this.pagination,
  });

  final List<GetAllDiseaseSituationData> data;
  final int total;
  final Pagination pagination;

  factory GetAllDiseaseSituation.fromRawJson(String str) => GetAllDiseaseSituation.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetAllDiseaseSituation.fromJson(Map<String, dynamic> json) => GetAllDiseaseSituation(
        data: json["data"] == null
            ? null
            : List<GetAllDiseaseSituationData>.from(json["data"].map((x) => GetAllDiseaseSituationData.fromJson(x))),
        total: json["total"] == null ? null : json["total"],
        pagination: json["pagination"] == null ? null : Pagination.fromJson(json["pagination"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total == null ? null : total,
        "pagination": pagination == null ? null : pagination.toJson(),
      };
}

class GetAllDiseaseSituationData {
  GetAllDiseaseSituationData({
    this.id,
    this.title,
    this.createdAt,
    this.updatedAt,
    this.listImages,
    this.description,
    this.reporterName,
    this.disease,
    this.staff,
    this.user,
    this.region,
  });
  final String id;
  final String title;
  final DateTime createdAt;
  final DateTime updatedAt;
  final List<String> listImages;
  final String description;
  final String reporterName;
  final Disease disease;
  final Staff staff;
  final dynamic user;
  final Region region;

  factory GetAllDiseaseSituationData.fromRawJson(String str) => GetAllDiseaseSituationData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetAllDiseaseSituationData.fromJson(Map<String, dynamic> json) => GetAllDiseaseSituationData(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        listImages: json["listImages"] == null ? null : List<String>.from(json["listImages"].map((x) => x)),
        description: json["description"] == null ? null : json["description"],
        reporterName: json["reporterName"] == null ? null : json["reporterName"],
        disease: json["disease"] == null ? null : Disease.fromJson(json["disease"]),
        staff: json["staff"] == null ? null : Staff.fromJson(json["staff"]),
        user: json["user"],
        region: json["region"] == null ? null : Region.fromJson(json["region"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
        "description": description == null ? null : description,
        "disease": disease == null ? null : disease.toJson(),
        "staff": staff == null ? null : staff.toJson(),
        "user": user,
        "region": region == null ? null : region.toJson(),
      };
}

class Disease {
  Disease({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.description,
    this.htmlContent,
    this.listImages,
    this.situationStats,
    this.regionCount,
    this.thumbnail,
  });

  final String id;
  final String name;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String description;
  final String htmlContent;
  final List<String> listImages;
  final SituationStats situationStats;
  final int regionCount;
  final String thumbnail;

  factory Disease.fromRawJson(String str) => Disease.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Disease.fromJson(Map<String, dynamic> json) => Disease(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        description: json["description"] == null ? null : json["description"],
        htmlContent: json["htmlContent"] == null ? null : json["htmlContent"],
        listImages: json["listImages"] == null ? null : List<String>.from(json["listImages"].map((x) => x)),
        situationStats: json["situationStats"] == null ? null : SituationStats.fromJson(json["situationStats"]),
        regionCount: json["regionCount"] == null ? null : json["regionCount"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "description": description == null ? null : description,
        "htmlContent": htmlContent == null ? null : htmlContent,
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
        "situationStats": situationStats == null ? null : situationStats.toJson(),
        "regionCount": regionCount == null ? null : regionCount,
        "thumbnail": thumbnail == null ? null : thumbnail,
      };
}

class SituationStats {
  SituationStats({
    this.total,
    this.resolved,
    this.opening,
    this.regionCount,
  });

  final int total;
  final int resolved;
  final int opening;
  final int regionCount;

  factory SituationStats.fromRawJson(String str) => SituationStats.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory SituationStats.fromJson(Map<String, dynamic> json) => SituationStats(
        total: json["total"] == null ? null : json["total"],
        resolved: json["resolved"] == null ? null : json["resolved"],
        opening: json["opening"] == null ? null : json["opening"],
        regionCount: json["regionCount"] == null ? null : json["regionCount"],
      );

  Map<String, dynamic> toJson() => {
        "total": total == null ? null : total,
        "resolved": resolved == null ? null : resolved,
        "opening": opening == null ? null : opening,
        "regionCount": regionCount == null ? null : regionCount,
      };
}

class Region {
  Region({
    this.id,
    this.name,
    this.listImages,
    this.province,
    this.note,
    this.district,
    this.ward,
    this.provinceId,
    this.districtId,
    this.wardId,
    this.locationLat,
    this.locationLng,
    this.polygon,
    this.location,
    this.area,
    this.managerId,
    this.zoneId,
    this.farmingType,
    this.ownerName,
    this.ownerPhone,
    this.ownerAddress,
    this.assigneeIds,
    this.materialId,
    this.materialName,
    this.htmlContent,
    this.farmingTypeId,
    this.certificates,
    this.createdAt,
    this.updatedAt,
    this.manager,
    this.zone,
    this.assignees,
    this.material,
  });

  final String id;
  final String name;
  List<String> listImages;
  final String province;
  final String note;
  final String district;
  final String ward;
  final String provinceId;
  final String districtId;
  final String wardId;
  final double locationLat;
  final double locationLng;
  final AgriPolygon polygon;
  final Location location;
  final int area;
  final String managerId;
  final String zoneId;
  final Disease farmingType;
  final String ownerName;
  final String ownerPhone;
  final String ownerAddress;
  final List<String> assigneeIds;
  final String materialId;
  final String materialName;
  final String htmlContent;
  final String farmingTypeId;
  final List<Certificate> certificates;
  final DateTime createdAt;
  final DateTime updatedAt;
  final Staff manager;
  final Zone zone;
  final List<Staff> assignees;
  final Disease material;

  factory Region.fromRawJson(String str) => Region.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Region.fromJson(Map<String, dynamic> json) => Region(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        listImages: json["listImages"] == null ? null : List<String>.from(json["listImages"].map((x) => x)),
        province: json["province"] == null ? null : json["province"],
        note: json["note"] == null ? null : json["note"],
        district: json["district"] == null ? null : json["district"],
        ward: json["ward"] == null ? null : json["ward"],
        provinceId: json["provinceId"] == null ? null : json["provinceId"],
        districtId: json["districtId"] == null ? null : json["districtId"],
        wardId: json["wardId"] == null ? null : json["wardId"],
        locationLat: json["locationLat"] == null ? null : json["locationLat"].toDouble(),
        locationLng: json["locationLng"] == null ? null : json["locationLng"].toDouble(),
        polygon: json["polygon"] == null ? null : AgriPolygon.fromJson(json["polygon"]),
        location: json["location"] == null ? null : Location.fromMap(json["location"]),
        area: json["area"] == null ? null : json["area"],
        managerId: json["managerId"] == null ? null : json["managerId"],
        zoneId: json["zoneId"] == null ? null : json["zoneId"],
        farmingType: json["farmingType"] == null ? null : Disease.fromJson(json["farmingType"]),
        ownerName: json["ownerName"] == null ? null : json["ownerName"],
        ownerPhone: json["ownerPhone"] == null ? null : json["ownerPhone"],
        ownerAddress: json["ownerAddress"] == null ? null : json["ownerAddress"],
        assigneeIds: json["assigneeIds"] == null ? null : List<String>.from(json["assigneeIds"].map((x) => x)),
        materialId: json["materialId"] == null ? null : json["materialId"],
        materialName: json["materialName"] == null ? null : json["materialName"],
        htmlContent: json["htmlContent"] == null ? null : json["htmlContent"],
        farmingTypeId: json["farmingTypeId"] == null ? null : json["farmingTypeId"],
        certificates: json["certificates"] == null
            ? null
            : List<Certificate>.from(json["certificates"].map((x) => Certificate.fromJson(x))),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        manager: json["manager"] == null ? null : Staff.fromJson(json["manager"]),
        zone: json["zone"] == null ? null : Zone.fromJson(json["zone"]),
        assignees: json["assignees"] == null
            ? null
            : List<Staff>.from(json["assignees"].map((x) => x == null ? null : Staff.fromJson(x))),
        material: json["material"] == null ? null : Disease.fromJson(json["material"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
        "province": province == null ? null : province,
        "note": note == null ? null : note,
        "district": district == null ? null : district,
        "ward": ward == null ? null : ward,
        "provinceId": provinceId == null ? null : provinceId,
        "districtId": districtId == null ? null : districtId,
        "wardId": wardId == null ? null : wardId,
        "locationLat": locationLat == null ? null : locationLat,
        "locationLng": locationLng == null ? null : locationLng,
        "polygon": null,
        "location": location == null ? null : location.toMap(),
        "area": area == null ? null : area,
        "managerId": managerId == null ? null : managerId,
        "zoneId": zoneId == null ? null : zoneId,
        "farmingType": farmingType == null ? null : farmingType.toJson(),
        "ownerName": ownerName == null ? null : ownerName,
        "ownerPhone": ownerPhone == null ? null : ownerPhone,
        "ownerAddress": ownerAddress == null ? null : ownerAddress,
        "assigneeIds": assigneeIds == null ? null : List<dynamic>.from(assigneeIds.map((x) => x)),
        "materialId": materialId == null ? null : materialId,
        "materialName": materialName == null ? null : materialName,
        "htmlContent": htmlContent == null ? null : htmlContent,
        "farmingTypeId": farmingTypeId == null ? null : farmingTypeId,
        "certificates": certificates == null ? null : List<dynamic>.from(certificates.map((x) => x.toJson())),
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "manager": manager == null ? null : manager.toJson(),
        "zone": zone == null ? null : zone.toJson(),
        "assignees": assignees == null ? null : List<dynamic>.from(assignees.map((x) => x == null ? null : x.toJson())),
        "material": material == null ? null : material.toJson(),
      };
}

class Staff {
  Staff({
    this.id,
    this.phone,
    this.email,
    this.name,
    this.avatar,
    this.uid,
    this.isBlock,
    this.address,
    this.province,
    this.district,
    this.ward,
    this.provinceId,
    this.districtId,
    this.wardId,
    this.createdAt,
    this.updatedAt,
    this.role,
    this.unseenNotify,
  });

  final String id;
  final String phone;
  final String email;
  final String name;
  final String avatar;
  final String uid;
  final bool isBlock;
  final String address;
  final String province;
  final String district;
  final String ward;
  final String provinceId;
  final String districtId;
  final String wardId;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String role;
  final int unseenNotify;

  factory Staff.fromRawJson(String str) => Staff.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Staff.fromJson(Map<String, dynamic> json) => Staff(
        id: json["id"] == null ? null : json["id"],
        phone: json["phone"] == null ? null : json["phone"],
        email: json["email"] == null ? null : json["email"],
        name: json["name"] == null ? null : json["name"],
        avatar: json["avatar"] == null ? null : json["avatar"],
        uid: json["uid"] == null ? null : json["uid"],
        isBlock: json["isBlock"] == null ? null : json["isBlock"],
        address: json["address"] == null ? null : json["address"],
        province: json["province"] == null ? null : json["province"],
        district: json["district"] == null ? null : json["district"],
        ward: json["ward"] == null ? null : json["ward"],
        provinceId: json["provinceId"] == null ? null : json["provinceId"],
        districtId: json["districtId"] == null ? null : json["districtId"],
        wardId: json["wardId"] == null ? null : json["wardId"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        role: json["role"] == null ? null : json["role"],
        unseenNotify: json["unseenNotify"] == null ? null : json["unseenNotify"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "phone": phone == null ? null : phone,
        "email": email == null ? null : email,
        "name": name == null ? null : name,
        "avatar": avatar == null ? null : avatar,
        "uid": uid == null ? null : uid,
        "isBlock": isBlock == null ? null : isBlock,
        "address": address == null ? null : address,
        "province": province == null ? null : province,
        "district": district == null ? null : district,
        "ward": ward == null ? null : ward,
        "provinceId": provinceId == null ? null : provinceId,
        "districtId": districtId == null ? null : districtId,
        "wardId": wardId == null ? null : wardId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "role": role == null ? null : role,
        "unseenNotify": unseenNotify == null ? null : unseenNotify,
      };
}

class Certificate {
  Certificate({
    this.title,
    this.description,
    this.listImages,
    this.effectiveDate,
    this.expiredDate,
  });

  final String title;
  final String description;
  final List<String> listImages;
  final DateTime effectiveDate;
  final DateTime expiredDate;

  factory Certificate.fromRawJson(String str) => Certificate.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Certificate.fromJson(Map<String, dynamic> json) => Certificate(
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        listImages: json["listImages"] == null ? null : List<String>.from(json["listImages"].map((x) => x)),
        effectiveDate: json["effectiveDate"] == null ? null : DateTime.parse(json["effectiveDate"]),
        expiredDate: json["expiredDate"] == null ? null : DateTime.parse(json["expiredDate"]),
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
        "effectiveDate": effectiveDate == null ? null : effectiveDate.toIso8601String(),
        "expiredDate": expiredDate == null ? null : expiredDate.toIso8601String(),
      };
}

class Location {
  Location({
    this.coordinates,
    this.id,
    this.type,
  });

  final List<double> coordinates;
  final String id;
  final String type;

  factory Location.fromMap(Map<String, dynamic> json) => Location(
        coordinates:
            json["coordinates"] == null ? null : List<double>.from(json["coordinates"][0][0].map((x) => x.toDouble())),
        id: json["_id"] == null ? null : json["_id"],
        type: json["type"] == null ? null : json["type"],
      );

  Map<String, dynamic> toMap() => {
        "coordinates": coordinates == null ? null : List<dynamic>.from(coordinates.map((x) => x)),
        "_id": id == null ? null : id,
        "type": type == null ? null : type,
      };
}

class AgriPolygon {
  AgriPolygon({
    this.paths,
    this.strokeColor,
    this.strokeOpacity,
    this.fillColor,
    this.fillOpacity,
  });

  final List<Path> paths;
  final Color strokeColor;
  final int strokeOpacity;
  final Color fillColor;
  final double fillOpacity;

  factory AgriPolygon.fromRawJson(String str) => AgriPolygon.fromJson(json.decode(str));

  factory AgriPolygon.fromJson(Map<String, dynamic> json) => AgriPolygon(
        paths: json["paths"] == null ? null : List<Path>.from(json["paths"].map((x) => Path.fromJson(x))),
        strokeColor: json["strokeColor"] == null ? null : fromHex(json["strokeColor"]),
        strokeOpacity: json["strokeOpacity"] == null ? null : json["strokeOpacity"],
        fillColor: json["fillColor"] == null ? null : fromHex(json["fillColor"]),
        fillOpacity: json["fillOpacity"] == null ? null : json["fillOpacity"].toDouble(),
      );
}

class Path {
  Path({
    this.lat,
    this.lng,
  });

  final double lat;
  final double lng;

  factory Path.fromRawJson(String str) => Path.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Path.fromJson(Map<String, dynamic> json) => Path(
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lng: json["lng"] == null ? null : json["lng"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
      };
}

class Zone {
  Zone({
    this.id,
    this.name,
    this.locationLat,
    this.locationLng,
    this.polygon,
    this.createdAt,
    this.updatedAt,
    this.regionCount,
  });

  final String id;
  final String name;
  final dynamic locationLat;
  final dynamic locationLng;
  final dynamic polygon;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int regionCount;

  factory Zone.fromRawJson(String str) => Zone.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Zone.fromJson(Map<String, dynamic> json) => Zone(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        locationLat: json["locationLat"],
        locationLng: json["locationLng"],
        polygon: json["polygon"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        regionCount: json["regionCount"] == null ? null : json["regionCount"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "locationLat": locationLat,
        "locationLng": locationLng,
        "polygon": polygon,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "regionCount": regionCount == null ? null : regionCount,
      };
}

class Pagination {
  Pagination({
    this.limit,
    this.offset,
    this.page,
    this.total,
  });

  final int limit;
  final int offset;
  final int page;
  final int total;

  factory Pagination.fromRawJson(String str) => Pagination.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
        limit: json["limit"] == null ? null : json["limit"],
        offset: json["offset"] == null ? null : json["offset"],
        page: json["page"] == null ? null : json["page"],
        total: json["total"] == null ? null : json["total"],
      );

  Map<String, dynamic> toJson() => {
        "limit": limit == null ? null : limit,
        "offset": offset == null ? null : offset,
        "page": page == null ? null : page,
        "total": total == null ? null : total,
      };
}
