class AppPlainQuery {
  static const getOneWeather = '''
query getAllWeather(\$q: QueryGetListInput) {
    getAllWeather(q: \$q) {
        data {
            id
            name
            regionId
            state
            location
            base
            current
            createdAt
            updatedAt
            hourly
            daily
        }
    }
}
  ''';

  static const getOneRegionQuery = '''
  query getOneRegion(\$i: ID!) {
    getOneRegion(id: \$i) {
            id
            name
            listImages
            province
            note
            district
            ward
            provinceId
            districtId
            wardId
            locationLat
            locationLng
            polygon {
                ...polygonFrag
            }
            location
            area
            managerId
            zoneId
            farmingType {
                id
                name
                description
                listImages
                createdAt
                updatedAt
                regionCount
            }
            ownerName
            ownerPhone
            ownerAddress
            assigneeIds
            materialId
            materialName
            htmlContent
            farmingTypeId
            certificates {
                title
                description
                listImages
                effectiveDate
                expiredDate
            }
            createdAt
            updatedAt
            manager {
                id
                uid
                email
                name
                phone
                address
                avatar
                province
                district
                ward
                provinceId
                districtId
                wardId
                role
                createdAt
                updatedAt
            }
            zone {
                id
                name
                locationLat
                locationLng
                polygon {
                   ...polygonFrag
                }
                createdAt
                updatedAt
                regionCount
            }
            assignees {
                id
                phone
                email
                name
                avatar
                uid
                isBlock
                address
                province
                district
                ward
                provinceId
                districtId
                wardId
                createdAt
                updatedAt
            }
            material {
                id
                name
                description
                htmlContent
                listImages
                thumbnail
                createdAt
                updatedAt
                regionCount
                }
          
    }
}

fragment polygonFrag on Polygon {
                paths {
                    lat
                    lng
                }
                strokeColor
                strokeOpacity
                fillColor
                fillOpacity 
}
  ''';

  static const getAllDiseaseSituationQuery = '''
  query getAllDiseaseSituation(\$q: QueryGetListInput) {
    getAllDiseaseSituation(q: \$q) {
        data {
            id
            title
            createdAt
            updatedAt
            listImages
            description
            reporterName
            disease {
                id
                name
                createdAt
                updatedAt
                description
                htmlContent
                listImages
                situationStats {
                    total
                    resolved
                    opening
                    regionCount
                }
            }
            staff {
              id
              phone
              email
              name
              avatar
              uid
              isBlock
              address
              province
              district
              ward
              provinceId
              districtId
              wardId
              createdAt
              updatedAt
              unseenNotify  
            }
            user {
                id
                uid
                email
                name
                phone
                address
                avatar
                provinceId
                district
                districtId
                ward
                wardId
                role
                unseenNotify
                createdAt
                updatedAt
            }
          region {
            id
            name
            listImages
            province
            note
            district
            ward
            provinceId
            districtId
            wardId
            locationLat
            locationLng
            polygon {
                ...polygonFrag
            }
            location
            area
            managerId
            zoneId
            farmingType {
                id
                name
                description
                listImages
                createdAt
                updatedAt
                regionCount
            }
            ownerName
            ownerPhone
            ownerAddress
            assigneeIds
            materialId
            materialName
            htmlContent
            farmingTypeId
            certificates {
                title
                description
                listImages
                effectiveDate
                expiredDate
            }
            createdAt
            updatedAt
            manager {
                id
                uid
                email
                name
                phone
                address
                avatar
                province
                district
                ward
                provinceId
                districtId
                wardId
                role
                createdAt
                updatedAt
            }
            zone {
                id
                name
                locationLat
                locationLng
                polygon {
                   ...polygonFrag
                }
                createdAt
                updatedAt
                regionCount
            }
            assignees {
                id
                phone
                email
                name
                avatar
                uid
                isBlock
                address
                province
                district
                ward
                provinceId
                districtId
                wardId
                createdAt
                updatedAt
            }
            material {
                id
                name
                description
                htmlContent
                listImages
                thumbnail
                createdAt
                updatedAt
                regionCount
                }
          }
        }
        total
        pagination {
            limit
            offset
            page
            total
        }
    }
}

fragment polygonFrag on Polygon {
                paths {
                    lat
                    lng
                }
                strokeColor
                strokeOpacity
                fillColor
                fillOpacity 
}
  ''';
}
