import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../base_config/base_config.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../model/phase.model.dart';
import '../../../share/widget/app_scaffold.dart';
import '../components/list_phase_log.dart';
import '../components/phase_card.dart';
import '../controllers/phase_controller.dart';
import '../components/phase_bottom_sheet.dart';

class PhasePage extends StatelessWidget {
  final dynamic phase;
  const PhasePage(this.phase);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<PhaseController>(
      create: (_) => PhaseController(phaseId: phase["id"]),
      child: const DiaryPhasePageBody(),
    );
  }
}

class DiaryPhasePageBody extends StatefulWidget {
  const DiaryPhasePageBody();

  @override
  _DiaryPhasePageBodyState createState() => _DiaryPhasePageBodyState();
}

class _DiaryPhasePageBodyState extends State<DiaryPhasePageBody> {
  PhaseController controller;
  @override
  void initState() {
    super.initState();
    controller = Provider.of<PhaseController>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      onBack: true,
      title: 'Chi tiết giai đoạn canh tác',
      child: NotificationListener(
        onNotification: (ScrollNotification scrollInfo) {
          if (scrollInfo is ScrollEndNotification && scrollInfo.metrics.extentAfter == 0) {
            controller.loadMorePhaseLogs();
          }
          return true;
        },
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ValueListenableBuilder<Phase>(
                  valueListenable: controller.phaseNotifer,
                  builder: (context, item, child) {
                    if (item == null) return Center(child: kLoadingSpinner);
                    return PhaseCard(
                        phase: item,
                        press: () {},
                        editPhase: () {
                          showModalBottomSheet(
                            context: context,
                            // useRootNavigator: true,
                            isScrollControlled: true,
                            builder: (context) => ListenableProvider.value(
                              value: controller,
                              child: const PhaseBottomSheet(),
                            ),
                          );
                        });
                  }),
              const SpacingBox(height: 1),
              const ListPhaseLog(),
            ],
          ),
        ),
      ),
    );
  }
}
