import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';
import 'package:mi_smart/base_config/src/utils/formart.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/model/phase.model.dart';
import 'package:mi_smart/src/share/components/icon_text.dart';
import 'package:mi_smart/src/share/components/status_text.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';

class PhaseCard extends StatelessWidget {
  final Phase phase;
  const PhaseCard({
    Key key,
    this.press,
    @required this.phase,
    this.editPhase,
  }) : super(key: key);
  final GestureTapCallback press;
  final GestureTapCallback editPhase;

  @override
  Widget build(BuildContext context) {
    const Map<String, dynamic> statusMap = {
      "OPENING": {"color": AppColor.primary, "display": "Đang mở"},
      "RESOLVED": {"color": AppColor.grey, "display": "Đã hoàn thành"},
      "PENDING": {"color": AppColor.redorange, "display": "Chưa thực hiện"},
    };
    return GestureDetector(
      onTap: press,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 12),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.white,
          ),
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                phase.name,
                style: ptHeadline(context).copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.2,
                    color: Colors.black87.withOpacity(0.7)),
              ),
              const SpacingBox(height: 1),
              if (phase.startAt != null) ...[
                IconText(
                    icon: Icons.calendar_today,
                    text: 'Bắt đầu: ${Format.ddsmmsyyyy(phase.startAt)}' +
                        (phase.endAt != null ? ' đến ${Format.ddsmmsyyyy(phase.endAt)}' : '')),
                const SpacingBox(height: 1),
              ],
              StatusText(text: statusMap[phase.status]["display"], color: statusMap[phase.status]["color"]),
              const SpacingBox(height: 2),
              if (editPhase != null) AppBtn('Chỉnh sửa', onPressed: editPhase),
            ],
          ),
        ),
      ),
    );
  }
}
