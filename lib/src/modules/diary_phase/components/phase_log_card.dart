import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../base_config/src/utils/constants.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../model/phase_log.model.dart';
import '../controllers/phase_controller.dart';
import 'phase_log_bottom_sheet.dart';
import 'phase_log_card_attribute.dart';
import 'timeline_card_wrapper.dart';

class PhaseLogCard extends StatefulWidget {
  final PhaseLog phaseLog;

  const PhaseLogCard({Key key, @required this.phaseLog}) : super(key: key);

  @override
  _PhaseLogCardState createState() => _PhaseLogCardState();
}

class _PhaseLogCardState extends State<PhaseLogCard> {
  PhaseController controller;
  PhaseLog phaseLog;

  // @override
  // void didChangeDependencies() {
  //   if (controller == null) {
  //     controller = Provider.of<DiaryPhaseController>(context);
  //   }
  //   super.didChangeDependencies();
  // }
  @override
  void initState() {
    super.initState();
    phaseLog = widget.phaseLog;
    controller = Provider.of<PhaseController>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return TimelineCardWrapper(
      color: phaseLog.getColor(),
      child: Column(
        children: [
          buildPhaseLogHeader(),
          buildPhaseLogAttributes(),
        ],
      ),
    );
  }

  Padding buildPhaseLogAttributes() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: ListView.separated(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: phaseLog.attributes.length,
          itemBuilder: (context, index) {
            // var attribute = phaseLog.attributes[index];
            return PhaseLogCardAttribute(
              attribute: phaseLog.attributes[index],
              enabled: false,
            );
          },
          separatorBuilder: (context, index) => const SpacingBox(height: 1),
        ),
      ),
    );
  }

  Container buildPhaseLogHeader() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
        color: phaseLog.getColor(),
      ),
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            phaseLog.name,
            style: ptHeadline(context)
                .copyWith(fontSize: 16, fontWeight: FontWeight.bold, letterSpacing: 0.2, color: Colors.white),
          ),
          const SpacingBox(height: 1),
          Text(
            'Người nhập: ${phaseLog.getCreaterName()}',
            style: const TextStyle(color: Colors.white),
          ),
          const SpacingBox(height: 0.5),
          Text('Cập nhật lần cuối: ${phaseLog.updatedAtToString()}', style: const TextStyle(color: Colors.white)),
          const SpacingBox(height: 1),
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) {
                      return ListenableProvider.value(
                        value: controller,
                        child: PhaseLogBottomSheet(
                          phaseLog: phaseLog,
                          isCreate: false,
                        ),
                      );
                    });
              },
              child: Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.white),
                padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                child: const Text('Chỉnh sửa'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
