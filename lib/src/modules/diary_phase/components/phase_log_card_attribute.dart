import 'package:flutter/material.dart';
import 'package:mi_smart/src/share/components/select_image_list_input.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/money_input_formatter.dart';

import '../../../model/phase_log_attribute.model.dart';
import '../../../share/components/text_input.dart';

class PhaseLogCardAttribute extends StatefulWidget {
  final PhaseLogAttribute attribute;
  final bool enabled;

  const PhaseLogCardAttribute({this.enabled = false, @required this.attribute});

  @override
  _PhaseLogCardAttributeState createState() => _PhaseLogCardAttributeState();
}

class _PhaseLogCardAttributeState extends State<PhaseLogCardAttribute> {
  PhaseLogAttribute attribute;
  @override
  void initState() {
    super.initState();
    attribute = widget.attribute;
  }

  @override
  Widget build(BuildContext context) {
    if (attribute.type == 'SUPPLIES') {
      return TextInput(
        suffix: Text(attribute?.supplies?.unit?.name ?? ''),
        labelText: attribute.display,
        value: attribute.value,
        enabled: widget.enabled,
      );
    }
    if (attribute.type == 'TEXT' || attribute.type == 'SELECTBOX') {
      return TextInput(
        labelText: attribute.display,
        value: attribute.value,
        enabled: widget.enabled,
      );
    }
    if (attribute.type == 'NUMBER') {
      return TextInput(
        labelText: attribute.display,
        value: MoneyInputFormatter.getMaskedValue(num.tryParse(attribute.value.toString()) ?? 0),
        enabled: widget.enabled,
        keyboardType: TextInputType.number,
        inputFormatters: [MoneyInputFormatter()],
      );
    }
    if (attribute.type == 'CHECKBOX' && attribute.value != null && attribute.value.length > 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              attribute.display,
              style: const TextStyle(color: Colors.black38, fontSize: 11),
            ),
          ),
          Wrap(
              children: attribute.value
                  .map<Widget>(
                    (item) => Padding(
                      padding: const EdgeInsets.all(5),
                      child: Chip(
                          backgroundColor: AppColor.primary,
                          label: Text(
                            item.toString(),
                            style: const TextStyle(color: Colors.white),
                          )),
                    ),
                  )
                  .toList()),
        ],
      );
    }

    if (attribute.type == 'IMAGES' && attribute.value != null && attribute.value.length > 0) {
      if ((attribute.value as List).length == 0) return Container();
      return SelectImageListInput(
        label: attribute.display,
        initialValue: attribute.value.cast<String>(),
        enabled: widget.enabled,
      );
    }

    return Container();
  }
}
