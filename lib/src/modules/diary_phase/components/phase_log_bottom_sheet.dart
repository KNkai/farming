import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';
import 'package:mi_smart/base_config/src/utils/validate_builder.dart';
import 'package:mi_smart/base_widget/base_widget.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/model/phase.model.dart';
import 'package:mi_smart/src/model/phase_log.model.dart';
import 'package:mi_smart/src/model/phase_log_attribute.model.dart';
import 'package:mi_smart/src/modules/diary_phase/components/phase_log_card_attribute_editable.dart';
import 'package:mi_smart/src/modules/diary_phase/controllers/phase_controller.dart';
import 'package:mi_smart/src/share/components/bottom_sheet_wrapper.dart';
import 'package:mi_smart/src/share/components/datepicker_input.dart';
import 'package:mi_smart/src/share/components/remove_button.dart';
import 'package:mi_smart/src/share/components/timepicker_input.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:provider/provider.dart';

class PhaseLogBottomSheet extends StatefulWidget {
  final bool isCreate;
  final PhaseLog phaseLog;
  const PhaseLogBottomSheet({
    Key key,
    this.isCreate = true,
    @required this.phaseLog,
  }) : super(key: key);

  @override
  _PhaseLogBottomSheetState createState() => _PhaseLogBottomSheetState();
}

class _PhaseLogBottomSheetState extends State<PhaseLogBottomSheet> {
  GlobalKey<FormState> phaseLogForm = new GlobalKey(debugLabel: "phaseLogForm");
  DateTime time, date;
  List<PhaseLogAttribute> attributes = [];

  @override
  void initState() {
    super.initState();
    time = DateTime.now();
    date = DateTime.now();
    attributes = widget.phaseLog.attributes;
  }

  @override
  Widget build(BuildContext context) {
    PhaseController controller = Provider.of<PhaseController>(context, listen: false);
    Phase phase = controller.phaseNotifer.value;
    return BottomSheetWrapper(
      appBar: buildSheetBar(
        context,
        title: "Lịch sử canh tác",
        btnTitle: widget.isCreate ? "Thêm mới" : "Lưu",
        onPress: () {
          if (phaseLogForm.currentState.validate()) {
            phaseLogForm.currentState.save();
            DateTime mergeTime = DateTime(date.year, date.month, date.day, time.hour, time.minute);
            if (widget.isCreate) {
              controller.createPhaseLog(attributes: attributes, time: mergeTime).then((res) {
                navigatorKey.currentState.pop(res);
              }).catchError((err) {
                showToast(err.toString(), flagColor: false);
              });
            } else {
              controller
                  .updatePhaseLog(phaseLogId: widget.phaseLog.id, attributes: attributes, time: mergeTime)
                  .then((res) {
                navigatorKey.currentState.pop(res);
              }).catchError((err) {
                showToast(err.toString(), flagColor: false);
              });
            }
          }
        },
      ),
      padding: const EdgeInsets.all(16),
      height: deviceHeight(context) - 85,
      formKey: phaseLogForm,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: TimepickerInput(
                labelText: "Thời gian",
                hintText: "Chọn thời gian",
                value: time,
                onSaved: (value) => time = value,
                validator: ValidateBuilder().empty().build(),
              ),
            ),
            const SpacingBox(width: 4),
            Expanded(
              child: DatepickerInput(
                labelText: "Ngày",
                hintText: "Chọn ngày",
                minTime: widget.phaseLog.createdAt,
                value: date,
                onSaved: (value) => date = value,
              ),
            ),
          ],
        ),
        const SpacingBox(height: 1),
        Text(
          'Dữ liệu canh tác'.toUpperCase(),
          style: const TextStyle(color: AppColor.primary, fontSize: 17, fontWeight: FontWeight.w700),
        ),
        const SpacingBox(height: 2),
        ...List.generate(attributes.length, (index) {
          var attr = attributes[index];
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PhaseLogCardAttributeEditable(
                attribute: attr,
                enabled: true,
                phaseAttribute: phase.attributes.firstWhere((element) => element.key == attr.key),
                onChanged: (value) => attributes[index] = value,
              ),
              const SpacingBox(height: 1),
            ],
          );
        }),
        if (!widget.isCreate) ...[
          const SpacingBox(height: 1),
          RemoveButton(
            label: "Xoá lịch sử canh tác",
            onRemove: () {
              controller.removePhaseLog(phaseLogId: widget.phaseLog.id).then((res) {
                navigatorKey.currentState.maybePop();
              }).catchError((err) {
                showToast(err.toString(), flagColor: false);
              });
            },
          ),
          const SpacingBox(height: 1),
        ]
      ],
    );
  }
}
