import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';

class TimelineCardWrapper extends StatelessWidget {
  final Color color;
  final Widget child;
  const TimelineCardWrapper({Key key, @required this.color, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 15,
                  width: 15,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: color,
                  ),
                ),
              ],
            ),
            const SpacingBox(width: 2),
            Expanded(child: child),
          ],
        ),
        Positioned(
          top: 14,
          left: 6,
          bottom: 0,
          child: Container(
            width: 3,
            decoration: const BoxDecoration(color: Colors.black12),
          ),
        )
      ],
    );
  }
}
