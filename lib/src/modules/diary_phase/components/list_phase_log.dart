import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../base_config/base_config.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../model/phase.model.dart';
import '../../../model/phase_log.model.dart';
import '../../../model/phase_log_attribute.model.dart';
import '../../../share/utils/color.dart';
import '../../../share/widget/empty.dart';
import '../controllers/phase_controller.dart';
import 'phase_log_bottom_sheet.dart';
import 'phase_log_card.dart';

class ListPhaseLog extends StatefulWidget {
  const ListPhaseLog();

  @override
  _ListPhaseLogState createState() => _ListPhaseLogState();
}

class _ListPhaseLogState extends State<ListPhaseLog> {
  PhaseController controller;
  Phase phase;

  @override
  void initState() {
    super.initState();
    controller = Provider.of<PhaseController>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'lịch sử canh tác'.toUpperCase(),
                style: TextStyle(color: Colors.black87.withOpacity(0.7), fontSize: 17, fontWeight: FontWeight.w700),
              ),
              IconButton(
                icon: const Icon(Icons.add, color: AppColor.primary),
                onPressed: () {
                  phase = controller.phaseNotifer.value;
                  showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      builder: (context) {
                        return ListenableProvider.value(
                          value: controller,
                          child: PhaseLogBottomSheet(
                              phaseLog: PhaseLog(
                            phaseId: phase.id,
                            attributes: phase.attributes
                                .map((e) => PhaseLogAttribute(
                                    min: e.min,
                                    max: e.max,
                                    key: e.key,
                                    display: e.display,
                                    type: e.type,
                                    supplies: e.supplies))
                                .toList(),
                          )),
                        );
                      });
                },
              )
            ],
          ),
          const SpacingBox(height: 3),
          ValueListenableBuilder<List<PhaseLog>>(
              valueListenable: controller.listPhaseLogNotifier,
              builder: (_, items, __) {
                if (items == null)
                  return Center(
                    child: kLoadingSpinner,
                  );
                if (items.length == 0) return const EmptyWidget(text: 'Chưa có giai đoạn canh tác nào');
                return Column(
                  children: List.generate(
                    items.length,
                    (index) => PhaseLogCard(key: ObjectKey(items[index].id), phaseLog: items[index]),
                  ),
                );
              })
        ],
      ),
    );
  }
}
