import 'package:flutter/material.dart';
import 'package:mi_smart/navigator.dart';
import 'package:provider/provider.dart';

import '../../../../base_config/base_config.dart';
import '../../../../base_config/src/utils/validate_builder.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../../base_widget/src/toast.dart';
import '../../../model/phase.model.dart';
import '../../../share/components/bottom_sheet_wrapper.dart';
import '../../../share/components/datepicker_input.dart';
import '../../../share/components/select_input.dart';
import '../../../share/components/select_list_sheet.dart';
import '../../../share/components/text_input.dart';
import '../../../share/model/enum.dart';
import '../controllers/phase_controller.dart';

class PhaseBottomSheet extends StatefulWidget {
  const PhaseBottomSheet();

  @override
  _PhaseBottomSheetState createState() => _PhaseBottomSheetState();
}

class _PhaseBottomSheetState extends State<PhaseBottomSheet> {
  GlobalKey<FormState> phaseForm = GlobalKey(debugLabel: "phaseForm");
  PhaseController controller;
  Phase phase;
  String name;
  DateTime startAt, endAt;
  String status;
  @override
  void initState() {
    super.initState();
    controller = Provider.of<PhaseController>(context, listen: false);
    phase = controller.phaseNotifer.value;
    name = phase.name;
    startAt = phase.startAt;
    endAt = phase.endAt;
    status = phase.status;
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheetWrapper(
      height: 400,
      appBar: buildSheetBar(context, title: "Giai đoạn canh tác", btnTitle: "Lưu", onPress: () {
        if (phaseForm.currentState.validate()) {
          phaseForm.currentState.save();
          controller.updatePhase(name: name, status: status, startAt: startAt, endAt: endAt).then((value) {
            navigatorKey.currentState.pop(value);
          }).catchError((err) => showToast("Có lỗi xảy ra: ${err.toString()}", flagColor: false));
        }
      }),
      padding: const EdgeInsets.all(16),
      formKey: phaseForm,
      children: [
        TextInput(
            labelText: "Tên giai đoạn",
            hintText: "Nhập tên giai đoạn",
            value: name,
            onSaved: (value) => name = value,
            validator: ValidateBuilder().empty().build()),
        const SpacingBox(height: 1),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: DatepickerInput(
                labelText: "Thời hạn từ",
                hintText: "Chọn ngày",
                maxTime: endAt,
                value: startAt,
                onSaved: (date) => startAt = date,
                validator: ValidateBuilder().empty().build(),
              ),
            ),
            const SpacingBox(width: 4),
            Expanded(
              child: DatepickerInput(
                labelText: "Thời hạn đến",
                hintText: "Chọn ngày",
                minTime: startAt,
                value: endAt,
                onSaved: (date) => endAt = date,
              ),
            ),
          ],
        ),
        const SpacingBox(height: 1),
        SelectListInput(
          sheetTitle: "Chọn trạng thái",
          labelText: "Trạng thái",
          selectedId: status,
          sheetItems:
              AppEnum.pharseStatusEnum.entries.map((e) => SelectListSheetItem(display: e.value, id: e.key)).toList(),
          onSaved: null,
          onSelect: (item) => status = item.id,
        ),
      ],
    );
  }
}
