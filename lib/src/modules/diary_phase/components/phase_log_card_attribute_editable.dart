import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/validate_builder.dart';
import 'package:mi_smart/src/share/components/select_image_list_input.dart';

import '../../../model/attribute.model.dart';
import '../../../model/phase_log_attribute.model.dart';
import '../../../share/components/select_chip_input.dart';
import '../../../share/components/select_input.dart';
import '../../../share/components/select_list_sheet.dart';
import '../../../share/components/text_input.dart';
import '../../../share/utils/money_input_formatter.dart';

class PhaseLogCardAttributeEditable extends StatefulWidget {
  final PhaseLogAttribute attribute;
  final Attribute phaseAttribute;
  final bool enabled;
  final Function(PhaseLogAttribute) onChanged;

  const PhaseLogCardAttributeEditable(
      {this.enabled = false, @required this.attribute, @required this.phaseAttribute, this.onChanged});

  @override
  _PhaseLogCardAttributeEditableState createState() => _PhaseLogCardAttributeEditableState();
}

class _PhaseLogCardAttributeEditableState extends State<PhaseLogCardAttributeEditable> {
  PhaseLogAttribute attribute;
  @override
  void initState() {
    super.initState();
    attribute = widget.attribute;
  }

  @override //
  Widget build(BuildContext context) {
    if (attribute.type == 'SUPPLIES') {
      return TextInput(
        labelText: attribute.display,
        hintText: "Nhập ${(attribute?.supplies?.unit?.name) ?? ''}",
        value: attribute.value,
        enabled: widget.enabled,
        keyboardType: TextInputType.number,
        suffix: Text(attribute?.supplies?.unit?.name ?? ''),
        onSaved: (value) {
          attribute.value = value;
          notifyOnChange();
        },
        validator: ValidateBuilder()
            .range(errMsg: 'Từ ${attribute?.min} tới ${attribute?.max}', min: attribute?.min, max: attribute?.max)
            .empty(skip: !widget.phaseAttribute.required)
            .build(),
      );
    }
    if (attribute.type == 'TEXT') {
      return TextInput(
        labelText: attribute.display,
        hintText: "Nhập ${attribute.display}",
        value: attribute.value,
        enabled: widget.enabled,
        onSaved: (value) {
          attribute.value = value;
          notifyOnChange();
        },
        validator: ValidateBuilder().empty(skip: !widget.phaseAttribute.required).build(),
      );
    }
    if (attribute.type == 'SELECTBOX') {
      return SelectListInput(
        labelText: attribute.display,
        hintText: "Chọn ${attribute.display}",
        sheetItems: widget.phaseAttribute.options.map((e) => SelectListSheetItem(display: e, id: e)).toList(),
        selectedId: attribute.value,
        onSaved: (value) {
          attribute.value = value;
          notifyOnChange();
        },
        validator: ValidateBuilder().empty(skip: !widget.phaseAttribute.required).build(),
      );
    }
    if (attribute.type == 'NUMBER') {
      return TextInput(
        labelText: attribute.display,
        hintText: "Nhập ${attribute.display}",
        value: MoneyInputFormatter.getMaskedValue(attribute.value),
        enabled: widget.enabled,
        keyboardType: TextInputType.number,
        inputFormatters: [MoneyInputFormatter()],
        onSaved: (value) {
          attribute.value = num.tryParse(MoneyInputFormatter.getUnMaskedValue(value));
          notifyOnChange();
        },
        validator: ValidateBuilder()
            .empty(skip: !widget.phaseAttribute.required)
            .transform((value) => MoneyInputFormatter.getUnMaskedValue(value))
            .range(min: widget.phaseAttribute.min, max: widget.phaseAttribute.max)
            .build(),
      );
    }
    if (attribute.type == 'CHECKBOX') {
      attribute.value = attribute.value ?? [];
      return SelectChipInput(
        labelText: attribute.display,
        chipItems: widget.phaseAttribute.options
            .map((e) => SelectChipItem(id: e, display: e, selected: (attribute.value as List).contains(e)))
            .toList(),
        onSaved: (value) {
          attribute.value = value;
          notifyOnChange();
        },
        validator: ValidateBuilder().empty(skip: !widget.phaseAttribute.required).build(),
      );
    }

    if (attribute.type == 'IMAGES') {
      attribute.value = attribute.value ?? [];
      return SelectImageListInput(
        label: attribute.display,
        initialValue: attribute.value.cast<String>(),
        enabled: widget.enabled,
        onChanged: (value) {
          attribute.value = value;
          notifyOnChange();
        },
        validator: ValidateBuilder().empty(skip: !widget.phaseAttribute.required).build(),
      );
    }

    return Container();
  }

  void notifyOnChange() {
    if (widget.onChanged != null) widget.onChanged(attribute);
  }
}
