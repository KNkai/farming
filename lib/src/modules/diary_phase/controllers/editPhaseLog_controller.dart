import 'package:flutter/material.dart';

import '../../../../base_config/base_config.dart';
import '../../../share/repo/phaseLog_repo.dart';

class EditPhaseLogController extends ChangeNotifier {
  dynamic phaseLog;
  dynamic phase;
  Function reload;
  EditPhaseLogController(this.phaseLog, this.reload, {this.phase});

  // data
  List listSettingAttribute = [];
  List listAttribute = [];

  // for log info
  TextEditingController dateC = TextEditingController(text: Formart.formatToDate(DateTime.now().toString()));
  TextEditingController timeC = TextEditingController(text: Formart.formatTime(DateTime.now().toString()));
  DateTime date = DateTime.now();
  DateTime time = DateTime.now();
  List mergedAttribute = [];

  //method
  reloadUi() {
    reload();
  }

  onChangeText(String key, String text) {
    final index = mergedAttribute.indexWhere((element) => element['key'] == key);
    if (mergedAttribute[index]['type'] == 'TEXT') {
      mergedAttribute[index]['value']['value'] = text;
    }
    if (mergedAttribute[index]['type'] == 'NUMBER') {
      mergedAttribute[index]['value']['value'] = double.parse(text);
    }
  }

  onChangeOne(String key, String value) {
    final index = mergedAttribute.indexWhere((element) => element['key'] == key);
    if (mergedAttribute[index]['type'] == 'SELECTBOX') {
      if (mergedAttribute[index]['value'] == null) {
        mergedAttribute[index]['value'] = {
          'type': 'SELECTBOX',
          'display': 'SELECTBOX',
          'value': null,
          'options': [],
        };
      }
      mergedAttribute[index]['value']['value'] = value;
    }
    reloadUi();
  }

  onChangeMulti(String key, String value, bool flag) {
    final index = mergedAttribute.indexWhere((element) => element['key'] == key);
    if (mergedAttribute[index]['type'] == 'CHECKBOX') {
      if (flag)
        (mergedAttribute[index]['value']['value'] as List).remove(value);
      else {
        if (!(mergedAttribute[index]['value']['value'] as List).contains(value)) {
          (mergedAttribute[index]['value']['value'] as List).add(value);
        }
      }
    }
    reloadUi();
  }

  onChangeImage(String key, List<String> values) {
    final index = mergedAttribute.indexWhere((element) => element['key'] == key);
    mergedAttribute[index]['value']['value'] = values.toList();
  }

  getAllFarmingDiaryPhaseLog() async {
    print('getAllFarmingDiaryPhaseLog');
    // if (phase != null) {
    //   //create
    //   listAttribute.clear();
    //   listSettingAttribute.clear();
    //   listAttribute.addAll(phase["attributes"]);
    //   listSettingAttribute.addAll(phase["attributes"]);
    //   mergeAttributeCreate();
    // } else {
    //   //update
    //   listAttribute = phaseLog['attributes'];
    //   phase = await DiaryPharseRepo().getOne(phaseLog["phaseId"]);
    //   listSettingAttribute.clear();
    //   listSettingAttribute.addAll(phase["attributes"]);
    //   mergeAttributeEdit();
    //   date = DateTime.tryParse(phaseLog["time"]).toLocal();
    //   time = DateTime.tryParse(phaseLog["time"]).toLocal();
    //   timeC.text = Formart.formatTime(phaseLog["time"]);
    //   dateC.text = Formart.formatToDate(phaseLog["time"]);
    // }
    // reloadUi();
  }

  mergeAttributeCreate() async {
    mergedAttribute = listSettingAttribute.map((e) {
      if (e["type"] == 'TEXT') {
        return {
          'key': e['key'],
          'type': 'TEXT',
          'display': e['display'],
          'value': {
            'type': 'TEXT',
            'display': e['display'],
            'value': null,
          },
        };
      }
      if (e["type"] == 'NUMBER') {
        return {
          'key': e['key'],
          'type': 'NUMBER',
          'display': e['display'],
          'min': e['min'],
          'max': e['max'],
          'value': {
            'type': 'TEXT',
            'display': e['display'],
            'value': null,
          },
        };
      }
      if (e["type"] == 'SELECTBOX') {
        return {
          'key': e['key'],
          'type': 'SELECTBOX',
          'display': e['display'],
          'options': e['options'],
          'value': {
            'type': 'SELECTBOX',
            'display': e['display'],
            'value': null,
            'options': [],
          },
        };
      }
      if (e["type"] == 'CHECKBOX') {
        return {
          'key': e['key'],
          'type': 'CHECKBOX',
          'display': e['display'],
          'options': e['options'],
          'value': {
            'type': 'CHECKBOX',
            'display': e['display'],
            'value': [],
            'options': [],
          },
        };
      }
      if (e["type"] == 'IMAGES') {
        return {
          'key': e['key'],
          'type': e["type"],
          'display': e['display'],
          'value': {
            'type': e["type"],
            'display': e['display'],
            'value': [],
          }
        };
      }
    }).toList();
    print(mergedAttribute);
  }

  mergeAttributeEdit() async {
    mergedAttribute = listSettingAttribute.map<Map<String, dynamic>>((e) {
      var value = listAttribute.firstWhere(
        (element) => element['key'] == e['key'],
        orElse: () => null,
      );

      if (value == null) {
        value = {
          'key': e['key'],
          'type': e["type"],
          'display': e['display'],
          'options': e['options'],
          'min': e['min'],
          'max': e['max'],
          'value': null,
        };
      }

      if (e["type"] == 'TEXT') {
        return {
          'key': e['key'],
          'type': 'TEXT',
          'display': e['display'],
          'value': value,
        };
      }
      if (e["type"] == 'NUMBER') {
        return {
          'key': e['key'],
          'type': 'NUMBER',
          'display': e['display'],
          'min': e['min'],
          'max': e['max'],
          'value': value,
        };
      }
      if (e["type"] == 'CHECKBOX') {
        return {
          'key': e['key'],
          'type': 'CHECKBOX',
          'display': e['display'],
          'options': e['options'],
          'value': value,
        };
      }
      if (e["type"] == 'SELECTBOX') {
        return {
          'key': e['key'],
          'type': e["type"],
          'display': e['display'],
          'options': e['options'],
          'value': value,
        };
      }
      if (e["type"] == 'IMAGES') {
        return {'key': e['key'], 'type': e["type"], 'display': e['display'], 'value': value};
      }
      return {};
    }).toList();
    print(mergedAttribute);
  }

  Future updateLog() async {
    try {
      DateTime mergeTime = DateTime(date.year, date.month, date.day, time.hour, time.minute);

      await PhaseLogRepo().updatePhaseLog(phaseLog['id'], mergeTime.toUtc().toString(), attributeToString());
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future createLog() async {
    try {
      DateTime mergeTime = DateTime(date.year, date.month, date.day, date.hour, date.minute);
      await PhaseLogRepo().createPhaseLog(
        phase['id'],
        mergeTime.toUtc().toString(),
        attributeToString(),
      );
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future deleteLog() async {
    try {
      await PhaseLogRepo().deletePhaseLog(phaseLog['id']);
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  String attributeToString() {
    String res = '[';
    mergedAttribute.forEach((element) {
      res += '''{
        display: "${element['display']}"
        key: "${element['key']}"
        value: ${Formart.dynamicToGraphqlString(Formart.getMapElement(element, ["value", "value"]))}
        type: "${element['type']}"
      }''';
    });
    res += ']';
    return res;
  }
}
