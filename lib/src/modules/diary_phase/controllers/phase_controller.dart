import 'package:mi_smart/src/model/phase_log_attribute.model.dart';
import 'package:mi_smart/src/share/model/pagination.dart';
import 'package:mi_smart/src/share/utils/util.dart';

import '../../../../base_config/base_config.dart';
import '../../../model/phase.model.dart';
import '../../../model/phase_log.model.dart';
import '../../../share/repo/diaryPharse_repo.dart';
import '../../../share/repo/phaseLog_repo.dart';

class PhaseController extends ChangeNotifier {
  final String phaseId;
  // data
  ValueNotifier<Phase> phaseNotifer = ValueNotifier(null);
  ValueNotifier<List<PhaseLog>> listPhaseLogNotifier = ValueNotifier(null);
  Pagination phaseLogPaging;
  bool isLoadingListPhaseLog = false;
  PhaseController({@required this.phaseId}) {
    loadPhase();
    loadPhaseLogs();
  }

  void loadMorePhaseLogs() async {
    if (phaseLogPaging.total == null) {
      loadPhaseLogs();
    } else {
      if (phaseLogPaging.total > listPhaseLogNotifier.value.length && !isLoadingListPhaseLog) {
        isLoadingListPhaseLog = true;
        try {
          phaseLogPaging.page++;
          listPhaseLogNotifier.value = [
            ...listPhaseLogNotifier.value,
            ...await PhaseLogRepo()
                .getFarmingDiaryPhaseLog(
                  phaseId,
                  paging: phaseLogPaging,
                  onPaging: (paging) => phaseLogPaging = paging,
                )
                .then((res) => res.map((e) => PhaseLog.fromJson(e)).toList())
          ];
        } finally {
          isLoadingListPhaseLog = false;
        }
      }
    }
  }

  void loadPhaseLogs() {
    phaseLogPaging = Pagination(limit: 5, page: 1);
    PhaseLogRepo()
        .getFarmingDiaryPhaseLog(
      phaseId,
      paging: phaseLogPaging,
      onPaging: (paging) => phaseLogPaging = paging,
    )
        .then((res) {
      listPhaseLogNotifier.value = res.map((e) => PhaseLog.fromJson(e)).toList();
      listPhaseLogNotifier.notifyListeners();
    });
  }

  void loadPhase() {
    DiaryPharseRepo().getOne(phaseId).then((res) {
      phaseNotifer.value = Phase.fromJson(res);
      phaseNotifer.notifyListeners();
    });
  }

  Future updatePhase({String name, DateTime startAt, DateTime endAt, String status}) {
    return DiaryPharseRepo().updatePhase(phaseId, name, status, startAt.toString(), endAt.toString()).then((res) {
      phaseNotifer.value = Phase.fromJson(res);
      phaseNotifer.notifyListeners();
      return phaseNotifer.value;
    });
  }

  Future<PhaseLog> createPhaseLog({DateTime time, List<PhaseLogAttribute> attributes}) {
    var phase = phaseNotifer.value;
    List attrString = attributes.map((a) => '{${parseJsonToFilter(a.toJson())}}').toList();
    return PhaseLogRepo().createPhaseLog(phase.id, time.toString(), '[${attrString.join(",")}]').then((value) {
      var phaseLog = PhaseLog.fromJson(value);
      listPhaseLogNotifier.value.insert(0, phaseLog);
      listPhaseLogNotifier.notifyListeners();
      return phaseLog;
    });
  }

  Future<PhaseLog> updatePhaseLog({String phaseLogId, DateTime time, List<PhaseLogAttribute> attributes}) {
    List attrString = attributes.map((a) => '{${parseJsonToFilter(a.toJson())}}').toList();
    return PhaseLogRepo().updatePhaseLog(phaseLogId, time.toString(), '[${attrString.join(",")}]').then((value) {
      var phaseLog = PhaseLog.fromJson(value);
      var index = listPhaseLogNotifier.value.indexWhere((element) => element.id == phaseLogId);
      listPhaseLogNotifier.value[index] = phaseLog;
      listPhaseLogNotifier.notifyListeners();
      return phaseLog;
    });
  }

  Future removePhaseLog({String phaseLogId}) {
    return PhaseLogRepo().deletePhaseLog(phaseLogId).then((res) {
      listPhaseLogNotifier.value.removeWhere((element) => element.id == phaseLogId);
      listPhaseLogNotifier.notifyListeners();
      return res;
    });
  }
}
