/// Donut chart example. This is a simple pie chart with a hole in the middle.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:mi_smart/src/modules/chart/model/chart_model.dart';
import 'package:mi_smart/src/modules/chart/model/wCircleChart_model.dart';

class DonutPieChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  const DonutPieChart(this.seriesList, {this.animate});

  /// Creates a [PieChart] with sample data and no transition.
  factory DonutPieChart.fromWCircleChart(WCircleChart chart) {
    return new DonutPieChart(
      createData(chart),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return charts.PieChart(
      seriesList,
      animate: animate,
      behaviors: [
        charts.DatumLegend(
          desiredMaxColumns: 3,
          outsideJustification: charts.OutsideJustification.endDrawArea,
          entryTextStyle: const charts.TextStyleSpec(color: charts.Color(r: 127, g: 63, b: 191), fontSize: 11),
        ),
      ],

      // Configure the width of the pie slices to 60px. The remaining space in
      // the chart will be left as a hole in the center.
      defaultRenderer: charts.ArcRendererConfig(arcWidth: 60, arcRendererDecorators: [new charts.ArcLabelDecorator()]),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<DPointModel, String>> createData(WCircleChart chart) {
    final List<charts.Series<DPointModel, String>> result = [];

    if (chart.dataChart.data.length == 0) return result;

    result.add(charts.Series<DPointModel, String>(
      id: chart.dataChart.data[0].key,
      domainFn: (DPointModel value, _) => value.key,
      measureFn: (DPointModel value, _) => value.value,
      colorFn: (DPointModel value, _) => charts.Color.fromHex(code: value.color ?? '#E74C3C'),
      labelAccessorFn: (DPointModel value, _) => '${value.value}',
      data: chart.dataChart.data,
    ));

    return result;
  }
}
