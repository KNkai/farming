/// Example of an ordinal combo chart with two series rendered as bars, and a
/// third rendered as a line.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/modules/chart/model/chart_model.dart';
import 'package:mi_smart/src/modules/chart/model/wColumnLineChart_model.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/hex_color.dart';

class OrdinalComboBarLineChart extends StatefulWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final WColumnLineChart chartData;

  const OrdinalComboBarLineChart(this.seriesList, {this.animate, this.chartData});

  factory OrdinalComboBarLineChart.fromWColumnLineChart(WColumnLineChart chart) {
    return OrdinalComboBarLineChart(
      createData(chart),
      // Disable animations for image tests.
      chartData: chart,
    );
  }

  @override
  _OrdinalComboBarLineChartState createState() => _OrdinalComboBarLineChartState();

  /// Create series list with multiple series
  static List<charts.Series<DPointModel, String>> createData(WColumnLineChart chart) {
    //warning!!! this is hard code mapping base on backend return data, it may hard to read
    final List<charts.Series<DPointModel, String>> result = [];

    if (chart.listColumnChart.length == 0) return result;

    for (int j = 0; j < chart.listColumnChart[0].data.length; j++) {
      List<DPointModel> listD = <DPointModel>[];
      for (int i = 0; i < chart.listColumnChart.length; i++) {
        listD.add(chart.listColumnChart[i].data[j]);
      }
      result.add(charts.Series<DPointModel, String>(
        id: chart.columnLabel[j],
        domainFn: (DPointModel value, _) => value.key,
        measureFn: (DPointModel value, _) => value.value,
        colorFn: (DPointModel value, _) => charts.Color.fromHex(code: value.color),
        data: listD,
      ));
    }

    for (int j = 0; j < chart.listLineChart[0].data.length; j++) {
      List<DPointModel> listD = <DPointModel>[];
      for (int i = 0; i < chart.listLineChart.length; i++) {
        listD.add(chart.listLineChart[i].data[j]);
      }
      result.add(charts.Series<DPointModel, String>(
        id: chart.lineLabel[j],
        domainFn: (DPointModel value, _) => value.key,
        measureFn: (DPointModel value, _) => value.value,
        colorFn: (DPointModel value, _) => charts.Color.fromHex(code: value.color),
        data: listD,
      )..setAttribute(charts.rendererIdKey, 'customLine'));
    }

    return result;
  }
}

class _OrdinalComboBarLineChartState extends State<OrdinalComboBarLineChart> {
  double tapY;
  int selectedIndex;
  GlobalKey colLineKey = GlobalKey();
  bool lockReload = false; //prevent hit chart to fast

  onTapUp() {
    RenderBox box = colLineKey.currentContext.findRenderObject();
    Offset position = box.localToGlobal(Offset.zero);
    tapY = position.dy - 90;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      key: colLineKey,
      children: [
        charts.OrdinalComboChart(
          widget.seriesList,
          animate: false,
          // Configure the default renderer as a bar renderer.
          defaultRenderer: charts.BarRendererConfig(groupingType: charts.BarGroupingType.grouped),
          // Custom renderer configuration for the line series. This will be used for
          // any series that does not define a rendererIdKey.
          customSeriesRenderers: [
            charts.LineRendererConfig(
                // ID used to link series to this renderer.
                customRendererId: 'customLine'),
          ],
          domainAxis: charts.OrdinalAxisSpec(viewport: charts.OrdinalViewport('key', widget.chartData.zoom ?? 5)),
          behaviors: [
            charts.SeriesLegend(
              outsideJustification: charts.OutsideJustification.endDrawArea,
              desiredMaxColumns: 3,
              entryTextStyle: const charts.TextStyleSpec(color: charts.Color(r: 127, g: 63, b: 191), fontSize: 11),
              // showMeasures: true,
              // legendDefaultMeasure: charts.LegendDefaultMeasure.sum,
              // measureFormatter: (num value) {
              //   return ': ${value.round()}';
              // },
            ),
            charts.PanAndZoomBehavior(),
          ],
          primaryMeasureAxis:
              const charts.NumericAxisSpec(tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 3)),
          secondaryMeasureAxis:
              const charts.NumericAxisSpec(tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 3)),
          selectionModels: [
            charts.SelectionModelConfig(changedListener: (charts.SelectionModel model) {
              if (lockReload) return;
              if (model.hasDatumSelection) {
                print(model.selectedDatum[0].index);
                setState(() {
                  selectedIndex = model.selectedDatum[0].index;
                  onTapUp();
                  lockReload = true;
                });
                Future.delayed(const Duration(milliseconds: 600), () {
                  lockReload = false;
                });
              }
            })
          ],
        ),
        if (tapY != null)
          Positioned(
            top: 90,
            right: 0,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: AppColor.primary),
                color: AppColor.primary.withOpacity(0.2),
              ),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.chartData.listLineChart[selectedIndex].data
                        .map((e) => Row(
                              children: [
                                Container(
                                  height: 3,
                                  width: 12,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(3),
                                    color: Hexcolor(e.color ?? '#FFFFFF'),
                                  ),
                                ),
                                const SpacingBox(
                                  width: 2,
                                ),
                                Text(e.value.toString()),
                              ],
                            ))
                        .toList(),
                  ),
                  const SpacingBox(
                    width: 5,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.chartData.listColumnChart[selectedIndex].data
                        .map((e) => Row(
                              children: [
                                Container(
                                  height: 10,
                                  width: 10,
                                  decoration: BoxDecoration(
                                    color: Hexcolor(e.color ?? '#FFFFFF'),
                                  ),
                                ),
                                const SpacingBox(
                                  width: 2,
                                ),
                                Text(e.value.toString()),
                              ],
                            ))
                        .toList(),
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}

// /// Bar chart with series legend example
// import 'package:flutter/material.dart';
// import 'package:charts_flutter/flutter.dart' as charts;
// import 'package:mi_smart/src/modules/chart/model/chart_model.dart';
// import 'package:mi_smart/src/modules/chart/model/wColumnLineChart_model.dart';
// import 'package:mi_smart/src/share/utils/hex_color.dart';

// class OrdinalComboBarLineChart extends StatefulWidget {
//   final WColumnLineChart chart;
//   final bool animate;

//   OrdinalComboBarLineChart({this.chart, this.animate});

//   @override
//   State<StatefulWidget> createState() => _OrdinalComboBarLineChartState();
// }

// class _OrdinalComboBarLineChartState extends State<OrdinalComboBarLineChart> {
//   List<charts.Series<DPointModel, String>> seriesList;
//   int selectedIndex = 0;

//   @override
//   Widget build(BuildContext context) {
//     bool checkCol = widget.chart.listColumnChart != null && widget.chart.listColumnChart.length > 0;
//     bool checkLine = widget.chart.listLineChart != null && widget.chart.listLineChart.length > 0;
//     seriesList = [];
//     if (checkCol) {
//       for (int i = 0; i < widget.chart.listColumnChart.length; i++) {
//         seriesList.add(charts.Series<DPointModel, String>(
//             id: widget.chart.columnLabel[i],
//             colorFn: (_, __) => charts.ColorUtil.fromDartColor(Hexcolor(widget.chart.columnColor[i])),
//             labelAccessorFn: (DPointModel point, _) => '${point.key}',
//             domainFn: (DPointModel point, _) => point.key,
//             measureFn: (DPointModel point, _) => point.value,
//             data: widget.chart.listColumnChart[i].data));
//       }
//     }
//     if (checkLine) {
//       for (int i = 0; i < widget.chart.listLineChart.length; i++) {
//         seriesList.add(
//           charts.Series<DPointModel, String>(
//               labelAccessorFn: (DPointModel point, _) => '${point.key}',
//               id: widget.chart.lineLabel[i],
//               colorFn: (_, __) => charts.ColorUtil.fromDartColor(Hexcolor(widget.chart.lineColor[i])),
//               domainFn: (DPointModel point, _) => point.key,
//               measureFn: (DPointModel point, _) => point.value.toInt(),
//               data: widget.chart.listLineChart[i].data)
//             ..setAttribute(charts.rendererIdKey, 'customLine'),
//         );
//       }
//     }
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.end,
//       children: <Widget>[
//         // Row(
//         //   children: [
//         //     Padding(
//         //       padding: const EdgeInsets.only(left: 30.0),
//         //       child: widget.chart.listColumnChart != null && widget.chart.listColumnChart.length > 0
//         //           ? Column(
//         //               crossAxisAlignment: CrossAxisAlignment.start,
//         //               children: widget.chart.listColumnChart.map<Widget>((e) {
//         //                 int index = widget.chart.listColumnChart.indexOf(e);
//         //                 return Padding(
//         //                   padding: const EdgeInsets.all(3.0),
//         //                   child: Row(
//         //                     mainAxisAlignment: MainAxisAlignment.start,
//         //                     children: <Widget>[
//         //                       Container(
//         //                         height: 12,
//         //                         width: 12,
//         //                         color: Hexcolor(widget.chart.columnColor[index]),
//         //                       ),
//         //                       SpacingBox(width: 2),
//         //                       Text(
//         //                         widget.chart.columnLabel[index] +
//         //                             ': ' +
//         //                             Formart.formatNumber(
//         //                                 widget.chart.listColumnChart[index].data[selectedIndex].value) +
//         //                             ' ',
//         //                         //widget.chart.unit,
//         //                         style: TextStyle(fontSize: 12),
//         //                       ),
//         //                     ],
//         //                   ),
//         //                 );
//         //               }).toList())
//         //           : SizedBox.shrink(),
//         //     ),
//         //     Padding(
//         //       padding: const EdgeInsets.only(left: 30.0),
//         //       child: widget.chart.listLineChart != null && widget.chart.listLineChart.length > 0
//         //           ? Column(
//         //               crossAxisAlignment: CrossAxisAlignment.start,
//         //               children: widget.chart.listLineChart.map<Widget>((e) {
//         //                 int index = widget.chart.listLineChart.indexOf(e);
//         //                 return Padding(
//         //                   padding: const EdgeInsets.all(3.0),
//         //                   child: Row(
//         //                     mainAxisAlignment: MainAxisAlignment.start,
//         //                     children: <Widget>[
//         //                       Container(
//         //                         height: 12,
//         //                         width: 12,
//         //                         color: Hexcolor(widget.chart.lineColor[index]),
//         //                       ),
//         //                       SpacingBox(width: 2),
//         //                       Text(
//         //                         widget.chart.lineLabel[index] +
//         //                             ': ' +
//         //                             Formart.formatNumber(widget.chart.listLineChart[index].data[selectedIndex].value) +
//         //                             ' ',
//         //                         //widget.chart.unit,
//         //                         style: TextStyle(fontSize: 12),
//         //                       ),
//         //                     ],
//         //                   ),
//         //                 );
//         //               }).toList())
//         //           : SizedBox.shrink(),
//         //     ),
//         //   ],
//         // ),
//         Container(
//           height: MediaQuery.of(context).size.height * 0.45,
//           child: charts.OrdinalComboChart(
//             seriesList,
//             animate: widget.animate,
//             behaviors: [
//               // charts.SeriesLegend(
//               //   outsideJustification: charts.OutsideJustification.endDrawArea,
//               //   desiredMaxColumns: 3,
//               //   entryTextStyle: charts.TextStyleSpec(color: charts.Color(r: 127, g: 63, b: 191), fontSize: 11),
//               //   showMeasures: true,
//               //   legendDefaultMeasure: charts.LegendDefaultMeasure.lastValue,
//               //   measureFormatter: (num value) {
//               //     return '${value.round()} ${widget.chart.unit}';
//               //   },
//               // ),
//               charts.PanAndZoomBehavior(),
//             ],
//             defaultRenderer: charts.BarRendererConfig(groupingType: charts.BarGroupingType.grouped),
//             domainAxis: charts.OrdinalAxisSpec(
//                 renderSpec: charts.GridlineRendererSpec(
//                   labelStyle: charts.TextStyleSpec(fontSize: 12, color: charts.MaterialPalette.black),
//                 ),
//                 viewport: checkCol
//                     ? charts.OrdinalViewport('${widget.chart.listColumnChart[0].data[0].key}', widget.chart.zoom ?? 5)
//                     : charts.OrdinalViewport('${widget.chart.listLineChart[0].data[0].key}', widget.chart.zoom ?? 5)),
//             customSeriesRenderers: [
//               charts.LineRendererConfig(customRendererId: 'customLine'),
//             ],
//             selectionModels: [
//               charts.SelectionModelConfig(changedListener: (charts.SelectionModel model) {
//                 if (model.hasDatumSelection) {
//                   print(model.selectedDatum[0].index);
//                   setState(() {
//                     selectedIndex = model.selectedDatum[0].index;
//                   });
//                 }
//               })
//             ],
//           ),
//         ),
//       ],
//     );
//   }
// }
