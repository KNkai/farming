import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/share/utils/color.dart';

import '../chart_bloc.dart';

class ChartTypeBtn extends StatefulWidget {
  final Function(String) onSubmit;
  final List values; // values of ChartType selection
  final ChartBloc bloc;
  const ChartTypeBtn(this.onSubmit, this.values, this.bloc);

  @override
  _ChartTypeBtnState createState() => _ChartTypeBtnState();
}

class _ChartTypeBtnState extends State<ChartTypeBtn> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  String selectedValue;
  String currentValue;
  String selectedTypeName;

  @override
  void initState() {
    if (widget.values.length > 0) {
      currentValue = widget.values[0]["code"].toString();
      selectedValue = widget.values[0]["code"].toString();
      widget.onSubmit(selectedValue);
      selectedTypeName = widget.values[0]['title'];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (currentValue != null) {
      final item =
          widget.values.firstWhere((element) => element["title"].toString() == currentValue, orElse: () => null);
      if (item != null) {
        selectedTypeName = item['title'];
      }
    }
    return GestureDetector(
      onTap: () => _showBotomSheet((val) => setState(() {
            currentValue = val;
            selectedTypeName = val;
          })),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            'Loại biểu đồ: ',
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
          ),
          Expanded(
            child: Text(selectedTypeName ?? '',
                style: const TextStyle(color: AppColor.primary, fontSize: 13.3, fontWeight: FontWeight.bold)),
          ),
          const Icon(
            Icons.arrow_drop_down,
            color: Colors.black38,
          ),
        ],
      ),
    );
  }

  _showBotomSheet(Function setStateCallback) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) => ConstrainedBox(
        constraints: BoxConstraints(maxHeight: deviceHeight(context) - 85),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              color: Colors.white,
              padding: const EdgeInsets.all(9),
              child: ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  physics: const AlwaysScrollableScrollPhysics(),
                  itemCount: widget.values.length,
                  itemBuilder: (context, index) {
                    final item = widget.values[index];
                    return ListTile(
                        title: Text(item["title"]),
                        onTap: () {
                          selectedValue = item['title'];
                          setStateCallback(selectedValue);
                          navigatorKey.currentState.maybePop();
                          widget.onSubmit(item['code']);
                        });
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
