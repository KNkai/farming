/// Bar chart example
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/modules/chart/model/chart_model.dart';
import 'package:mi_smart/src/modules/chart/model/wColumnChart_model.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/hex_color.dart';

class ColumnChart extends StatefulWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final WColumnChart chartData;

  const ColumnChart(this.seriesList, {this.animate, this.chartData});

  factory ColumnChart.fromWColumnChart(WColumnChart chart) {
    return ColumnChart(
      createData(chart),
      // Disable animations for image tests.
      animate: false,
      chartData: chart,
    );
  }

  @override
  _ColumnChartState createState() => _ColumnChartState();

  /// Create series list with multiple series
  static List<charts.Series<DPointModel, String>> createData(WColumnChart chart) {
    //warning!!! this is hard code mapping base on backend return data, it may hard to read
    final List<charts.Series<DPointModel, String>> result = [];

    if (chart.listColumnChart.length == 0) return result;

    for (int j = 0; j < chart.listColumnChart[0].data.length; j++) {
      List<DPointModel> listD = <DPointModel>[];
      for (int i = 0; i < chart.listColumnChart.length; i++) {
        listD.add(chart.listColumnChart[i].data[j]);
      }
      result.add(charts.Series<DPointModel, String>(
        id: chart.columnLabel[j],
        domainFn: (DPointModel value, _) => value.key,
        measureFn: (DPointModel value, _) => value.value,
        colorFn: (DPointModel value, _) => charts.Color.fromHex(code: value.color),
        data: listD,
      ));
    }

    return result;
  }
}

class _ColumnChartState extends State<ColumnChart> {
  double tapY;
  int selectedIndex;
  GlobalKey colKey = GlobalKey();
  bool lockReload = false; //prevent hit chart to fast

  onTapUp() {
    RenderBox box = colKey.currentContext.findRenderObject();
    Offset position = box.localToGlobal(Offset.zero);
    tapY = position.dy - 90;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      key: colKey,
      children: [
        charts.BarChart(
          widget.seriesList,
          animate: widget.animate,
          barGroupingType: charts.BarGroupingType.grouped,
          domainAxis: charts.OrdinalAxisSpec(viewport: charts.OrdinalViewport('key', widget.chartData.zoom)),
          behaviors: [
            charts.PanAndZoomBehavior(),
            charts.SeriesLegend(
              desiredMaxColumns: 3,
              outsideJustification: charts.OutsideJustification.endDrawArea,
              entryTextStyle: const charts.TextStyleSpec(color: charts.Color(r: 127, g: 63, b: 191), fontSize: 11),
            ),
          ],
          selectionModels: [
            charts.SelectionModelConfig(changedListener: (charts.SelectionModel model) {
              if (lockReload) return;
              if (model.hasDatumSelection) {
                print(model.selectedDatum[0].index);
                setState(() {
                  selectedIndex = model.selectedDatum[0].index;
                  onTapUp();
                  lockReload = true;
                });
                Future.delayed(const Duration(milliseconds: 600), () {
                  lockReload = false;
                });
              }
            })
          ],
          primaryMeasureAxis:
              const charts.NumericAxisSpec(tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 3)),
          secondaryMeasureAxis:
              const charts.NumericAxisSpec(tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 3)),
        ),
        if (tapY != null)
          Positioned(
            top: 90,
            right: 0,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: AppColor.primary),
                color: AppColor.primary.withOpacity(0.2),
              ),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.chartData.listColumnChart[selectedIndex].data
                        .map((e) => Row(
                              children: [
                                Container(
                                  height: 10,
                                  width: 10,
                                  decoration: BoxDecoration(
                                    color: Hexcolor(e.color ?? '#FFFFFF'),
                                  ),
                                ),
                                const SpacingBox(
                                  width: 2,
                                ),
                                Text(e.value.toString()),
                              ],
                            ))
                        .toList(),
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}
