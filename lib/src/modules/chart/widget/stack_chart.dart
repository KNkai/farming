/// Bar chart example
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/modules/chart/model/chart_model.dart';
import 'package:mi_smart/src/modules/chart/model/wStackChart_model.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/hex_color.dart';

class StackedHorizontalChart extends StatefulWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final WStackChart chartData;

  const StackedHorizontalChart(this.seriesList, {this.animate, this.chartData});

  /// Creates a stacked [BarChart] with sample data and no transition.
  factory StackedHorizontalChart.fromStackChart(WStackChart chart) {
    return StackedHorizontalChart(
      createData(chart),
      // Disable animations for image tests.
      animate: false,
      chartData: chart,
    );
  }

  @override
  _StackedHorizontalChartState createState() => _StackedHorizontalChartState();

  /// Create series list with multiple series
  static List<charts.Series<DPointModel, String>> createData(WStackChart chart) {
    //warning!!! this is hard code mapping base on backend return data, it may hard to read
    final List<charts.Series<DPointModel, String>> result = [];

    if (chart.listColChart.length == 0) return result;

    for (int j = 0; j < chart.listColChart[0].data.length; j++) {
      List<DPointModel> listD = <DPointModel>[];
      for (int i = 0; i < chart.listColChart.length; i++) {
        listD.add(chart.listColChart[i].data[j]);
      }
      result.add(charts.Series<DPointModel, String>(
        id: chart.colLabel[j],
        domainFn: (DPointModel value, _) => value.key,
        measureFn: (DPointModel value, _) => value.value,
        colorFn: (DPointModel value, _) => charts.Color.fromHex(code: value.color),
        data: listD,
      ));
    }

    return result;
  }
}

class _StackedHorizontalChartState extends State<StackedHorizontalChart> {
  double tapY;
  int selectedIndex;
  GlobalKey colKey = GlobalKey();
  bool lockReload = false; //prevent hit chart to fast

  onTapUp() {
    RenderBox box = colKey.currentContext.findRenderObject();
    Offset position = box.localToGlobal(Offset.zero);
    tapY = position.dy - 90;
  }

  @override
  Widget build(BuildContext context) {
    // For horizontal bar charts, set the [vertical] flag to false.
    return Stack(
      key: colKey,
      children: [
        charts.BarChart(
          widget.seriesList,
          animate: widget.animate,
          barGroupingType: charts.BarGroupingType.stacked,
          vertical: false,
          behaviors: [
            charts.SeriesLegend(
              desiredMaxColumns: 3,
              outsideJustification: charts.OutsideJustification.endDrawArea,
              entryTextStyle: const charts.TextStyleSpec(color: charts.Color(r: 127, g: 63, b: 191), fontSize: 11),
            ),
          ],
          selectionModels: [
            charts.SelectionModelConfig(changedListener: (charts.SelectionModel model) {
              if (lockReload) return;
              if (model.hasDatumSelection) {
                print(model.selectedDatum[0].index);
                setState(() {
                  selectedIndex = model.selectedDatum[0].index;
                  onTapUp();
                  lockReload = true;
                });
                Future.delayed(const Duration(milliseconds: 600), () {
                  lockReload = false;
                });
              }
            })
          ],
          // It is important when using both primary and secondary axes to choose
          // the same number of ticks for both sides to get the gridlines to line
          // up.
          primaryMeasureAxis:
              const charts.NumericAxisSpec(tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 3)),
          secondaryMeasureAxis:
              const charts.NumericAxisSpec(tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 3)),
        ),
        if (tapY != null)
          Positioned(
            top: 90,
            right: 0,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: AppColor.primary),
                color: AppColor.primary.withOpacity(0.2),
              ),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.chartData.listColChart[selectedIndex].data
                        .map((e) => Row(
                              children: [
                                Container(
                                  height: 10,
                                  width: 10,
                                  decoration: BoxDecoration(
                                    color: Hexcolor(e.color ?? '#FFFFFF'),
                                  ),
                                ),
                                const SpacingBox(
                                  width: 2,
                                ),
                                Text(e.value.toString()),
                              ],
                            ))
                        .toList(),
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}
