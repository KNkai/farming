import 'package:flutter/material.dart';
import 'package:mi_smart/src/modules/chart/model/wChartTable_model.dart';
import 'package:mi_smart/src/share/utils/color.dart';

import '../../../share/utils/color.dart';
import '../model/wChartTable_model.dart';

class TableChart extends StatelessWidget {
  final WChartTable model;
  const TableChart(this.model);
  @override
  Widget build(BuildContext context) {
    List<TableRow> data = [];
    model.colValues.forEach((element) {
      data.add(TableRow(
          decoration: BoxDecoration(
            color: AppColor.primary.withOpacity(0.2),
          ),
          children: element
              .map((e) => Center(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(e),
                  )))
              .toList()));
    });
    data.insert(
      0,
      TableRow(
          decoration: BoxDecoration(
            color: AppColor.redorange.withOpacity(0.2),
          ),
          children: model.colLabels
              .map((e) => Center(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(
                      e,
                      style: const TextStyle(fontWeight: FontWeight.w800, fontSize: 15),
                    ),
                  )))
              .toList()),
    );
    return Table(
      border: TableBorder.all(color: AppColor.primary),
      children: data,
    );
  }
}
