import 'package:flutter/material.dart';

import '../../../share/utils/color.dart';
import '../model/widgetChart_model.dart';

class WidgetChart extends StatelessWidget {
  final WidgetChartModel model;
  const WidgetChart(this.model);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: AppColor.primary),
        color: AppColor.primary.withOpacity(0.2),
      ),
      child: ListTile(
        title: Text(
          model.title,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        leading: model.icon != null
            ? SizedBox(height: 50, width: 50, child: Image.network(model.icon))
            : SizedBox(height: 50, width: 50, child: Image.asset('assets/icon.png')),
        subtitle: Text(
          model.data.toString(),
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ),
    );
  }
}
