import 'dart:async';

import 'package:mi_smart/src/modules/chart/model/wChartTable_model.dart';
import 'package:mi_smart/src/modules/chart/model/wCircleChart_model.dart';
import 'package:mi_smart/src/modules/chart/model/wColumnLineChart_model.dart';
import 'package:mi_smart/src/modules/chart/model/widgetChart_model.dart';

import 'chart_srv.dart';
import 'model/chart_model.dart';
import 'model/wChart_model.dart';
import 'model/wColumnChart_model.dart';
import 'model/wHorizontalChart_model.dart';
import 'model/wStackChart_model.dart';

class ChartRepo {
  ChartSrv _chartSrv = ChartSrv();

  Future<dynamic> getAllChartType({int limit, String filter, String search, int page, int offset}) async {
    try {
      final res = await _chartSrv.queryEnum('getAllReportCode');
      return res["getAllReportCode"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List<ChartModel>> getChartData({String code, ChartFilter filter}) async {
    try {
      final res = await _chartSrv.query('getReportData', '''
      code : "$code"
      option: {
        paging: {
          page: 1
          limit: 20
        }
      }
      ''');
      final List chartMap = res["getReportData"]["charts"];
      List<ChartModel> chart = [];
      chartMap.forEach((element) {
        if (element['type'] == "COLUMN_CHART") {
          chart.add(WColumnChart.fromJson(element as Map));
        }
        if (element['type'] == "STACK_CHART") {
          chart.add(WStackChart.fromJson(element as Map));
        }
        if (element['type'] == "HORIZONTAL_CHART") {
          chart.add(WHorizontalChart.fromJson(element as Map));
        }
        if (element['type'] == "COLUMN_LINE_CHART") {
          chart.add(WColumnLineChart.fromJson(element as Map));
        }
        if (element['type'] == "CIRCLE_CHART") {
          chart.add(WCircleChart.fromJson(element as Map));
        }
        if (element['type'] == "WIDGET") {
          chart.add(WidgetChartModel.fromJson(element as Map));
        }
        if (element['type'] == "TABLE_CHART") {
          chart.add(WChartTable.fromJson(element as Map));
        }
      });
      return chart;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}
