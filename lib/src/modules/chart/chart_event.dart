import 'package:mi_smart/base_config/src/base/base_event.dart';

import 'model/chart_model.dart';

class GetAllChartType extends BaseEvent {}

class GetChartDataEvent extends BaseEvent {
  String code;
  String chartType;
  ChartFilter filter;
  GetChartDataEvent(this.code, this.chartType, this.filter);
}
