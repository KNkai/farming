import 'package:mi_smart/base_config/src/base/base_state.dart';

import 'model/wChart_model.dart';

class LoadingListTypeChartState extends BaseState {}

class GetAllChartTypeSuccessState extends BaseState {
  List<dynamic> charts;
  GetAllChartTypeSuccessState(this.charts);
}

class GetAllChartTypeFailState extends BaseState {
  String err;
  GetAllChartTypeFailState(this.err);
}

class LoadingChartDataState extends BaseState {}

class GetChartDataSuccessState extends BaseState {
  List<ChartModel> chart;
  GetChartDataSuccessState(this.chart);
}

class GetChartDataFailState extends BaseState {
  String err;
  GetChartDataFailState(this.err);
}
