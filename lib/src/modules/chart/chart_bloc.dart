import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/base_bloc.dart';
import 'package:mi_smart/base_config/src/base/base_event.dart';
import 'package:rxdart/rxdart.dart';

import 'chart_event.dart';
import 'chart_repo.dart';
import 'chart_state.dart';

class ChartBloc extends BaseBloc {
  ChartRepo _chartRepo = ChartRepo();
  int page = 0;
  List listData = [];
  String searchText;

  final _chartTypeState$ = BehaviorSubject<BaseState>();
  Stream<BaseState> get chartTypeStream => _chartTypeState$.stream;
  Sink<BaseState> get chartTypeSink => _chartTypeState$.sink;
  BaseState get chartTypeValue => _chartTypeState$.value;

  final _month$ = BehaviorSubject<int>();
  Stream<int> get monthStream => _month$.stream;
  Sink<int> get monthSink => _month$.sink;
  int get monthValue => _month$.value;

  final _year$ = BehaviorSubject<int>();
  Stream<int> get yearStream => _year$.stream;
  Sink<int> get yearSink => _year$.sink;
  int get yearValue => _year$.value;

  final _currentChartType$ = BehaviorSubject<dynamic>();
  Stream<dynamic> get currentChartTypeStream => _currentChartType$.stream;
  Sink<dynamic> get currentChartTypeSink => _currentChartType$.sink;
  dynamic get currentChartTypeValue => _currentChartType$.value;

  final _chartDataState$ = BehaviorSubject<BaseState>();
  Stream<BaseState> get chartDataStream => _chartDataState$.stream;
  Sink<BaseState> get chartDataSink => _chartDataState$.sink;
  BaseState get chartDataValue => _chartDataState$.value;

  @override
  void dispose() {
    _year$.close();
    _month$.close();
    _currentChartType$.close();
    _chartTypeState$.close();
    _chartDataState$.close();
    super.dispose();
  }

  initData(int month, int year, dynamic chart0) {
    yearSink.add(year);
    monthSink.add(month);
    currentChartTypeSink.add(chart0);
  }

  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case GetAllChartType:
        getAllChartType();
        break;
      case GetChartDataEvent:
        getChartData(event);
        break;
    }
  }

  Future<void> getAllChartType() async {
    try {
      loadingSink.add(true);
      final data = await _chartRepo.getAllChartType();
      chartTypeSink.add(GetAllChartTypeSuccessState(data));
    } catch (e) {
      chartTypeSink.add(GetAllChartTypeFailState(e.message));
    } finally {
      loadingSink.add(false);
    }
  }

  Future<void> getChartData(GetChartDataEvent event) async {
    try {
      loadingSink.add(true);
      final data = await _chartRepo.getChartData(code: event.code, filter: event.filter);
      chartDataSink.add(GetChartDataSuccessState(data));
    } catch (e) {
      chartDataSink.add(GetChartDataFailState(e.message));
    } finally {
      loadingSink.add(false);
    }
  }
}
