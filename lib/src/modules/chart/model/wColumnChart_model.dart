import 'chart_model.dart';
import 'wChart_model.dart';

class WColumnChart extends ChartModel {
  int line;
  String title;
  String viewType;
  String mapping;
  String pageInfo;
  int reportId;
  int columnCount;
  List<String> columnLabel;
  List<String> columnColor;
  String unit;
  List<DChartModel> listColumnChart;
  int zoom;

  WColumnChart({
    this.title,
    this.viewType,
    this.unit,
    this.reportId,
    this.pageInfo,
    this.mapping,
    this.line,
    this.columnCount,
    this.columnLabel,
    this.columnColor,
    this.listColumnChart,
    this.zoom,
  });

  factory WColumnChart.fromJson(Map<String, dynamic> widget) {
    print(widget);
    return WColumnChart(
      columnCount: widget['colColors'].length,
      columnColor: widget['colColors'] == null
          ? null
          : (widget['colColors'] as List).map<String>((val) => val.toString()).toList(),
      columnLabel: (widget["colLabels"] as List).cast<String>(),
      title: widget['title'],
      zoom: widget['zoom'],
      listColumnChart: setListColumnChat(json: widget, count: widget['data'].length),
    );
  }

  static setListColumnChat({dynamic json, int count}) {
    return WColumnChart.parseDataChart(json: json, count: count);
  }

  static List<DChartModel> parseDataChart({int count, dynamic json}) {
    List<DChartModel> l = [];

    for (int i = 0; i < count; i++) {
      DChartModel model = DChartModel(data: <DPointModel>[]);
      for (int j = 0; j < json["colColors"].length; j++) {
        model.data.add(DPointModel(
            key: json["data"][i]["xLabel"],
            value: json["data"][i]["colValues"][j].toDouble(),
            color: json["colColors"][j]?? '#FFFFFF'));
      }
      l.add(model);
    }
    return l;
  }
}
