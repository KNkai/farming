import 'package:mi_smart/src/modules/chart/model/wChart_model.dart';

class WChartTable extends ChartModel {
  String title;
  int total;
  int offset;
  int limit;
  List<String> colLabels;
  List<List<String>> colValues;

  WChartTable({
    this.title,
    this.total,
    this.offset,
    this.limit,
    this.colLabels,
    this.colValues,
  });

  factory WChartTable.fromJson(Map<String, dynamic> widget) {
    List<List<String>> list = [];
    (widget['colValues'] as List).forEach((element) {
      list.add((element as List).map((e) => e.toString()).toList());
    });
    return WChartTable(
        title: widget['title'],
        total: widget['total'],
        offset: widget['offset'],
        limit: widget['limit'],
        colLabels: widget['colLabels'].cast<String>(),
        colValues: list);
  }
}
