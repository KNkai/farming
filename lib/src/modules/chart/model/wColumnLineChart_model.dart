import 'chart_model.dart';
import 'wChart_model.dart';

class WColumnLineChart extends ChartModel {
  String title;
  String viewType;
  String mapping;
  String pageInfo;
  int reportId;
  int columnCount;
  int lineCount;
  List<String> columnLabel;
  List<String> columnColor;
  List<String> lineLabel;
  List<String> lineColor;
  String unit;
  List<DChartModel> listColumnChart;
  List<DChartModel> listLineChart;
  int zoom;

  WColumnLineChart({
    this.title,
    this.viewType,
    this.unit,
    this.reportId,
    this.pageInfo,
    this.mapping,
    this.columnCount,
    this.columnLabel,
    this.columnColor,
    this.lineColor,
    this.lineCount,
    this.lineLabel,
    this.listLineChart,
    this.listColumnChart,
    this.zoom,
  });

  factory WColumnLineChart.fromJson(Map<String, dynamic> widget) {
    print(widget);
    return WColumnLineChart(
      columnCount: widget['colColors'].length,
      lineCount: widget['lineColors'].length,
      columnColor: widget['colColors'] == null
          ? null
          : (widget['colColors'] as List).map<String>((val) => val.toString()).toList(),
      lineColor: widget['lineColors'] == null
          ? null
          : (widget['lineColors'] as List).map<String>((val) => val.toString()).toList(),
      columnLabel: (widget["colLabels"] as List).cast<String>(),
      lineLabel: (widget["lineLabels"] as List).cast<String>(),
      title: widget['title'],
      zoom: widget['zoom'],
      listColumnChart: setListColumnChat(json: widget, count: widget['data'].length),
      listLineChart: setListLineChat(json: widget, count: widget['data'].length),
    );
  }

  static setListColumnChat({dynamic json, int count}) {
    return WColumnLineChart.parseDataChart(json: json, count: count, key: 'col');
  }

  static setListLineChat({dynamic json, int count}) {
    return WColumnLineChart.parseDataChart(json: json, count: count, key: 'line');
  }

  static List<DChartModel> parseDataChart({int count, dynamic json, String key}) {
    List<DChartModel> l = [];

    for (int i = 0; i < count; i++) {
      DChartModel model = DChartModel(data: <DPointModel>[]);
      for (int j = 0; j < json["data"][i]["${key}Values"].length; j++) {
        model.data.add(DPointModel(
            key: json["data"][i]["xLabel"],
            value: json["data"][i]["${key}Values"][j].toDouble(),
            color: json["${key}Colors"][j]));
      }
      l.add(model);
    }
    return l;
  }
}
