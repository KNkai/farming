import 'chart_model.dart';
import 'wChart_model.dart';

class WStackChart extends ChartModel {
  int line;
  String title;
  String viewType;
  String mapping;
  String pageInfo;
  int reportId;
  int colCount;
  List<String> colLabel;
  List<String> colColor;
  String unit;
  List<DChartModel> listColChart;

  WStackChart({
    this.title,
    this.viewType,
    this.unit,
    this.reportId,
    this.pageInfo,
    this.mapping,
    this.line,
    this.colCount,
    this.colLabel,
    this.colColor,
    this.listColChart,
  });

  static List<WStackChart> fromListMap(List<dynamic> listMap) {
    return listMap.map((e) => WStackChart.fromJson(e)).toList();
  }

  factory WStackChart.fromJson(Map<String, dynamic> widget) {
    final res = WStackChart(
      colCount: widget['colColors'].length,
      colColor: widget['colColors'] == null
          ? null
          : (widget['colColors'] as List).map<String>((val) => val.toString()).toList(),
      colLabel: (widget["colLabels"] as List).map<String>((e) => e.toString()).toList(),
      line: widget['line'],
      mapping: widget['mapping'],
      pageInfo: widget['pageInfo'],
      reportId: widget['reportId'],
      title: widget['title'],
      unit: widget['unit'],
      viewType: widget['viewType'],
      listColChart: setListColChart(json: widget, count: widget['data'].length),
    );
    return res;
  }

  static List<String> parseListString({String key, int count, Map<String, dynamic> json}) {
    List<String> l = [];
    for (int i = 0; i < count; i++) {
      l.add(json['$key$i']);
    }
    return l;
  }

  static setListColChart({dynamic json, int count}) {
    return WStackChart.parseDataChart(key: 'col', count: count, json: json);
  }

  static List<DChartModel> parseDataChart({String key, int count, dynamic json}) {
    List<DChartModel> l = [];

    for (int i = 0; i < count; i++) {
      DChartModel model = DChartModel(data: <DPointModel>[]);
      for (int j = 0; j < json["data"][i]["colValues"].length; j++) {
        model.data.add(DPointModel(
            key: json["data"][i]["xLabel"],
            value: json["data"][i]["colValues"][j].toDouble(),
            color: json["colColors"][j]));
      }
      l.add(model);
    }
    return l;
  }
}
