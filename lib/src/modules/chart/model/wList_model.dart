import 'wChart_model.dart';

class WListModel extends ChartModel {
  String title;
  dynamic data;
  String icon;
  String unit;

  WListModel({
    this.title,
    this.data,
    this.icon,
    this.unit
  });

  factory WListModel.fromJson(Map<String, dynamic> widget) {
    return WListModel(
      title: widget['title'],
      data: widget['data'],
      icon: widget['icon'],
      unit: widget['unit'],
    );
  }
}
