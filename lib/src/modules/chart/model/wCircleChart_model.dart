import 'chart_model.dart';
import 'wChart_model.dart';

class WCircleChart extends ChartModel {
  String title;
  DChartModel dataChart;

  WCircleChart({
    this.title,
    this.dataChart,
  });

  factory WCircleChart.fromJson(Map<String, dynamic> widget) {
    return WCircleChart(
      title: widget['title'],
      dataChart: setChart(widget['data']),
    );
  }

  static DChartModel setChart(dynamic json) {
    return parseDataChart(json: json);
  }

  static DChartModel parseDataChart({String key, int count, dynamic json}) {
    var list = json as List;

    DChartModel dChartModel = DChartModel(data: []);
    if (list.length != 0) {
      for (int j = 0; j < list.length; j++) {
        var item = list[j];
        DPointModel point =
            DPointModel(key: item['label'], value: item['value'].toDouble() ?? 0, color: item['color'] ?? '#FFFFFF');
        dChartModel.data.add(point);
      }
    }

    return dChartModel;
  }
}
