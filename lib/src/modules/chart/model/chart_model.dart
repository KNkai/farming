// class BaseChartModel {
//   String code;
//   String title;
//   bool requireFilter;
//   ChartFilter filter;
//   String type;
//   String unit;
//   List<String> colColors;
//   List<String> colLabels;
//   List<dynamic> colData;
//   int zoom;

//   void copyWith(
//       {String code,
//       String title,
//       bool requireFilter,
//       ChartFilter filter,
//       String type,
//       String unit,
//       List<String> colColors,
//       List<String> colLabels,
//       List<dynamic> colData,
//       int zoom}) {
//     this.code = code??this.code;
//     this.title = title??this.title;
//     this.requireFilter = requireFilter??this.requireFilter;
//     this.filter = filter??this.filter;
//     this.type = type??this.type;
//     this.unit = unit??this.unit;
//     this.colColors = colColors??this.colColors;
//     this.colLabels = colLabels??this.colLabels;
//     this.colData = colData??this.colData;
//     this.zoom = zoom??this.zoom;
//   }
// }

// class ChartCountContractReport extends BaseChartModel {}

// class ChartCountAppointmentReport extends BaseChartModel {}

// class ChartAppointmentDurationReport extends BaseChartModel {}

// class ChartAmountContractReport extends BaseChartModel {}

class ChartFilter {
  int month;
  int year;
  ChartFilter(this.month, this.year);
}

class DPointModel {
  String key;
  double value;
  String color;
  DPointModel({this.key, this.color, this.value});
}

class DChartModel {
  List<DPointModel> data;
  DChartModel({this.data});
}