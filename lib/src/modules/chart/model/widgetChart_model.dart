import 'wChart_model.dart';

class WidgetChartModel extends ChartModel {
  String title;
  dynamic data;
  String icon;
  String unit;

  WidgetChartModel({
    this.title,
    this.data,
    this.icon,
    this.unit
  });

  factory WidgetChartModel.fromJson(Map<String, dynamic> widget) {
    return WidgetChartModel(
      title: widget['title'],
      data: widget['data'],
      icon: widget['icon'],
      unit: widget['unit'],
    );
  }
}
