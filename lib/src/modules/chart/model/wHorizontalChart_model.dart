import 'chart_model.dart';
import 'wChart_model.dart';

class WHorizontalChart extends ChartModel {
  int line;
  String title;
  String viewType;
  String mapping;
  String pageInfo;
  int reportId;
  int rowCount;
  List<String> rowLabel;
  List<String> rowColor;
  String unit;
  List<DChartModel> listRowChart;
  int zoom;

  WHorizontalChart(
      {this.title,
      this.viewType,
      this.unit,
      this.reportId,
      this.pageInfo,
      this.mapping,
      this.line,
      this.rowCount,
      this.rowLabel,
      this.rowColor,
      this.listRowChart,
      this.zoom});

  static List<WHorizontalChart> fromListMap(List<dynamic> listMap) {
    return listMap.map((e) => WHorizontalChart.fromJson(e)).toList();
  }

  factory WHorizontalChart.fromJson(Map<String, dynamic> widget) {
    final res = WHorizontalChart(
        rowCount: widget['colColors'].length,
        rowColor: widget['colColors'] == null
            ? null
            : (widget['colColors'] as List).map<String>((val) => val.toString()).toList(),
        rowLabel: (widget["colLabels"] as List).map<String>((e) => e.toString()).toList(),
        line: widget['line'],
        mapping: widget['mapping'],
        pageInfo: widget['pageInfo'],
        reportId: widget['reportId'],
        title: widget['title'],
        unit: widget['unit'],
        viewType: widget['viewType'],
        listRowChart: setListRowChart(json: widget, count: widget['data'].length),
        zoom: widget['zoom']);
    return res;
  }

  static List<String> parseListString({String key, int count, Map<String, dynamic> json}) {
    List<String> l = [];
    for (int i = 0; i < count; i++) {
      l.add(json['$key$i']);
    }
    return l;
  }

  static setListRowChart({dynamic json, int count}) {
    return WHorizontalChart.parseDataChart(key: 'col', count: count, json: json);
  }

  static List<DChartModel> parseDataChart({String key, int count, dynamic json}) {
    List<DChartModel> l = [];

    for (int i = 0; i < count; i++) {
      DChartModel model = DChartModel(data: <DPointModel>[]);
      for (int j = 0; j < json["colColors"].length; j++) {
        model.data.add(DPointModel(
            key: json["data"][i]["yLabel"],
            value: json["data"][i]["colValues"][j].toDouble(),
            color: json["colColors"][j] ?? '#FFFFFF'));
      }
      l.add(model);
    }
    return l;
  }
}
