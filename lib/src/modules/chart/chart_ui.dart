import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/base_widget.dart';
import 'package:mi_smart/base_widget/src/loading_task.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/modules/chart/model/wChartTable_model.dart';
import 'package:mi_smart/src/modules/chart/model/wChart_model.dart';
import 'package:mi_smart/src/modules/chart/model/wCircleChart_model.dart';
import 'package:mi_smart/src/modules/chart/model/wColumnLineChart_model.dart';
import 'package:mi_smart/src/modules/chart/model/widgetChart_model.dart';
import 'package:mi_smart/src/modules/chart/widget/column_line_chart.dart';
import 'package:mi_smart/src/modules/chart/widget/pie_chart.dart';
import 'package:mi_smart/src/modules/chart/widget/stack_chart.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';
import 'package:mi_smart/src/share/widget/empty.dart';

import 'chart_bloc.dart';
import 'chart_event.dart';
import 'chart_state.dart';
import 'model/chart_model.dart';
import 'model/wColumnChart_model.dart';
import 'model/wHorizontalChart_model.dart';
import 'model/wStackChart_model.dart';
import 'widget/group_column_chart.dart';
import 'widget/group_horizontal_chart.dart';
import 'widget/table_chart.dart';
import 'widget/typeChart_btn.dart';
import 'widget/widget_chart.dart';

class ChartUi extends StatefulWidget {
  const ChartUi();

  @override
  _ChartUiState createState() => _ChartUiState();
}

class _ChartUiState extends State<ChartUi> {
  ChartBloc bloc = ChartBloc();

  @override
  void initState() {
    bloc.getAllChartType();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingTask(
      bloc: bloc,
      child: AppScafold(
        title: 'Thống kê',
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: StreamBuilder<Object>(
                  stream: bloc.chartTypeStream,
                  builder: (context, snapshot) {
                    if (snapshot.data is LoadingListTypeChartState) {
                      return const SizedBox(width: 60, height: 60, child: CircularProgressIndicator());
                    }
                    if (snapshot.data is GetAllChartTypeFailState) {
                      return const Text("Không lấy được dữ liệu, vui lòng kiểm tra kết nối mạng");
                    }
                    if (snapshot.data is GetAllChartTypeSuccessState) {
                      final charts = (snapshot.data as GetAllChartTypeSuccessState).charts;
                      bloc.initData(DateTime.now().month, DateTime.now().year, charts[0]);
                      return ChartTypeBtn((val) {
                        bloc.currentChartTypeSink.add(val);
                        bloc.add(GetChartDataEvent(bloc.currentChartTypeValue, bloc.currentChartTypeValue,
                            ChartFilter(bloc.monthValue, bloc.yearValue)));
                      }, charts, bloc);
                    }
                    return Container();
                  }),
            ),
            const SpacingBox(height: 2),
            Expanded(
              child: ListView(
                children: [
                  StreamBuilder(
                    stream: bloc.chartDataStream,
                    initialData: LoadingChartDataState(),
                    builder: (context, snapshot) {
                      if (snapshot.data is LoadingChartDataState) {
                        return Container();
                      }
                      if (snapshot.data is GetChartDataFailState) {
                        return const Center(
                          child: Text("Đã xảy ra lỗi"),
                        );
                      }
                      if (snapshot.data is GetChartDataSuccessState) {
                        final chart = (snapshot.data as GetChartDataSuccessState).chart;
                        if (chart is List<ChartModel>) {
                          if (chart.length == 0)
                            return Padding(
                              padding: EdgeInsets.only(top: SizeConfig.setHeight(20)),
                              child: const EmptyWidget(text: 'Không có dữ liệu'),
                            );
                          return Column(
                            children: chart.map((e) {
                              if (e is WHorizontalChart) {
                                return Padding(
                                  padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                                  child: Column(
                                    children: [
                                      Text(
                                        e.title ?? 'title',
                                        textAlign: TextAlign.center,
                                        style: ptSubtitle(context).copyWith(color: AppColor.primary),
                                      ),
                                      const SpacingBox(height: 1),
                                      SizedBox(
                                          height: e.listRowChart.length != 0
                                              ? deviceHeight(context) * 0.1 * e.listRowChart[0].data.length +
                                                  deviceHeight(context) * 0.1 +
                                                  e.listRowChart.length * 0.025 * deviceHeight(context)
                                              : deviceHeight(context) * 0.1,
                                          child: HorizontalChart.fromWHorizontalChart(e)),
                                    ],
                                  ),
                                );
                              }
                              if (e is WCircleChart) {
                                return Padding(
                                  padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                                  child: Column(
                                    children: [
                                      Text(
                                        e.title ?? 'title',
                                        textAlign: TextAlign.center,
                                        style: ptSubtitle(context).copyWith(color: AppColor.primary),
                                      ),
                                      const SpacingBox(height: 1),
                                      SizedBox(
                                          height: deviceWidth(context) * 0.75,
                                          child: DonutPieChart.fromWCircleChart(e)),
                                    ],
                                  ),
                                );
                              }
                              if (e is WColumnChart) {
                                return Padding(
                                  padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                                  child: Column(
                                    children: [
                                      Text(
                                        e.title ?? 'title',
                                        textAlign: TextAlign.center,
                                        style: ptSubtitle(context).copyWith(color: AppColor.primary),
                                      ),
                                      const SpacingBox(height: 1),
                                      SizedBox(
                                          height: deviceHeight(context) * 0.6, child: ColumnChart.fromWColumnChart(e)),
                                    ],
                                  ),
                                );
                              }
                              if (e is WColumnLineChart) {
                                return Padding(
                                  padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                                  child: Column(
                                    children: [
                                      Text(
                                        e.title ?? 'title',
                                        textAlign: TextAlign.center,
                                        style: ptSubtitle(context).copyWith(color: AppColor.primary),
                                      ),
                                      const SpacingBox(height: 1),
                                      SizedBox(
                                          height: deviceHeight(context) * 0.6,
                                          child: OrdinalComboBarLineChart.fromWColumnLineChart(e)),
                                    ],
                                  ),
                                );
                              }
                              if (e is WidgetChartModel) {
                                return Padding(
                                  padding: const EdgeInsets.only(top: 0.0, bottom: 0),
                                  child: Column(
                                    children: [
                                      WidgetChart(e),
                                    ],
                                  ),
                                );
                              }
                              if (e is WChartTable) {
                                return Padding(
                                  padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                                  child: Column(
                                    children: [
                                      const SpacingBox(height: 1),
                                      Text(
                                        e.title ?? '',
                                        textAlign: TextAlign.center,
                                        style: ptSubtitle(context).copyWith(color: AppColor.primary),
                                      ),
                                      const SpacingBox(height: 1),
                                      TableChart(e),
                                    ],
                                  ),
                                );
                              }
                              if (e is WStackChart) {
                                return Padding(
                                  padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                                  child: Column(
                                    children: [
                                      const SpacingBox(height: 1),
                                      Text(
                                        e.title ?? '',
                                        textAlign: TextAlign.center,
                                        style: ptSubtitle(context).copyWith(color: AppColor.primary),
                                      ),
                                      const SpacingBox(height: 1),
                                      SizedBox(
                                        height: e.listColChart.length != 0
                                            ? deviceHeight(context) * 0.1 * e.listColChart[0].data.length +
                                                deviceHeight(context) * 0.1 +
                                                e.listColChart.length * 0.025 * deviceHeight(context)
                                            : deviceHeight(context) * 0.1,
                                        child: StackedHorizontalChart.fromStackChart(e),
                                      ),
                                      const Divider(
                                        height: 30,
                                        thickness: 2,
                                      ),
                                    ],
                                  ),
                                );
                              }
                            }).toList(),
                          );
                        }
                      }
                      return Container();
                    },
                  ),
                  const SpacingBox(
                    height: 3,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
