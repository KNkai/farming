import 'package:flutter/material.dart';

import '../../../base_config/base_config.dart';
import '../../share/utils/base_api.dart';
import 'events/startAuth_event.dart';
import 'state/authFail_state.dart';
import 'state/authSuccess_state.dart';
import 'state/authTokenExpired_state.dart';

class SplashBloc extends BaseBloc with ChangeNotifier {
  // SplashRepo _splashRepo = SplashRepo();
  // UserRepo _userRepo = UserRepo();
  SplashBloc() {
    startAuth();
  }
  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case StartAuthEvent:
        startAuth();
        break;
      default:
    }
    notifyListeners();
  }

  startAuth() async {
    try {
      final token = await SPref.instance.get(CustomString.KEY_TOKEN);
      if (token != null) {
        BaseApi.instance.init(token);
        processStateSink.add(AuthSuccessState());
      } else {
        processStateSink.add(AuthFailState());
      }
    } catch (e) {
      processStateSink.add(AuthTokenExpiredState());
    }
  }
}
