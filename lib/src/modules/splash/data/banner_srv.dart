import 'package:mi_smart/base_config/base_config.dart';

class BannerSrv extends BaseService {
  BannerSrv() : super(module: 'Banner', fragment: ''' 
_id: ID
createdAt: DateTime
updatedAt: DateTime
title: String
message: String
picture: String
startTime: DateTime
endTime: DateTime
publicFor: [String]
isBanner: Boolean
countSend: Int
  ''');
}
