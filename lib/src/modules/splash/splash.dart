import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/bloc_listener.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/navigator.dart';

import '../../../main.dart';
import 'splash_bloc.dart';
import 'state/authFail_state.dart';
import 'state/authSuccess_state.dart';
import 'state/authTimeOut_state.dart';
import 'state/authTokenExpired_state.dart';

class Splash extends StatelessWidget {
  handleState(BaseState event) {
    if (event is AuthSuccessState) {
      Future.delayed(const Duration(milliseconds: 1000), () async {
        await navigatorKey.currentState.maybePop();
        navigatorKey.currentState.pushReplacementNamed(View.dashboard);
        return;
      });
      // navigatorKey.currentState.pushReplacementNamed('/login');
      // return;
    }
    if (event is AuthFailState) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        navigatorKey.currentState.pushReplacementNamed(View.intro);
        return;
      });
    }
    if (event is AuthTokenExpiredState) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        navigatorKey.currentState.pushReplacementNamed(View.login);
        return;
      });
    }
    if (event is AuthTimeOutState) {
      final snackBar = SnackBar(
        content: Text(event.message),
        backgroundColor: Colors.red,
      );
      Scaffold.of(navigatorKey.currentContext).showSnackBar(snackBar);
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    SplashBloc bloc = SplashBloc();
    return BlocListener<SplashBloc>(
      listener: handleState,
      bloc: bloc,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          // Image.asset(
          //   Assets.bg_splash,
          //   fit: BoxFit.cover,
          // ),
          Material(
            child: Container(
              color: Colors.white,
              child: Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(width: deviceWidth(context) / 1.3, child: Hero(tag: 'logo', child: logo)),
                    const SpacingBox(
                      height: 1,
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 17.0),
                      child: Text(
                        'Ứng dụng nhật kí cho nông dân',
                        style: TextStyle(fontSize: 17, fontWeight: FontWeight.w900, color: Colors.black87),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
