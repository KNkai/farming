import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/modules/profile/model/update_user_password/update_user_password.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

class ChangePasswordPage extends StatelessWidget {
  static void go(BuildContext context, String email) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => ChangePasswordPage(email)));
  }

  ChangePasswordPage(this.email);

  final String email;
  final _controllerCurrent = TextEditingController();
  final _controllerNew = TextEditingController();
  final _controllerConfirmNew = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _node1 = FocusNode();
  final _node2 = FocusNode();
  final _node3 = FocusNode();

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      title: 'Đổi mật khẩu',
      onBack: true,
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: _studentsDismiss,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Đổi mật khẩu',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    _field(_controllerCurrent, _node1, 'Mật khẩu hiện tại', _currentAndNewValidator),
                    _field(_controllerNew, _node2, 'Mật khẩu mới', _currentAndNewValidator),
                    _field(_controllerConfirmNew, _node3, 'Xác nhận mật khẩu', _onlyConfirmValidator),
                  ],
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Align(
                alignment: Alignment.center,
                child: AppBtn(
                  'Đổi mật khẩu',
                  onPressed: () {
                    _onChangePass(context);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _onChangePass(BuildContext context) {
    _studentsDismiss();
    if (_checkValidate()) {
      FirebaseAuth.instance.currentUser().then((user) {
        user
            .reauthenticateWithCredential(
                EmailAuthProvider.getCredential(email: email, password: _controllerCurrent.text))
            .then((authResult) {
          SPref.instance.get(CustomString.KEY_ID).then((i) {
            BaseApi.instance
                .execute(
                    UpdateUserPasswordMutation(variables: UpdateUserPasswordArguments(i: i, p: _controllerNew.text)))
                .then((value) {
              if (value?.data?.updateUserPassword?.id != null)
                WarningDialog.show(context, 'Đổi mật khẩu thành công', "OK", title: 'Thông báo', onCloseDialog: () {
                  Navigator.of(context).pop();
                });
              else {
                _reset();
                WarningDialog.show(context, 'Đôi mật khẩu không thành công', "OK", title: 'Thông báo');
              }
            });
          });
        }).catchError((onError) {
          if (onError is PlatformException && onError.code == 'ERROR_WRONG_PASSWORD') {
            WarningDialog.show(context, 'Mật khẩu cũ không đúng', 'OK', title: 'Thông báo');
          }
        });
      });
    }
  }

  _checkValidate() {
    return _formKey.currentState.validate();
  }

  _studentsDismiss() {
    _node2.unfocus();
  }

  _reset() {
    _controllerNew.clear();
    _controllerConfirmNew.clear();
    _controllerCurrent.clear();
  }

  String _currentAndNewValidator(String value) {
    if (value == null || value.isEmpty) {
      return 'Bắt buộc nhập';
    }

    if (value.length < 6) return 'Độ dài phải tối thiểu 6 kí tự';

    return null;
  }

  String _onlyConfirmValidator(String value) {
    if (value == null || value.isEmpty) {
      return 'Bắt buộc nhập';
    }

    if (value.length < 6) return 'Độ dài phải tối thiểu 6 kí tự';

    if (_controllerConfirmNew.text != _controllerNew.text) {
      return "Mật khẩu xác nhận không trùng";
    }

    return null;
  }

  _field(TextEditingController _controller, FocusNode node, String title, String Function(String) validator) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(
            height: 40,
          ),
          Text(
            title,
            style: const TextStyle(fontWeight: FontWeight.w500, color: Colors.grey),
          ),
          const SizedBox(
            height: 5,
          ),
          TextFormField(
              focusNode: node,
              validator: validator,
              controller: _controller,
              obscureText: true,
              cursorColor: AppColor.primary,
              scrollPadding: EdgeInsets.zero,
              style: const TextStyle(fontSize: 30),
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(0),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                ),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                ),
              ))
        ],
      );
}
