import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';

import '../../share/utils/color.dart';

class PolicyText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 2, horizontal: 2),
      child: GestureDetector(
        onTap: () {
          showNow(context);
        },
        child: RichText(
          maxLines: null,
          textAlign: TextAlign.start,
          text: const TextSpan(
            style: TextStyle(
              color: Colors.black87,
              fontSize: 13,
            ),
            children: <TextSpan>[
              TextSpan(
                text: 'Tôi đồng ý với tất cả ',
              ),
              TextSpan(
                text: 'điều kiện và điều khoản sử dụng',
                style: TextStyle(color: AppColor.primary, decoration: TextDecoration.underline),
              ),
              TextSpan(text: ' của '),
              TextSpan(
                text: 'Ứng dụng MiSmart',
                style: TextStyle(color: AppColor.primary),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showNow(BuildContext context) {
    showGeneralDialog(
      context: context,
      barrierColor: Colors.black12.withOpacity(0.6), // background color
      barrierDismissible: false, // should dialog be dismissed when tapped outside
      barrierLabel: "Dialog", // label for barrier
      transitionDuration: const Duration(milliseconds: 400), // how long it takes to popup dialog after button click
      pageBuilder: (_, __, ___) {
        // your widget implementation
        return Material(
          child: SizedBox(
            width: deviceWidth(context),
            height: deviceHeight(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  color: AppColor.primary,
                  padding: const EdgeInsets.only(
                    top: kToolbarHeight / 2 + 5,
                    left: 15,
                    right: 15,
                    bottom: 10,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Icon(
                          Icons.keyboard_arrow_left,
                          color: Colors.white,
                          size: 30,
                        ),
                      ),
                      const SpacingBox(width: 2),
                      const Expanded(
                          child: FittedBox(
                        child: Text(
                          'Điều khoản sử dụng MiSmart',
                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      )),
                      const SpacingBox(width: 4),
                    ],
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        children: const <Widget>[
                          Text('''

                            '''),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
