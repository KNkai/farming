import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/bloc_listener.dart';
import 'package:mi_smart/base_widget/base_widget.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/modules/login/events/requestOtp_event.dart';
import 'package:mi_smart/src/modules/login/login_bloc.dart';
import 'package:mi_smart/src/modules/login/policy_string.dart';
import 'package:mi_smart/src/modules/login/states/login_state.dart';
import 'package:mi_smart/src/modules/login/states/sendOtp_state.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/text_input.dart';

import '../../share/utils/color.dart';
import 'events/loginEmail_event.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with SingleTickerProviderStateMixin {
  LoginBloc _bloc;
  TabController _tabController;

  @override
  void initState() {
    _bloc = LoginBloc();
    _tabController = TabController(length: 1, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void stateHandler(BaseState state) {
    if (state is LoginSuccessState) {
      Timer.run(() {
        navigatorKey.currentState.pushNamed(View.dashboard);
      });
      _bloc.processStateSink.add(NeutralState());
    }
    if (state is LoginFailState) {
      showToast(state.err, flagColor: false);
      _bloc.processStateSink.add(NeutralState());
    }
    if (state is SendOtpFailState) {
      showToast(state.err, flagColor: false);
      _bloc.processStateSink.add(NeutralState());
    }
    if (state is SendOtpSuccessState) {
      if (ModalRoute.of(context).settings.name == View.login) {
        Timer.run(() {
          navigatorKey.currentState.pushNamed(View.otp, arguments: _bloc);
        });
        _bloc.processStateSink.add(NeutralState());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    bool keyBoardVisible = MediaQuery.of(context).viewInsets.bottom != 0;
    return Scaffold(
      body: SafeArea(
        child: LoadingTask(
          bloc: _bloc,
          child: BlocListener(
            bloc: _bloc,
            listener: stateHandler,
            child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
              (!keyBoardVisible)
                  ? Expanded(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 50),
                            child: Image.asset('assets/logo.png'),
                          ),
                          const SpacingBox(
                            height: 1,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 17.0),
                            child: Text(
                              'Ứng dụng nhật kí cho nông dân',
                              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w900, color: Colors.black87),
                            ),
                          ),
                        ],
                      ),
                    )
                  : const SizedBox(height: 35),
              Align(
                alignment: Alignment.center,
                child: TabBar(
                    indicatorSize: TabBarIndicatorSize.tab,
                    indicatorWeight: 3,
                    indicatorColor: AppColor.primary,
                    indicatorPadding: const EdgeInsets.symmetric(horizontal: 10),
                    controller: _tabController,
                    isScrollable: true,
                    labelColor: Colors.black87,
                    unselectedLabelStyle: const TextStyle(fontSize: 16, color: Colors.black54),
                    labelStyle: const TextStyle(fontSize: 16, color: Colors.black87, fontWeight: FontWeight.bold),
                    tabs: [
                      // SizedBox(width: deviceWidth(context) / 3, child: const Tab(text: 'Chuyên viên')),
                      // SizedBox(
                      //   width: deviceWidth(context) / 3,
                      //   child: const Tab(text: 'Quản trị viên'),
                      // ),
                      const Tab(text: 'Quản trị viên'),
                    ]),
              ),
              Expanded(
                flex: 9,
                child: Material(
                  elevation: 5,
                  child: Container(
                    color: Colors.grey[100],
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        // LoginStaff(_bloc),
                        LoginAdmin(_bloc),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}

class LoginStaff extends StatefulWidget {
  final LoginBloc bloc;
  const LoginStaff(this.bloc);
  @override
  _LoginStaffState createState() => _LoginStaffState();
}

class _LoginStaffState extends State<LoginStaff> {
  final TextEditingController _phoneController = TextEditingController();

  final GlobalKey<FormState> _formStaffKey = GlobalKey<FormState>();

  bool _btnCanSubmit = false;

  bool _agreePolicy = false;

  bool formValid() {
    if (Validator.isPhone(_phoneController.text)) return true;
    return false;
  }

  void validatBtn(String val) {
    if (formValid()) {
      setState(() {
        _btnCanSubmit = true;
      });
    } else {
      setState(() {
        _btnCanSubmit = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formStaffKey,
      child: Column(
        children: [
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: TextInput(
                    keyboardType: TextInputType.phone,
                    hint: 'Số điện thoại đăng nhập',
                    controller: _phoneController,
                    icon: const Icon(Icons.phone),
                    onChange: validatBtn,
                    shouldValidate: true,
                  ),
                ),
                const SpacingBox(height: 2),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 33.0,
                        width: 33.0,
                        child: Checkbox(
                          value: _agreePolicy,
                          onChanged: (flag) {
                            setState(() {
                              _agreePolicy = flag;
                            });
                          },
                          activeColor: AppColor.primary,
                        ),
                      ),
                      Expanded(child: PolicyText()),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: AppBtn(
                  'Xác nhận',
                  color: AppColor.primary.withOpacity(0.8),
                  onPressed: _btnCanSubmit && _agreePolicy
                      ? () {
                          if (!_formStaffKey.currentState.validate()) {
                            return;
                          }
                          widget.bloc.add(RequestOtpEvent(_phoneController.text));
                        }
                      : null,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class LoginAdmin extends StatefulWidget {
  final LoginBloc bloc;
  const LoginAdmin(this.bloc);
  @override
  _LoginAdminState createState() => _LoginAdminState();
}

class _LoginAdminState extends State<LoginAdmin> {
  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _passController = TextEditingController();

  final GlobalKey<FormState> _formAdminKey = GlobalKey<FormState>();

  bool _btnCanSubmit = false;

  bool _agreePolicy = false;

  bool formValid() {
    if (Validator.isEmail(_emailController.text) && Validator.isPassword(_passController.text)) return true;
    return false;
  }

  void validatBtn(String val) {
    if (formValid()) {
      setState(() {
        _btnCanSubmit = true;
      });
    } else {
      setState(() {
        _btnCanSubmit = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formAdminKey,
      child: Column(
        children: [
          Expanded(
            flex: 2,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SpacingBox(height: 2),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: TextInput(
                      hint: 'Nhập Email',
                      shouldValidate: true,
                      keyboardType: TextInputType.emailAddress,
                      controller: _emailController,
                      onChange: validatBtn,
                      icon: const Icon(Icons.email),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: TextInput(
                      hint: 'Nhập mật khẩu',
                      shouldValidate: true,
                      controller: _passController,
                      onChange: validatBtn,
                      hinden: true,
                      icon: const Icon(Icons.lock_outline),
                    ),
                  ),
                  const SpacingBox(height: 2),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 33.0,
                          width: 33.0,
                          child: Checkbox(
                            value: _agreePolicy,
                            onChanged: (flag) {
                              setState(() {
                                _agreePolicy = flag;
                              });
                            },
                            activeColor: AppColor.primary,
                          ),
                        ),
                        Expanded(child: PolicyText()),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    child: AppBtn(
                      'Xác nhận',
                      color: AppColor.primary.withOpacity(0.8),
                      onPressed: _btnCanSubmit && _agreePolicy
                          ? () {
                              if (!_formAdminKey.currentState.validate()) {
                                return;
                              }
                              widget.bloc.add(LoginEmailEvent(_emailController.text, _passController.text));
                            }
                          : null,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: () {
                      navigatorKey.currentState.pushNamed(View.resetPass, arguments: widget.bloc);
                    },
                    child: const Padding(
                      padding: EdgeInsets.only(bottom: 20.0),
                      child: Text('Quên mật khẩu?'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
