import 'package:mi_smart/base_config/src/base/base_event.dart';

class ResetPassEvent extends BaseEvent {
  String email;
  ResetPassEvent(this.email);
}
