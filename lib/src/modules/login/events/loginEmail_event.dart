import 'package:mi_smart/base_config/src/base/base_event.dart';

class LoginEmailEvent extends BaseEvent {
  String email;
  String password;
  LoginEmailEvent(this.email, this.password);
}
