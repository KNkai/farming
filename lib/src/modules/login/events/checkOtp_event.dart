import 'package:mi_smart/base_config/src/base/base_event.dart';

class CheckOtpEvent extends BaseEvent {
  String otpCode;
  CheckOtpEvent(this.otpCode);
}
