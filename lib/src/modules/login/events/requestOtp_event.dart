import 'package:mi_smart/base_config/src/base/base_event.dart';

class RequestOtpEvent extends BaseEvent {
  String phone;
  RequestOtpEvent(this.phone);
}
