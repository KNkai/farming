import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/bloc_listener.dart';
import 'package:mi_smart/base_widget/base_widget.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/modules/login/states/login_state.dart';
import 'package:mi_smart/src/modules/login/states/sendOtp_state.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/text_input.dart';

import 'count_down_otp.dart';
import 'events/checkOtp_event.dart';
import 'login_bloc.dart';

class ConfirmOtpPage extends StatefulWidget {
  final LoginBloc bloc;
  const ConfirmOtpPage(this.bloc);
  @override
  _ConfirmOtpPageState createState() => _ConfirmOtpPageState();
}

class _ConfirmOtpPageState extends State<ConfirmOtpPage> with AutomaticKeepAliveClientMixin {
  TextEditingController _otpController = TextEditingController();

  bool _btnCanSubmit = false;

  final GlobalKey<FormState> _formOtpKey = GlobalKey<FormState>();

  bool formValid() {
    if (_otpController.text.length == 6) return true;
    return false;
  }

  void validatBtn(String val) {
    if (formValid()) {
      setState(() {
        _btnCanSubmit = true;
      });
    } else {
      setState(() {
        _btnCanSubmit = false;
      });
    }
  }

  void stateHandler(BaseState state) {
    if (state is LoginSuccessState) {
      // print('e');
      widget.bloc.processStateSink.add(NeutralState());
    }
    if (state is LoginFailState) {
      showToast(state.err, flagColor: false);
      widget.bloc.processStateSink.add(NeutralState());
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    bool keyBoardVisible = MediaQuery.of(context).viewInsets.bottom != 0;
    return Scaffold(
      body: LoadingTask(
        bloc: widget.bloc,
        child: BlocListener(
          bloc: widget.bloc,
          listener: stateHandler,
          child: Form(
            key: _formOtpKey,
            child: Column(
              children: <Widget>[
                const SpacingBox(height: 1),
                (!keyBoardVisible)
                    ? Expanded(
                        flex: 3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 50),
                              child: Image.asset('assets/otp.png'),
                            ),
                          ],
                        ),
                      )
                    : const SpacingBox(height: 12),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Nhập OTP chúng tôi gửi qua tin nhắn cho bạn',
                    textAlign: TextAlign.center,
                    style: ptHeadStyleText(context),
                  ),
                ),
                const SpacingBox(height: 1),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: TextInput(
                      keyboardType: TextInputType.number,
                      hint: 'Nhập mã OTP',
                      controller: _otpController,
                      icon: const Icon(Icons.lock_outline),
                      onChange: validatBtn,
                      shouldValidate: true,
                    ),
                  ),
                ),
                countDownOtp(widget.bloc),
                const SpacingBox(height: 2),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: AppBtn(
                    'Xác nhận',
                    color: AppColor.primary.withOpacity(0.8),
                    onPressed: _btnCanSubmit
                        ? () {
                            if (!_formOtpKey.currentState.validate()) {
                              return;
                            }
                            widget.bloc.add(CheckOtpEvent(_otpController.text));
                          }
                        : null,
                  ),
                ),
                const SpacingBox(height: 1),
                InkWell(
                  onTap: () => navigatorKey.currentState.pop(),
                  child: const Padding(
                    padding: EdgeInsets.only(bottom: 20.0),
                    child: Text(
                      'Đổi số điện thoại?',
                      style: TextStyle(color: AppColor.primary),
                    ),
                  ),
                ),
                const SpacingBox(height: 2),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  get wantKeepAlive => true;
}
