import 'package:firebase_auth/firebase_auth.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/base_bloc.dart';
import 'package:mi_smart/base_config/src/base/base_event.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/modules/login/events/loginEmail_event.dart';
import 'package:mi_smart/src/modules/login/login_repo.dart';
import 'package:mi_smart/src/modules/login/states/login_state.dart';
import 'package:rxdart/rxdart.dart';

import 'events/checkOtp_event.dart';
import 'events/requestOtp_event.dart';
import 'events/resetPass_event.dart';
import 'states/resetPass_state.dart';
import 'states/sendOtp_state.dart';

enum AuthStatus {
  unAuth,
  codeSent,
  authed,
}

class LoginBloc extends BaseBloc with ChangeNotifier {
  String idFireBase;
  String smsVerifyCode;
  String tokenFB;
  String currPhone;

  AuthCredential authCredential;
  UserRepo _userRepo = UserRepo();

  final _countdownStart$ = BehaviorSubject<bool>();
  Stream<bool> get countdownStartStream => _countdownStart$.stream;
  Sink<bool> get countdownStartSink => _countdownStart$.sink;
  bool get countdownStartValue => _countdownStart$.value;

  @override
  void dispatchEvent(BaseEvent event) {
    if (event is RequestOtpEvent) {
      requestOtp(event.phone);
    }
    if (event is LoginEmailEvent) {
      loginAgencyByEmailPasswordFirebase(event.email, event.password);
    }
    if (event is CheckOtpEvent) {
      submitOtp(event.otpCode);
    }
    if (event is ResetPassEvent) {
      resetPass(event.email);
    }
  }

  @override
  void dispose() {
    _countdownStart$.close();
    super.dispose();
  }

  static logout([BuildContext context]) async {
    await SPref.instance.remove(CustomString.KEY_TOKEN);
    await SPref.instance.remove(CustomString.KEY_NAME);

    await navigatorKey.currentState.pushNamedAndRemoveUntil(View.splash, ModalRoute.withName('/'));
  }

  Future requestOtp(String phone, {bool isResend = false}) async {
    loadingSink.add(true);
    final String phoneNumber = "+84" + phone.toString().substring(1, 10);
    currPhone = phoneNumber;
    try {
      FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: const Duration(seconds: 60),
        verificationCompleted: (authCredential) {},
        verificationFailed: (e) {
          loadingSink.add(false);
          processStateSink.add(SendOtpFailState(Formart.formatErrFirebaseLoginToString(e.code)));
        },
        codeSent: (verificationId, [code]) {
          countdownStartSink.add(true);
          loadingSink.add(false);
          smsVerifyCode = verificationId;
          processStateSink.add(isResend ? ReSendOtpSuccessState() : SendOtpSuccessState());
        },
        codeAutoRetrievalTimeout: (verificationId) {
          smsVerifyCode = verificationId;
        },
      );
    } catch (e) {
      loadingSink.add(false);
      processStateSink.add(SendOtpFailState('Có mỗi xảy ra, vui lòng kiểm tra kết nối mạng.'));
    }
  }

  Future resendOtp() async {
    loadingSink.add(true);
    if (currPhone != null && Validator.isPhoneNumber(currPhone)) {
      await requestOtp(currPhone, isResend: true);
    } else {
      processStateSink.add(SendOtpFailState('Có mỗi xảy ra, vui lòng khởi động lại ứng dụng.'));
    }
    loadingSink.add(false);
  }

  Future submitOtp(String otp) async {
    try {
      loadingSink.add(true);
      authCredential = PhoneAuthProvider.getCredential(verificationId: smsVerifyCode, smsCode: otp);
      final auth = await FirebaseAuth.instance.signInWithCredential(authCredential);
      if (auth.user != null) {
        // print('good');
        final res = await _userRepo.loginByOtpFirebase(auth);
        idFireBase = auth.user.uid;
        auth.user.getIdToken().then((token) => tokenFB = token.token);
        processStateSink.add(LoginSuccessState());
        return res;
      } else {
        // print('not good');
        processStateSink.add(LoginFailState('Người dùng không tồn tại.'));
      }
    } catch (e) {
      if (e.message == 'Exception: Tài khoản này đã bị khoá')
        processStateSink.add(LoginFailState('Tài khoản đã bị khoá'));

      processStateSink.add(LoginFailState(Formart.formatErrFirebaseLoginToString(e.code)));
    } finally {
      loadingSink.add(false);
    }
  }

  Future loginAgencyByEmailPasswordFirebase(String email, String password) async {
    AuthResult authResult;
    try {
      loadingSink.add(true);
      authResult = await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
      if (authResult.user == null) {
        processStateSink.add(SendOtpFailState('Người dùng không tồn tại.'));
      } else {
        await _userRepo.loginByEmailFirebase(authResult);
        processStateSink.add(LoginSuccessState());
      }
    } catch (e) {
      processStateSink.add(LoginFailState(Formart.formatErrFirebaseLoginToString(e.code)));
    } finally {
      loadingSink.add(false);
    }
  }

  Future resetPass(String email) async {
    try {
      loadingSink.add(true);
      final res = await _userRepo.resetPassword(email);
      if (res == 'ok') {
        processStateSink.add(ResetPassSuccessState());
      } else {
        processStateSink.add(ResetPassFailState(Formart.formatErrFirebaseLoginToString(res)));
      }
    } catch (e) {
      processStateSink.add(ResetPassFailState(Formart.formatErrFirebaseLoginToString(e?.message?.code)));
    } finally {
      loadingSink.add(false);
    }
  }
}
