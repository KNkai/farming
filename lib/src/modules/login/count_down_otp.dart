import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/base_widget.dart';
import 'package:mi_smart/src/share/utils/color.dart';

import 'login_bloc.dart';

Widget countDownOtp(LoginBloc bloc) {
  return StreamBuilder(
    initialData: true,
    stream: bloc.countdownStartStream,
    builder: (context, snap) => snap.data == true
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Mã sẽ tự động hết hạn sau ',
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Countdown(
                duration: const Duration(seconds: 60),
                onFinish: () {
                  bloc.countdownStartSink.add(false);
                },
                builder: (ctx, remaining) {
                  return Text(
                    '${remaining.inSeconds}',
                    style: const TextStyle(
                      color: AppColor.redorange,
                      fontSize: 13,
                      fontWeight: FontWeight.w600,
                    ),
                  );
                },
              ),
              const Text(
                ' giây',
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Không nhận được ? ',
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                ),
              ),
              InkWell(
                onTap: () {
                  showToast('Đang gửi ...');
                  bloc.resendOtp();
                },
                child: const Text(
                  'Gửi lại mã',
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    color: AppColor.primary,
                    fontSize: 13,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
  );
}
