import 'package:mi_smart/base_config/base_config.dart';

class UserSrv extends BaseService {
  UserSrv() : super(module: 'User', fragment: ''' 
id: String
uid: String
name: String
avatar: String
dob: DateTime
overView: String
email: String
position: String
createdAt: DateTime
updatedAt: DateTime
  ''');
}
