import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/bloc_listener.dart';
import 'package:mi_smart/base_widget/base_widget.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/modules/login/events/resetPass_event.dart';
import 'package:mi_smart/src/modules/login/states/resetPass_state.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/text_input.dart';

import 'login_bloc.dart';
import 'states/sendOtp_state.dart';

class ResetPasswordPage extends StatefulWidget {
  final LoginBloc bloc;
  const ResetPasswordPage(this.bloc);
  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> with AutomaticKeepAliveClientMixin {
  TextEditingController _emailController = TextEditingController();

  bool _btnCanSubmit = false;

  final GlobalKey<FormState> _formResetPassKey = GlobalKey<FormState>();

  bool formValid() {
    if (Validator.isEmail(_emailController.text)) return true;
    return false;
  }

  void validatBtn(String val) {
    if (formValid()) {
      setState(() {
        _btnCanSubmit = true;
      });
    } else {
      setState(() {
        _btnCanSubmit = false;
      });
    }
  }

  void stateHandler(BaseState state) {
    if (state is ResetPassSuccessState) {
      showToast('Gửi yêu cầu thành công, vui lòng kiểm tra hộp thư trong Email.');
      widget.bloc.processStateSink.add(NeutralState());
    }
    if (state is ResetPassFailState) {
      showToast(state.err, flagColor: false);
      FocusScope.of(context).unfocus();
      widget.bloc.processStateSink.add(NeutralState());
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    bool keyBoardVisible = MediaQuery.of(context).viewInsets.bottom != 0;
    return Scaffold(
      body: LoadingTask(
        bloc: widget.bloc,
        child: BlocListener(
          bloc: widget.bloc,
          listener: stateHandler,
          child: Form(
            key: _formResetPassKey,
            child: Column(
              children: <Widget>[
                const SpacingBox(height: 1),
                (!keyBoardVisible)
                    ? Expanded(
                        flex: 3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 50),
                              child: Image.asset('assets/otp.png'),
                            ),
                          ],
                        ),
                      )
                    : const SpacingBox(height: 12),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Nhập Email để tiếp tục\n',
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: ptHeadStyleText(context),
                  ),
                ),
                const SpacingBox(height: 2),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: TextInput(
                      keyboardType: TextInputType.emailAddress,
                      hint: 'Nhập Email',
                      controller: _emailController,
                      icon: const Icon(Icons.lock_outline),
                      onChange: validatBtn,
                      shouldValidate: true,
                    ),
                  ),
                ),
                const Text(
                  'Email cần phải khớp với email đăng kí tài khoản',
                  style: TextStyle(
                    decoration: TextDecoration.none,
                    fontSize: 12.5,
                    color: Colors.black87,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SpacingBox(height: 1),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: AppBtn(
                    'Xác nhận',
                    color: AppColor.primary.withOpacity(0.8),
                    onPressed: _btnCanSubmit
                        ? () {
                            if (!_formResetPassKey.currentState.validate()) {
                              return;
                            }
                            widget.bloc.add(ResetPassEvent(_emailController.text));
                          }
                        : null,
                  ),
                ),
                const SpacingBox(height: 1),
                InkWell(
                  onTap: () => navigatorKey.currentState.pop(),
                  child: const Padding(
                    padding: EdgeInsets.only(bottom: 20.0),
                    child: Text(
                      'Quay lại',
                      style: TextStyle(color: AppColor.primary),
                    ),
                  ),
                ),
                const SpacingBox(height: 2),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  get wantKeepAlive => true;
}
