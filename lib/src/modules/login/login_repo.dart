import 'dart:async';
import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/firebase/firebase_service.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';
import 'package:mi_smart/src/share/utils/util.dart';

import 'user_srv.dart';

class UserRepo {
  UserSrv _userSrv = UserSrv();

  Future<String> loginByOtpFirebase(AuthResult authResult) async {
    try {
      window.onBeginFrame(const Duration(days: 1));
      final firebaseToken = await authResult.user.getIdToken();
      final token = firebaseToken.token;
      final fbToken = await FirebaseService.instance.getDeviceToken();
      final deviceId = await getUniqueDeviceId();
      final input = ' idToken : "$token", deviceId: "$deviceId", deviceToken: "$fbToken"';
      final res = await _userSrv.mutate('loginStaff', input, fragment: 'token , staff { id }');
      final data = res["loginStaff"];
      // print('token: $token');
      await SPref.instance.set(CustomString.KEY_TOKEN, data["token"]);
      BaseApi.instance.init(data['token']);
      await SPref.instance.set(CustomString.KEY_ID, data["staff"]["id"]);
      SPref.instance.set(CustomString.KEY_NAME, data["staff"]["name"]);
      return data["token"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<String> loginByEmailFirebase(AuthResult authResult) async {
    try {
      final firebaseToken = await authResult.user.getIdToken();
      final token = firebaseToken.token;
      final fbToken = await FirebaseService.instance.getDeviceToken();
      final deviceId = await getUniqueDeviceId();
      final input = ' idToken : "$token", deviceId: "$deviceId", deviceToken: "$fbToken"';
      final res = await _userSrv.mutate('login', input, fragment: 'token , user { id name }');
      final data = res["login"];
      // print('token: $token');
      await SPref.instance.set(CustomString.KEY_TOKEN, data["token"]);
      BaseApi.instance.init(data['token']);
      await SPref.instance.set(CustomString.KEY_ID, data["user"]["id"]);
      SPref.instance.set(CustomString.KEY_NAME, data["user"]["name"]);
      return data["token"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<String> changePassByOtp(String pass, String token) async {
    try {
      //final token = firebaseToken.token;
      //final uid = authResult.user.uid;
      const uid = "tATa0D0OdrP0CmlXAUbFfNnLtFj1";
      const token =
          "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc0Mzg3ZGUyMDUxMWNkNDgzYTIwZDIyOGQ5OTI4ZTU0YjNlZTBlMDgiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdG9tLWdpb25nIiwiYXVkIjoidG9tLWdpb25nIiwiYXV0aF90aW1lIjoxNTkxMjU2NDQ4LCJ1c2VyX2lkIjoidEFUYTBEME9kclAwQ21sWEFVYkZmTm5MdEZqMSIsInN1YiI6InRBVGEwRDBPZHJQMENtbFhBVWJGZk5uTHRGajEiLCJpYXQiOjE1OTEyNTY0NDgsImV4cCI6MTU5MTI2MDA0OCwiZW1haWwiOiJhZ2VuY3lAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbImFnZW5jeUBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.nn0cmCcHUZ4DY-zw1gnvn34fAAU9XfB7gfTiZLGkYd-_MEohohnRg80CIwBhROBuprchbI3D_rvwwRxWechaW0SEy709I3xlugCQxibvC6e2RkfK7dgJEUWOa2vfOKT2yxXDyZAkrl7HSDL8kHUBMvgfw9bLixSieS9VIw9nE0vM_0LUqazJazzBDoufY1zcxdnbdoIQfDDY9YJ-aahjlSlo5tKU35HE--LJZJE95Gysz4oCkRICOkxibOXI4fkSA3RYjpg035Xb0pEyWtO1N21chOFFaOvKWTdolc198zTj_UNTVswwapl-zeSzj8MXI34xZjMsp4Y6fwFjG0o_sw";
      final input = 'token : "FB|$uid|$token" , pin "$pass"';
      final res = await _userSrv.mutate('updatePinByFirebaseToken', input, fragment: ' _id ');
      final data = res["updatePinByFirebaseToken"];
      return data["_id"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<String> resetPassword(String email) async {
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
    } catch (e) {
      throw Exception(e);
    }
    return 'ok';
  }
}
