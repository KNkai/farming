import 'package:mi_smart/base_config/base_config.dart';

class ResetPassSuccessState extends BaseState {
  ResetPassSuccessState();
}

class ResetPassFailState extends BaseState {
  String err;
  ResetPassFailState(this.err);
}
