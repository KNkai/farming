import 'package:mi_smart/base_config/src/base/base_state.dart';

class LoginSuccessState extends BaseState {}

class LoginFailState extends BaseState {
  String err;
  LoginFailState(this.err);
}
