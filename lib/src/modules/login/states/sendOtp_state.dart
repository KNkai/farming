import 'package:mi_smart/base_config/base_config.dart';

class SendOtpSuccessState extends BaseState {
  SendOtpSuccessState();
}

class ReSendOtpSuccessState extends BaseState {
  ReSendOtpSuccessState();
}

class SendOtpFailState extends BaseState {
  String err;
  SendOtpFailState(this.err);
}

class NeutralState extends BaseState {}
