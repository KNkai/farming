import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mi_smart/src/modules/select_area/model/map_model.dart';
import 'package:mi_smart/src/modules/weather/weather_repo.dart';

class WeatherWidgetBloc {
  final stateWeather = StreamController<StateWeather>();

  final _repo = WeatherRepo();

  void eventLoad() {
    stateWeather.add(StateWeatherLoading());
    _repo.getOneRegion().then((weather) {
      if (weather != null) {
        if (weather is List<GetOneWeatherData>) {
          stateWeather.add(StateWeatherLoaded(weather: weather));
        }
        if (weather is OneRegionFailedLocation) {
          stateWeather.add(StateWeatherLoadLocationFailed());
        }
      } else
        stateWeather.add(StateWeatherLoadFailed());
    });
  }

  void dispose() {
    stateWeather.close();
  }
}

abstract class StateWeather {}

class StateWeatherLoading extends StateWeather {}

class StateWeatherLoaded extends StateWeather {
  final List<GetOneWeatherData> weather;

  StateWeatherLoaded({@required this.weather});
}

class StateWeatherLoadFailed extends StateWeather {}

class StateWeatherLoadLocationFailed extends StateWeather {}
