import 'package:geolocator/geolocator.dart';
import 'package:mi_smart/src/modules/select_area/model/app_plain_query.dart';
import 'package:mi_smart/src/modules/select_area/model/map_model.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';

class WeatherRepo {
  // List<GetOneWeatherData>
  // OneRegionFailedLocation
  // null
  Future getOneRegion() async {
    try {
      final position = await getCurrentPosition(timeLimit: const Duration(seconds: 5));
      return BaseApi.instance.rawBodyRawDataExecute(AppPlainQuery.getOneWeather, variables: {
        "q": {
          'limit': 5,
          "filter": {
            "location": {
              "\$near": {
                "\$geometry": {
                  "coordinates": [position.longitude, position.latitude],
                  "type": "Point"
                }
              }
            }
          }
        },
      }).then((value) {
        final weatherData = GetOneWeatherQuery.fromMap(value.data).getAllWeather;
        if (weatherData?.data == null || weatherData.data.isEmpty) return null;

        return weatherData.data;
      }).catchError((_) => null);
    } catch (_) {
      return OneRegionFailedLocation();
    }
  }
}

class OneRegionFailedLocation {}
