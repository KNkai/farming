import 'dart:async';

import 'package:mi_smart/src/modules/select_area/model/map_model.dart';

class WeatherScreenBloc {
  final stateHourly = StreamController<Current>();

  void eventChangeHour(Current hour) {
    stateHourly.add(hour);
  }

  void dispose() {
    stateHourly.close();
  }
}
