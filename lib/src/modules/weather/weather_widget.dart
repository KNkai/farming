import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/modules/select_area/model/map_model.dart';
import 'package:mi_smart/src/modules/weather/weather_screen.dart';
import 'package:mi_smart/src/modules/weather/weather_widget_bloc.dart';
import 'package:provider/provider.dart';

import 'weather_screen.dart';
import 'weather_widget_bloc.dart';

class WeatherWidget extends StatelessWidget {
  const WeatherWidget();
  @override
  Widget build(BuildContext context) {
    return Provider<WeatherWidgetBloc>(
      create: (_) => WeatherWidgetBloc(),
      dispose: (context, value) => value.dispose(),
      builder: (context, child) {
        return Consumer<WeatherWidgetBloc>(builder: (_, bloc, ___) {
          bloc.eventLoad();

          return StreamBuilder<StateWeather>(
            stream: bloc.stateWeather.stream,
            builder: (context, ss) {
              if (ss?.data == null) return const SizedBox.shrink();

              var state = ss.data;

              if (state is StateWeatherLoading) {
                return SizedBox(
                  height: SizeConfig.setHeight(141),
                  width: double.infinity,
                  child: Center(
                    child: SizedBox(
                      width: 50,
                      height: 50,
                      child: CupertinoActivityIndicator(
                        radius: 15,
                      ),
                    ),
                  ),
                );
              } else if (state is StateWeatherLoadFailed || state is StateWeatherLoadLocationFailed) {
                return Column(
                  children: [
                    if (state is StateWeatherLoadLocationFailed)
                      const Text(
                        'Không lấy được toạ độ hiện tại xin thử lại.',
                      ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        bloc.eventLoad();
                      },
                      child: SizedBox(
                        height: SizeConfig.setHeight(141),
                        width: double.infinity,
                        child: Center(
                          child: SizedBox(
                            width: 50,
                            height: 50,
                            child: Icon(
                              Icons.refresh_rounded,
                              size: 30,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                );
              }

              var listWeather = (state as StateWeatherLoaded).weather;

              return SizedBox(
                height: SizeConfig.setHeight(141),
                child: PageView(
                  children: [
                    _WeatherBanner(weather: listWeather[0]),
                    _WeatherBanner(weather: listWeather[1]),
                    _WeatherBanner(weather: listWeather[2]),
                    _WeatherBanner(weather: listWeather[3]),
                    _WeatherBanner(weather: listWeather[4]),
                  ],
                ),
              );
              // StateWeatherLoaded
            },
          );
        });
      },
    );
  }
}

class _WeatherBanner extends StatelessWidget {
  final GetOneWeatherData weather;

  const _WeatherBanner({@required this.weather});

  @override
  Widget build(BuildContext context) {
    var currentWeather = weather.current;
    var weatherIcon =
        currentWeather?.weather == null || currentWeather.weather.isEmpty ? null : currentWeather.weather[0].icon;

    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        WeatherScreen.go(context, weather);
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(currentWeather.tempText(),
                        style: const TextStyle(fontWeight: FontWeight.w800, fontSize: 30, color: Colors.black)),
                    const SizedBox(
                      width: 10,
                    ),
                    _Percent(
                      icon: Image.asset('asset/umbrella.png'),
                      percent: (currentWeather?.dewPoint ?? 0.0).toInt(),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    _Percent(
                      icon: Image.asset('asset/drop.png'),
                      percent: (currentWeather.humidity ?? 0.0).toInt(),
                    )
                  ],
                ),
                Text(
                  weather.name + '\n' ?? '',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: const TextStyle(color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Image.network(
            'https://openweathermap.org/img/wn/$weatherIcon@4x.png',
            height: 120,
            width: 120,
          )
        ],
      ),
    );
  }
}

class _Percent extends StatelessWidget {
  final Widget icon;
  final int percent;

  const _Percent({@required this.icon, @required this.percent});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 17,
          height: 17,
          child: icon,
        ),
        const SizedBox(
          width: 2,
        ),
        Text(
          (percent == null ? '  ' : percent.toString()) + '%',
          style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
        )
      ],
    );
  }
}
