import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../share/widget/app_btn.dart';
import '../select_area/model/map_model.dart';
import 'component/list_weather_hourly_properties.dart';
import 'component/twenty_four_hour_widget.dart';
import 'component/weather_header.dart';
import 'component/week_widget.dart';
import 'weather_screen_bloc.dart';

class WeatherScreen extends StatefulWidget {
  final GetOneWeatherData weather;

  const WeatherScreen(this.weather);

  static void go(BuildContext context, GetOneWeatherData weather) =>
      Navigator.of(context).push(MaterialPageRoute(builder: (_) => WeatherScreen(weather)));

  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Provider<WeatherScreenBloc>(
            create: (_) => WeatherScreenBloc(),
            dispose: (context, value) => value.dispose(),
            child: Consumer<WeatherScreenBloc>(
              builder: (context, bloc, child) {
                return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  StreamBuilder<Current>(
                      initialData: widget?.weather?.current,
                      stream: bloc.stateHourly.stream,
                      builder: (context, ss) {
                        final hourlyWeather = ss.data;
                        final weatherIcon = hourlyWeather == null
                            ? null
                            : hourlyWeather.weather == null || hourlyWeather.weather.isEmpty
                                ? null
                                : hourlyWeather.weather[0].icon;

                        final time = _timeFromTs('HH:mm', hourlyWeather?.dt);
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            WeatherHeader(
                                time: time,
                                locationName: widget.weather.name,
                                weatherIcon: weatherIcon,
                                currentWeather: hourlyWeather),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(
                                    'Dự báo trong ngày',
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  SizedBox(
                                      height: 30,
                                      child: TwentyFourHourWidget(
                                          currentHour: hourlyWeather, hourlys: widget.weather.hourly))
                                ],
                              ),
                            ),
                            const Divider(),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: ListWeatherHourlyProperty(hourlyWeather: hourlyWeather),
                            ),
                          ],
                        );
                      }),
                  const Divider(),
                  SafeArea(
                    top: false,
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Dự báo trong tuần',
                            style: TextStyle(fontSize: 20),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          WeekWidget(
                            dailys: widget.weather.daily,
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          AppBtn(
                            'Về trang chủ',
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ]);
              },
            ),
          ),
        ),
      ),
    );
  }

  String _timeFromTs(String format, int ts) =>
      ts == null ? '' : DateFormat(format).format(DateTime.fromMillisecondsSinceEpoch(ts * 1000));
}
