import 'package:flutter/material.dart';
import 'package:mi_smart/src/modules/select_area/model/map_model.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:provider/provider.dart';

import '../weather_screen_bloc.dart';

class TwentyFourHourWidget extends StatefulWidget {
  final List<Current> hourlys;
  final Current currentHour;

  const TwentyFourHourWidget({@required this.hourlys, this.currentHour});

  @override
  _TwentyFourHourWidgetState createState() => _TwentyFourHourWidgetState();
}

class _TwentyFourHourWidgetState extends State<TwentyFourHourWidget> {
  final _widthItem = 40.0;
  int _selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    if (widget.hourlys == null || widget.hourlys.isEmpty || widget.currentHour == null) return const SizedBox.shrink();

    final newHourlys = <Current>[];
    final hours = <int>[];
    final currentHour = DateTime.fromMillisecondsSinceEpoch(widget.currentHour.dt * 1000).hour;

    for (int c = 0; c < widget.hourlys.length; c++) {
      final hour = DateTime.fromMillisecondsSinceEpoch(widget.hourlys[c].dt * 1000).hour;
      newHourlys.add(widget.hourlys[c]);
      hours.add(hour);

      if (hour == 23) break;
    }

    final bloc = Provider.of<WeatherScreenBloc>(context);

    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: newHourlys.length,
        itemBuilder: (_, index) {
          final itemColor = _itemTextColor(index, hours[index], currentHour);

          if (index == _selectedIndex)
            return DecoratedBox(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColor.primary,
                ),
                child: SizedBox(
                  width: _widthItem,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      hours[index].toString() + 'h',
                      style: TextStyle(fontSize: 15, color: itemColor),
                    ),
                  ),
                ));

          return GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              bloc.eventChangeHour(newHourlys[index]);
              setState(() {
                _selectedIndex = index;
              });
            },
            child: SizedBox(
              width: _widthItem,
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  hours[index].toString() + 'h',
                  style: TextStyle(fontSize: 15, color: itemColor),
                ),
              ),
            ),
          );
        });
  }

  Color _itemTextColor(int index, int indexHour, int currentHour) {
    if (index == _selectedIndex) return Colors.white;
    if (indexHour == currentHour) return AppColor.primary;
    return null;
  }
}
