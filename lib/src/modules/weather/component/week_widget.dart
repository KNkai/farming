import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mi_smart/src/modules/select_area/model/map_model.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class WeekWidget extends StatefulWidget {
  final List<Daily> dailys;
  const WeekWidget({@required this.dailys});
  @override
  _WeekWidgetState createState() => _WeekWidgetState();
}

class _WeekWidgetState extends State<WeekWidget> {
  int _selectIndex = 0;

  @override
  Widget build(BuildContext context) {
    if (widget.dailys == null || widget.dailys.isEmpty) return const SizedBox.shrink();

    final selectItem = widget.dailys[_selectIndex];

    return Column(
      children: [
        SizedBox(
          height: 90,
          child: ListView.builder(
            itemCount: widget.dailys.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              final e = widget.dailys[index];
              return _Card(
                iconCode: e.weather[0].icon,
                ts: e.dt,
                select: index == _selectIndex,
                onTap: () {
                  setState(() {
                    _selectIndex = index;
                  });
                },
              );
            },
          ),
        ),
        const SizedBox(
          height: 25,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              _AttributeWidget(
                color: const Color(0xffFF6724),
                title: 'Nhiệt độ',
                assetName: 'asset/thermometer_white.png',
                content:
                    "${selectItem.temp.max.toStringAsFixed(1).toString()}°C - ${selectItem.temp.min.toStringAsFixed(1).toString()}°C",
              ),
              const SizedBox(
                width: 10,
              ),
              _AttributeWidget(
                color: const Color(0xff51B1FB),
                title: 'Mưa',
                assetName: 'asset/umbrella_white.png',
                content: selectItem.dewPoint.toStringAsFixed(1).toString() + '%',
              ),
              const SizedBox(
                width: 10,
              ),
              _AttributeWidget(
                color: const Color(0xffACDBFF),
                title: 'Độ ẩm',
                assetName: 'asset/umbrella_white.png',
                content: selectItem.humidity.toStringAsFixed(1).toString() + '%',
              )
            ],
          ),
        )
      ],
    );
  }
}

class _Card extends StatelessWidget {
  final bool select;
  final void Function() onTap;
  final int ts;
  final String iconCode;

  final DateTime _dateTime;
  _Card({this.select = false, this.onTap, this.ts, this.iconCode})
      : _dateTime = ts == null ? null : DateTime.fromMillisecondsSinceEpoch(ts * 1000);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) onTap();
      },
      child: SizedBox(
        width: 60,
        child: Padding(
          padding: const EdgeInsets.only(right: 10),
          child: DecoratedBox(
            decoration: BoxDecoration(
                color: select ? AppColor.primary : null,
                border: Border.all(color: AppColor.primary, width: 1),
                borderRadius: const BorderRadius.all(Radius.circular(8))),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Text(
                  _dateTime == null ? '' : _dateTime.day.toString(),
                  style: TextStyle(fontWeight: FontWeight.bold, color: select ? Colors.white : null),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  _dateTime == null ? '' : DateFormat('EEE').format(_dateTime),
                  style: TextStyle(color: select ? Colors.white : null),
                ),
                const SizedBox(
                  height: 5,
                ),
                Expanded(
                  child: ConstrainedBox(
                    constraints: const BoxConstraints.expand(),
                    child: DecoratedBox(
                      decoration: const BoxDecoration(
                          color: AppColor.primary,
                          borderRadius:
                              BorderRadius.only(bottomLeft: Radius.circular(8), bottomRight: Radius.circular(8))),
                      child: iconCode == null
                          ? const SizedBox.shrink()
                          : Image.network(
                              'https://openweathermap.org/img/wn/$iconCode@4x.png',
                            ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _AttributeWidget extends StatelessWidget {
  final String title;
  final String content;
  final String assetName;
  final Color color;

  const _AttributeWidget(
      {@required this.title, @required this.color, @required this.content, @required this.assetName});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: 35,
          height: 35,
          child: DecoratedBox(
            decoration: BoxDecoration(color: color, borderRadius: const BorderRadius.all(Radius.circular(8))),
            child: Padding(
              padding: const EdgeInsets.all(
                9,
              ),
              child: Image.asset(assetName),
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title),
            Text(
              content,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        )
      ],
    );
  }
}
