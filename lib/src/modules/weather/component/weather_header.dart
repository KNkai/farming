import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/modules/select_area/model/map_model.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class WeatherHeader extends StatelessWidget {
  const WeatherHeader({
    Key key,
    @required this.time,
    @required this.locationName,
    @required this.weatherIcon,
    @required this.currentWeather,
  }) : super(key: key);

  final String time, locationName;
  final String weatherIcon;
  final Current currentWeather;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
      color: AppColor.primary,
      child: Column(
        children: [
          Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Dự báo lúc',
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  ),
                  Text(
                    time,
                    style: const TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  const SpacingBox(height: 1),
                  Text(
                    locationName,
                    style: const TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Image.network(
              'https://openweathermap.org/img/wn/$weatherIcon@4x.png',
              height: 120,
              width: 120,
            )
          ]),
          Row(
            children: [
              Text(
                (currentWeather?.temp == null ? '' : currentWeather.temp.toString()) + '°C',
                style: const TextStyle(fontWeight: FontWeight.w800, fontSize: 35, color: Colors.white),
              ),
              const SpacingBox(width: 10),
              _Percent(
                icon: Image.asset('asset/umbrella_white.png'),
                percent: currentWeather?.dewPoint?.toInt(),
              ),
              const SpacingBox(width: 4),
              _Percent(
                icon: Image.asset('asset/drop_white.png'),
                percent: currentWeather?.humidity?.toInt(),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class _Percent extends StatelessWidget {
  final Widget icon;
  final int percent;

  const _Percent({@required this.icon, @required this.percent});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 17,
          height: 17,
          child: icon,
        ),
        const SizedBox(
          width: 2,
        ),
        Text(
          (percent == null ? '  ' : percent.toString()) + '%',
          style: const TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color: Colors.white),
        )
      ],
    );
  }
}
