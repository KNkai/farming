import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../select_area/model/map_model.dart';
import 'weather_hourly_property.dart';

class ListWeatherHourlyProperty extends StatelessWidget {
  const ListWeatherHourlyProperty({
    Key key,
    @required this.hourlyWeather,
  }) : super(key: key);

  final Current hourlyWeather;

  @override
  Widget build(BuildContext context) {
    var properties = [
      [
        {
          "title": "Mặt trời mọc lúc",
          "icon": "asset/sun.png",
          "content": _timeFromTs('HH:mm', hourlyWeather?.sunrise),
        },
        {
          "title": "Mặt trời lặn lúc",
          "icon": "asset/sunset.png",
          "content": _timeFromTs('HH:mm', hourlyWeather?.sunset)
        },
      ],
      [
        {"title": "Nhiệt độ cảm nhận", "content": hourlyWeather?.feelsLikeText(), "icon": "asset/thermometer.png"},
        {
          "title": "Chỉ số UV",
          "content": hourlyWeather?.uvi == null ? '' : hourlyWeather.uvi.toString(),
          "icon": "asset/uv.png"
        }
      ],
      [
        {"title": "Sương mù ở", "content": hourlyWeather?.dewPointText(), "icon": "asset/fog.png"},
        {"title": "Tỷ lệ có mây", "content": hourlyWeather?.cloudsText(), "icon": "asset/cloud.png"}
      ],
      [
        {"title": "Tầm nhìn trung bình", "content": hourlyWeather?.visibilityText(), "icon": "asset/vision.png"},
        {"title": "Tốc độ gió", "content": hourlyWeather?.windSpeedText(), "icon": "asset/wind_speed.png"}
      ],
      [
        {"title": "Áp suất", "content": hourlyWeather?.pressureText(), "icon": "asset/wind.png"},
        {"title": "Hướng gió", "content": hourlyWeather?.windDegText(), "icon": "asset/wind_direction.png"}
      ]
    ];
    return Column(
        children: properties
            .map((r) => Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Row(
                    children: r
                        .map((p) => Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: WeatherHourlyProperty(
                                  title: p["title"],
                                  content: p["content"],
                                  icon: p["icon"],
                                ),
                              ),
                            ))
                        .toList(),
                  ),
                ))
            .toList());
  }

  String _timeFromTs(String format, int ts) =>
      ts == null ? '' : DateFormat(format).format(DateTime.fromMillisecondsSinceEpoch(ts * 1000));
}
