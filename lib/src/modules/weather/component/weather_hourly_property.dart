import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';

class WeatherHourlyProperty extends StatelessWidget {
  const WeatherHourlyProperty({
    Key key,
    @required this.content,
    @required this.title,
    @required this.icon,
  }) : super(key: key);

  final String content, title, icon;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 20,
          height: 20,
          child: Image.asset(icon),
        ),
        const SpacingBox(width: 2),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title ?? "",
              style: const TextStyle(color: Colors.black54),
            ),
            Text(
              content ?? "",
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            )
          ],
        )
      ],
    );
  }
}
