import 'package:flutter/material.dart';

import '../../../base_widget/base_widget.dart';
import '../../../base_widget/src/spacing_box.dart';
import '../../../navigator.dart';
import '../../share/components/diary_card.dart';
import '../../share/widget/app_scaffold.dart';
import '../../share/widget/components/filter_sheet.dart';
import '../../share/widget/empty.dart';
import '../../share/widget/error.dart';
import '../../share/widget/load_more.dart';
import '../../share/widget/sort_btn.dart';
import 'diary_bloc.dart';
import 'diary_bottom_sheet.dart';
import 'listDiary_event.dart';
import 'listDiary_state.dart';

class DiaryPage extends StatefulWidget {
  @override
  _DiaryPageState createState() => _DiaryPageState();
}

class _DiaryPageState extends State<DiaryPage> {
  DiaryBloc bloc = DiaryBloc();
  List<FilterGroup> filterGroups;
  @override
  void initState() {
    bloc.add(LoadListDiaryEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      onBack: true,
      onSearch: (text) {
        bloc.add(ChangeFilterEvent(filter: bloc.filterValue.copyWith(search: text)));
      },
      onAdd: () {
        //showDialog(context: context, builder: (context) => AddDiaryPage());
        showModalBottomSheet(
            context: context,
            useRootNavigator: true,
            isScrollControlled: true,
            builder: (context) => DiaryBottomSheet(
                  onCreated: (diary) {
                    navigatorKey.currentState.pushNamed(View.detailDiaryPage, arguments: diary).then((value) {
                      bloc.add(ChangeFilterEvent(filter: bloc.filterValue));
                    });
                  },
                ));
        // BottomDiaryPopupSheet()).then((value) => bloc.add(LoadListDiaryEvent()));
      },
      child: LoadingTask(
        bloc: bloc,
        child: Column(
          children: [
            buildDiarySortOption(),
            const SpacingBox(height: 2),
            Expanded(
              child: StreamBuilder(
                stream: bloc.diaryStream,
                initialData: LoadingListDiaryState(),
                builder: (context, snap) {
                  if (snap.data is LoadingListDiaryState) {
                    return const Center();
                  }
                  if (snap.data is LoadListDiaryFailState) {
                    return const Center(
                      child: ErrorImgWidget(),
                    );
                  }
                  if (snap.data is LoadListDiarySuccessState) {
                    final list = (snap.data as LoadListDiarySuccessState).diary;
                    if (list.length == 0) {
                      return const EmptyWidget(text: 'Không tìm thấy kết quả');
                    }
                    return LoadMoreScrollView(
                        physics: const AlwaysScrollableScrollPhysics(),
                        bloc: bloc,
                        itemCount: list.length,
                        itemBuilder: (context, index) => buildDiaryCard(list[index]),
                        separatorBuilder: (context, index) => const SpacingBox(height: 0));
                  }
                  return Container();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  DiaryCard buildDiaryCard(e) {
    return DiaryCard(
      key: ObjectKey(e["id"]),
      name: e["name"],
      createrName: e["createrName"] ?? 'Chuyên viên',
      startDate: e["startAt"] != null ? DateTime.tryParse(e["startAt"])?.toLocal() : null,
      endDate: e["endAt"] != null ? DateTime.tryParse(e["endAt"])?.toLocal() : null,
      production: (e["production"] ?? 0).toDouble(),
      unit: e["unit"],
      status: e["status"],
      data: e,
      press: () {
        navigatorKey.currentState.pushNamed(View.detailDiaryPage, arguments: e).then((value) {
          bloc.add(ChangeFilterEvent(filter: bloc.filterValue));
        });
      },
    );
  }

  FutureBuilder<List> buildDiarySortOption() {
    return FutureBuilder<List>(
        initialData: const [],
        future: bloc.getSortOption(),
        builder: (context, snapshot) {
          if (!snapshot.hasData || snapshot.data.length == 0) {
            return Container();
          }
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SortBtn((value) {
                  bloc.add(ChangeFilterEvent(filter: bloc.filterValue.copyWith(order: value)));
                }, snapshot.data),
              ],
            ),
          );
        });
  }
}
