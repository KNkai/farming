import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/model/filter_model.dart';

class LoadListDiaryEvent extends BaseEvent {
  LoadListDiaryEvent();
}

class ChangeFilterEvent extends BaseEvent {
  FilterModel filter;
  ChangeFilterEvent({this.filter});
}
