import '../../../base_config/base_config.dart';
import '../../../base_config/src/utils/validate_builder.dart';
import '../../../base_widget/base_widget.dart';
import '../../../base_widget/src/spacing_box.dart';
import '../../../navigator.dart';
import '../../share/components/datepicker_input.dart';
import '../../share/components/select_input.dart';
import '../../share/components/select_list_sheet.dart';
import '../../share/components/text_input.dart';
import '../../share/model/enum.dart';
import '../../share/repo/diary_repo.dart';
import '../region/region_srv.dart';

class DiaryBottomSheet extends StatefulWidget {
  final bool isCreate;
  final String regionId;
  final String materialId;
  final dynamic diary;
  final Function onChanged, onCreated;
  const DiaryBottomSheet({
    Key key,
    this.isCreate = true,
    this.regionId,
    this.materialId,
    this.diary,
    this.onChanged,
    this.onCreated,
  }) : super(key: key);

  @override
  _DiaryBottomSheetState createState() => _DiaryBottomSheetState();
}

class _DiaryBottomSheetState extends State<DiaryBottomSheet> {
  GlobalKey<FormState> diaryForm = new GlobalKey(debugLabel: "diaryForm");
  String materialId, regionId;
  String name;
  DateTime startAt, endAt;
  int production = 0;
  String unit;
  String status;

  @override
  void initState() {
    regionId = widget.regionId;
    materialId = widget.materialId;
    if (!widget.isCreate && widget.diary != null) {
      name = widget.diary["name"] ?? '';
      production = (widget.diary["production"] ?? 0);
      unit = widget.diary["unit"] ?? '';
      status = widget.diary["status"] ?? 'OPENING';
      startAt = widget.diary["startAt"] != null ? DateTime.tryParse(widget.diary["startAt"]) : null;
      endAt = widget.diary["endAt"] != null ? DateTime.tryParse(widget.diary["endAt"]) : null;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.5 + MediaQuery.of(context).viewInsets.bottom,
      child: Scaffold(
        appBar: buildSheetBar(context,
            title: "Nhật ký canh tác", btnTitle: widget.isCreate ? "Thêm mới" : "Lưu", onPress: updateDiary),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: diaryForm,
            child: ListView(
              children: [
                if (widget.regionId == null) ...[
                  SelectListInput(
                    sheetTitle: "Chọn địa điểm canh tác",
                    labelText: "Địa điểm canh tác",
                    hintText: 'Chọn địa điểm canh tác',
                    sheetItems: getRegionSelectListItems(),
                    onSaved: (value) => regionId = value,
                    onSelect: (item) {
                      regionId = item.id;
                      materialId = item.data["materialId"];
                    },
                    validator: ValidateBuilder().empty().build(),
                  ),
                  const SpacingBox(height: 1)
                ],
                TextInput(
                  labelText: "Tên nhật ký",
                  hintText: "Nhập tên nhật ký",
                  value: name,
                  onSaved: (value) => name = value,
                  validator: ValidateBuilder().empty().build(),
                ),
                const SpacingBox(height: 1),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: DatepickerInput(
                        labelText: "Thời hạn từ",
                        hintText: "Chọn ngày",
                        maxTime: endAt,
                        value: startAt,
                        onSelected: (date) {
                          setState(() {
                            startAt = date;
                          });
                        },
                        onSaved: (date) => startAt = date,
                        validator: ValidateBuilder().empty().build(),
                      ),
                    ),
                    const SpacingBox(width: 4),
                    Expanded(
                      child: DatepickerInput(
                        labelText: "Thời hạn đến",
                        hintText: "Chọn ngày",
                        minTime: startAt,
                        value: endAt,
                        onSelected: (date) {
                          setState(() {
                            endAt = date;
                          });
                        },
                        onSaved: (date) => endAt = date,
                      ),
                    ),
                  ],
                ),
                const SpacingBox(height: 1),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: TextInput(
                        labelText: "Sản lượng",
                        hintText: "Nhập sản lượng",
                        value: production.toString() ?? "0",
                        keyboardType: TextInputType.number,
                        onSaved: (value) => production = int.parse(value),
                        validator: (val) {
                          final check = int.tryParse(val);
                          return check == null ? '' : null;
                        },
                      ),
                    ),
                    const SpacingBox(width: 4),
                    Expanded(
                        child: TextInput(
                      labelText: "Đơn vị",
                      hintText: "Nhập đơn vị",
                      value: unit,
                      onSaved: (value) => unit = value,
                      validator: ValidateBuilder().empty().build(),
                    )),
                  ],
                ),
                const SpacingBox(height: 1),
                if (!widget.isCreate) ...[
                  SelectListInput(
                    labelText: "Trạng thái",
                    hintText: "Chọn trạng thái",
                    sheetItems: AppEnum.diaryStatusEnum.entries
                        .map((e) => SelectListSheetItem(display: e.value, id: e.key))
                        .toList(),
                    selectedId: status,
                    onSaved: (value) => status = value,
                    validator: ValidateBuilder().empty().build(),
                  ),
                  const SpacingBox(height: 1),
                ],
                const SpacingBox(height: 2),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void updateDiary() {
    if (diaryForm.currentState.validate()) {
      diaryForm.currentState.save();
      if (widget.isCreate) {
        DiaryRepo()
            .createDiary(
                regionId: regionId,
                materialId: materialId,
                name: name,
                startAt: startAt.toString(),
                endAt: endAt.toString(),
                production: production,
                unit: unit)
            .then((diary) {
          navigatorKey.currentState.pop();
          if (widget.onCreated != null) widget.onCreated(diary);
        }).catchError((err) {
          showToast("Có lỗi xảy ra: ${err.toString()}");
        });
      } else {
        DiaryRepo()
            .updateDiary(
                id: widget.diary["id"],
                name: name,
                startAt: startAt.toString(),
                endAt: endAt.toString(),
                production: production,
                unit: unit,
                status: status)
            .then((diary) {
          navigatorKey.currentState.pop();
          showToast("Cập nhật thành công");
          if (widget.onChanged != null) {
            widget.onChanged(diary);
          }
        }).catchError((err) {
          showToast("Có lỗi xảy ra: ${err.toString()}");
        });
      }
    }
  }

  Future<List<SelectListSheetItem>> getRegionSelectListItems() {
    return RegionSrv().getList(limit: 100, fragment: "name id materialId").then((value) {
      return value["data"]
          .map<SelectListSheetItem>((e) => SelectListSheetItem(display: e['name'], id: e['id'], data: e))
          .toList();
    });
  }
}
