import 'dart:async';

import 'package:flutter/material.dart';

import '../../../base_config/base_config.dart';
import '../../../base_config/src/base/bloc_listener.dart';
import '../../../base_widget/base_widget.dart';
import '../../../base_widget/src/dialog.dart';
import '../../../navigator.dart';
import '../../share/components/select_input.dart';
import '../../share/components/select_list_sheet.dart';
import '../../share/utils/color.dart';
import '../../share/widget/text_form.dart';
import '../region/listRegion_repo.dart';
import 'detailDiary_event.dart';
import 'diary_bloc.dart';
import 'listDiary_state.dart';

class BottomDiaryPopupSheet extends StatefulWidget {
  final bool isCreate;
  final String regionId;
  final String materialId;
  final dynamic diary;
  const BottomDiaryPopupSheet({
    this.isCreate = true,
    this.regionId,
    this.diary,
    this.materialId,
  });

  @override
  _BottomDiaryPopupSheetState createState() => _BottomDiaryPopupSheetState();
}

class _BottomDiaryPopupSheetState extends State<BottomDiaryPopupSheet> {
  final TextEditingController _name = TextEditingController();
  GlobalKey<FormState> _fromKey = new GlobalKey();
  DateTime _fromDate;

  DateTime _toDate;

  String _materialId;
  String _regionId;

  final TextEditingController _fromDateC = TextEditingController();

  final TextEditingController _toDateC = TextEditingController();

  final TextEditingController _status = TextEditingController();

  final TextEditingController _production = TextEditingController();

  final TextEditingController _unit = TextEditingController();

  DiaryBloc _bloc = DiaryBloc();

  @override
  void initState() {
    _regionId = widget.regionId;
    _materialId = widget.materialId;
    if (!widget.isCreate && widget.diary != null) {
      _name.text = widget.diary["name"] ?? '';
      _production.text = (widget.diary["production"] ?? '').toString();
      _unit.text = widget.diary["unit"] ?? '';
      _status.text = widget.diary["status"] ?? 'CLOSED';
      _fromDate = widget.diary["startAt"] != null ? DateTime.tryParse(widget.diary["startAt"]) : null;
      _toDate = widget.diary["endAt"] != null ? DateTime.tryParse(widget.diary["endAt"]) : null;
      _fromDateC.text = widget.diary["startAt"] != null ? Formart.formatToDate(widget.diary["startAt"]) : '';
      _toDateC.text = widget.diary["endAt"] != null ? Formart.formatToDate(widget.diary["endAt"]) : '';
    }
    super.initState();
  }

  _handleState(BaseState state) {
    if (state is UpdateDiaryFailState) {
      Timer.run(() {
        showAlertDialog(context, 'Cập nhật không thành công. Vui lòng thử lại.', navigatorKey: navigatorKey);
      });
    }
    if (state is UpdateDiarySuccessState) {
      showToast('Cập nhật thành công.');
      navigatorKey.currentState.maybePop();
    }
    if (state is CreateDiaryFailState) {
      Timer.run(() {
        showAlertDialog(context, 'Có lỗi xảy ra: ${state.err}', navigatorKey: navigatorKey);
      });
    }
    if (state is CreateDiarySuccessState) {
      showToast('Cập nhật thành công.');
      navigatorKey.currentState.maybePop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: deviceHeight(context) - 85),
      child: LoadingTask(
        bloc: _bloc,
        child: BlocListener(
          bloc: _bloc,
          listener: _handleState,
          child: Container(
            color: Colors.white,
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 65, left: 15, bottom: 15, right: 15),
                    child: Form(
                      key: _fromKey,
                      child: Column(
                        children: [
                          if (widget.regionId == null)
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SelectListInput(
                                  sheetTitle: "Chọn địa điểm canh tác",
                                  labelText: "Địa điểm canh tác",
                                  hintText: 'Chọn địa điểm canh tác',
                                  sheetItems: RegionRepo().getList(limit: 100).then((value) => value
                                      .map((e) => SelectListSheetItem(display: e['name'], id: e['id'], data: e))
                                      .toList()),
                                  onSaved: null,
                                  onSelect: (item) {
                                    _regionId = item.id;
                                    _materialId = item.data[""];
                                  }),
                            ),
                          TextForm(
                            hint: 'Tên nhật ký',
                            keyboardType: TextInputType.text,
                            shouldValidate: true,
                            controller: _name,
                            hasBorder: true,
                            placeholder: "Nhập tên nhật ký",
                            onChange: (text) {
                              widget.diary["name"] = text;
                            },
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  child: TextDateForm(
                                    hint: 'Thời hạn từ',
                                    placeholder: 'Chọn thời gian',
                                    initialValue: _fromDate,
                                    shouldValidate: true,
                                    hasBorder: true,
                                    onSelected: (date) {
                                      _fromDate = date;
                                      _fromDateC.text = "${date.day}/${date.month}/${date.year}";
                                      widget.diary['startAt'] = date.toString();
                                    },
                                    controller: _fromDateC,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  child: TextDateForm(
                                    onSelected: (date) {
                                      _toDate = date;
                                      _toDateC.text = "${date.day}/${date.month}/${date.year}";
                                      widget.diary['endAt'] = date.toString();
                                    },
                                    initialValue: _toDate,
                                    hint: 'Thời hạn đến',
                                    placeholder: 'Chọn thời gian',
                                    shouldValidate: true,
                                    hasBorder: true,
                                    controller: _toDateC,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: TextForm(
                                  hint: 'Sản lượng',
                                  placeholder: 'Nhập sản lượng',
                                  keyboardType: TextInputType.number,
                                  shouldValidate: true,
                                  controller: _production,
                                  hasBorder: true,
                                  onChange: (text) {
                                    widget.diary['production'] = double.parse(text);
                                  },
                                ),
                              ),
                              Expanded(
                                child: TextForm(
                                  hint: 'Đơn vị',
                                  placeholder: 'Nhập đơn vị',
                                  keyboardType: TextInputType.text,
                                  shouldValidate: true,
                                  controller: _unit,
                                  hasBorder: true,
                                  onChange: (text) {
                                    widget.diary['unit'] = text;
                                  },
                                ),
                              ),
                            ],
                          ),
                          if (!widget.isCreate)
                            Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SelectListInput(
                                  sheetTitle: "Chọn trạng thái",
                                  labelText: "Trạng thái",
                                  selectedId: _status.text,
                                  sheetItems: [
                                    SelectListSheetItem(display: "Đang mở", id: "OPENING"),
                                    SelectListSheetItem(display: "Đã đóng", id: "CLOSED"),
                                  ],
                                  onSaved: (value) {},
                                  onSelect: (item) => _status.text = item.id,
                                )),
                          SizedBox(
                            height: MediaQuery.of(context).viewInsets.bottom,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  height: 50,
                  width: deviceWidth(context),
                  decoration: const BoxDecoration(
                    color: AppColor.primary,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () => navigatorKey.currentState.maybePop(),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      const Text(
                        'Nhật ký canh tác',
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                      ),
                      const Spacer(),
                      InkWell(
                        onTap: () {
                          if (!_fromKey.currentState.validate()) {
                            return;
                          }
                          widget.isCreate
                              ? _bloc.add(CreateDiaryEvent(
                                  regionId: _regionId,
                                  materialId: _materialId,
                                  name: _name.text,
                                  startAt: _fromDate.toString(),
                                  endAt: _toDate.toString(),
                                  //status: _status.text,
                                  production: int.tryParse(_production.text) ?? 0,
                                  unit: _unit.text))
                              : _bloc.add(UpdateDiaryEvent(
                                  id: widget.diary["id"],
                                  name: _name.text,
                                  startAt: _fromDate.toString(),
                                  endAt: _toDate.toString(),
                                  status: _status.text,
                                  production: int.tryParse(_production.text) ?? 0,
                                  unit: _unit.text));
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4), border: Border.all(color: Colors.white)),
                          child: Text(
                            widget.isCreate ? 'Tạo mới' : "Lưu nhật ký",
                            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
