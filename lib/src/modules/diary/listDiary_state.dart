import 'package:mi_smart/base_config/base_config.dart';

class LoadListDiarySuccessState extends BaseState {
  List diary;
  LoadListDiarySuccessState(this.diary);
}

class LoadListDiaryFailState extends BaseState {
  String err;
  LoadListDiaryFailState(this.err);
}

class LoadingListDiaryState extends BaseState {}

class UpdateDiarySuccessState extends BaseState {
  UpdateDiarySuccessState();
}

class UpdateDiaryFailState extends BaseState {
  String err;
  UpdateDiaryFailState(this.err);
}

class CreateDiarySuccessState extends BaseState {
  CreateDiarySuccessState();
}

class CreateDiaryFailState extends BaseState {
  String err;
  CreateDiaryFailState(this.err);
}
