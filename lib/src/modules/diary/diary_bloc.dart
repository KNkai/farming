import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/base_bloc.dart';
import 'package:mi_smart/base_config/src/base/base_event.dart';
import 'package:mi_smart/src/share/model/filter_model.dart';
import 'package:mi_smart/src/share/repo/diaryPharse_repo.dart';
import 'package:mi_smart/src/share/repo/diary_repo.dart';
import 'package:mi_smart/src/share/repo/material_repo.dart';
import 'package:mi_smart/src/share/repo/zone_repo.dart';
import 'package:mi_smart/src/share/widget/load_more.dart';
import 'package:rxdart/rxdart.dart';

import '../../../base_config/base_config.dart';
import '../../../base_config/src/base/base_bloc.dart';
import '../../../base_config/src/base/base_event.dart';
import '../../share/model/filter_model.dart';
import '../../share/repo/diaryPharse_repo.dart';
import '../../share/repo/diary_repo.dart';
import '../../share/repo/material_repo.dart';
import '../../share/repo/zone_repo.dart';
import '../../share/widget/load_more.dart';
import 'detailDiary_event.dart';
import 'listDiary_event.dart';
import 'listDiary_state.dart';

class DiaryBloc extends BaseBloc with ChangeNotifier {
  DiaryRepo _diaryRepo = DiaryRepo();
  List lList = [];
  List materials = [];
  List zones = [];

  final _diaryState$ = BehaviorSubject<BaseState>();
  Stream<BaseState> get diaryStream => _diaryState$.stream;
  Sink<BaseState> get diarySink => _diaryState$.sink;
  BaseState get diaryValue => _diaryState$.value;

  final _filter$ = BehaviorSubject<FilterModel>();
  Stream<FilterModel> get filterStream => _filter$.stream;
  Sink<FilterModel> get filterSink => _filter$.sink;
  FilterModel get filterValue => _filter$.value;

  final _diaryPharse$ = BehaviorSubject<List>();
  Stream<List> get diaryPharseStream => _diaryPharse$.stream;
  Sink<List> get diaryPharseSink => _diaryPharse$.sink;
  List get diaryPharseValue => _diaryPharse$.value;

  bool isLoadingListDiary = false;

  DiaryBloc() {
    getListFilters();

    FilterModel initFilter = FilterModel(limit: 5, page: 1, order: "{createdAt : -1}");
    filterSink.add(initFilter);
  }

  @override
  void dispatchEvent(BaseEvent event) {
    if (event is LoadListDiaryEvent) getList(event);
    if (event is ChangeFilterEvent) changeFilter(event);
    if (event is LoadMoreEvent) loadMore();
    if (event is CreateDiaryEvent) createDiary(event);
    if (event is UpdateDiaryEvent) updateDiary(event);
  }

  @override
  void dispose() {
    _diaryState$.close();
    _filter$.close();
    _diaryPharse$.close();
    super.dispose();
  }

  Future<List> getSortOption() async {
    return _diaryRepo.getSortOptions();
  }

  void getListFilters() {
    MaterialRepo().getAllMaterial().then((value) {
      materials = value;
    });
    ZoneRepo().getAllZone().then((value) {
      zones = value;
    });
  }

  Future getList(LoadListDiaryEvent event) async {
    try {
      loadingSink.add(true);
      final res = await _diaryRepo.getList(
          page: filterValue?.page,
          limit: filterValue?.limit,
          filter: filterValue?.filter,
          search: filterValue?.search,
          offset: filterValue?.offset,
          order: filterValue?.order);
      diarySink.add(LoadListDiarySuccessState(res));
      lList = res;
    } catch (e) {
      diarySink.add(LoadListDiaryFailState(e.message));
    } finally {
      loadingSink.add(false);
    }
  }

  Future loadMore() async {
    if (isLoadingListDiary) return;
    try {
      isLoadingListDiary = true;
      final res = await _diaryRepo.getList(
          page: (filterValue?.page ?? 1) + 1,
          limit: filterValue?.limit,
          filter: filterValue?.filter,
          search: filterValue?.search,
          offset: filterValue?.offset,
          order: filterValue?.order);
      if (res.length > 0) {
        lList.addAll(res);
        filterValue.page++;
        filterSink.add(filterValue);
        diarySink.add(LoadListDiarySuccessState(lList));
      }
    } catch (e) {
      diarySink.add(LoadListDiaryFailState(e.toString()));
    } finally {
      isLoadingListDiary = false;
    }
  }

  Future changeFilter(ChangeFilterEvent event) async {
    try {
      loadingSink.add(true);
      filterSink.add(event.filter.copyWith(page: 1));
      getList(LoadListDiaryEvent());
    } catch (e) {
      diarySink.add(LoadListDiaryFailState(e.toString()));
    } finally {
      loadingSink.add(false);
    }
  }

  Future createDiary(CreateDiaryEvent event) async {
    try {
      await _diaryRepo.createDiary(
          name: event.name,
          startAt: event.startAt,
          endAt: event.endAt,
          production: event.production,
          materialId: event.materialId,
          regionId: event.regionId,
          unit: event.unit);
      processStateSink.add(CreateDiarySuccessState());
    } catch (e) {
      processStateSink.add(CreateDiaryFailState(e));
    } finally {}
  }

  Future updateDiary(UpdateDiaryEvent event) async {
    try {
      await _diaryRepo.updateDiary(
          id: event.id,
          name: event.name,
          startAt: event.startAt,
          endAt: event.endAt,
          status: event.status,
          production: event.production,
          unit: event.unit);
      processStateSink.add(CreateDiarySuccessState());
    } catch (e) {
      processStateSink.add(CreateDiaryFailState(e.toString()));
    } finally {}
  }

  Future getAllPharseOfDiary(String id) async {
    try {
      final res = await DiaryPharseRepo().getListByDiaryiId(id);
      diaryPharseSink.add(res);
    } catch (e) {
      diaryPharseSink.add([]);
    } finally {}
  }
}
