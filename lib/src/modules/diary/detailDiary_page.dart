import 'package:flutter/material.dart';
import 'package:mi_smart/navigator.dart';

import '../../../base_widget/src/spacing_box.dart';
import '../../share/components/diary_card.dart';
import '../../share/components/diary_info.dart';
import '../../share/widget/app_scaffold.dart';
import 'diary_bloc.dart';
import 'diary_bottom_sheet.dart';

class DetailDiaryPage extends StatefulWidget {
  final dynamic diary;
  const DetailDiaryPage(this.diary);

  @override
  _DetailDiaryPageState createState() => _DetailDiaryPageState();
}

class _DetailDiaryPageState extends State<DetailDiaryPage> {
  DiaryBloc _bloc = DiaryBloc();
  dynamic diary;

  @override
  void initState() {
    super.initState();
    diary = widget.diary;
    _bloc.getAllPharseOfDiary(diary["id"]);
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      onBack: true,
      title: 'Chi tiết nhật ký',
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            DiaryCard(
              data: diary,
              name: diary["name"],
              createrName: diary["createrName"] ?? "Chuyên viên",
              startDate: diary["startAt"] != null ? DateTime.tryParse(diary["startAt"])?.toLocal() : null,
              endDate: diary["endAt"] != null ? DateTime.tryParse(diary["endAt"])?.toLocal() : null,
              production: (diary["production"] ?? 0).toDouble(),
              unit: diary["unit"],
              status: diary["status"],
              press: () {},
              editDiary: () {
                showModalBottomSheet(
                    context: context,
                    useRootNavigator: true,
                    isScrollControlled: true,
                    builder: (context) => DiaryBottomSheet(
                          isCreate: false,
                          regionId: diary["regionId"],
                          diary: diary,
                          onChanged: (data) {
                            setState(() {
                              diary = data;
                            });
                          },
                        ));
              },
            ),
            const SpacingBox(height: 1),
            StreamBuilder<List>(
              stream: _bloc.diaryPharseStream,
              builder: (context, snap) {
                if (snap.hasData) {
                  return DiaryInfo(snap.data, itemTap);
                }
                return Container();
              },
            ),
          ],
        ),
      ),
    );
  }

  void itemTap(item) {
    navigatorKey.currentState
        .pushNamed(View.phase, arguments: item)
        .then((value) => _bloc.getAllPharseOfDiary(diary["id"]));
  }
}
