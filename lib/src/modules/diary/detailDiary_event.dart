import 'package:mi_smart/base_config/base_config.dart';

class CreateDiaryEvent extends BaseEvent {
  String name;
  String startAt;
  String endAt;
  int production;
  String unit;
  String materialId;
  String regionId;
  CreateDiaryEvent({
    this.name,
    this.startAt,
    this.endAt,
    this.production,
    this.unit,
    this.materialId,
    this.regionId,
  });
}

class UpdateDiaryEvent extends BaseEvent {
  String id;
  String name;
  String startAt;
  String endAt;
  int production;
  String unit;
  String status;
  UpdateDiaryEvent({
    @required this.id,
    this.name,
    this.startAt,
    this.endAt,
    this.production,
    this.unit,
    this.status,
  });
}
