// import 'package:flutter/material.dart';
// import 'package:mi_smart/base_config/base_config.dart';
// import 'package:mi_smart/navigator.dart';
// import 'package:mi_smart/src/share/utils/color.dart';

// class BottomSelectRegion extends StatefulWidget {
//   BottomSelectRegion();

//   @override
//   _BottomSelectRegionState createState() => _BottomSelectRegionState();
// }

// class _BottomSelectRegionState extends State<BottomSelectRegion> {
//   @override
//   Widget build(BuildContext context) {
//     return ConstrainedBox(
//       constraints: BoxConstraints(maxHeight: deviceHeight(context) - 85),
//       child: Container(
//         color: Colors.white,
//         child: Stack(
//           children: [
//             // ListView.builder(
//             //   padding: const EdgeInsets.only(top: 65, left: 15, bottom: 15, right: 15),
//             // ),
//             Container(
//               padding: EdgeInsets.only(left: 20, right: 20),
//               height: 50,
//               width: deviceWidth(context),
//               decoration: BoxDecoration(
//                 color: AppColor.primary,
//               ),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: [
//                   InkWell(
//                     onTap: () => navigatorKey.currentState.maybePop(),
//                     child: Padding(
//                       padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
//                       child: Icon(
//                         Icons.close,
//                         color: Colors.white,
//                       ),
//                     ),
//                   ),
//                   Text(
//                     'Chọn vùng canh tác',
//                     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
//                   ),
//                   Spacer(),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
