import 'package:flutter/material.dart';

import '../../../../base_config/src/utils/constants.dart';
import '../../../../base_config/src/utils/validate_builder.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../../base_widget/src/toast.dart';
import '../../../../navigator.dart';
import '../../../model/disease_comment.model.dart';
import '../../../share/components/bottom_sheet_wrapper.dart';
import '../../../share/components/remove_button.dart';
import '../../../share/components/select_image_list_input.dart';
import '../../../share/components/textarea_input.dart';
import '../../../share/repo/diseaseComment_repo.dart';

class CommentSheet extends StatefulWidget {
  final bool isCreate;
  final DiseaseComment comment;
  final Function(DiseaseComment) onUpdated;
  final Function onRemoved;
  const CommentSheet({
    Key key,
    this.isCreate = true,
    @required this.comment,
    this.onUpdated,
    this.onRemoved,
  }) : super(key: key);

  @override
  _CommentSheetState createState() => _CommentSheetState();
}

class _CommentSheetState extends State<CommentSheet> {
  String message;
  List<String> listImages;
  GlobalKey<FormState> commentForm = new GlobalKey(debugLabel: "commentForm");

  @override
  void initState() {
    super.initState();
    message = widget.comment.message;
    listImages = widget.comment.listImages ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheetWrapper(
      appBar: buildSheetBar(
        context,
        title: "Bình luận",
        btnTitle: widget.isCreate ? "Đăng" : "Cập nhật",
        onPress: updateComment,
      ),
      padding: const EdgeInsets.all(16),
      formKey: commentForm,
      children: [
        TextareaInput(
          labelText: "Tin nhắn",
          hintText: "Nhập tin nhắn",
          autofocus: true,
          value: message,
          onSaved: (value) => message = value,
          validator: ValidateBuilder().empty().build(),
        ),
        const SpacingBox(height: 2),
        SelectImageListInput(
          label: "Hình ảnh đính kèm",
          initialValue: listImages,
          onChanged: (images) => listImages = images,
        ),
        const SpacingBox(height: 2),
        if (!widget.isCreate) ...[
          RemoveButton(
            label: "Xoá bình luận",
            onRemove: removeComment,
          ),
          const SpacingBox(height: 2),
        ]
      ],
    );
  }

  removeComment() {
    DiseaseCommentRepo().removeComment(widget.comment.id).then((res) {
      if (widget.onRemoved != null) widget.onRemoved();
      navigatorKey.currentState.pop();
    }).catchError(handleError);
  }

  void updateComment() {
    if (commentForm.currentState.validate()) {
      commentForm.currentState.save();
      if (widget.isCreate) {
        DiseaseCommentRepo().createComment(widget.comment.situationId, message, listImages).then((value) {
          DiseaseComment comment = DiseaseComment.fromJson(value);
          if (widget.onUpdated != null) widget.onUpdated(comment);
          navigatorKey.currentState.pop(comment);
        }).catchError(handleError);
      } else {
        DiseaseCommentRepo().updateComment(widget.comment.id, message: message, listImages: listImages).then((value) {
          DiseaseComment comment = DiseaseComment.fromJson(value);
          if (widget.onUpdated != null) widget.onUpdated(comment);
          navigatorKey.currentState.pop(comment);
        }).catchError(handleError);
      }
    }
  }

  handleError(err) {
    showToast("Có lỗi xảy ra: ${err.toString()}", flagColor: false);
  }
}
