import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../base_widget/base_widget.dart';
import '../../../../base_widget/src/dialog.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../../navigator.dart';
import '../../../model/disease_situation.model.dart';
import '../../../share/repo/disease_repo.dart';
import '../../tabs_region_detail/components/situation_sheet.dart';
import '../controller/disease_situation_controller.dart';
import 'situation_footer.dart';
import 'situation_header.dart';
import 'situation_list_image.dart';
import 'situation_status_sheet.dart';

class SituationFullCard extends StatelessWidget {
  const SituationFullCard({
    Key key,
    @required this.situation,
  }) : super(key: key);
  final DiseaseSituation situation;

  @override
  Widget build(BuildContext context) {
    DiseaseSituationController controller = Provider.of<DiseaseSituationController>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SituationHeader(
          situation: situation,
          editable: true,
          onUpdate: () => buildEditSheet(context, controller),
          onDelete: () => buildRemoveConfirmDiary(context),
          onUpdateStatus: () => handleUpdateStatus(context, controller),
        ),
        const SpacingBox(height: 1),
        Text(
          situation.title,
          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
        ),
        const SpacingBox(height: 1),
        if (situation.description != null && situation.description.isNotEmpty) ...[
          Text(
            situation.description,
            style: const TextStyle(fontSize: 16),
          ),
          const SpacingBox(height: 1)
        ],
        Text(
          [situation.regionName, situation.ward, situation.district, situation.province].join(', '),
          style: const TextStyle(fontStyle: FontStyle.italic, fontSize: 13),
        ),
        const SpacingBox(height: 2),
        if (situation.listImages.length > 0) ...[
          SituationListImage(images: situation.listImages),
          const SpacingBox(height: 1),
        ],
        SituationFooter(
          status: situation.status,
          commentCount: situation.commentCount,
          diseaseName: situation.disease.name,
          onPress: () => handleUpdateStatus(context, controller),
          //nameLabel: situation.regionName,
        ),
      ],
    );
  }

  Future handleUpdateStatus(BuildContext context, DiseaseSituationController controller) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return SituationStatusSheet(
            situation: situation,
            onUpdated: (data) => controller.setSituation(data),
          );
        });
  }

  void buildRemoveConfirmDiary(BuildContext context) {
    return showConfirmDialog(context, 'Xác nhận xóa?', navigatorKey: navigatorKey, confirmTap: () {
      DiseaseSituationSrv().delete(situation.id).then((value) {
        navigatorKey.currentState.pop();
        showToast("Đã xoá", flagColor: false);
      }).catchError((err) {
        showToast("Có lỗi xảy ra: ${err.toString()}", flagColor: false);
      });
    });
  }

  Future buildEditSheet(BuildContext context, DiseaseSituationController controller) {
    return showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return SituationSheet(
            isCreate: false,
            situation: situation,
            onUpdated: (data) {
              controller.situationNotifier.value = data;
            },
          );
        });
  }
}
