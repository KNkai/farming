import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mi_smart/src/share/widget/image_view.dart';

class SituationListImage extends StatefulWidget {
  const SituationListImage({
    Key key,
    @required this.images,
  }) : super(key: key);
  final List<String> images;

  @override
  _SituationListImageState createState() => _SituationListImageState();
}

class _SituationListImageState extends State<SituationListImage> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  List<Widget> images;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: widget.images.map((image) {
              return Container(
                key: ObjectKey(image),
                height: 230,
                width: 230,
                margin: const EdgeInsets.only(right: 12),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(30),
                    child: ImageView(
                      tag: 'img' + Random().nextInt(100000000).toString(), // this need a unique id
                      url: image,
                    )),
              );
            }).toList() ??
            [],
      ),
    );
  }

  Container buildImage(String image) {
    return Container(
      height: 230,
      width: 230,
      margin: const EdgeInsets.only(right: 12),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: ImageView(
            tag: 'img' + Random().nextInt(100000000).toString(), // this need a unique id
            url: image,
            w: 230,
            h: 230,
          )),
    );
  }
}
