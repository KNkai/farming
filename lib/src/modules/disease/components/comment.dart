import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/share/widget/list_img_picker.dart';
import 'package:readmore/readmore.dart';

class Comment extends StatelessWidget {
  final String avatar;
  final String name;
  final String date;
  final String content;
  final List<String> images;
  final Function onDelete;

  const Comment({Key key, this.avatar, this.name, this.date, this.content, this.images, this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            CircleAvatar(
              radius: 20,
              backgroundImage: NetworkImage(avatar),
            ),
            const SpacingBox(width: 2),
            Text(
              name,
              style: const TextStyle(color: Colors.black87, fontWeight: FontWeight.w600),
            ),
            const Spacer(),
            Text(Formart.formatToDate(date)),
          ],
        ),
        const SpacingBox(height: 1.5),
        ReadMoreText(
          content ?? '',
          trimLines: 3,
          colorClickableText: Colors.grey,
          trimMode: TrimMode.Line,
          trimCollapsedText: '... Xem Thêm',
          trimExpandedText: ' Rút gọn',
          style: TextStyle(color: Colors.black87.withOpacity(0.8), fontWeight: FontWeight.w500, fontSize: 14),
        ),
        const SpacingBox(height: 1.5),
        if (images.length > 0)
          SizedBox(
            height: 70,
            child: ImageRowPicker(
              images,
              canRemove: false,
            ),
          ),
        if (onDelete != null)
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.max,
            children: [
              // IconButton(
              //   icon: Icon(
              //     Icons.edit,
              //     color: Colors.black45,
              //   ),
              //   onPressed: onDelete,
              // ),
              IconButton(
                icon: const Icon(
                  Icons.delete,
                  color: Colors.black45,
                ),
                onPressed: onDelete,
              ),
            ],
          ),
      ],
    );
  }
}
