import 'package:flutter/material.dart';

import '../../../../base_config/src/utils/formart.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../model/disease_comment.model.dart';
import '../../../share/components/status_text.dart';
import '../../../share/utils/color.dart';
import 'comment_images.dart';

class CommentCart extends StatelessWidget {
  const CommentCart({
    Key key,
    @required this.comment,
    this.onPress,
  }) : super(key: key);
  final DiseaseComment comment;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    bool dateMoreThanOneWeek = comment.createdAt.isBefore(DateTime.now().subtract(const Duration(days: 7)));
    String commentDateText =
        dateMoreThanOneWeek ? Format.ddsmmsyyyyhhmm(comment.createdAt) : Format.timeAgo(comment.createdAt);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            CircleAvatar(
              radius: 15,
              backgroundImage: NetworkImage(comment.user != null ? comment.user.avatar : comment.staff.avatar),
            ),
            const SpacingBox(width: 2),
            Text(
              comment.user != null ? comment.user.name : comment.staff.name,
              style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            const Spacer(),
            if (onPress != null)
              InkWell(
                onTap: onPress,
                child: const StatusText(text: "Chỉnh sửa", color: AppColor.primary),
              ),
          ]),
          const SpacingBox(height: 1),
          Text(comment.message, style: const TextStyle(fontSize: 16)),
          const SpacingBox(height: 1),
          Text(commentDateText, style: const TextStyle(fontSize: 12)),
          const SpacingBox(height: 1),
          if (comment.listImages.length > 0) ...[CommentImages(images: comment.listImages), const SpacingBox(height: 1)]
        ],
      ),
    );
  }
}
