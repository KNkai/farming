import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/src/share/components/comment.dart';
import 'package:mi_smart/src/share/components/status_text.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class SituationFooter extends StatelessWidget {
  const SituationFooter({
    Key key,
    @required this.status,
    this.onPress,
    this.commentCount = 0,
    this.nameLabel,
    this.diseaseName,
  }) : super(key: key);
  final String status;
  final int commentCount;
  final String nameLabel;
  final Function onPress;
  final String diseaseName;

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> statusMap = {
      "OPENING": {"color": AppColor.warning, "display": "Đang diễn ra"},
      "RESOLVED": {"color": AppColor.primary, "display": "Đã kết thúc"},
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (diseaseName != null)
          Row(
            children: [
              StatusText(
                text: diseaseName,
                color: AppColor.grey,
                textColor: Colors.black,
              ),
              const Spacer(),
              Text(nameLabel ?? ''),
            ],
          ),
        Row(
          children: [
            Comment(count: commentCount),
            const SpacingBox(width: 4),
            const Spacer(),
            InkWell(
                onTap: onPress,
                child: StatusText(
                  text: statusMap[status]['display'],
                  color: statusMap[status]['color'],
                )),
          ],
        ),
      ],
    );
  }
}
