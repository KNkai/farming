import 'package:flutter/material.dart';

import '../../../../base_config/src/utils/formart.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../model/disease_situation.model.dart';
import '../../../share/components/select_list_sheet.dart';

class SituationHeader extends StatelessWidget {
  const SituationHeader({
    Key key,
    @required this.situation,
    this.onDelete,
    this.onUpdate,
    this.onUpdateStatus,
    this.editable = false,
  }) : super(key: key);
  final Function onDelete, onUpdate, onUpdateStatus;
  final bool editable;
  final DiseaseSituation situation;

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      CircleAvatar(
        radius: 15,
        backgroundImage: NetworkImage(situation.getReporterAvatar() ?? "https://placehold.it/200"),
      ),
      const SpacingBox(width: 2),
      Text(
        situation.getReporterName(),
        style: const TextStyle(color: Colors.black87, fontWeight: FontWeight.w600),
      ),
      const Spacer(),
      Text(Format.ddsmmsyyyy(situation.reportedAt)),
      if (editable) ...[
        const SpacingBox(width: 2),
        InkWell(
          onTap: () {
            showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                builder: (context) {
                  return SizedBox(
                    height: 250,
                    child: SelectListSheet(
                      title: 'Chọn hành động',
                      items: [
                        SelectListSheetItem(display: "Chỉnh sửa", id: 'update'),
                        SelectListSheetItem(display: "Cập nhật trạng thái", id: 'update_status'),
                        SelectListSheetItem(display: "Xoá", id: 'delete')
                      ],
                      onSelect: (value) async {
                        Future.delayed(const Duration(milliseconds: 200)).then((_) {
                          if (value.id == 'update' && onUpdate != null) onUpdate();
                          if (value.id == 'update_status' && onUpdate != null) onUpdateStatus();
                          if (value.id == 'delete' && onUpdate != null) onDelete();
                        });
                      },
                    ),
                  );
                });
          },
          child: const Icon(
            Icons.settings,
            size: 20,
            color: Colors.black45,
          ),
        ),
      ]
    ]);
  }
}
