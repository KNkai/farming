import 'package:flutter/material.dart';

import '../../../../base_config/src/utils/constants.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../model/disease_comment.model.dart';
import '../../../share/widget/empty.dart';
import '../controller/disease_situation_controller.dart';
import 'comment_card.dart';
import 'comment_sheet.dart';

class CommentList extends StatelessWidget {
  const CommentList({
    Key key,
    @required this.controller,
  }) : super(key: key);

  final DiseaseSituationController controller;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<List<DiseaseComment>>(
        valueListenable: controller.commentsNotifier,
        builder: (_, items, __) {
          if (items == null) {
            return kLoadingSpinner;
          }
          if (items.length == 0) {
            return const Center(child: EmptyWidget(text: "Chưa có bình luận nào."));
          }
          List<Widget> children = [];
          items.asMap().forEach((index, e) {
            children.addAll([
              CommentCart(
                  key: ObjectKey(e.id),
                  comment: e,
                  onPress: e.commenterId == controller.userId ? () => buildEditCommentSheet(context, e, index) : null),
              const SpacingBox(height: 1),
              const Divider(),
              const SpacingBox(height: 1),
            ]);
          });
          return Column(children: children);
        });
  }

  void buildEditCommentSheet(BuildContext context, DiseaseComment e, int index) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return CommentSheet(
            comment: e,
            isCreate: false,
            onUpdated: (comment) => controller.setComment(comment, index),
            onRemoved: () => controller.removeComment(index),
          );
        });
  }
}
