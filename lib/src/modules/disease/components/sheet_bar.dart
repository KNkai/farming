import 'package:flutter/material.dart';

import '../../../../base_config/src/utils/constants.dart';
import '../../../../navigator.dart';
import '../../../share/utils/color.dart';

class SheetBar extends StatelessWidget {
  const SheetBar({
    Key key,
    @required this.title,
    @required this.btnTitle,
    this.onPress,
  }) : super(key: key);
  final String title, btnTitle;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      height: 50,
      width: deviceWidth(context),
      decoration: const BoxDecoration(color: AppColor.primary),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          InkWell(
            onTap: () => navigatorKey.currentState.maybePop(),
            child: const Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
              child: Icon(
                Icons.close,
                color: Colors.white,
              ),
            ),
          ),
          Text(
            title,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
          ),
          const Spacer(),
          InkWell(
            onTap: onPress,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(4), border: Border.all(color: Colors.white)),
              child: Text(
                btnTitle,
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
