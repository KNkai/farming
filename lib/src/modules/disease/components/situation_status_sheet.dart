import 'package:flutter/material.dart';

import '../../../../base_config/src/utils/constants.dart';
import '../../../../base_config/src/utils/validate_builder.dart';
import '../../../../base_widget/base_widget.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../../navigator.dart';
import '../../../model/disease_situation.model.dart';
import '../../../share/components/bottom_sheet_wrapper.dart';
import '../../../share/components/datepicker_input.dart';
import '../../../share/components/select_input.dart';
import '../../../share/components/select_list_sheet.dart';
import '../../../share/model/enum.dart';
import '../../../share/repo/disease_repo.dart';

class SituationStatusSheet extends StatefulWidget {
  const SituationStatusSheet({Key key, @required this.situation, this.onUpdated}) : super(key: key);
  final DiseaseSituation situation;
  final Function(DiseaseSituation) onUpdated;

  @override
  _SituationStatusSheetState createState() => _SituationStatusSheetState();
}

class _SituationStatusSheetState extends State<SituationStatusSheet> {
  GlobalKey<FormState> situationStatusForm = new GlobalKey(debugLabel: "situationStatusForm");
  String status;
  DateTime reportedAt;

  @override
  void initState() {
    super.initState();
    status = widget.situation.status;
    reportedAt = widget.situation.reportedAt;
    print("reportedAt $reportedAt");
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheetWrapper(
      height: 350,
      appBar: buildSheetBar(
        context,
        title: "Trạng thái dịch bệnh",
        btnTitle: "Lưu",
        onPress: updateStatus,
      ),
      padding: const EdgeInsets.all(16),
      formKey: situationStatusForm,
      children: [
        SelectListInput(
          labelText: "Trạng thái dịch bệnh",
          hintText: "Chọn trạng thái",
          selectedId: status,
          sheetItems:
              AppEnum.diseaseStatusEnum.entries.map((e) => SelectListSheetItem(display: e.value, id: e.key)).toList(),
          onSaved: (value) => status = value,
          validator: ValidateBuilder().empty().build(),
        ),
        const SpacingBox(height: 1),
        DatepickerInput(
          labelText: "Ngày cập nhật",
          hintText: "Chọn ngày cập nhật",
          minTime: widget.situation.createdAt,
          value: reportedAt,
          maxTime: DateTime.now(),
          onSaved: (value) => reportedAt = value,
          validator: ValidateBuilder().empty().build(),
        ),
        const SpacingBox(height: 1),
        SizedBox(
          height: MediaQuery.of(context).viewInsets.bottom,
        )
      ],
    );
  }

  void updateStatus() {
    if (situationStatusForm.currentState.validate()) {
      situationStatusForm.currentState.save();
      DiseaseRepo().editSituationStatus(widget.situation.id, status, reportedAt, widget.situation.title).then((value) {
        widget.situation.status = status;
        widget.situation.reportedAt = reportedAt;
        if (widget.onUpdated != null) widget.onUpdated(widget.situation);
        navigatorKey.currentState.pop(widget.situation);
      }).catchError((err) {
        showToast("Có lỗi xảy ra: ${err.toString()}", flagColor: false);
      });
    }
  }
}
