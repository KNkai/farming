import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../base_config/base_config.dart';
import '../../../base_widget/src/spacing_box.dart';
import '../../model/disease_comment.model.dart';
import '../../model/disease_situation.model.dart';
import '../../share/widget/app_btn.dart';
import 'components/comment_list.dart';
import 'components/comment_sheet.dart';
import 'components/situation_full_card.dart';
import 'controller/disease_situation_controller.dart';

class DiseaseSituationPage extends StatefulWidget {
  final String situationId;
  const DiseaseSituationPage({
    Key key,
    @required this.situationId,
  }) : super(key: key);

  @override
  _DiseaseSituationPageState createState() => _DiseaseSituationPageState();
}

class _DiseaseSituationPageState extends State<DiseaseSituationPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DiseaseSituationController>(
        create: (context) => DiseaseSituationController(widget.situationId),
        builder: (context, child) {
          DiseaseSituationController controller = Provider.of<DiseaseSituationController>(context);
          return Scaffold(
            appBar: buildPageBar(context, "Tình hình dịch bệnh", callBackData: () {
              return controller.situationNotifier.value;
            }),
            body: ValueListenableBuilder<DiseaseSituation>(
              valueListenable: controller.situationNotifier,
              child: Column(
                children: [
                  const SpacingBox(height: 1),
                  const Divider(),
                  const SpacingBox(height: 1),
                  AppBtn(
                    "Viết bình luận",
                    onPressed: () => buildCreateCommentSheet(context, controller),
                  ),
                  const SpacingBox(height: 2),
                  CommentList(controller: controller),
                ],
              ),
              builder: (_, item, child) {
                if (item == null) {
                  return Center(child: kLoadingSpinner);
                }
                return SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          SituationFullCard(situation: item),
                          child,
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        });
  }

  void buildCreateCommentSheet(BuildContext context, DiseaseSituationController controller) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return CommentSheet(
          comment: DiseaseComment(situationId: controller.situationId),
          onUpdated: (comment) => controller.insertComments(comment),
        );
      },
    );
  }
}
