import '../../../base_config/base_config.dart';
import '../../model/disease_situation.model.dart';
import '../../share/repo/diseaseComment_repo.dart';
import '../../share/repo/disease_repo.dart';

class DiseaseDetailController {
  dynamic situation;
  ValueNotifier diseaseNotifier;
  List<dynamic> diseaseTypes = [];
  String idUser;
  Function reloadUi;
  DiseaseDetailController(this.situation, this.reloadUi);
  // data
  ValueNotifier<List> commentsNotifier = new ValueNotifier([]);

  //method
  init() {
    diseaseNotifier = new ValueNotifier(this.situation);
    getIdUser();
    getListComment();
  }

  Future getDisease() async {
    situation = await DiseaseRepo().getOne(situation["id"]);
    diseaseNotifier.value = situation;
  }

  Future getIdUser() async {
    idUser = await SPref.instance.get(CustomString.KEY_ID);
  }

  Future getListComment() async {
    commentsNotifier.value = await DiseaseCommentRepo().getList(situation["id"]);
  }

  Future<String> deletedDiease() async {
    try {
      await DiseaseRepo().remove(situation["id"]);
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future<String> createComment(String message, List<String> images) async {
    try {
      await DiseaseCommentRepo().createComment(situation["id"], message, images);
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future<String> removeComment(String id) async {
    try {
      await DiseaseCommentRepo().removeComment(id);
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future<String> editStatus(String status) async {
    try {
      DiseaseSituation object = new DiseaseSituation.fromJson(situation);
      await DiseaseRepo().editSituationStatus(object.id, status, object.reportedAt, object.title);
      situation["status"] = status;
      reloadUi();
      return null;
    } catch (e) {
      return e.toString();
    }
  }
}
