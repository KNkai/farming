import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/model/filter_model.dart';

class LoadListDiseaseEvent extends BaseEvent {
  LoadListDiseaseEvent();
}

class ChangeFilterEvent extends BaseEvent {
  FilterModel filter;
  ChangeFilterEvent({this.filter});
}
