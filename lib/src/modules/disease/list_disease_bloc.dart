import 'package:mi_smart/src/modules/region/region_srv.dart';
import 'package:rxdart/rxdart.dart';

import '../../../base_config/base_config.dart';
import '../../../base_config/src/base/base_bloc.dart';
import '../../../base_config/src/base/base_event.dart';
import '../../share/model/filter_model.dart';
import '../../share/repo/disease_repo.dart';
import '../../share/widget/load_more.dart';
import 'listDiary_state.dart';
import 'list_disease_event.dart';

class DiseaseBloc extends BaseBloc with ChangeNotifier {
  DiseaseRepo _diseaseRepo = DiseaseRepo();
  List lList = [];
  List diseases = [];
  List regions = [];

  final _diseaseState$ = BehaviorSubject<BaseState>();
  Stream<BaseState> get diseaseStream => _diseaseState$.stream;
  Sink<BaseState> get diseaseSink => _diseaseState$.sink;
  BaseState get diseaseValue => _diseaseState$.value;

  final _filter$ = BehaviorSubject<FilterModel>();
  Stream<FilterModel> get filterStream => _filter$.stream;
  Sink<FilterModel> get filterSink => _filter$.sink;
  FilterModel get filterValue => _filter$.value;

  final _diseasePharse$ = BehaviorSubject<List>();
  Stream<List> get diseasePharseStream => _diseasePharse$.stream;
  Sink<List> get diseasePharseSink => _diseasePharse$.sink;
  List get diseasePharseValue => _diseasePharse$.value;

  bool isLoadingListSutuation = false;

  DiseaseBloc() {
    getListFilters();

    FilterModel initFilter = FilterModel(limit: 5, page: 1, order: "{createdAt : -1}");
    filterSink.add(initFilter);
  }

  @override
  void dispatchEvent(BaseEvent event) {
    if (event is LoadListDiseaseEvent) getList(event);
    if (event is ChangeFilterEvent) changeFilter(event);
    if (event is LoadMoreEvent) loadMore();
  }

  @override
  void dispose() {
    _diseaseState$.close();
    _filter$.close();
    _diseasePharse$.close();
    super.dispose();
  }

  Future<List> getSortOption() async {
    return _diseaseRepo.getSortOptions();
  }

  void getListFilters() {
    DiseaseSrv().getList(fragment: "id name").then((value) => diseases = value['data']);
    RegionSrv().getList(fragment: "id name").then((value) => regions = value['data']);
  }

  Future getList(LoadListDiseaseEvent event) async {
    try {
      loadingSink.add(true);

      final res = await _diseaseRepo.getList(
          page: filterValue?.page,
          limit: filterValue?.limit,
          filter: filterValue?.filter,
          search: filterValue?.search,
          offset: filterValue?.offset,
          order: filterValue?.order);
      diseaseSink.add(LoadListDiseaseSuccessState(res));
      lList = res;
    } catch (e) {
      diseaseSink.add(LoadListDiseaseFailState(e.message));
    } finally {
      loadingSink.add(false);
    }
  }

  Future loadMore() async {
    if (isLoadingListSutuation) return;
    try {
      isLoadingListSutuation = true;
      //loadingSink.add(true);

      final res = await _diseaseRepo.getList(
          page: (filterValue?.page ?? 1) + 1,
          limit: filterValue?.limit,
          filter: filterValue?.filter,
          search: filterValue?.search,
          offset: filterValue?.offset,
          order: filterValue?.order);
      if (res.length > 0) {
        lList.addAll(res);
        filterValue.page++;
        filterSink.add(filterValue);
        diseaseSink.add(LoadListDiseaseSuccessState(lList));
      }
    } catch (e) {
      diseaseSink.add(LoadListDiseaseFailState(e.toString()));
    } finally {
      isLoadingListSutuation = false;
      //loadingSink.add(false);
    }
  }

  Future changeFilter(ChangeFilterEvent event) async {
    try {
      loadingSink.add(true);
      filterSink.add(event.filter.copyWith(page: 1));
      getList(LoadListDiseaseEvent());
    } catch (e) {
      diseaseSink.add(LoadListDiseaseFailState(e.toString()));
    } finally {
      loadingSink.add(false);
    }
  }
}
