import '../../../../base_config/base_config.dart';
import '../../../model/disease_comment.model.dart';
import '../../../model/disease_situation.model.dart';
import '../../../share/model/pagination.dart';
import '../../../share/repo/diseaseComment_repo.dart';
import '../../../share/repo/disease_repo.dart';

class DiseaseSituationController extends ChangeNotifier {
  final String situationId;
  ValueNotifier<DiseaseSituation> situationNotifier = new ValueNotifier(null);
  ValueNotifier<List<DiseaseComment>> commentsNotifier = new ValueNotifier([]);
  Pagination commentPaging;
  String userId;
  DiseaseSituationController(this.situationId) {
    loadSituation();
    loadComments();
    getUserID();
  }

  void setSituation(DiseaseSituation value) {
    situationNotifier.value = value;
    situationNotifier.notifyListeners();
  }

  void getUserID() {
    SPref.instance.get(CustomString.KEY_ID).then((value) => userId = value);
  }

  void loadComments() {
    commentPaging = Pagination(limit: 100, page: 1);
    DiseaseCommentSrv()
        .getList(
            filter: 'situationId:"$situationId"',
            order: '{createdAt: -1}',
            limit: commentPaging.limit,
            page: commentPaging.page)
        .then((value) {
      commentPaging = Pagination.fromJson(value["pagination"]);
      commentsNotifier.value = value["data"].map((i) => DiseaseComment.fromJson(i)).toList().cast<DiseaseComment>();
    });
  }

  void loadSituation() {
    DiseaseSituationSrv().getItem(situationId, fragment: getSituationFragment()).then((value) {
      situationNotifier.value = DiseaseSituation.fromJson(value);
    });
  }

  String getSituationFragment() {
    return """
          id
          title
          description
          createdAt
          listImages
          commentCount
          status
          reporterId
          diseaseId
          regionId
          reportedAt
          disease { 
            id 
            name 
          }
          user {
            id
            name
            avatar
          }
          region {
            name
            district
            ward
            province
          }
          staff {
            id
            name
            avatar
          }
      """;
  }

  void insertComments(DiseaseComment value) {
    commentsNotifier.value.insert(0, value);
    situationNotifier.value.commentCount++;
    commentsNotifier.notifyListeners();
    situationNotifier.notifyListeners();
  }

  void setComment(DiseaseComment value, index) {
    commentsNotifier.value[index] = value;
    commentsNotifier.notifyListeners();
  }

  void removeComment(index) {
    commentsNotifier.value.removeAt(index);
    commentsNotifier.notifyListeners();
  }
}
