import 'package:flutter/material.dart';
import 'package:mi_smart/src/model/disease_situation.model.dart';
import 'package:mi_smart/src/modules/tabs_region_detail/components/situation_sheet.dart';

import '../../../base_config/base_config.dart';
import '../../../base_widget/base_widget.dart';
import '../../../base_widget/src/spacing_box.dart';
import '../../../navigator.dart';
import '../../share/components/situation_card.dart';
import '../../share/utils/color.dart';
import '../../share/utils/util.dart';
import '../../share/widget/app_scaffold.dart';
import '../../share/widget/components/filter_sheet.dart';
import '../../share/widget/empty.dart';
import '../../share/widget/error.dart';
import '../../share/widget/load_more.dart';
import '../../share/widget/sort_btn.dart';
import '../find_regions/find_regions_page.dart';
import 'listDiary_state.dart';
import 'list_disease_bloc.dart';
import 'list_disease_event.dart';

class ListDiseasePage extends StatefulWidget {
  @override
  _ListDiseasePageState createState() => _ListDiseasePageState();
}

class _ListDiseasePageState extends State<ListDiseasePage> {
  DiseaseBloc bloc = DiseaseBloc();
  List<FilterGroup> filterGroups;
  @override
  void initState() {
    bloc.add(LoadListDiseaseEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      onBack: true,
      onSearch: (text) {
        bloc.add(ChangeFilterEvent(filter: bloc.filterValue.copyWith(search: text)));
      },
      onFilter: onFilter,
      onAdd: () {
        showModalBottomSheet(
            context: context,
            useRootNavigator: true,
            isScrollControlled: true,
            builder: (context) => SituationSheet(
                  situation: DiseaseSituation(),
                  isCreate: true,
                  onCreated: (situation) {
                    navigatorKey.currentState.pushNamed(View.diseaseDetail, arguments: situation.id).then((value) {
                      bloc.add(ChangeFilterEvent(filter: bloc.filterValue));
                    });
                  },
                ));
      },
      child: LoadingTask(
        bloc: bloc,
        child: Column(
          children: [
            Row(children: [buildDiseaseSortOption(), buildFindRegionButton(context)]),
            const SpacingBox(height: 2),
            Expanded(
              child: StreamBuilder(
                stream: bloc.diseaseStream,
                initialData: LoadingListDiseaseState(),
                builder: (context, snap) {
                  if (snap.data is LoadingListDiseaseState) {
                    return const Center();
                  }

                  if (snap.data is LoadListDiseaseFailState) {
                    return const Center(
                      child: ErrorImgWidget(),
                    );
                  }
                  if (snap.data is LoadListDiseaseSuccessState) {
                    final list = (snap.data as LoadListDiseaseSuccessState).disease;
                    if (list.length == 0) {
                      return const EmptyWidget(text: 'Không tìm thấy kết quả');
                    }
                    return LoadMoreScrollView(
                      physics: const AlwaysScrollableScrollPhysics(),
                      padding: const EdgeInsets.only(bottom: 25),
                      bloc: bloc,
                      itemCount: list.length,
                      itemBuilder: (context, index) => buildSituationCard(list[index]),
                      separatorBuilder: (context, index) => const SpacingBox(height: 3),
                    );
                  }
                  return Container();
                },
              ),
            ),
          ],
        ),
      ),
      // onAdd: () {
      //   //showDialog(context: context, builder: (context) => AddListDiseasePage());
      //   showModalBottomSheet(
      //       context: context,
      //       useRootNavigator: true,
      //       isScrollControlled: true,
      //       builder: (context) => BottomDiseasePopupSheet()).then((value) => bloc.add(LoadListDiseaseEvent()));
      // },
    );
  }

  void onFilter() {
    if (bloc.diseases.length != 0 && bloc.regions.length != 0) {
      if (filterGroups == null) {
        filterGroups = [
          FilterGroup(
              label: "Loại dịch bệnh",
              colCount: 2,
              items: bloc.diseases.map((e) => FilterItem(label: e["name"], value: e["id"], selected: false)).toList()),
          FilterGroup(
              label: "Địa điểm canh tác",
              colCount: 2,
              items: bloc.regions.map((e) => FilterItem(label: e["name"], value: e["id"], selected: false)).toList()),
        ];
      }
      showModalBottomSheet(
        context: context,
        useRootNavigator: true,
        isScrollControlled: true,
        builder: (context) => FilterSheet(
          groups: filterGroups,
          onSubmit: (List<FilterGroup> res) {
            List filterOr = [];
            List selectedDisease = res[0].selectedValues();
            List selectedRegion = res[1].selectedValues();
            if (selectedDisease.length > 0) {
              filterOr.add({
                "diseaseId": {"__in": selectedDisease}
              });
            }
            if (selectedRegion.length > 0) {
              filterOr.add({
                "regionId": {"__in": selectedRegion}
              });
            }

            bloc.add(ChangeFilterEvent(
              filter: bloc.filterValue.copyWith(
                filter: filterOr.length > 0 ? parseJsonToFilter({"__or": filterOr}) : '',
              ),
            ));
          },
        ),
      );
    }
  }

  Expanded buildFindRegionButton(BuildContext context) {
    return Expanded(
      child: Align(
        alignment: Alignment.bottomRight,
        child: IconButton(
          onPressed: () {
            FindRegionsPage.go(context, isDiseaseModule: true);
          },
          icon: const Icon(
            Icons.map,
            color: AppColor.primary,
          ),
        ),
      ),
    );
  }

  SituationCard buildSituationCard(item) {
    return SituationCard(
      key: ObjectKey(item["id"]),
      situation: DiseaseSituation.fromJson(item),
      onTap: () {
        navigatorKey.currentState.pushNamed(View.diseaseDetail, arguments: item["id"]).then((value) {
          bloc.add(ChangeFilterEvent(filter: bloc.filterValue));
        });
      },
    );
  }

  FutureBuilder<List> buildDiseaseSortOption() {
    return FutureBuilder<List>(
        initialData: const [],
        future: bloc.getSortOption(),
        builder: (context, snapshot) {
          if (!snapshot.hasData || snapshot.data.length == 0) {
            return Container();
          }
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SortBtn((value) {
                  bloc.add(ChangeFilterEvent(filter: bloc.filterValue.copyWith(order: value)));
                }, snapshot.data),
              ],
            ),
          );
        });
  }
}
