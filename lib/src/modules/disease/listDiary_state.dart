import 'package:mi_smart/base_config/base_config.dart';

class LoadListDiseaseSuccessState extends BaseState {
  List disease;
  LoadListDiseaseSuccessState(this.disease);
}

class LoadListDiseaseFailState extends BaseState {
  String err;
  LoadListDiseaseFailState(this.err);
}

class LoadingListDiseaseState extends BaseState {}

class UpdateDiseaseSuccessState extends BaseState {
  UpdateDiseaseSuccessState();
}

class UpdateDiseaseFailState extends BaseState {
  String err;
  UpdateDiseaseFailState(this.err);
}

class CreateDiseaseSuccessState extends BaseState {
  CreateDiseaseSuccessState();
}

class CreateDiseaseFailState extends BaseState {
  String err;
  CreateDiseaseFailState(this.err);
}
