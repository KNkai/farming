import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class IntroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const styleBody = TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Colors.black54);
    final listPagesViewModel = <PageViewModel>[
      PageViewModel(
          decoration: const PageDecoration(
              pageColor: Colors.white, bodyTextStyle: styleBody, imagePadding: EdgeInsets.only(top: 30)),
          image: Image.asset('assets/Artboard8.png'),
          title: 'Nông nghiệp và tự động hóa',
          body:
              'Không còn vất vả đội nắng mưa cào đất, tưới rau, tiết kiệm được rất nhiều thời gian và công sức nhờ ứng dụng tự động hóa.'),
      PageViewModel(
          decoration: const PageDecoration(
              pageColor: Colors.white, bodyTextStyle: styleBody, imagePadding: EdgeInsets.only(top: 30)),
          image: Image.asset('assets/Artboard9.png'),
          title: 'Cung cấp thông tin đầy đủ',
          body:
              'Cung cấp đầy đủ thông tin các sản phẩm rau , quả, vật nuôi ... đồng thời, hạn chế thiệt hại do thời tiết sâu bệnh gây ra.'),
      PageViewModel(
          decoration: const PageDecoration(
              pageColor: Colors.white, bodyTextStyle: styleBody, imagePadding: EdgeInsets.only(top: 30)),
          image: Image.asset('assets/Artboard10.png'),
          title: 'Lấy nông nghiệp làm trung tâm',
          body:
              'Tạo nhóm hoặc công đồng trong khu vực cũng như có thể giao dịch, cho vay, mua hoặc chia sẻ bất kỳ hàng hóa nào với những nông dân khác.'),
    ];

    return IntroductionScreen(
      dotsDecorator: DotsDecorator(
          size: const Size.square(10.0),
          activeSize: const Size(20.0, 10.0),
          activeColor: AppColor.primary,
          color: Colors.black26,
          spacing: const EdgeInsets.symmetric(horizontal: 3.0),
          activeShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0))),
      skip: const Text("Bỏ qua", style: TextStyle(fontWeight: FontWeight.w500, color: AppColor.primary)),
      showSkipButton: true,
      next: const Text("Tiếp tục", style: TextStyle(fontWeight: FontWeight.w500, color: AppColor.primary)),
      pages: listPagesViewModel,
      done: const Text("Bắt đầu", style: TextStyle(fontWeight: FontWeight.w500, color: AppColor.primary)),
      onDone: () {
        navigatorKey.currentState.pushReplacementNamed(View.login);
      },
    );
  }
}
