import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:imgur/imgur.dart' as ig;
import 'package:mi_smart/src/modules/profile/model/get_district/get_district.dart';
import 'package:mi_smart/src/modules/profile/model/get_province/get_province.graphql.dart';
import 'package:mi_smart/src/modules/profile/model/get_ward/get_ward.dart';
import 'package:mi_smart/src/modules/profile/model/profile_model.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/address_vn.dart';

import 'profile_repo.dart';

class ProfileBloc {
  final stateShowBtn = ValueNotifier<bool>(false);
  final stateLoading = ValueNotifier<bool>(false);
  final stateSide = StreamController<StateSideBooking>();
  final stateImg = StreamController<UpdateImgState>();
  final stateNameError = ValueNotifier<bool>(false);
  final statePhoneError = ValueNotifier<bool>(false);
  final stateEmailError = ValueNotifier<bool>(false);
  final _repo = ProfileRepo();
  final imgurService = ig.Imgur(ig.Authentication.fromClientId('d4de8224fa0042f')).image;

  String _initName;
  String _initPhone;
  String _initMail;
  String _name;
  String _phone;
  String _mail;
  String _id;

  String _initAddress;
  Province _initProvince;
  District _initDistrict;
  Ward _initWard;
  String _address;
  Province _province;
  District _district;
  Ward _ward;

  void eventChangeName(String name) {
    if (name.isNotEmpty) {
      stateNameError.value = false;

      _name = name;
    } else
      stateNameError.value = true;
  }

  void eventChangePhone(String phone) {
    if (phone.isNotEmpty && phoneNumberMatch(phone) && !AppRole.isStaff) {
      statePhoneError.value = false;
      stateShowBtn.value = (phone != _initPhone);
      _phone = phone;
    } else
      statePhoneError.value = true;
  }

  void eventChangeEmail(String mail) {
    if (mail.isNotEmpty && emailMatch(mail) && AppRole.isStaff) {
      stateEmailError.value = false;
      stateShowBtn.value = (mail != _initMail);
      _mail = mail;
    } else
      stateEmailError.value = true;
  } //

  void eventGoChangePassword() {
    stateSide.add(StateSideGoChangePassword(_mail));
  }

  void eventupdateAddress(String address, Province p, District d, Ward w) {
    _address = address;
    _province = p;
    _district = d;
    _ward = w;

    stateShowBtn.value = (_address != _initAddress) ||
        (_province?.id != _initProvince?.id) ||
        (_district?.id != _initDistrict?.id) ||
        (_ward?.id != _initWard?.id);
  }

  void eventReset() {
    stateSide.add(StateSideUpdateName(_initName));
    stateSide.add(StateSideUpdatePhone(_initPhone));
    stateSide.add(StateSideUpdateEmail(_initMail));
    stateSide.add(StateSideUpdateAddress(AddressVnValues(_initAddress, _initProvince, _initDistrict, _initWard)));
    stateShowBtn.value = false;
  }

  void eventUpdate() {
    if (!stateEmailError.value && !statePhoneError.value && !stateNameError.value) {
      _repo.updateMe(
        _id,
        name: _name,
        phone: _phone,
        email: _mail,
        address: _address,
        province: _province?.name,
        provinceId: _province?.id,
        district: _district?.name,
        districtId: _district?.id,
        ward: _ward?.name,
        wardId: _ward?.id,
      );

      _initName = _name;
      _initPhone = _phone;
      _initMail = _mail;

      _initAddress = _address;
      _initProvince = _province;
      _initDistrict = _district;
      _initWard = _ward;

      stateShowBtn.value = false;
    }
  }

  void eventUpdateImage(String path) {
    if (path != null) {
      FlutterExifRotation.rotateImage(path: path).then((file) {
        stateImg.add(UpdateImgState(filePath: path));
        imgurService.uploadImage(imageFile: file).then((value) {
          _repo.updateMe(_id, avatar: value.link);
        });
      });
    }
  }

  void eventInit() {
    stateLoading.value = true;
    _repo.getMe().then((value) {
      try {
        if (value is ProfileModel) {
          _initName = value.name;
          _initPhone = value.phone;
          _initMail = value.email;
          _id = value.id;
          _name = value.name;
          _phone = value.phone;
          _mail = value.email;

          _address = value.address;
          _province = Province(name: value.province, id: value.provinceId);
          _district = District(name: value.district, id: value.districtId);
          _ward = Ward(name: value.ward, id: value.wardId);
          _initAddress = value.address;
          _initProvince = Province(name: value.province, id: value.provinceId);
          _initDistrict = District(name: value.district, id: value.districtId);
          _initWard = Ward(name: value.ward, id: value.wardId);

          stateSide.add(StateSideUpdateAddress(AddressVnValues(_initAddress, _initProvince, _initDistrict, _initWard)));

          stateImg.add(UpdateImgState(imgurUrl: value.avatar));
          stateSide.add(StateSideUpdateName(value.name));
          stateSide.add(StateSideUpdatePhone(value.phone));
          stateSide.add(StateSideUpdateEmail(value.email));
        }
      } finally {
        stateLoading.value = false;
      }
    });
  }

  void dispose() {
    stateShowBtn.dispose();
    stateLoading.dispose();
    stateSide.close();
    stateImg.close();
    stateNameError.dispose();
    statePhoneError.dispose();
    stateEmailError.dispose();
  }
}

class StateSideBooking {}

class LoadAddressState {
  final String address;
  final Province province;
  final District district;
  final Ward ward;
  LoadAddressState(this.address, this.province, this.district, this.ward);
}

class StateSideUpdateName extends StateSideBooking {
  final String value;
  StateSideUpdateName(this.value);
}

class StateSideGoChangePassword extends StateSideBooking {
  final String email;
  StateSideGoChangePassword(this.email);
}

class StateSideUpdatePhone extends StateSideBooking {
  final String value;
  StateSideUpdatePhone(this.value);
}

class StateSideUpdateEmail extends StateSideBooking {
  final String value;
  StateSideUpdateEmail(this.value);
}

class StateSideUpdateAddress extends StateSideBooking {
  final AddressVnValues addressValues;
  StateSideUpdateAddress(this.addressValues);
}

class UpdateImgState {
  final String imgurUrl;
  final String filePath;
  UpdateImgState({this.imgurUrl, this.filePath});
}
