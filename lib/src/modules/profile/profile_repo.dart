import 'package:mi_smart/src/modules/profile/model/profile_model.dart';
import 'package:mi_smart/src/modules/profile/model/staff_get_me/staff_get_me.dart';
import 'package:mi_smart/src/modules/profile/model/update_staff/update_staff.graphql.dart';
import 'package:mi_smart/src/modules/profile/model/user_get_me/user_get_me.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';
import 'package:mi_smart/src/share/utils/util.dart';

import 'model/error_model.dart';

class ProfileRepo {
  Future getMe() {
    return BaseApi.instance.rawDataExecute(AppRole.isStaff ? StaffGetMeQuery() : UserGetMeQuery()).then((value) {
      if (value?.data == null) {
        return ResultError(msg: 'Có lỗi xảy ra');
      } else {
        dynamic data;
        if (AppRole.isStaff) {
          data = value.data['staffGetMe'];
        } else {
          data = value.data['userGetMe'];
        }

        return data == null ? ResultError(msg: 'Có lỗi xảy ra') : ProfileModel.fromJson(data);
      }
    }).catchError((onError) {
      return ResultError(msg: 'Có lỗi xảy ra');
    });
  }

  void updateMe(String id,
      {String name,
      String phone,
      String email,
      String avatar,
      String address,
      String province,
      String provinceId,
      String district,
      String districtId,
      String ward,
      String wardId}) {
    if (AppRole.isStaff) {
      BaseApi.instance.rawDataExecute(UpdateStaffMutation(
          variables: UpdateStaffArguments(
              i: id,
              d: UpdateStaff$UpdateStaffInput(
                  name: name,
                  avatar: avatar,
                  address: address,
                  province: province,
                  provinceId: provinceId,
                  district: district,
                  districtId: districtId,
                  ward: ward,
                  email: email,
                  wardId: wardId))));
    } else {
      BaseApi.instance.rawBodyRawDataExecute('''
          mutation update(\$q: UpdateUserInput!) {
    updateUser(id: "$id",data: \$q) {id}
}
          ''', variables: {
        "q": {"name": name, "phone": phone, "provinceId": provinceId, "districtId": districtId, "wardId": wardId}
      });
    }
  }
}
