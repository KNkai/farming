// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:artemis/artemis.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:json_annotation/json_annotation.dart';

part 'staff_get_me.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class StaffGetMe$Query$Staff with EquatableMixin {
  StaffGetMe$Query$Staff();

  factory StaffGetMe$Query$Staff.fromJson(Map<String, dynamic> json) => _$StaffGetMe$Query$StaffFromJson(json);

  String id;

  String phone;

  String name;

  String avatar;

  String uid;

  bool isBlock;

  String address;

  String province;

  String district;

  String ward;

  String email;

  String provinceId;

  String districtId;

  String wardId;

  DateTime createdAt;

  DateTime updatedAt;

  int unseenNotify;

  @override
  List<Object> get props => [
        id,
        phone,
        name,
        avatar,
        uid,
        isBlock,
        address,
        province,
        district,
        ward,
        provinceId,
        districtId,
        wardId,
        createdAt,
        updatedAt,
        email
      ];
  Map<String, dynamic> toJson() => _$StaffGetMe$Query$StaffToJson(this);
}

@JsonSerializable(explicitToJson: true)
class StaffGetMe$Query with EquatableMixin {
  StaffGetMe$Query();

  factory StaffGetMe$Query.fromJson(Map<String, dynamic> json) => _$StaffGetMe$QueryFromJson(json);

  StaffGetMe$Query$Staff staffGetMe;

  @override
  List<Object> get props => [staffGetMe];
  Map<String, dynamic> toJson() => _$StaffGetMe$QueryToJson(this);
}

class StaffGetMeQuery extends GraphQLQuery<StaffGetMe$Query, JsonSerializable> {
  StaffGetMeQuery();

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'staffGetMe'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'staffGetMe'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(name: NameNode(value: 'id'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'phone'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'name'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'avatar'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(name: NameNode(value: 'uid'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'email'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'isBlock'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'address'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'province'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'district'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'ward'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'provinceId'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'districtId'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'wardId'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'createdAt'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'updatedAt'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'unseenNotify'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'staffGetMe';

  @override
  List<Object> get props => [document, operationName];
  @override
  StaffGetMe$Query parse(Map<String, dynamic> json) => StaffGetMe$Query.fromJson(json);
}
