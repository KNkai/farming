// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'staff_get_me.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StaffGetMe$Query$Staff _$StaffGetMe$Query$StaffFromJson(Map<String, dynamic> json) {
  return StaffGetMe$Query$Staff()
    ..id = json['id'] as String
    ..phone = json['phone'] as String
    ..name = json['name'] as String
    ..avatar = json['avatar'] as String
    ..email = json['email'] as String
    ..uid = json['uid'] as String
    ..isBlock = json['isBlock'] as bool
    ..address = json['address'] as String
    ..province = json['province'] as String
    ..district = json['district'] as String
    ..ward = json['ward'] as String
    ..provinceId = json['provinceId'] as String
    ..districtId = json['districtId'] as String
    ..wardId = json['wardId'] as String
    ..createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String)
    ..unseenNotify = json['unseenNotify'] as int;
}

Map<String, dynamic> _$StaffGetMe$Query$StaffToJson(StaffGetMe$Query$Staff instance) => <String, dynamic>{
      'id': instance.id,
      'phone': instance.phone,
      'name': instance.name,
      'avatar': instance.avatar,
      'uid': instance.uid,
      'isBlock': instance.isBlock,
      'email': instance.email,
      'address': instance.address,
      'province': instance.province,
      'district': instance.district,
      'ward': instance.ward,
      'provinceId': instance.provinceId,
      'districtId': instance.districtId,
      'wardId': instance.wardId,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

StaffGetMe$Query _$StaffGetMe$QueryFromJson(Map<String, dynamic> json) {
  return StaffGetMe$Query()
    ..staffGetMe =
        json['staffGetMe'] == null ? null : StaffGetMe$Query$Staff.fromJson(json['staffGetMe'] as Map<String, dynamic>);
}

Map<String, dynamic> _$StaffGetMe$QueryToJson(StaffGetMe$Query instance) => <String, dynamic>{
      'staffGetMe': instance.staffGetMe?.toJson(),
    };
