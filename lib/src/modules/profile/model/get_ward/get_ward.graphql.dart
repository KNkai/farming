// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:artemis/artemis.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'get_ward.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class Ward with EquatableMixin {
  Ward({this.name, this.id});

  factory Ward.fromJson(Map<String, dynamic> json) =>
      _$GetWard$Query$WardDataFromJson(json);

  String id;

  String name;

  @override
  List<Object> get props => [id, name];
  Map<String, dynamic> toJson() => _$GetWard$Query$WardDataToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetWard$Query with EquatableMixin {
  GetWard$Query();

  factory GetWard$Query.fromJson(Map<String, dynamic> json) =>
      _$GetWard$QueryFromJson(json);

  List<Ward> getWard;

  @override
  List<Object> get props => [getWard];
  Map<String, dynamic> toJson() => _$GetWard$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetWardArguments extends JsonSerializable with EquatableMixin {
  GetWardArguments({@required this.d});

  factory GetWardArguments.fromJson(Map<String, dynamic> json) =>
      _$GetWardArgumentsFromJson(json);

  final String d;

  @override
  List<Object> get props => [d];
  Map<String, dynamic> toJson() => _$GetWardArgumentsToJson(this);
}

class GetWardQuery extends GraphQLQuery<GetWard$Query, GetWardArguments> {
  GetWardQuery({this.variables});

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'getWard'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'd')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'getWard'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'districtId'),
                    value: VariableNode(name: NameNode(value: 'd')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'ward'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'getWard';

  @override
  final GetWardArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  GetWard$Query parse(Map<String, dynamic> json) =>
      GetWard$Query.fromJson(json);
}
