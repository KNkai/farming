// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_ward.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Ward _$GetWard$Query$WardDataFromJson(Map<String, dynamic> json) {
  return Ward()
    ..id = json['id'] as String
    ..name = json['ward'] as String;
}

Map<String, dynamic> _$GetWard$Query$WardDataToJson(Ward instance) =>
    <String, dynamic>{
      'id': instance.id,
      'ward': instance.name,
    };

GetWard$Query _$GetWard$QueryFromJson(Map<String, dynamic> json) {
  return GetWard$Query()
    ..getWard = (json['getWard'] as List)
        ?.map(
            (e) => e == null ? null : Ward.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetWard$QueryToJson(GetWard$Query instance) =>
    <String, dynamic>{
      'getWard': instance.getWard?.map((e) => e?.toJson())?.toList(),
    };

GetWardArguments _$GetWardArgumentsFromJson(Map<String, dynamic> json) {
  return GetWardArguments(
    d: json['d'] as String,
  );
}

Map<String, dynamic> _$GetWardArgumentsToJson(GetWardArguments instance) =>
    <String, dynamic>{
      'd': instance.d,
    };
