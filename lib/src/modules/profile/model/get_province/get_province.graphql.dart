// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:artemis/artemis.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:json_annotation/json_annotation.dart';

part 'get_province.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class Province with EquatableMixin {
  Province({this.id, this.name});

  factory Province.fromJson(Map<String, dynamic> json) =>
      _$GetProvince$Query$ProvinceDataFromJson(json);

  String id;

  String name;

  @override
  List<Object> get props => [id, name];
  Map<String, dynamic> toJson() => _$GetProvince$Query$ProvinceDataToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetProvince$Query with EquatableMixin {
  GetProvince$Query();

  factory GetProvince$Query.fromJson(Map<String, dynamic> json) =>
      _$GetProvince$QueryFromJson(json);

  List<Province> getProvince;

  @override
  List<Object> get props => [getProvince];
  Map<String, dynamic> toJson() => _$GetProvince$QueryToJson(this);
}

class GetProvinceQuery
    extends GraphQLQuery<GetProvince$Query, JsonSerializable> {
  GetProvinceQuery();

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'getProvince'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'getProvince'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'province'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'getProvince';

  @override
  List<Object> get props => [document, operationName];
  @override
  GetProvince$Query parse(Map<String, dynamic> json) =>
      GetProvince$Query.fromJson(json);
}
