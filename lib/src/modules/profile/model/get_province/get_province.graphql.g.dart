// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_province.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Province _$GetProvince$Query$ProvinceDataFromJson(Map<String, dynamic> json) {
  return Province()
    ..id = json['id'] as String
    ..name = json['province'] as String;
}

Map<String, dynamic> _$GetProvince$Query$ProvinceDataToJson(
        Province instance) =>
    <String, dynamic>{
      'id': instance.id,
      'province': instance.name,
    };

GetProvince$Query _$GetProvince$QueryFromJson(Map<String, dynamic> json) {
  return GetProvince$Query()
    ..getProvince = (json['getProvince'] as List)
        ?.map((e) =>
            e == null ? null : Province.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetProvince$QueryToJson(GetProvince$Query instance) =>
    <String, dynamic>{
      'getProvince': instance.getProvince?.map((e) => e?.toJson())?.toList(),
    };
