// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_get_me.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserGetMe$Query$User _$UserGetMe$Query$UserFromJson(Map<String, dynamic> json) {
  return UserGetMe$Query$User()
    ..id = json['id'] as String
    ..uid = json['uid'] as String
    ..email = json['email'] as String
    ..name = json['name'] as String
    ..phone = json['phone'] as String
    ..avatar = json['avatar'] as String
    ..address = json['address'] as String
    ..province = json['province'] as String
    ..district = json['district'] as String
    ..ward = json['ward'] as String
    ..province = json['provinceId'] as String
    ..provinceId = json['provinceId'] as String
    ..districtId = json['districtId'] as String
    ..wardId = json['wardId'] as String
    ..createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String)
    ..unseenNotify = json['unseenNotify'] as int;
}

Map<String, dynamic> _$UserGetMe$Query$UserToJson(UserGetMe$Query$User instance) => <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'email': instance.email,
      'name': instance.name,
      'phone': instance.phone,
      'avatar': instance.avatar,
      'address': instance.address,
      'province': instance.province,
      'district': instance.district,
      'ward': instance.ward,
      'role': instance.role,
      'provinceId': instance.provinceId,
      'districtId': instance.districtId,
      'wardId': instance.wardId,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

UserGetMe$Query _$UserGetMe$QueryFromJson(Map<String, dynamic> json) {
  return UserGetMe$Query()
    ..userGetMe =
        json['userGetMe'] == null ? null : UserGetMe$Query$User.fromJson(json['userGetMe'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserGetMe$QueryToJson(UserGetMe$Query instance) => <String, dynamic>{
      'userGetMe': instance.userGetMe?.toJson(),
    };
