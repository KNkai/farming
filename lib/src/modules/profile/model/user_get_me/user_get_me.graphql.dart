// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:artemis/artemis.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_get_me.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UserGetMe$Query$User with EquatableMixin {
  UserGetMe$Query$User();

  factory UserGetMe$Query$User.fromJson(Map<String, dynamic> json) => _$UserGetMe$Query$UserFromJson(json);

  String id;

  String uid;

  String email;

  String name;

  String phone;

  String avatar;

  String address;

  String province;

  String district;

  String ward;

  String provinceId;

  String districtId;

  String wardId;

  String role;

  DateTime createdAt;

  DateTime updatedAt;

  int unseenNotify;

  @override
  List<Object> get props =>
      [id, uid, email, name, phone, avatar, address, province, district, ward, role, createdAt, updatedAt];
  Map<String, dynamic> toJson() => _$UserGetMe$Query$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserGetMe$Query with EquatableMixin {
  UserGetMe$Query();

  factory UserGetMe$Query.fromJson(Map<String, dynamic> json) => _$UserGetMe$QueryFromJson(json);

  UserGetMe$Query$User userGetMe;

  @override
  List<Object> get props => [userGetMe];
  Map<String, dynamic> toJson() => _$UserGetMe$QueryToJson(this);
}

class UserGetMeQuery extends GraphQLQuery<UserGetMe$Query, JsonSerializable> {
  UserGetMeQuery();

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'userGetMe'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'userGetMe'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(name: NameNode(value: 'id'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(name: NameNode(value: 'uid'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'email'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'name'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'phone'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'avatar'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'address'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'province'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'district'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'ward'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'provinceId'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'districtId'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'wardId'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'role'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'createdAt'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'updatedAt'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'unseenNotify'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'userGetMe';

  @override
  List<Object> get props => [document, operationName];
  @override
  UserGetMe$Query parse(Map<String, dynamic> json) => UserGetMe$Query.fromJson(json);
}
