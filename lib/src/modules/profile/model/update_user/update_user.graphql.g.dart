// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_user.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateUser$Mutation$User _$UpdateUser$Mutation$UserFromJson(Map<String, dynamic> json) {
  return UpdateUser$Mutation$User()..id = json['id'] as String;
}

Map<String, dynamic> _$UpdateUser$Mutation$UserToJson(UpdateUser$Mutation$User instance) => <String, dynamic>{
      'id': instance.id,
    };

UpdateUser$Mutation _$UpdateUser$MutationFromJson(Map<String, dynamic> json) {
  return UpdateUser$Mutation()
    ..updateUser = json['updateUser'] == null
        ? null
        : UpdateUser$Mutation$User.fromJson(json['updateUser'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateUser$MutationToJson(UpdateUser$Mutation instance) => <String, dynamic>{
      'updateUser': instance.updateUser?.toJson(),
    };

UpdateUser$UpdateUserInput _$UpdateUser$UpdateUserInputFromJson(Map<String, dynamic> json) {
  return UpdateUser$UpdateUserInput(
    name: json['name'] as String,
    phone: json['phone'] as String,
    address: json['address'] as String,
    avatar: json['avatar'] as String,
    role: json['role'] as String,
  );
}

Map<String, dynamic> _$UpdateUser$UpdateUserInputToJson(UpdateUser$UpdateUserInput instance) {
  final map = Map<String, dynamic>();
  if (instance.name != null) map['name'] = instance.name;
  if (instance.phone != null) map['phone'] = instance.phone;
  if (instance.address != null) map['address'] = instance.address;
  if (instance.provinceId != null) map['province'] = instance.provinceId;
  if (instance.districtId != null) map['district'] = instance.districtId;
  if (instance.wardId != null) map['wardId'] = instance.wardId;
  if (instance.role != null) map['role'] = instance.role;
  if (instance.avatar != null) map['avatar'] = instance.avatar;
  return map;
}

UpdateUserArguments _$UpdateUserArgumentsFromJson(Map<String, dynamic> json) {
  return UpdateUserArguments(
    i: json['i'] as String,
    d: json['d'] == null ? null : UpdateUser$UpdateUserInput.fromJson(json['d'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UpdateUserArgumentsToJson(UpdateUserArguments instance) => <String, dynamic>{
      'i': instance.i,
      'd': instance.d?.toJson(),
    };
