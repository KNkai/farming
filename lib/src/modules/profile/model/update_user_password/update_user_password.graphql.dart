// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
part 'update_user_password.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateUserPassword$Mutation$User with EquatableMixin {
  UpdateUserPassword$Mutation$User();

  factory UpdateUserPassword$Mutation$User.fromJson(
          Map<String, dynamic> json) =>
      _$UpdateUserPassword$Mutation$UserFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() =>
      _$UpdateUserPassword$Mutation$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateUserPassword$Mutation with EquatableMixin {
  UpdateUserPassword$Mutation();

  factory UpdateUserPassword$Mutation.fromJson(Map<String, dynamic> json) =>
      _$UpdateUserPassword$MutationFromJson(json);

  UpdateUserPassword$Mutation$User updateUserPassword;

  @override
  List<Object> get props => [updateUserPassword];
  Map<String, dynamic> toJson() => _$UpdateUserPassword$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateUserPasswordArguments extends JsonSerializable with EquatableMixin {
  UpdateUserPasswordArguments({@required this.i, @required this.p});

  factory UpdateUserPasswordArguments.fromJson(Map<String, dynamic> json) =>
      _$UpdateUserPasswordArgumentsFromJson(json);

  final String i;

  final String p;

  @override
  List<Object> get props => [i, p];
  Map<String, dynamic> toJson() => _$UpdateUserPasswordArgumentsToJson(this);
}

class UpdateUserPasswordMutation extends GraphQLQuery<
    UpdateUserPassword$Mutation, UpdateUserPasswordArguments> {
  UpdateUserPasswordMutation({this.variables});

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'updateUserPassword'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'i')),
              type: NamedTypeNode(name: NameNode(value: 'ID'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'p')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'updateUserPassword'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'i'))),
                ArgumentNode(
                    name: NameNode(value: 'password'),
                    value: VariableNode(name: NameNode(value: 'p')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'updateUserPassword';

  @override
  final UpdateUserPasswordArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  UpdateUserPassword$Mutation parse(Map<String, dynamic> json) =>
      UpdateUserPassword$Mutation.fromJson(json);
}
