// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_user_password.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateUserPassword$Mutation$User _$UpdateUserPassword$Mutation$UserFromJson(
    Map<String, dynamic> json) {
  return UpdateUserPassword$Mutation$User()..id = json['id'] as String;
}

Map<String, dynamic> _$UpdateUserPassword$Mutation$UserToJson(
        UpdateUserPassword$Mutation$User instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

UpdateUserPassword$Mutation _$UpdateUserPassword$MutationFromJson(
    Map<String, dynamic> json) {
  return UpdateUserPassword$Mutation()
    ..updateUserPassword = json['updateUserPassword'] == null
        ? null
        : UpdateUserPassword$Mutation$User.fromJson(
            json['updateUserPassword'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateUserPassword$MutationToJson(
        UpdateUserPassword$Mutation instance) =>
    <String, dynamic>{
      'updateUserPassword': instance.updateUserPassword?.toJson(),
    };

UpdateUserPasswordArguments _$UpdateUserPasswordArgumentsFromJson(
    Map<String, dynamic> json) {
  return UpdateUserPasswordArguments(
    i: json['i'] as String,
    p: json['p'] as String,
  );
}

Map<String, dynamic> _$UpdateUserPasswordArgumentsToJson(
        UpdateUserPasswordArguments instance) =>
    <String, dynamic>{
      'i': instance.i,
      'p': instance.p,
    };
