// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_staff.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateStaff$Mutation$Staff _$UpdateStaff$Mutation$StaffFromJson(Map<String, dynamic> json) {
  return UpdateStaff$Mutation$Staff()..id = json['id'] as String;
}

Map<String, dynamic> _$UpdateStaff$Mutation$StaffToJson(UpdateStaff$Mutation$Staff instance) => <String, dynamic>{
      'id': instance.id,
    };

UpdateStaff$Mutation _$UpdateStaff$MutationFromJson(Map<String, dynamic> json) {
  return UpdateStaff$Mutation()
    ..updateStaff = json['updateStaff'] == null
        ? null
        : UpdateStaff$Mutation$Staff.fromJson(json['updateStaff'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UpdateStaff$MutationToJson(UpdateStaff$Mutation instance) => <String, dynamic>{
      'updateStaff': instance.updateStaff?.toJson(),
    };

UpdateStaff$UpdateStaffInput _$UpdateStaff$UpdateStaffInputFromJson(Map<String, dynamic> json) {
  return UpdateStaff$UpdateStaffInput(
    name: json['name'] as String,
    avatar: json['avatar'] as String,
    isBlock: json['isBlock'] as bool,
    address: json['address'] as String,
    province: json['province'] as String,
    district: json['district'] as String,
    ward: json['ward'] as String,
  );
}

Map<String, dynamic> _$UpdateStaff$UpdateStaffInputToJson(UpdateStaff$UpdateStaffInput instance) {
  final map = Map<String, dynamic>();
  if (instance.name != null) map['name'] = instance.name;
  if (instance.avatar != null) map['avatar'] = instance.avatar;
  if (instance.isBlock != null) map['isBlock'] = instance.isBlock;
  if (instance.address != null) map['address'] = instance.address;
  if (instance.province != null) map['province'] = instance.province;
  if (instance.district != null) map['district'] = instance.district;
  if (instance.ward != null) map['ward'] = instance.ward;
  if (instance.provinceId != null) map['provinceId'] = instance.provinceId;
  if (instance.districtId != null) map['districtId'] = instance.districtId;
  if (instance.wardId != null) map['wardId'] = instance.wardId;
  if (instance.email != null) map['email'] = instance.email;
  return map;
}

UpdateStaffArguments _$UpdateStaffArgumentsFromJson(Map<String, dynamic> json) {
  return UpdateStaffArguments(
    i: json['i'] as String,
    d: json['d'] == null ? null : UpdateStaff$UpdateStaffInput.fromJson(json['d'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UpdateStaffArgumentsToJson(UpdateStaffArguments instance) => <String, dynamic>{
      'i': instance.i,
      'd': instance.d?.toJson(),
    };
