// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:artemis/artemis.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'update_staff.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateStaff$Mutation$Staff with EquatableMixin {
  UpdateStaff$Mutation$Staff();

  factory UpdateStaff$Mutation$Staff.fromJson(Map<String, dynamic> json) =>
      _$UpdateStaff$Mutation$StaffFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() => _$UpdateStaff$Mutation$StaffToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateStaff$Mutation with EquatableMixin {
  UpdateStaff$Mutation();

  factory UpdateStaff$Mutation.fromJson(Map<String, dynamic> json) =>
      _$UpdateStaff$MutationFromJson(json);

  UpdateStaff$Mutation$Staff updateStaff;

  @override
  List<Object> get props => [updateStaff];
  Map<String, dynamic> toJson() => _$UpdateStaff$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateStaff$UpdateStaffInput with EquatableMixin {
  UpdateStaff$UpdateStaffInput(
      {this.name,
      this.avatar,
      this.isBlock,
      this.address,
      this.province,
      this.district,
      this.ward,
      this.provinceId,
      this.districtId,
      this.email,
      this.wardId});

  factory UpdateStaff$UpdateStaffInput.fromJson(Map<String, dynamic> json) =>
      _$UpdateStaff$UpdateStaffInputFromJson(json);

  String name;

  String avatar;

  bool isBlock;

  String address;

  String province;

  String district;

  String ward;

  String wardId;

  String provinceId;

  String districtId;

  String email;

  @override
  List<Object> get props =>
      [name, avatar, isBlock, address, province, district, ward];
  Map<String, dynamic> toJson() => _$UpdateStaff$UpdateStaffInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdateStaffArguments extends JsonSerializable with EquatableMixin {
  UpdateStaffArguments({@required this.i, @required this.d});

  factory UpdateStaffArguments.fromJson(Map<String, dynamic> json) =>
      _$UpdateStaffArgumentsFromJson(json);

  final String i;

  final UpdateStaff$UpdateStaffInput d;

  @override
  List<Object> get props => [i, d];
  Map<String, dynamic> toJson() => _$UpdateStaffArgumentsToJson(this);
}

class UpdateStaffMutation
    extends GraphQLQuery<UpdateStaff$Mutation, UpdateStaffArguments> {
  UpdateStaffMutation({this.variables});

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'updateStaff'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'i')),
              type: NamedTypeNode(name: NameNode(value: 'ID'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'd')),
              type: NamedTypeNode(
                  name: NameNode(value: 'UpdateStaffInput'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'updateStaff'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'id'),
                    value: VariableNode(name: NameNode(value: 'i'))),
                ArgumentNode(
                    name: NameNode(value: 'data'),
                    value: VariableNode(name: NameNode(value: 'd')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'updateStaff';

  @override
  final UpdateStaffArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  UpdateStaff$Mutation parse(Map<String, dynamic> json) =>
      UpdateStaff$Mutation.fromJson(json);
}
