// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:artemis/artemis.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'get_district.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class District with EquatableMixin {
  District({this.name, this.id});

  factory District.fromJson(Map<String, dynamic> json) =>
      _$GetDistrict$Query$DistrictDataFromJson(json);

  String id;

  String name;

  @override
  List<Object> get props => [id, name];
  Map<String, dynamic> toJson() => _$GetDistrict$Query$DistrictDataToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetDistrict$Query with EquatableMixin {
  GetDistrict$Query();

  factory GetDistrict$Query.fromJson(Map<String, dynamic> json) =>
      _$GetDistrict$QueryFromJson(json);

  List<District> getDistrict;

  @override
  List<Object> get props => [getDistrict];
  Map<String, dynamic> toJson() => _$GetDistrict$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetDistrictArguments extends JsonSerializable with EquatableMixin {
  GetDistrictArguments({@required this.p});

  factory GetDistrictArguments.fromJson(Map<String, dynamic> json) =>
      _$GetDistrictArgumentsFromJson(json);

  final String p;

  @override
  List<Object> get props => [p];
  Map<String, dynamic> toJson() => _$GetDistrictArgumentsToJson(this);
}

class GetDistrictQuery
    extends GraphQLQuery<GetDistrict$Query, GetDistrictArguments> {
  GetDistrictQuery({this.variables});

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'getDistrict'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'p')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'getDistrict'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'provinceId'),
                    value: VariableNode(name: NameNode(value: 'p')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'district'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'getDistrict';

  @override
  final GetDistrictArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  GetDistrict$Query parse(Map<String, dynamic> json) =>
      GetDistrict$Query.fromJson(json);
}
