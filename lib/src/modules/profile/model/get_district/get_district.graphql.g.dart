// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_district.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

District _$GetDistrict$Query$DistrictDataFromJson(Map<String, dynamic> json) {
  return District()
    ..id = json['id'] as String
    ..name = json['district'] as String;
}

Map<String, dynamic> _$GetDistrict$Query$DistrictDataToJson(
        District instance) =>
    <String, dynamic>{
      'id': instance.id,
      'district': instance.name,
    };

GetDistrict$Query _$GetDistrict$QueryFromJson(Map<String, dynamic> json) {
  return GetDistrict$Query()
    ..getDistrict = (json['getDistrict'] as List)
        ?.map((e) =>
            e == null ? null : District.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetDistrict$QueryToJson(GetDistrict$Query instance) =>
    <String, dynamic>{
      'getDistrict': instance.getDistrict?.map((e) => e?.toJson())?.toList(),
    };

GetDistrictArguments _$GetDistrictArgumentsFromJson(Map<String, dynamic> json) {
  return GetDistrictArguments(
    p: json['p'] as String,
  );
}

Map<String, dynamic> _$GetDistrictArgumentsToJson(
        GetDistrictArguments instance) =>
    <String, dynamic>{
      'p': instance.p,
    };
