class ProfileModel {
  String id;
  String phone;
  String name;
  String avatar;
  String uid;
  String email;
  bool isBlock;
  String address;
  String province;
  String district;
  String ward;
  String provinceId;
  String districtId;
  String wardId;
  DateTime createdAt;
  DateTime updatedAt;
  int unseenNotify;

  ProfileModel({
    this.id,
    this.phone,
    this.name,
    this.avatar,
    this.email,
    this.uid,
    this.isBlock,
    this.address,
    this.province,
    this.district,
    this.ward,
    this.provinceId,
    this.districtId,
    this.wardId,
    this.createdAt,
    this.updatedAt,
    this.unseenNotify,
  });

  ProfileModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    phone = json['phone'];
    name = json['name'];
    avatar = json['avatar'];
    uid = json['uid'];
    isBlock = json['isBlock'];
    email = json['email'];
    address = json['address'];
    province = json['province'];
    district = json['district'];
    ward = json['ward'];
    provinceId = json['provinceId'];
    districtId = json['districtId'];
    wardId = json['wardId'];
    createdAt = json['createdAt'] == null ? null : DateTime.tryParse(json['createdAt'] as String)?.toLocal();
    updatedAt = json['updatedAt'] == null ? null : DateTime.tryParse(json['updatedAt'] as String)?.toLocal();
    unseenNotify = json['unseenNotify'];
  }
}
