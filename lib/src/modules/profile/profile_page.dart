import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/modules/change_password/change_password_page.dart';
import 'package:mi_smart/src/modules/login/login_bloc.dart';
import 'package:mi_smart/src/modules/profile/profile_bloc.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/loading_widget.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:mi_smart/src/share/widget/address_vn.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/app_form.dart';
import 'package:mi_smart/src/share/widget/image_picker.dart';

class ProfilePage extends StatefulWidget {
  static void go(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => ProfilePage()));
  }

  @override
  _BookingProfilePageState createState() => _BookingProfilePageState();
}

class _BookingProfilePageState extends State<ProfilePage> {
  final _paddingBelowLayout = (window.physicalSize.height / window.devicePixelRatio) * .35;
  final _ctrlName = TextEditingController();
  final _ctrlPhone = TextEditingController();
  final _ctrlMail = TextEditingController();
  final _ctrlAddress = ValueNotifier<AddressVnValues>(null);
  ProfileBloc _bloc;
  @override
  void initState() {
    _bloc = ProfileBloc();
    _bloc.eventInit();
    _bloc.stateSide.stream.listen((event) {
      if (event is StateSideUpdateName) {
        _ctrlName.text = event.value;
      } else if (event is StateSideUpdatePhone) {
        _ctrlPhone.text = event.value;
      } else if (event is StateSideUpdateEmail) {
        _ctrlMail.text = event.value;
      } else if (event is StateSideUpdateAddress) {
        _ctrlAddress.value = event.addressValues;
      } else if (event is StateSideGoChangePassword) {
        ChangePasswordPage.go(context, event.email);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        body: SafeArea(
          child: LoadingWidget(
            valueListenable: _bloc.stateLoading,
            child: SingleChildScrollView(
              child: Stack(
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(children: [
                          Expanded(
                              child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: const Text(
                              'Trở về',
                              style: TextStyle(color: AppColor.primary, fontSize: 18),
                            ),
                          )),
                          GestureDetector(
                            onTap: () {
                              LoginBloc.logout();
                            },
                            child: const Text(
                              'Đăng xuất',
                              style: TextStyle(color: AppColor.primary, fontSize: 18),
                            ),
                          )
                        ]),
                        const SizedBox(
                          height: 10,
                        ),
                        GestureDetector(
                          onTap: () => imagePicker(
                              context, (path) => _bloc.eventUpdateImage(path), (path) => _bloc.eventUpdateImage(path)),
                          child: StreamBuilder<UpdateImgState>(
                              stream: _bloc.stateImg.stream,
                              builder: (_, ss) {
                                if (ss?.data?.imgurUrl != null)
                                  return Container(
                                    height: 99,
                                    width: 99,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image:
                                            DecorationImage(fit: BoxFit.cover, image: NetworkImage(ss.data.imgurUrl))),
                                  );

                                if (ss?.data?.filePath != null)
                                  return Container(
                                    height: 99,
                                    width: 99,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(fit: BoxFit.cover, image: FileImage(
                                            // Do not add AssetImage in here, because file path access from platform native
                                            File(ss.data.filePath)))),
                                  );

                                return Container(
                                    height: 99,
                                    width: 99,
                                    child: const Icon(
                                      Icons.person,
                                      size: 40,
                                    ),
                                    decoration: BoxDecoration(
                                      color: Colors.grey[100],
                                      shape: BoxShape.circle,
                                    ));
                              }),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          enabled: false,
                          controller: _ctrlName,
                          textAlign: TextAlign.center,
                          decoration: const InputDecoration(
                              isDense: true, border: InputBorder.none, contentPadding: EdgeInsets.zero),
                          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                        ),
                        GestureDetector(
                          onTap: () => imagePicker(
                              context, (path) => _bloc.eventUpdateImage(path), (path) => _bloc.eventUpdateImage(path),
                              title: 'CẬP NHẬT AVATAR'),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Icon(Icons.cloud_upload),
                              SizedBox(
                                width: 5,
                              ),
                              Text('Tải ảnh lên')
                            ],
                          ),
                        )
                      ],
                    ),
                    height: _paddingBelowLayout,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.only(bottomRight: Radius.circular(15), bottomLeft: Radius.circular(15))),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(23),
                    child: Column(
                      children: [
                        SizedBox(
                          height: _paddingBelowLayout,
                        ),
                        ValueListenableBuilder<bool>(
                          valueListenable: _bloc.statePhoneError,
                          builder: (_, ss, ___) {
                            return _form(
                              'Số điện thoại',
                              _ctrlPhone,
                              (phone) {
                                _bloc.eventChangePhone(phone);
                              },
                              errorText: ss ? 'Sai số điện thoại' : null,
                              keyboardType: TextInputType.phone,
                              enabled: !AppRole.isStaff,
                              colorBg: !AppRole.isStaff ? null : Colors.grey[400],
                              textStyle: !AppRole.isStaff ? null : const TextStyle(color: Colors.white),
                            );
                          },
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        ValueListenableBuilder<bool>(
                          valueListenable: _bloc.stateNameError,
                          builder: (_, ss, ___) {
                            return _form('Họ và tên tài khoản', _ctrlName, (name) {
                              _bloc.eventChangeName(name);
                            }, errorText: ss ? 'Sai tên' : null);
                          },
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        AppForm(
                          title: 'Thông tin',
                          child: AddressVn(
                            controller: _ctrlAddress,
                            onData: (detail, p, d, w) {
                              _bloc.eventupdateAddress(detail, p, d, w);
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        ValueListenableBuilder<bool>(
                          valueListenable: _bloc.stateEmailError,
                          builder: (_, ss, ___) {
                            return _form(
                              'Email',
                              _ctrlMail,
                              (email) {
                                _bloc.eventChangeEmail(email);
                              },
                              errorText: ss ? 'Sai email' : null,
                              enabled: AppRole.isStaff,
                              colorBg: AppRole.isStaff ? null : Colors.grey[400],
                              textStyle: AppRole.isStaff ? null : const TextStyle(color: Colors.white),
                            );
                          },
                        ),
                        if (!AppRole.isStaff)
                          const SizedBox(
                            height: 30,
                          ),
                        if (!AppRole.isStaff)
                          AppFormIcon(
                            const Text('Đổi mật khẩu'),
                            onTap: () {
                              _bloc.eventGoChangePassword();
                            },
                            trailingHint: const Icon(
                              Icons.arrow_forward,
                              color: Colors.grey,
                            ),
                          ),
                        const SizedBox(
                          height: 30,
                        ),
                        ValueListenableBuilder<bool>(
                            valueListenable: _bloc.stateShowBtn,
                            builder: (_, ss, ___) {
                              return Row(
                                children: [
                                  Expanded(
                                    child: AppBtn(
                                      'Huỷ',
                                      borderColor: AppColor.primary,
                                      paddingBtn: EdgeInsets.zero,
                                      color: Colors.white,
                                      textColor: AppColor.primary,
                                      onPressed: ss
                                          ? () {
                                              _bloc.eventReset();
                                            }
                                          : null,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                      child: AppBtn(
                                    'Lưu thay đổi',
                                    onPressed: ss
                                        ? () {
                                            _bloc.eventUpdate();
                                          }
                                        : null,
                                    paddingBtn: EdgeInsets.zero,
                                  )),
                                ],
                              );
                            })
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _form(String title, TextEditingController controller, void Function(String) onChanged,
      {TextInputType keyboardType, String errorText, bool enabled, Color colorBg, TextStyle textStyle}) {
    return AppForm(
      title: title,
      colorBg: colorBg,
      padding: EdgeInsets.zero,
      child: TextFormField(
        onChanged: onChanged,
        enabled: enabled,
        keyboardType: keyboardType,
        controller: controller,
        style: textStyle,
        decoration: InputDecoration(
            errorText: errorText, border: InputBorder.none, contentPadding: const EdgeInsets.symmetric(horizontal: 10)),
      ),
    );
  }
}
