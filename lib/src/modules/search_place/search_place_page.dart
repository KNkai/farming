import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/loading_widget.dart';
import 'package:mi_smart/src/modules/search_place/search_field.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

import 'search_place_bloc.dart';
import 'search_place_repo.dart';

class SearchPage extends StatefulWidget {
  static void goSelect(BuildContext context, void Function(AutoCompletePrediction) onTap) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => SearchPage())).then((value) {
      if (value != null) onTap(value);
    });
  }

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState<T> extends State<SearchPage> {
  final _bloc = SearchPlaceBloc();

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      title: 'Tìm kiếm',
      child: LoadingWidget(
        valueListenable: _bloc.stateLoading,
        child: Column(
          children: <Widget>[
            SearchField(
              hint: 'Nhập',
              onChange: (text) {
                _bloc.eventRunAutoComplete(text);
              },
            ),
            const SizedBox(
              height: 27,
            ),
            Expanded(
              child: StreamBuilder<ListState>(
                stream: _bloc.stateList.stream,
                builder: (_, ss) {
                  final state = ss?.data;
                  if (state != null) {
                    if (state is ListEmptyState) {
                      return const Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            'Dữ liệu trống',
                            style: TextStyle(fontSize: 20),
                          ));
                    } else if (state is ListLoadState) {
                      return ListView.separated(
                          itemBuilder: (_, index) {
                            return GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                Navigator.of(context).pop(state.data[index]);
                              },
                              child: Text(state.data[index].description),
                            );
                          },
                          separatorBuilder: (_, index) {
                            return const Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Divider(
                                height: 2,
                              ),
                            );
                          },
                          itemCount: state.data.length);
                    }
                  }

                  return const SizedBox.shrink();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
