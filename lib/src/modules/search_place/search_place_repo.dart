import 'package:mi_smart/base_config/src/utils/formart.dart';
import 'package:mi_smart/src/share/utils/client_dio.dart';

class SearchPlaceRepo {
  Future autoComplete(String input) {
    return ClientDio.instance.get(HOST_PLACES_API + 'autocomplete/json', queryParameters: {
      'key': GOOGLE_MAP_API_KEY,
      'input': input,
      'language': 'vi',
      'components': 'country:vn'
    }).then((response) {
      if (response?.data == null) return ResultError();

      final result = AutoCompleteResponse.fromJson(response.data);

      if (result.predictions.isEmpty) return ResultError();

      return result.predictions;
    }).catchError((_) {
      return ResultError();
    });
  }
}

class AutoCompleteResponse {
  final List<AutoCompletePrediction> predictions;

  AutoCompleteResponse(this.predictions);

  factory AutoCompleteResponse.fromJson(Map<String, dynamic> json) {
    return AutoCompleteResponse(
        (json['predictions'] as List)?.map((e) => e == null ? null : AutoCompletePrediction.fromJson(e))?.toList());
  }
}

class AutoCompletePrediction {
  final String description;
  final String placeId;

  AutoCompletePrediction({this.description, this.placeId});

  factory AutoCompletePrediction.fromJson(Map<String, dynamic> json) {
    return AutoCompletePrediction(description: json['description'], placeId: json['place_id']);
  }
}
