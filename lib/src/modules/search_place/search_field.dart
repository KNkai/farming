import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  final Function(String) onFieldSubmitted;
  final Function(String) onChange;
  final String hint;
  const SearchField({this.onFieldSubmitted, this.onChange, this.hint});

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController();
    return Container(
      height: 40,
      padding: const EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: const Color.fromRGBO(255, 255, 255, 0.7)),
      child: TextFormField(
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChange,
        controller: controller,
        decoration: InputDecoration(
            hintText: hint ?? 'Tìm kiếm',
            contentPadding: const EdgeInsets.only(top: 4),
            border: InputBorder.none,
            prefixIcon: const Icon(Icons.search),
            suffixIcon: InkWell(
                onTap: () {
                  controller.clear();
                  onChange('');
                },
                child: const Padding(
                  padding: EdgeInsets.only(left: 5, top: 5, right: 5),
                  child: Icon(
                    Icons.close,
                    size: 16,
                  ),
                ))),
      ),
    );
  }
}
