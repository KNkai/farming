import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mi_smart/src/modules/search_place/search_place_repo.dart';

class SearchPlaceBloc {
  final _repo = SearchPlaceRepo();
  final stateLoading = ValueNotifier<bool>(false);
  final stateList = StreamController<ListState>();
  void eventRunAutoComplete(String input) {
    _repo.autoComplete(input).then((value) {
      if (value is List<AutoCompletePrediction>) {
        stateList.add(ListLoadState(value));
      } else
        stateList.add(ListEmptyState());
    });
  }

  void dispose() {
    stateLoading.dispose();
    stateList.close();
  }
}

abstract class ListState {}

class ListLoadState extends ListState {
  List<AutoCompletePrediction> data;
  ListLoadState(this.data);
}

class ListEmptyState extends ListState {}

class ResultSuccess<T> {
  final int total;
  final List<T> dataList;
  ResultSuccess(this.total, this.dataList);
}

class ResultError<T> {
  final String msg;
  final Exception exception;
  final T data;
  ResultError({this.msg, this.exception, this.data});
}
