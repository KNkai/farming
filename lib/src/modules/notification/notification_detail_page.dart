import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/modules/notification/components/notification_card.dart';
import 'package:mi_smart/src/share/widget/app_scaffold.dart';

import 'model/get_all_notification/get_all_notification.dart';

class NotificationDetailPage extends StatelessWidget {
  static void go(BuildContext context, AgriNotification item) => Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => NotificationDetailPage(item),
      ));

  final AgriNotification item;
  const NotificationDetailPage(this.item);

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      title: 'Chi tiết',
      child: SingleChildScrollView(
        child: NotificationCard(
          time: item.createdAt,
          title: item.title,
          body: item.body,
          imageLink: item.image,
        ),
      ),
    );
  }
}
