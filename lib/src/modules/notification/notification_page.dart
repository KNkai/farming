import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/base/base_list/base_list_page.dart';
import 'package:mi_smart/src/modules/notification/components/notification_card.dart';
import 'package:mi_smart/src/modules/notification/model/get_all_notification/get_all_notification.graphql.dart';
import 'package:mi_smart/src/modules/notification/notification_repo.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';

import 'notification_detail_page.dart';

class NotificationPage extends StatelessWidget {
  static void go({@required BuildContext context, @required VoidCallback onBack}) => Navigator.of(context)
          .push(MaterialPageRoute(
        builder: (context) => const NotificationPage(),
      ))
          .then((value) {
        onBack();
      });

  const NotificationPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final repo = NotificationRepo();
    BaseApi.instance.rawBodyRawDataExecute('mutation readAllNotification {readAllNotification}');

    return BaseListPage<AgriNotification>(
      (item) => NotificationCard(
        time: item.createdAt,
        title: item.title,
        maxLines: 2,
        body: item.body,
        imageLink: item.image,
      ),
      separatorBuilder: (_, __) {
        return const SizedBox(
          height: 10,
        );
      },
      onTap: (_, item) {
        NotificationDetailPage.go(context, item);
      },
      searchBaseListUseCase: repo,
      getListUseCase: repo,
      loadMoreBaseListUseCase: repo,
    );
  }
}
