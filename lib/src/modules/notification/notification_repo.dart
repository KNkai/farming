import 'package:mi_smart/base_config/src/base/base_list/base_list_bloc.dart';
import 'package:mi_smart/src/modules/notification/model/get_all_notification/get_all_notification.graphql.dart';
import 'package:mi_smart/src/share/utils/base_api.dart';

class NotificationRepo implements GetListUseCase, LoadMoreBaseListUseCase, SearchBaseListUseCase {
  @override
  Future search({String search}) {
    return BaseApi.instance
        .execute(GetAllNotificationQuery(
            variables: GetAllNotificationArguments(
                q: GetAllNotification$QueryGetListInput(search: search, order: {"createdAt": -1}))))
        .then((value) {
      if (value?.data?.getAllNotification?.data == null) {
        return null;
      } else if (value.data.getAllNotification.data.isEmpty) {
        return null;
      } else {
        return ResultSuccess(value.data.getAllNotification.total, value.data.getAllNotification.data);
      }
    }).catchError((_) {
      return null;
    });
  }

  @override
  Future getList() {
    return BaseApi.instance
        .execute(GetAllNotificationQuery(
            variables: GetAllNotificationArguments(q: GetAllNotification$QueryGetListInput(order: {"createdAt": -1}))))
        .then((value) {
      if (value?.data?.getAllNotification?.data == null) {
        return null;
      } else if (value.data.getAllNotification.data.isEmpty) {
        return null;
      } else {
        return ResultSuccess(value.data.getAllNotification.total, value.data.getAllNotification.data);
      }
    }).catchError((_) {
      return null;
    });
  }

  @override
  Future loadMore({int page}) {
    return BaseApi.instance
        .execute(GetAllNotificationQuery(
            variables: GetAllNotificationArguments(
                q: GetAllNotification$QueryGetListInput(page: page, order: {"createdAt": -1}))))
        .then((value) {
      if (value?.data?.getAllNotification?.data == null) {
        return null;
      } else if (value.data.getAllNotification.data.isEmpty) {
        return null;
      } else {
        return ResultSuccess(value.data.getAllNotification.total, value.data.getAllNotification.data);
      }
    }).catchError((_) {
      return null;
    });
  }
}
