// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_all_notification.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AgriNotification _$GetAllNotification$Query$NotificationPageData$NotificationFromJson(Map<String, dynamic> json) {
  return AgriNotification()
    ..id = json['id'] as String
    ..userId = json['userId'] as String
    ..title = json['title'] as String
    ..body = json['body'] as String
    ..data = json['data']
    ..seen = json['seen'] as bool
    ..seenAt = json['seenAt'] == null ? null : DateTime.parse(json['seenAt'] as String)
    ..hash = json['hash'] as String
    ..image = json['image'] as String
    ..createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String);
}

Map<String, dynamic> _$GetAllNotification$Query$NotificationPageData$NotificationToJson(AgriNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'title': instance.title,
      'body': instance.body,
      'data': instance.data,
      'seen': instance.seen,
      'seenAt': instance.seenAt?.toIso8601String(),
      'hash': instance.hash,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

GetAllNotification$Query$NotificationPageData$Pagination
    _$GetAllNotification$Query$NotificationPageData$PaginationFromJson(Map<String, dynamic> json) {
  return GetAllNotification$Query$NotificationPageData$Pagination()
    ..limit = json['limit'] as int
    ..offset = json['offset'] as int
    ..page = json['page'] as int
    ..total = json['total'] as int;
}

Map<String, dynamic> _$GetAllNotification$Query$NotificationPageData$PaginationToJson(
        GetAllNotification$Query$NotificationPageData$Pagination instance) =>
    <String, dynamic>{
      'limit': instance.limit,
      'offset': instance.offset,
      'page': instance.page,
      'total': instance.total,
    };

GetAllNotification$Query$NotificationPageData _$GetAllNotification$Query$NotificationPageDataFromJson(
    Map<String, dynamic> json) {
  return GetAllNotification$Query$NotificationPageData()
    ..data = (json['data'] as List)
        ?.map((e) => e == null ? null : AgriNotification.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..total = json['total'] as int
    ..pagination = json['pagination'] == null
        ? null
        : GetAllNotification$Query$NotificationPageData$Pagination.fromJson(json['pagination'] as Map<String, dynamic>);
}

Map<String, dynamic> _$GetAllNotification$Query$NotificationPageDataToJson(
        GetAllNotification$Query$NotificationPageData instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
      'total': instance.total,
      'pagination': instance.pagination?.toJson(),
    };

GetAllNotification$Query _$GetAllNotification$QueryFromJson(Map<String, dynamic> json) {
  return GetAllNotification$Query()
    ..getAllNotification = json['getAllNotification'] == null
        ? null
        : GetAllNotification$Query$NotificationPageData.fromJson(json['getAllNotification'] as Map<String, dynamic>);
}

Map<String, dynamic> _$GetAllNotification$QueryToJson(GetAllNotification$Query instance) => <String, dynamic>{
      'getAllNotification': instance.getAllNotification?.toJson(),
    };

GetAllNotification$QueryGetListInput _$GetAllNotification$QueryGetListInputFromJson(Map<String, dynamic> json) {
  return GetAllNotification$QueryGetListInput(
    limit: json['limit'] as int,
    offset: json['offset'] as int,
    page: json['page'] as int,
    order: json['order'],
    filter: json['filter'],
    search: json['search'] as String,
  );
}

Map<String, dynamic> _$GetAllNotification$QueryGetListInputToJson(GetAllNotification$QueryGetListInput instance) {
  final map = Map<String, dynamic>();
  if (instance.limit != null) map['limit'] = instance.limit;
  if (instance.offset != null) map['offset'] = instance.offset;
  if (instance.page != null) map['page'] = instance.page;
  if (instance.order != null) map['order'] = instance.order;
  if (instance.filter != null) map['filter'] = instance.filter;
  if (instance.search != null) map['search'] = instance.search;
  return map;
}

GetAllNotificationArguments _$GetAllNotificationArgumentsFromJson(Map<String, dynamic> json) {
  return GetAllNotificationArguments(
    q: json['q'] == null ? null : GetAllNotification$QueryGetListInput.fromJson(json['q'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$GetAllNotificationArgumentsToJson(GetAllNotificationArguments instance) => <String, dynamic>{
      'q': instance.q?.toJson(),
    };
