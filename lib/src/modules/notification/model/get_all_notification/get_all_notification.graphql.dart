// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:artemis/artemis.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:json_annotation/json_annotation.dart';

part 'get_all_notification.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class AgriNotification with EquatableMixin {
  AgriNotification();

  factory AgriNotification.fromJson(Map<String, dynamic> json) =>
      _$GetAllNotification$Query$NotificationPageData$NotificationFromJson(json);

  String id;

  String userId;

  String title;

  String body;

  dynamic data;

  bool seen;

  DateTime seenAt;

  String hash;

  DateTime createdAt;

  DateTime updatedAt;

  String image;

  @override
  List<Object> get props => [id, userId, title, body, data, seen, seenAt, hash, createdAt, updatedAt];
  Map<String, dynamic> toJson() => _$GetAllNotification$Query$NotificationPageData$NotificationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllNotification$Query$NotificationPageData$Pagination with EquatableMixin {
  GetAllNotification$Query$NotificationPageData$Pagination();

  factory GetAllNotification$Query$NotificationPageData$Pagination.fromJson(Map<String, dynamic> json) =>
      _$GetAllNotification$Query$NotificationPageData$PaginationFromJson(json);

  int limit;

  int offset;

  int page;

  int total;

  @override
  List<Object> get props => [limit, offset, page, total];
  Map<String, dynamic> toJson() => _$GetAllNotification$Query$NotificationPageData$PaginationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllNotification$Query$NotificationPageData with EquatableMixin {
  GetAllNotification$Query$NotificationPageData();

  factory GetAllNotification$Query$NotificationPageData.fromJson(Map<String, dynamic> json) =>
      _$GetAllNotification$Query$NotificationPageDataFromJson(json);

  List<AgriNotification> data;

  int total;

  GetAllNotification$Query$NotificationPageData$Pagination pagination;

  @override
  List<Object> get props => [data, total, pagination];
  Map<String, dynamic> toJson() => _$GetAllNotification$Query$NotificationPageDataToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllNotification$Query with EquatableMixin {
  GetAllNotification$Query();

  factory GetAllNotification$Query.fromJson(Map<String, dynamic> json) => _$GetAllNotification$QueryFromJson(json);

  GetAllNotification$Query$NotificationPageData getAllNotification;

  @override
  List<Object> get props => [getAllNotification];
  Map<String, dynamic> toJson() => _$GetAllNotification$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllNotification$QueryGetListInput with EquatableMixin {
  GetAllNotification$QueryGetListInput({this.limit, this.offset, this.page, this.order, this.filter, this.search});

  factory GetAllNotification$QueryGetListInput.fromJson(Map<String, dynamic> json) =>
      _$GetAllNotification$QueryGetListInputFromJson(json);

  int limit;

  int offset;

  int page;

  dynamic order;

  dynamic filter;

  String search;

  @override
  List<Object> get props => [limit, offset, page, order, filter, search];
  Map<String, dynamic> toJson() => _$GetAllNotification$QueryGetListInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllNotificationArguments extends JsonSerializable with EquatableMixin {
  GetAllNotificationArguments({this.q});

  factory GetAllNotificationArguments.fromJson(Map<String, dynamic> json) =>
      _$GetAllNotificationArgumentsFromJson(json);

  final GetAllNotification$QueryGetListInput q;

  @override
  List<Object> get props => [q];
  Map<String, dynamic> toJson() => _$GetAllNotificationArgumentsToJson(this);
}

class GetAllNotificationQuery extends GraphQLQuery<GetAllNotification$Query, GetAllNotificationArguments> {
  GetAllNotificationQuery({this.variables});

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'getAllNotification'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'q')),
              type: NamedTypeNode(name: NameNode(value: 'QueryGetListInput'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'getAllNotification'),
              alias: null,
              arguments: [ArgumentNode(name: NameNode(value: 'q'), value: VariableNode(name: NameNode(value: 'q')))],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'data'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'), alias: null, arguments: [], directives: [], selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'userId'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'title'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'body'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'data'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'seen'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'seenAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'hash'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'createdAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'updatedAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'image'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'total'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'pagination'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'limit'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'offset'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'page'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'total'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'getAllNotification';

  @override
  final GetAllNotificationArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  GetAllNotification$Query parse(Map<String, dynamic> json) => GetAllNotification$Query.fromJson(json);
}
