import 'package:flutter/material.dart';

import 'notification_card.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<dynamic> notifications = List.generate(10, (index) {
      return {
        "title": "Cho tôm giống ăn lần $index",
        "body":
            "Cùng với tác động kép của thiên tai và dịch bệnh, nhiều doanh nghiệp vẫn loay hoay với các phương thức chuyển đổi số, đưa công nghệ vào sản xuất bắt kịp xu thế.",
        "date": DateTime.now(),
        "image": "https://placehold.it/320x200"
      };
    });
    return ListView.separated(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
      itemCount: notifications.length,
      itemBuilder: (context, index) {
        var icon = notifications[index];
        return NotificationCard(
          title: icon["title"],
          body: icon["body"],
          time: icon["date"],
          imageLink: icon["image"],
        );
      },
      separatorBuilder: (context, index) => const SizedBox(
        height: 32,
      ),
    );
  }
}
