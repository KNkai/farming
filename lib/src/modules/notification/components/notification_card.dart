import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';
import 'package:mi_smart/base_config/src/utils/formart.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class NotificationCard extends StatelessWidget {
  const NotificationCard({@required this.title, this.body, @required this.time, this.imageLink, this.maxLines});
  final String title, body, imageLink;
  final DateTime time;
  final int maxLines;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...imageLink != null
              ? [
                  ClipRRect(
                    child: Image.network(
                      imageLink,
                      width: deviceWidth(context),
                      fit: BoxFit.cover,
                      loadingBuilder: (context, child, loadingProgress) => loadingProgress != null
                          ? const Center(
                              child: SizedBox(height: 50, width: 50, child: CircularProgressIndicator()),
                            )
                          : child,
                    ),
                  )
                ]
              : [],
          const SizedBox(height: 12),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              title,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 8),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              body,
              maxLines: maxLines,
              overflow: maxLines != null ? TextOverflow.ellipsis : null,
              style: const TextStyle(color: Color(0xFF0D1904)),
            ),
          ),
          const SizedBox(height: 8),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              Format.ddsmmsyyyy(time),
              style: const TextStyle(color: AppColor.primary),
            ),
          ),
          const SizedBox(height: 16)
        ],
      ),
    );
  }
}
