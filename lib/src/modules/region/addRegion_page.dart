import 'dart:async';

import 'package:flutter/material.dart';

import '../../../base_config/base_config.dart';
import '../../../base_config/src/base/bloc_listener.dart';
import '../../../base_config/src/utils/validate_builder.dart';
import '../../../base_widget/base_widget.dart';
import '../../../base_widget/src/dialog.dart';
import '../../../base_widget/src/spacing_box.dart';
import '../../../navigator.dart';
import '../../share/components/address_input.dart';
import '../../share/components/select_image_list_input.dart';
import '../../share/components/select_input.dart';
import '../../share/components/select_list_sheet.dart';
import '../../share/components/text_input.dart';
import '../../share/components/textarea_input.dart';
import '../../share/repo/farmingType_repo.dart';
import '../../share/repo/material_repo.dart';
import '../../share/repo/zone_repo.dart';
import '../../share/widget/app_btn.dart';
import '../../share/widget/app_scaffold.dart';
import 'addRegion_event.dart';
import 'addRegion_state.dart';
import 'region_bloc.dart';

class AddRegionPage extends StatefulWidget {
  final dynamic region;
  const AddRegionPage({this.region});
  @override
  _AddRegionPageState createState() => _AddRegionPageState();
}

class _AddRegionPageState extends State<AddRegionPage> {
  RegionBloc bloc = RegionBloc();

  String ownerName, ownerPhone, ownerAddress, name;
  double area;
  String provinceId, districtId, wardId;
  List<String> imageList;
  String farmingTypeId, materialId, zoneId;
  String note;
  GlobalKey<FormState> addRegionForm = new GlobalKey(debugLabel: "addRegionForm");

  _handleState(BaseState state) {
    if (state is UpdateRegionFailState) {
      Timer.run(() {
        showAlertDialog(context, 'Có lỗi xảy ra: ${state.err}', navigatorKey: navigatorKey);
      });
    }
    if (state is UpdateRegionSuccessState) {
      showToast('Cập nhật thành công.');
    }
    if (state is CreateRegionFailState) {
      Timer.run(() {
        showAlertDialog(context, 'Có lỗi xảy ra: ${state.err}', navigatorKey: navigatorKey);
      });
    }
    if (state is CreateRegionSuccessState) {
      Future.delayed(const Duration(milliseconds: 200)).then((value) {
        navigatorKey.currentState.pushReplacementNamed(View.tabsRegion, arguments: state.region);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      onBack: true,
      title: 'Địa điểm canh tác',
      bgColor: const Color(0xFFF5F5F5),
      child: LoadingTask(
        bloc: bloc,
        child: BlocListener(
          bloc: bloc,
          listener: _handleState,
          child: Form(
            key: addRegionForm,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextInput(
                    labelText: "Người sở hữu",
                    hintText: "Nhập tên người sở hữu",
                    onSaved: (value) => ownerName = value,
                    validator: ValidateBuilder().empty().build(),
                    autofocus: true,
                  ),
                  const SpacingBox(height: 1),
                  TextInput(
                    labelText: "Số điện thoại người sở hữu",
                    hintText: "Nhập số điện thoại",
                    keyboardType: TextInputType.phone,
                    onSaved: (value) => ownerPhone = value,
                    validator: ValidateBuilder().empty().phone().build(),
                  ),
                  const SpacingBox(height: 1),
                  TextInput(
                    labelText: "Địa chỉ người sở hữu",
                    hintText: "Nhập số nhà và tên đường",
                    onSaved: (value) => ownerName = value,
                    validator: ValidateBuilder().empty().build(),
                  ),
                  const SpacingBox(height: 1),
                  TextInput(
                    labelText: "Tên địa điểm canh tác",
                    hintText: "Nhập tên địa điểm canh tác",
                    onSaved: (value) => name = value,
                    validator: ValidateBuilder().empty().build(),
                  ),
                  const SpacingBox(height: 1),
                  TextInput(
                    labelText: "Diện tích canh tác",
                    hintText: "Nhập diện tích canh tác",
                    keyboardType: TextInputType.number,
                    suffix: const Text("ha"),
                    onSaved: (value) => area = double.parse(value),
                    validator: ValidateBuilder().empty().build(),
                  ),
                  const SpacingBox(height: 1),
                  AddressInput(
                    onProvinceSaved: (value) => provinceId = value,
                    onDistrictSaved: (value) => districtId = value,
                    onWardSaved: (value) => wardId = value,
                    provinceValidator: ValidateBuilder().empty().build(),
                    districtValidator: ValidateBuilder().empty().build(),
                    wardValidator: ValidateBuilder().empty().build(),
                  ),
                  const SpacingBox(height: 1),
                  SelectImageListInput(
                    label: "Hình ảnh địa điểm canh tác",
                    onChanged: (images) => imageList = images,
                  ),
                  const SpacingBox(height: 2),
                  SelectListInput(
                    sheetTitle: "Chọn khu vực",
                    labelText: "Khu vực",
                    hintText: "Chọn khu vực",
                    sheetItems: ZoneRepo().getAllZone().then(
                        (value) => value.map((e) => SelectListSheetItem(display: e["name"], id: e["id"])).toList()),
                    onSaved: (value) => zoneId = value,
                    validator: ValidateBuilder().empty().build(),
                    onSelect: null,
                  ),
                  const SpacingBox(height: 1),
                  SelectListInput(
                    sheetTitle: "Chọn hình thức canh tác",
                    labelText: "Hình thức canh tác",
                    hintText: "Chọn hình thức canh tác",
                    sheetItems: FarmingTypeRepo().getAllFarmingType().then(
                        (value) => value.map((e) => SelectListSheetItem(display: e["name"], id: e["id"])).toList()),
                    onSaved: (value) => farmingTypeId = value,
                    validator: ValidateBuilder().empty().build(),
                    onSelect: null,
                  ),
                  const SpacingBox(height: 1),
                  SelectListInput(
                    sheetTitle: "Chọn nguyên liệu canh tác",
                    labelText: "Nguyên liệu canh tác",
                    hintText: "Chọn nguyên liệu canh tác",
                    sheetItems: MaterialRepo().getAllMaterial().then(
                        (value) => value.map((e) => SelectListSheetItem(display: e["name"], id: e["id"])).toList()),
                    onSaved: (value) => materialId = value,
                    validator: ValidateBuilder().empty().build(),
                    onSelect: null,
                  ),
                  const SpacingBox(height: 1),
                  TextareaInput(
                    labelText: 'Ghi chú',
                    hintText: "Nhập ghi chú",
                    maxLines: 4,
                    onSaved: (value) => note = value,
                  ),
                  const SpacingBox(height: 4),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppBtn('Lưu', onPressed: () {
                      if (addRegionForm.currentState.validate()) {
                        addRegionForm.currentState.save();
                        print('after save');
                        bloc.add(
                          CreateRegionEvent(
                            name: name,
                            provinceId: provinceId,
                            districtId: districtId,
                            wardId: wardId,
                            area: area,
                            materialId: materialId,
                            farmingTypeId: farmingTypeId,
                            images: imageList,
                            zoneId: zoneId,
                          ),
                        );
                      }
                    }),
                  ),
                  const SpacingBox(height: 1),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
