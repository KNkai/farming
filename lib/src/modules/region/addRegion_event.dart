import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/base_event.dart';

class UpdateImageEvent extends BaseEvent {
  String id;
  List<String> images;
  UpdateImageEvent(this.id, this.images);
}

class UpdateRegionEvent extends BaseEvent {
  String id;
  String name;
  String provinceId;
  String districtId;
  String wardId;
  double locationLat;
  double locationLng;
  double area;
  String managerId;
  String zoneId;
  String ownerName;
  String ownerPhone;
  String ownerAddress;
  List<String> assigneeIds;
  String htmlContent;
  String farmingTypeId;
  UpdateRegionEvent({
    @required this.id,
    this.name,
    this.provinceId,
    this.districtId,
    this.wardId,
    this.locationLat,
    this.locationLng,
    this.area,
    this.managerId,
    this.zoneId,
    this.ownerName,
    this.ownerPhone,
    this.assigneeIds,
    this.htmlContent,
    this.farmingTypeId,
  });
}

class CreateRegionEvent extends BaseEvent {
  String name;
  String provinceId;
  String districtId;
  String wardId;
  double area;
  String materialId;
  String farmingTypeId;
  String zoneId;
  List<String> images;
  CreateRegionEvent({
    this.name,
    this.provinceId,
    this.districtId,
    this.wardId,
    this.area,
    this.materialId,
    this.farmingTypeId,
    this.images,
    this.zoneId,
  });
}
