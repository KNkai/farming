import 'package:mi_smart/base_config/base_config.dart';

class LoadListRegionSuccessState extends BaseState {
  List regions;
  LoadListRegionSuccessState(this.regions);
}

class LoadListRegionFailState extends BaseState {
  String err;
  LoadListRegionFailState(this.err);
}

class LoadingListRegionState extends BaseState {}
