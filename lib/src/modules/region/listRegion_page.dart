import 'package:flutter/material.dart';
import 'package:mi_smart/src/page/new_diary/new_diary_page.dart';
import 'package:mi_smart/src/page/refresh.dart';
import 'package:mi_smart/src/share/utils/money_input_formatter.dart';

import '../../../base_config/base_config.dart';
import '../../../base_config/src/utils/constants.dart';
import '../../../base_widget/base_widget.dart';
import '../../../base_widget/src/spacing_box.dart';
import '../../share/utils/color.dart';
import '../../share/utils/util.dart';
import '../../share/widget/app_scaffold.dart';
import '../../share/widget/components/filter_sheet.dart';
import '../../share/widget/empty.dart';
import '../../share/widget/error.dart';
import '../../share/widget/load_more.dart';
import '../../share/widget/sort_btn.dart';
import '../find_regions/find_regions_page.dart';
import 'components/region_images.dart';
import 'listRegion_event.dart';
import 'listRegion_state.dart';
import 'region_bloc.dart';

class LisRegionPage extends StatefulWidget {
  @override
  _LisRegionPageState createState() => _LisRegionPageState();
}

class _LisRegionPageState extends State<LisRegionPage> with RouteAware, WidgetsBindingObserver, Refresh {
  RegionBloc bloc = RegionBloc();
  List<FilterGroup> filterGroups;
  @override
  void initState() {
    bloc.add(LoadListRegionEvent());
    super.initState();
  }

  @override
  void pleaseRefresh() {
    bloc.add(LoadListRegionEvent());
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      onBack: true,
      onSearch: (text) {
        bloc.add(ChangeFilterEvent(filter: bloc.filterValue.copyWith(search: text)));
      },
      onFilter: onFilter,
      child: LoadingTask(
        bloc: bloc,
        child: Column(
          children: [
            Row(children: [
              FutureBuilder<List>(
                  initialData: const [],
                  future: bloc.getSortOption(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Container();
                    }
                    if (snapshot.data.length == 0) {
                      return Container();
                    }
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SortBtn((value) {
                            bloc.add(ChangeFilterEvent(filter: bloc.filterValue.copyWith(order: value)));
                          }, snapshot.data),
                        ],
                      ),
                    );
                  }),
              Expanded(
                  child: Align(
                      alignment: Alignment.bottomRight,
                      child: IconButton(
                        onPressed: () {
                          FindRegionsPage.go(context);
                        },
                        icon: const Icon(
                          Icons.map,
                          color: AppColor.primary,
                        ),
                      )))
            ]),
            const SpacingBox(height: 2),
            Expanded(
              child: StreamBuilder(
                stream: bloc.regionStream,
                initialData: LoadingListRegionState(),
                builder: (context, snap) {
                  if (snap.data is LoadingListRegionState) {
                    return const Center();
                  }
                  if (snap.data is LoadListRegionFailState) {
                    return const Center(
                      child: ErrorImgWidget(),
                    );
                  }
                  if (snap.data is LoadListRegionSuccessState) {
                    final list = (snap.data as LoadListRegionSuccessState).regions;
                    if (list.length == 0) {
                      return const EmptyWidget(text: 'Không tìm thấy kết quả');
                    }
                    return LoadMoreScrollView(
                        physics: const AlwaysScrollableScrollPhysics(),
                        bloc: bloc,
                        itemCount: list.length,
                        itemBuilder: (context, index) => _regionItem(list[index]),
                        separatorBuilder: (context, index) => const SpacingBox(
                              height: 2,
                            ));
                  }
                  return Container();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onFilter() {
    if (bloc.materials.length != 0 && bloc.zones.length != 0) {
      if (filterGroups == null) {
        filterGroups = [
          FilterGroup(
              label: "Sản phẩm",
              colCount: 2,
              items: List.generate(bloc.materials.length, (index) {
                dynamic i = bloc.materials[index];
                return FilterItem(label: i["name"], value: i["id"], selected: false);
              })),
          FilterGroup(
              label: "Khu vực",
              colCount: 1,
              items: List.generate(bloc.zones.length, (index) {
                dynamic i = bloc.zones[index];
                return FilterItem(label: i["name"], value: i["id"], selected: false);
              })),
        ];
      }
      showModalBottomSheet(
        context: context,
        useRootNavigator: true,
        isScrollControlled: true,
        builder: (context) => FilterSheet(
          groups: filterGroups,
          onSubmit: (List<FilterGroup> res) {
            List<dynamic> filterOr = [];
            List<dynamic> selectedMaterial = res[0].selectedValues();
            List<dynamic> selectedZoneId = res[1].selectedValues();
            if (selectedMaterial.length > 0) {
              filterOr.add({
                "materialId": {"__in": selectedMaterial}
              });
            }
            if (selectedZoneId.length > 0) {
              filterOr.add({
                "zoneId": {"__in": selectedZoneId}
              });
            }
            bloc.add(ChangeFilterEvent(
              filter: bloc.filterValue.copyWith(
                filter: filterOr.length > 0 ? parseJsonToFilter({"__or": filterOr}) : '',
              ),
            ));
          },
        ),
      );
    }
  }

  _regionItem(dynamic item) {
    return GestureDetector(
      key: ObjectKey(item["id"]),
      onTap: () {
        // navigatorKey.currentState
        //     .pushNamed(View.tabsRegion, arguments: item)
        //     .then((value) => bloc.add(ChangeFilterEvent(filter: bloc.filterValue)));
        NewDiaryPage.go(context: context, id: item["id"]);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              item["name"] ?? '',
              style: ptHeadline(context).copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 0.2,
                  color: Colors.black87.withOpacity(0.7)),
            ),
            const SpacingBox(height: 1),
            Text('Diện tích: ${MoneyInputFormatter.getMaskedValue(item["area"] ?? 0)} ha'),
            Text('Sản phẩm: ${Formart.getMapElement<String>(item, ["material", "name"], defaultValue: '')}'),
            Text('Khu Vực: ${Formart.getMapElement<String>(item, ["zone", "name"], defaultValue: '')}'),
            Text('Ngày cập nhật: ${Formart.formatToDateTime(item["updatedAt"]) ?? ''}'),
            const SpacingBox(height: 1),
            if (Formart.getMapElement<List>(item, ["listImages"]).length != 0)
              RegionImages(images: item["listImages"].cast<String>()),
          ],
        ),
      ),
    );
  }
}
