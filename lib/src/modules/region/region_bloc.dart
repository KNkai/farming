import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_config/src/base/base_bloc.dart';
import 'package:mi_smart/base_config/src/base/base_event.dart';
import 'package:mi_smart/src/modules/region/addRegion_state.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';
import 'package:mi_smart/src/share/model/filter_model.dart';
import 'package:mi_smart/src/share/repo/material_repo.dart';
import 'package:mi_smart/src/share/repo/zone_repo.dart';
import 'package:mi_smart/src/share/widget/load_more.dart';
import 'package:rxdart/rxdart.dart';

import 'addRegion_event.dart';
import 'listRegion_event.dart';
import 'listRegion_repo.dart';
import 'listRegion_state.dart';

class RegionBloc extends BaseBloc with ChangeNotifier {
  RegionRepo _regionRepo = RegionRepo();
  List lList = [];
  List materials = [];
  List zones = [];

  final _regionState$ = BehaviorSubject<BaseState>();
  Stream<BaseState> get regionStream => _regionState$.stream;
  Sink<BaseState> get regionSink => _regionState$.sink;
  BaseState get regionValue => _regionState$.value;

  final _filter$ = BehaviorSubject<FilterModel>();
  Stream<FilterModel> get filterStream => _filter$.stream;
  Sink<FilterModel> get filterSink => _filter$.sink;
  FilterModel get filterValue => _filter$.value;

  RegionBloc() {
    getListFilters();

    FilterModel initFilter = FilterModel(limit: 10, page: 1, order: "{createdAt : -1}");
    filterSink.add(initFilter);
  }

  @override
  void dispatchEvent(BaseEvent event) {
    if (event is LoadListRegionEvent) getList(event);
    if (event is ChangeFilterEvent) changeFilter(event);
    if (event is LoadMoreEvent) loadMore();
    if (event is UpdateImageEvent) updateImage(event);
    if (event is UpdateRegionEvent) updateRegion(event);
    if (event is CreateRegionEvent) createRegion(event);
  }

  @override
  void dispose() {
    _regionState$.close();
    _filter$.close();
    super.dispose();
  }

  Future<List> getSortOption() async {
    return _regionRepo.getSortOptions();
  }

  void getListFilters() {
    MaterialRepo().getAllMaterial().then((value) {
      materials = value;
    });
    ZoneRepo().getAllZone().then((value) {
      zones = value;
    });
  }

  Future getList(LoadListRegionEvent event) async {
    try {
      loadingSink.add(true);
      final res = HiveService.instnace.boxRegions.values.first.data;
      regionSink.add(LoadListRegionSuccessState(res));
      lList = res;
    } catch (e) {
      regionSink.add(LoadListRegionFailState(e.message));
    } finally {
      loadingSink.add(false);
    }
  }

  Future loadMore() async {
    try {
      //loadingSink.add(true);
      filterSink.add(filterValue.copyWith(page: filterValue.page + 1));
      final res = await _regionRepo.getList(
          page: filterValue?.page,
          limit: filterValue?.limit,
          filter: filterValue?.filter,
          search: filterValue?.search,
          offset: filterValue?.offset,
          order: filterValue?.order);
      lList.addAll(res);
      filterValue.page++;
      regionSink.add(LoadListRegionSuccessState(lList));
    } catch (e) {
      regionSink.add(LoadListRegionFailState(e.toString()));
    } finally {
      //loadingSink.add(false);
    }
  }

  Future changeFilter(ChangeFilterEvent event) async {
    // try {
    //   loadingSink.add(true);
    //   filterSink.add(event.filter.copyWith(page: 1));
    //   getList(LoadListRegionEvent());
    // } catch (e) {
    //   regionSink.add(LoadListRegionFailState(e.toString()));
    // } finally {
    //   loadingSink.add(false);
    // }
  }

  Future updateImage(UpdateImageEvent event) async {
    try {
      await _regionRepo.updateRegionImages(id: event.id, listImages: event.images);
    } catch (e) {
      // print('nooooo');
    } finally {}
  }

  Future updateRegion(UpdateRegionEvent event) async {
    try {
      await _regionRepo.updateRegion(
        id: event.id,
        name: event.name,
        provinceId: event.provinceId,
        districtId: event.districtId,
        wardId: event.wardId,
        locationLat: event.locationLat,
        locationLng: event.locationLng,
        area: event.area,
        managerId: event.managerId,
        zoneId: event.zoneId,
        ownerName: event.ownerName,
        ownerPhone: event.ownerPhone,
        assigneeIds: event.assigneeIds,
        htmlContent: event.htmlContent,
        farmingTypeId: event.farmingTypeId,
      );
      processStateSink.add(UpdateRegionSuccessState());
    } catch (e) {
      processStateSink.add(UpdateRegionFailState(e.toString()));
    } finally {}
  }

  Future createRegion(CreateRegionEvent event) async {
    try {
      var result = await _regionRepo.createRegion(
          name: event.name,
          provinceId: event.provinceId,
          districtId: event.districtId,
          wardId: event.wardId,
          area: event.area,
          farmingTypeId: event.farmingTypeId,
          materialId: event.materialId,
          listImages: event.images,
          zoneId: event.zoneId);
      processStateSink.add(CreateRegionSuccessState(result));
    } catch (e) {
      print("LỖi ${e.toString()}");
      processStateSink.add(CreateRegionFailState(e.toString()));
    } finally {}
  }
}
