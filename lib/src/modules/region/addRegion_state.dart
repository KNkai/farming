import 'package:mi_smart/base_config/base_config.dart';

class UpdateRegionSuccessState extends BaseState {
  UpdateRegionSuccessState();
}

class UpdateRegionFailState extends BaseState {
  String err;
  UpdateRegionFailState(this.err);
}

class CreateRegionSuccessState extends BaseState {
  final dynamic region;
  CreateRegionSuccessState(this.region);
}

class CreateRegionFailState extends BaseState {
  String err;
  CreateRegionFailState(this.err);
}
