import 'package:mi_smart/base_config/base_config.dart';

class RegionSrv extends BaseService {
  RegionSrv() : super(module: 'Region', fragment: ''' 
      id
      name
      province
      provinceId
      district
      districtId
      ward
      wardId
      locationLat
      locationLng
      area
      updatedAt
      zone {
        name
      }
      farmingTypeId
      farmingType {
        name
        listImages
      }
      materialId
      material {
        name
        listImages
      }
      listImages
  ''');
}
