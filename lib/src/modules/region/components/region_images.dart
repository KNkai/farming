import 'dart:math';

import 'package:flutter/material.dart';

import '../../../share/widget/image_view.dart';

class RegionImages extends StatelessWidget {
  const RegionImages({
    Key key,
    @required this.images,
  }) : super(key: key);
  final List<String> images;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: images.map((e) => buildCommentImage(image: e)).toList(),
      ),
    );
  }

  Container buildCommentImage({String image}) {
    return Container(
      height: 180,
      width: 230,
      margin: const EdgeInsets.only(right: 12),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: ImageView(
              tag: 'img' + Random().nextInt(100000000).toString(), // this need a unique id
              url: image,
              w: 230,
              h: 180)),
    );
  }
}
