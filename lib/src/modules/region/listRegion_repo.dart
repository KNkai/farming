import 'package:mi_smart/base_config/base_config.dart';

import 'region_srv.dart';

class RegionRepo {
  RegionSrv _srv = RegionSrv();

  Future<List<dynamic>> getList(
      {int limit = 10, String filter, String search, String order, int page = 1, int offset}) async {
    try {
      final res =
          await _srv.getList(limit: limit, filter: filter, search: search, offset: offset, order: order, page: page);
      return res["data"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List> getSortOptions() async {
    try {
      final res = await _srv.queryEnum("getRegionOrderConst");
      return res["getRegionOrderConst"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<dynamic> updateRegion({
    @required String id,
    String name,
    String provinceId,
    String districtId,
    String wardId,
    double locationLat,
    double locationLng,
    double area,
    String managerId,
    String zoneId,
    String ownerName,
    String ownerPhone,
    String ownerAddress,
    String htmlContent,
    String farmingTypeId,
    List<String> assigneeIds,
  }) async {
    final res = await _srv.update(id: id, data: '''
name: "$name"
provinceId: "$provinceId"
districtId: "$districtId"
wardId: "$wardId"
locationLat: ${locationLat ?? 0}
locationLng: ${locationLng ?? 0}
area: ${area ?? 0}
managerId: "$managerId"
zoneId: "$zoneId"
ownerName: "$ownerName"
ownerPhone: "$ownerPhone"
ownerAddress: "$ownerAddress"
htmlContent: "$htmlContent"
farmingTypeId: "$farmingTypeId"
assigneeIds: ${Formart.listStringToGraphqlString(assigneeIds)}
      ''');
    return res;
  }

  Future<dynamic> createRegion({
    String name,
    String provinceId,
    String districtId,
    String wardId,
    double area,
    String materialId,
    String htmlContent,
    String farmingTypeId,
    String zoneId,
    List<String> listImages,
  }) async {
    final res = await _srv.add('''
name: "$name"
provinceId: "$provinceId"
districtId: "$districtId"
wardId: "$wardId"
area: ${area ?? 0}
materialId: "$materialId"
htmlContent: "$htmlContent"
farmingTypeId: "$farmingTypeId"
zoneId: "$zoneId"
listImages: ${Formart.listStringToGraphqlString(listImages)}
''');
    return res;
  }

  Future<dynamic> updateRegionImages({
    @required String id,
    @required List<String> listImages,
  }) async {
    try {
      final res = await _srv.update(id: id, data: '''
      listImages: ${Formart.listStringToGraphqlString(listImages)}
      ''');
      return res;
    } catch (e) {
      throw Exception(e);
    }
  }
}
