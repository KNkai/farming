import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/model/filter_model.dart';

class LoadListRegionEvent extends BaseEvent {
  LoadListRegionEvent();
}

class ChangeFilterEvent extends BaseEvent {
  FilterModel filter;
  ChangeFilterEvent({this.filter});
}
