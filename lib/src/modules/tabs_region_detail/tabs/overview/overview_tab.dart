import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../../base_config/base_config.dart';
import '../../../../../base_widget/base_widget.dart';
import '../../../../share/components/address_input.dart';
import '../../../../share/components/remove_button.dart';
import '../../../../share/components/select_image_list_input.dart';
import '../../../../share/components/select_input.dart';
import '../../../../share/components/select_list_sheet.dart';
import '../../../../share/components/text_input.dart';
import '../../../../share/components/textarea_input.dart';
import '../../../../share/repo/farmingType_repo.dart';
import '../../../../share/repo/material_repo.dart';
import '../../../../share/utils/money_input_formatter.dart';
import '../../../../share/widget/app_btn.dart';
import '../../tabs_region_controller.dart';
import '../controllers/overview_controller.dart';
// import './components/text_field.dart';

class OverviewTab extends StatefulWidget {
  const OverviewTab({Key key}) : super(key: key);

  @override
  _OverviewTabState createState() => _OverviewTabState();
}

class _OverviewTabState extends State<OverviewTab> {
  OverviewController _controller;
  TabsRegionController _tabRegionController;
  GlobalKey<FormState> overviewForm = new GlobalKey(debugLabel: "overview");
  bool updating = false;
  @override
  void initState() {
    super.initState();
    _controller = Provider.of<OverviewController>(context, listen: false);
    _tabRegionController = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _controller.getRegionInfo(),
      builder: (context, snapshot) {
        return snapshot.hasData
            ? SingleChildScrollView(
                child: Form(
                  key: overviewForm,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextInput(
                        labelText: "Người sở hữu",
                        hintText: "Nhập tên người sở hữu",
                        value: _controller.region["ownerName"],
                        onSaved: (value) => _controller.region["ownerName"] = value,
                      ),
                      buildFieldSpacing(),
                      TextInput(
                        labelText: "Số điện thoại người sở hữu",
                        hintText: "Nhập số điện thoại",
                        value: _controller.region["ownerPhone"],
                        onSaved: (value) => _controller.region["ownerPhone"] = value,
                      ),
                      buildFieldSpacing(),
                      TextInput(
                        labelText: "Địa chỉ người sở hữu",
                        hintText: "Nhập địa chỉ",
                        value: _controller.region["ownerAddress"],
                        onSaved: (value) => _controller.region["ownerAddress"] = value,
                      ),
                      buildFieldSpacing(),
                      TextInput(
                        labelText: "Tên địa điểm canh tác",
                        hintText: "Nhập tên địa điểm",
                        value: _controller.region["name"],
                        onSaved: (value) => _controller.region["name"] = value,
                      ),
                      buildFieldSpacing(),
                      TextInput(
                        labelText: "Diện tích canh tác",
                        hintText: "Nhập diện tích canh tác",
                        suffix: const Text("ha"),
                        keyboardType: TextInputType.number,
                        value: MoneyInputFormatter.getMaskedValue(_controller.region["area"]),
                        onSaved: (value) =>
                            _controller.region["area"] = int.parse(MoneyInputFormatter.getUnMaskedValue(value)),
                        inputFormatters: [MoneyInputFormatter()],
                      ),
                      buildFieldSpacing(),
                      AddressInput(
                        provinceId: _controller.region["provinceId"],
                        districtId: _controller.region["districtId"],
                        wardId: _controller.region["wardId"],
                        onProvinceSaved: (value) => _controller.region["provinceId"] = value,
                        onDistrictSaved: (value) => _controller.region["districtId"] = value,
                        onWardSaved: (value) => _controller.region["wardId"] = value,
                      ),
                      buildFieldSpacing(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: SelectImageListInput(
                          label: "Hình ảnh điểm canh tác",
                          initialValue: _controller.region["listImages"].cast<String>(),
                          onChanged: (images) {
                            _controller.region["listImages"] = images;
                            _tabRegionController.eventChangeHeaderImage(images.cast<String>());
                          },
                        ),
                      ),
                      buildFieldSpacing(),
                      SelectListInput(
                        sheetTitle: 'Chọn hình thức canh tác',
                        labelText: 'Hình thức canh tác',
                        hintText: 'Chọn hình thức canh tác',
                        sheetItems: FarmingTypeRepo().getAllFarmingType().then((items) {
                          return items.map((e) => SelectListSheetItem(display: e["name"], id: e["id"])).toList();
                        }),
                        selectedId: _controller.region["farmingTypeId"],
                        onSaved: (e) {
                          _controller.region["farmingTypeId"] = e;
                        },
                        onSelect: (e) {},
                      ),
                      buildFieldSpacing(),
                      SelectListInput(
                        sheetTitle: 'Chọn nguyên liệu canh tác',
                        labelText: 'Nguyên liệu canh tác',
                        hintText: 'Chọn nguyên liệu canh tác',
                        disabled: true,
                        sheetItems: MaterialRepo().getAllMaterial().then((items) {
                          return items.map((e) => SelectListSheetItem(display: e["name"], id: e["id"])).toList();
                        }),
                        selectedId: _controller.region["materialId"],
                        onSaved: (e) {},
                        onSelect: (e) {},
                      ),
                      buildFieldSpacing(),
                      TextareaInput(
                        labelText: "Ghi chú",
                        hintText: "Nhập ghi chú",
                        value: _controller.region["note"],
                        onSaved: (value) => _controller.region["note"] = value,
                      ),
                      SizedBox(height: SizeConfig.setHeight(18)),
                      RemoveButton(
                        label: "Xoá địa điểm",
                        onRemove: () {
                          _controller.deleteRegion();
                        },
                      ),
                      buildFieldSpacing(),
                      AppBtn(
                        'Lưu',
                        onPressed: updating
                            ? null
                            : () {
                                setState(() {
                                  updating = true;
                                });
                                try {
                                  if (!overviewForm.currentState.validate()) {
                                    showToast("Dữ liệu không hợp lệ");
                                  }
                                  overviewForm.currentState.save();
                                  _controller.updateRegionV2();
                                } finally {
                                  setState(() {
                                    updating = false;
                                  });
                                }
                              },
                      ),
                      buildFieldSpacing(),
                    ],
                  ),
                ),
              )
            : const Center(child: Text("Chưa có dữ liệu"));
      },
    );
  }

  SizedBox buildFieldSpacing() => SizedBox(height: SizeConfig.setHeight(12));
}
