import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/modules/diary/bottomDiaryPopupSheet.dart';
import 'package:mi_smart/src/modules/tabs_region_detail/tabs/controllers/diary_controller.dart';
import 'package:mi_smart/src/modules/tabs_region_detail/tabs/controllers/overview_controller.dart';
import 'package:mi_smart/src/share/components/diary_card.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:provider/provider.dart';

class Diary extends StatefulWidget {
  @override
  _DiaryState createState() => _DiaryState();
}

class _DiaryState extends State<Diary> {
  DiaryController _controller;
  final _scrollController = ScrollController();

  @override
  void didChangeDependencies() {
    if (_controller == null) {
      _controller = Provider.of<DiaryController>(context);
      _controller.getListDiary();
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    OverviewController overviewCtrl = Provider.of<OverviewController>(context);
    return NotificationListener<ScrollNotification>(
      onNotification: (ScrollNotification scrollInfo) {
        if (scrollInfo is ScrollEndNotification && scrollInfo.metrics.extentAfter == 0) {
          _controller.loadMore();
        }
        return true;
      },
      child: SingleChildScrollView(
        controller: _scrollController,
        physics: const ClampingScrollPhysics(),
        child: Column(
          children: [
            AppBtn(
              'Thêm nhật ký',
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    useRootNavigator: true,
                    isScrollControlled: true,
                    builder: (context) => BottomDiaryPopupSheet(
                          regionId: _controller.idRegion,
                          materialId: overviewCtrl.region['materialId'],
                        )).then((value) => _controller.getListDiary());
              },
            ),
            const SpacingBox(
              height: 4,
            ),
            ValueListenableBuilder<List>(
                valueListenable: _controller.listDiaryNotifer,
                builder: (_, items, loadMoreBtn) {
                  return Column(
                    children: [
                      ListView(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          children: List.generate(items.length, (index) => buildDiaryCard(items[index], index)))
                    ],
                  );
                }),
          ],
        ),
      ),
    );
  }

  DiaryCard buildDiaryCard(e, int index) {
    return DiaryCard(
      key: ObjectKey(e["id"]),
      name: e["name"],
      createrName: e["createrName"] ?? "Chuyên viên",
      startDate: DateTime.tryParse(e["startAt"] ?? '')?.toLocal(),
      endDate: DateTime.tryParse(e["endAt"] ?? '')?.toLocal(),
      production: (e["production"]?.toDouble()) ?? 0.0,
      data: e,
      unit: e["unit"],
      status: e["status"],
      press: () {
        navigatorKey.currentState
            .pushNamed(View.detailDiaryPage, arguments: e)
            .then((value) => _controller.reloadDiary(index));
      },
    );
  }
}
