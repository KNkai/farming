import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../../base_config/base_config.dart';
import '../../../../../base_widget/src/toast.dart';
import '../../../../../navigator.dart';
import '../../../../model/certificate.model.dart';
import '../../../../model/staff.model.dart';
import '../../../../share/repo/region_repo.dart';
import '../../../../share/repo/staff_repo.dart';
import '../../../../share/utils/color.dart';
import '../../../../share/widget/address_vn.dart';
import '../../../../share/widget/map_with_polygon.dart';
import '../../../profile/model/get_district/get_district.dart';
import '../../../profile/model/get_province/get_province.dart';
import '../../../profile/model/get_ward/get_ward.dart';

class OverviewController extends ChangeNotifier {
  final String idRegion;
  OverviewController(this.idRegion);
  // data
  dynamic region;
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController nameRegion = TextEditingController();
  TextEditingController area = TextEditingController();
  String farmingTypeId;
  String materialId;
  TextEditingController note = TextEditingController();
  final ctrlAddress = ValueNotifier<AddressVnValues>(null);
  List images = [];
  PolygonData polygon;
  ValueNotifier<List<Certificate>> listCertificateNotifier = new ValueNotifier(null);
  ValueNotifier<List<Staff>> listStaffNotifier = new ValueNotifier(null);

  //method
  Future getRegionInfo() async {
    region = await RegionRepo().getRegionInfo(idRegion);
    name.text = region["ownerName"];
    phone.text = region["ownerPhone"];
    address.text = region["ownerAddress"];
    nameRegion.text = region["name"];
    farmingTypeId = region["farmingTypeId"];
    materialId = region["materialId"];
    note.text = region["note"];
    area.text = region["area"].toString();
    Province _province = Province(id: region["provinceId"], name: region["province"]);
    District _district = District(id: region["districtId"], name: region["district"]);
    Ward _ward = Ward(id: region["wardId"], name: region["ward"]);
    ctrlAddress.value = AddressVnValues('', _province, _district, _ward);
    polygon = region["polygon"] != null
        ? PolygonData.fromJson(region["polygon"])
        : PolygonData(
            fillColor: AppColor.primary,
            fillOpacity: 0.5,
            strokeColor: AppColor.primary,
            strokeOpacity: 1,
            strokeWeight: 1,
            paths: []);
    images = region["listImages"];
    region["certificates"] = region["certificates"] ?? [];
    listCertificateNotifier.value =
        List.generate(region["certificates"].length, (index) => Certificate.fromJson(region["certificates"][index]));
    region["assignees"] = region["assignees"] ?? [];
    listStaffNotifier.value =
        (region["assignees"] as List).where((a) => a != null).map((a) => Staff.fromJson(a)).toList();
    print('listStaffNotifier ${listStaffNotifier.value}');
    notifyListeners();
    return region;
  }

  Future<bool> addCertificate({Certificate certificate}) async {
    try {
      await RegionRepo().addCertificateToRegion(
        idRegion: idRegion,
        title: certificate.title,
        description: certificate.description,
        listImgs: certificate.listImages,
        effectiveDate: certificate.effectiveDate.toString(),
        expiredDate: certificate.expiredDate.toString(),
      );
      listCertificateNotifier.value.add(certificate);
      listCertificateNotifier.notifyListeners();
      return true;
    } catch (e) {
      showToast('Có lỗi xảy ra.');
      return false;
    }
  }

  Future<bool> updateCertificate({Certificate certificate}) async {
    try {
      var index = listCertificateNotifier.value.indexWhere((element) => element.id == certificate.id);
      var list = [...listCertificateNotifier.value];
      if (index == -1) return false;
      list[index] = certificate;
      await RegionRepo().updateCertificateToRegion(id: idRegion, certificates: list.map((c) => c.toJson()).toList());
      listCertificateNotifier.value = list;
      listCertificateNotifier.notifyListeners();
      return true;
    } catch (e) {
      showToast(e.toString(), flagColor: false);
      return false;
    }
  }

  Future removeCertificate(String id) async {
    var list = [...listCertificateNotifier.value];
    list.removeWhere((element) => element.id == id);
    await RegionRepo().updateCertificateToRegion(id: idRegion, certificates: list.map((c) => c.toJson()).toList());
    listCertificateNotifier.value = list;
    listCertificateNotifier.notifyListeners();
  }

  Future<bool> updateRegion() async {
    try {
      await RegionRepo().updateRegion(
        id: idRegion,
        name: nameRegion.text,
        provinceId: ctrlAddress.value.province.id,
        districtId: ctrlAddress.value.district.id,
        area: double.parse(area.text),
        note: note.text,
        wardId: ctrlAddress.value.ward.id,
        ownerName: name.text,
        ownerAddress: address.text,
        ownerPhone: phone.text,
        images: images.cast<String>(),
        farmingTypeId: farmingTypeId,
      );
      showToast('Cập nhật thành công.');
      return true;
    } catch (e) {
      showToast('Có lỗi xảy ra.');
      return false;
    }
  }

  Future<bool> updateRegionV2() async {
    try {
      await RegionRepo().updateRegion(
        id: idRegion,
        name: region["name"],
        provinceId: region["provinceId"],
        districtId: region["districtId"],
        wardId: region["wardId"],
        area: region["area"].toDouble(),
        note: region["note"],
        ownerName: region["ownerName"],
        ownerAddress: region["ownerAddress"],
        ownerPhone: region["ownerPhone"],
        images: region["listImages"].cast<String>(),
        farmingTypeId: region["farmingTypeId"],
      );
      showToast('Cập nhật thành công.');
      return true;
    } catch (e) {
      showToast('Có lỗi xảy ra: $e', flagColor: false);
      return false;
    }
  }

  Future<bool> updatePolygon(List<LatLng> newPolygonPath, MapWithPolygonDelegate delegate) async {
    try {
      await RegionRepo().updateRegion(
          id: idRegion,
          name: nameRegion.text,
          provinceId: ctrlAddress.value.province.id,
          districtId: ctrlAddress.value.district.id,
          area: double.parse(area.text),
          note: note.text,
          wardId: ctrlAddress.value.ward.id,
          ownerName: name.text,
          ownerAddress: address.text,
          ownerPhone: phone.text,
          polygon: polygon.copyWithNewPolygon(newPolygonPath),
          images: images.cast<String>(),
          farmingTypeId: farmingTypeId);
      showToast('Cập nhật thành công.');
      polygon.paths = newPolygonPath;
      delegate?.updatePolygonPath(newPolygonPath);
      notifyListeners();
      return true;
    } catch (e) {
      showToast('Có lỗi xảy ra.');
      return false;
    }
  }

  Future<bool> deleteRegion() async {
    try {
      await RegionRepo().deleteRegion(id: idRegion);
      showToast('Xoá thành công.');
      navigatorKey.currentState.maybePop();
      return true;
    } catch (e) {
      showToast('Có lỗi xảy ra.');
      return false;
    }
  }

  Future deleteStaff(String id) async {
    await StaffRepo().deleteStaff(id);
    listStaffNotifier.value.removeWhere((element) => element.id == id);
    listStaffNotifier.notifyListeners();
  }

  Future<dynamic> assignStaffToRegion(String phone, String name) async {
    await RegionRepo().assignStaffToRegion(regionId: region["id"], phone: phone, name: name);
    await getRegionInfo();
    listStaffNotifier.notifyListeners();
  }
}
