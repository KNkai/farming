import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/model/pagination.dart';
import 'package:mi_smart/src/share/repo/diary_repo.dart';

class DiaryController extends ChangeNotifier {
  final String idRegion;
  DiaryController(this.idRegion);
  Pagination diaryPaging;
  ValueNotifier<List> listDiaryNotifer = new ValueNotifier([]);
  bool isLoadingListDiary = false;

  //method
  getListDiary() async {
    diaryPaging = Pagination(limit: 5, page: 1);
    listDiaryNotifer.value = await DiaryRepo().getListDiary(
      idRegion,
      paging: diaryPaging,
      onPaging: (paging) => diaryPaging = paging,
    );
  }

  loadMore() async {
    if (diaryPaging.total == null) {
      return await getListDiary();
    } else {
      if (diaryPaging.total > listDiaryNotifer.value.length && !isLoadingListDiary) {
        isLoadingListDiary = true;
        try {
          diaryPaging.page++;
          listDiaryNotifer.value = [
            ...listDiaryNotifer.value,
            ...await DiaryRepo().getListDiary(
              idRegion,
              paging: diaryPaging,
              onPaging: (paging) => diaryPaging = paging,
            )
          ];
        } finally {
          isLoadingListDiary = false;
        }
      }
    }
  }

  Future reloadDiary(int index) async {
    var diary = listDiaryNotifer.value[index];
    var res = await DiaryRepo().getDiary(diary["id"]);
    listDiaryNotifer.value[index] = res;
    listDiaryNotifer.notifyListeners();
  }
}
