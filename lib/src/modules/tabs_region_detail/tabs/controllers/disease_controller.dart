import 'dart:async';

import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/share/model/pagination.dart';
import 'package:mi_smart/src/share/repo/disease_repo.dart';

class DiseaseController extends ChangeNotifier {
  final String idRegion;
  DiseaseController(this.idRegion);
  // data
  ValueNotifier<List> listDiseaseNotifier = ValueNotifier([]);
  Pagination situationPaging;
  List<String> listImg = [];
  TextEditingController title = TextEditingController();
  TextEditingController diseaseId = TextEditingController();
  TextEditingController diseaseName = TextEditingController();
  TextEditingController description = TextEditingController();
  String idCurDisease;
  bool isLoadingSituations = false;

  //method
  getListDisease() async {
    situationPaging = Pagination(limit: 5, page: 1);
    listDiseaseNotifier.value = await DiseaseRepo()
        .getListDisease(idRegion, paging: situationPaging, onPaging: (paging) => situationPaging = paging);
  }

  loadMoreSituations() async {
    if (situationPaging.total == null) {
      return await getListDisease();
    } else {
      if (situationPaging.total > listDiseaseNotifier.value.length && !isLoadingSituations) {
        isLoadingSituations = true;
        try {
          situationPaging.page++;
          listDiseaseNotifier.value = [
            ...listDiseaseNotifier.value,
            ...await DiseaseRepo()
                .getListDisease(idRegion, paging: situationPaging, onPaging: (paging) => situationPaging = paging)
          ];
        } finally {
          isLoadingSituations = false;
        }
      }
    }
  }

  setDefault() async {
    listImg = [];
    title = TextEditingController();
    diseaseId = TextEditingController();
    diseaseName = TextEditingController();
    description = TextEditingController();
    idCurDisease = null;
  }

  Future<String> createDisease() async {
    try {
      await DiseaseRepo().createDiseaseSituation(
          title.text, description.text, listImg, idRegion, DateTime.now().toString(), diseaseId.text);
      setDefault();
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future createDisease2({String diseaseId, String title, String description, List<String> listImages}) async {
    return await DiseaseRepo()
        .createDiseaseSituation(title, description, listImages, idRegion, DateTime.now().toString(), diseaseId);
  }

  Future<String> editDisease() async {
    try {
      await DiseaseRepo().editDiseaseSituation(idCurDisease, title.text, description.text, listImg, diseaseId.text);
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future editDisease2({String diseaseId, String title, String description, List<String> listImages}) async {
    return await DiseaseRepo().editDiseaseSituation(idCurDisease, title, description, listImages, diseaseId);
  }
}
