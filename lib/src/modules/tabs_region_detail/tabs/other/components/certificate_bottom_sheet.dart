import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../../../base_config/base_config.dart';
import '../../../../../../base_config/src/utils/validate_builder.dart';
import '../../../../../../base_widget/base_widget.dart';
import '../../../../../../base_widget/src/spacing_box.dart';
import '../../../../../../navigator.dart';
import '../../../../../model/certificate.model.dart';
import '../../../../../share/components/bottom_sheet_wrapper.dart';
import '../../../../../share/components/datepicker_input.dart';
import '../../../../../share/components/remove_button.dart';
import '../../../../../share/components/select_image_list_input.dart';
import '../../../../../share/components/text_input.dart';
import '../../controllers/overview_controller.dart';

class CertificateBottomSheet extends StatefulWidget {
  final Certificate data;
  final bool isCreate;
  const CertificateBottomSheet({Key key, this.data, this.isCreate = true}) : super(key: key);

  @override
  _CertificateBottomSheetState createState() => _CertificateBottomSheetState();
}

class _CertificateBottomSheetState extends State<CertificateBottomSheet> {
  GlobalKey<FormState> certificateForm = new GlobalKey(debugLabel: "certificateForm");
  Certificate certificateData;
  OverviewController controller;

  @override
  void initState() {
    super.initState();
    certificateData = widget.data ?? new Certificate();
  }

  @override
  Widget build(BuildContext context) {
    controller = Provider.of<OverviewController>(context);
    return BottomSheetWrapper(
      appBar: buildSheetBar(context, title: "Chứng nhận", btnTitle: "Cập nhật", onPress: updateCertificate),
      padding: const EdgeInsets.all(16),
      formKey: certificateForm,
      height: deviceHeight(context) * 0.6,
      children: [
        TextInput(
          value: certificateData.title,
          labelText: "Tên chứng nhận",
          hintText: "Nhập tên",
          onSaved: (value) => certificateData.title = value,
          validator: ValidateBuilder().empty().build(),
        ),
        const SpacingBox(
          height: 2,
        ),
        TextInput(
          value: certificateData.description,
          labelText: "Mô tả",
          hintText: "Nhập mô tả",
          onSaved: (value) => certificateData.description = value,
          validator: ValidateBuilder().empty().build(),
        ),
        const SpacingBox(
          height: 2,
        ),
        SelectImageListInput(
          label: "Hình ảnh đính kèm",
          initialValue: certificateData.listImages ?? [],
          onSaved: (value) => certificateData.listImages = value,
        ),
        const SpacingBox(
          height: 2,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: DatepickerInput(
                labelText: "Hiệu lực từ",
                hintText: "Chọn ngày",
                value: certificateData.effectiveDate,
                onSaved: (date) => certificateData.effectiveDate = date,
                onSelected: (date) => certificateData.effectiveDate = date,
                validator: ValidateBuilder().empty().validate((value) {
                  return certificateData.expiredDate != null &&
                          DateTime.parse(value).isAfter(certificateData.expiredDate)
                      ? "Dữ liệu không hợp lệ"
                      : null;
                }).build(),
              ),
            ),
            const SpacingBox(width: 4),
            Expanded(
              child: DatepickerInput(
                labelText: "Hiệu lực đến",
                hintText: "Chọn ngày",
                value: certificateData.expiredDate,
                onSaved: (date) => certificateData.expiredDate = date,
                onSelected: (date) => certificateData.expiredDate = date,
                validator: ValidateBuilder().empty().validate((value) {
                  return certificateData.effectiveDate != null &&
                          DateTime.parse(value).isBefore(certificateData.effectiveDate)
                      ? "Dữ liệu không hợp lệ"
                      : null;
                }).build(),
              ),
            ),
          ],
        ),
        if (!widget.isCreate) ...[
          const SpacingBox(height: 2),
          RemoveButton(
            label: "Xoá chứng nhận",
            onRemove: () {
              controller
                  .removeCertificate(certificateData.id)
                  .then((value) => navigatorKey.currentState.pop())
                  .catchError((error) {
                showToast(error.toString(), flagColor: false);
              });
            },
          ),
        ],
        const SpacingBox(height: 1),
      ],
    );
  }

  void updateCertificate() {
    if (certificateForm.currentState.validate()) {
      certificateForm.currentState.save();
      if (widget.isCreate) {
        controller.addCertificate(certificate: certificateData);
      } else {
        controller.updateCertificate(certificate: certificateData);
      }
      Navigator.of(context).pop(certificateData);
    }
  }
}
