import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/base_widget.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/modules/tabs_region_detail/tabs/controllers/overview_controller.dart';
import 'package:provider/provider.dart';

import '../../../../../../base_config/base_config.dart';
import '../../../../../../base_config/src/utils/validate_builder.dart';
import '../../../../../../base_widget/src/spacing_box.dart';
import '../../../../../share/components/text_input.dart';

class AssignStaffData {
  String phone, name;

  AssignStaffData({
    @required this.phone,
    @required this.name,
  });
}

class AddStaffBottomSheet extends StatefulWidget {
  const AddStaffBottomSheet({Key key}) : super(key: key);

  @override
  _AddStaffBottomSheetState createState() => _AddStaffBottomSheetState();
}

class _AddStaffBottomSheetState extends State<AddStaffBottomSheet> {
  GlobalKey<FormState> addStaffForm = new GlobalKey(debugLabel: "addStaffForm");
  AssignStaffData staffData;
  OverviewController controller;
  @override
  void initState() {
    super.initState();
    this.staffData = AssignStaffData(phone: null, name: null);
  }

  @override
  Widget build(BuildContext context) {
    controller = Provider.of<OverviewController>(context);
    return SizedBox(
      height: MediaQuery.of(context).size.height / 2 + MediaQuery.of(context).viewInsets.bottom,
      child: Scaffold(
        appBar: buildSheetBar(context, title: "Thêm chuyên viên", btnTitle: "Tạo", onPress: addStaff),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: addStaffForm,
            child: Column(
              children: [
                TextInput(
                  labelText: "Số điện thoại liên hệ",
                  hintText: "Nhập số điện thoại",
                  keyboardType: TextInputType.phone,
                  onSaved: (value) => staffData.phone = value,
                  validator: ValidateBuilder().empty().phone().build(),
                ),
                const SpacingBox(
                  height: 2,
                ),
                TextInput(
                  labelText: "Tên chuyên viên",
                  hintText: "Nhập họ và tên",
                  onSaved: (value) => staffData.name = value,
                  validator: ValidateBuilder().empty().build(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void addStaff() {
    if (addStaffForm.currentState.validate()) {
      addStaffForm.currentState.save();
      controller
          .assignStaffToRegion(staffData.phone, staffData.name)
          .then((value) => navigatorKey.currentState.pop())
          .catchError((err) {
        showToast(err.toString(), flagColor: false);
      });
    }
  }
}
