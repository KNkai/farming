import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../../../base_config/base_config.dart';
import '../../../../../../base_widget/base_widget.dart';
import '../../../../../../base_widget/src/dialog.dart';
import '../../../../../../base_widget/src/spacing_box.dart';
import '../../../../../../navigator.dart';
import '../../../../../model/staff.model.dart';
import '../../../../../share/widget/app_btn.dart';
import '../../../../../share/widget/custom_listtile.dart';
import '../../controllers/overview_controller.dart';
import 'add_staff_bottom_sheet.dart';

class ListStaff extends StatelessWidget {
  const ListStaff({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OverviewController controller = Provider.of<OverviewController>(context, listen: false);
    return Container(
      padding: const EdgeInsets.all(13),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        children: [
          const ListHeader(text: "Chuyên viên quản lý"),
          const SpacingBox(height: 1),
          const Divider(),
          ValueListenableBuilder<List<Staff>>(
              valueListenable: controller.listStaffNotifier,
              builder: (_, items, __) {
                if (items == null) return kLoadingSpinner;
                if (items.length == 0) return const EmptyDataWidget("Chưa có chuyên viên");
                return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: items.length,
                  itemBuilder: (context, index) => StaffItem(
                    staff: items[index],
                    onPress: () {
                      showConfirmDialog(context, 'Xác nhận xóa', navigatorKey: navigatorKey, confirmTap: () {
                        controller.deleteStaff(items[index].id).catchError((error) {
                          showToast(error.toString(), flagColor: false);
                        });
                      });
                    },
                  ),
                );
              }),
          const SpacingBox(height: 2),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: AppBtn(
              'Thêm chuyên viên',
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (_) => ListenableProvider.value(
                          value: controller,
                          child: const AddStaffBottomSheet(),
                        ));
              },
            ),
          ),
        ],
      ),
    );
  }
}

class StaffItem extends StatelessWidget {
  const StaffItem({
    Key key,
    @required this.staff,
    this.onPress,
  }) : super(key: key);
  final Staff staff;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return CustomListTile(
      dense: true,
      leading: staff.avatar != null
          ? CircleAvatar(backgroundImage: NetworkImage(staff.avatar))
          : const CircleAvatar(backgroundImage: AssetImage('assets/icon.png')),
      title: Text(staff.name ?? "Chuyên viên"),
      subtitle: Text(staff.phone),
      trailing: InkWell(
        onTap: onPress,
        child: const Icon(Icons.close),
      ),
    );
  }
}

class ListHeader extends StatelessWidget {
  const ListHeader({
    Key key,
    @required this.text,
  }) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          text,
          style: ptHeadline(context).copyWith(
              fontSize: 16, fontWeight: FontWeight.bold, letterSpacing: 0.2, color: Colors.black87.withOpacity(0.7)),
        ),
      ],
    );
  }
}
