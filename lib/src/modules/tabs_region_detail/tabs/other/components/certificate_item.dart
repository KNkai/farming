import 'package:flutter/material.dart';

import '../../../../../../base_config/src/utils/formart.dart';
import '../../../../../model/certificate.model.dart';
import '../../../../../share/widget/custom_listtile.dart';

class CertificateItem extends StatelessWidget {
  const CertificateItem({
    Key key,
    @required this.certificate,
    this.onPress,
  }) : super(key: key);
  final Certificate certificate;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return CustomListTile(
      dense: true,
      leading: (certificate.listImages ?? []).length > 0
          ? CircleAvatar(
              backgroundImage: NetworkImage(certificate.listImages[0]),
            )
          : const CircleAvatar(
              backgroundImage: AssetImage('assets/icon.png'),
            ),
      title: Text(certificate.title),
      subtitle: Text('Thời hạn đến ${Format.ddsmmsyyyy(certificate.expiredDate)}'),
      trailing: InkWell(
        onTap: onPress,
        child: const Icon(
          Icons.keyboard_arrow_right,
          size: 30,
        ),
      ),
    );
  }
}
