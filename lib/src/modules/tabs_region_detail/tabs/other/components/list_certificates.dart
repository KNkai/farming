import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../../../base_config/base_config.dart';
import '../../../../../../base_widget/base_widget.dart';
import '../../../../../../base_widget/src/spacing_box.dart';
import '../../../../../model/certificate.model.dart';
import '../../../../../share/widget/app_btn.dart';
import '../../controllers/overview_controller.dart';
import 'certificate_bottom_sheet.dart';
import 'certificate_item.dart';

class ListCertificates extends StatelessWidget {
  const ListCertificates({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var controller = Provider.of<OverviewController>(context, listen: false);
    return Container(
      padding: const EdgeInsets.all(13),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Giấy chứng nhận",
                style: ptHeadline(context).copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.2,
                    color: Colors.black87.withOpacity(0.7)),
              ),
              //Icon(Icons.info_outline, color: Colors.blue, size: 22),
            ],
          ),
          const SpacingBox(height: 1),
          const Divider(),
          ValueListenableBuilder<List<Certificate>>(
              valueListenable: controller.listCertificateNotifier,
              builder: (_, items, __) {
                if (items == null) return kLoadingSpinner;
                if (items.length == 0) return const EmptyDataWidget("Chưa có chứng nhận");
                return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: items.length,
                  itemBuilder: (context, index) => CertificateItem(
                    key: ObjectKey(items[index].id),
                    certificate: items[index],
                    onPress: () {
                      showModalBottomSheet(
                        context: context,
                        builder: (_) {
                          return ListenableProvider.value(
                            value: controller,
                            child: CertificateBottomSheet(
                              data: items[index],
                              isCreate: false,
                            ),
                          );
                        },
                      );
                    },
                  ),
                );
              }),
          const SpacingBox(height: 2),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: AppBtn(
              'Thêm chứng nhận',
              onPressed: () {
                showModalBottomSheet(
                  context: context,
                  isScrollControlled: true,
                  builder: (_) => ListenableProvider.value(value: controller, child: const CertificateBottomSheet()),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
