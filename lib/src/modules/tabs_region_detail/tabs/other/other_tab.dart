import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../../base_config/base_config.dart';
import '../../../../../base_widget/src/spacing_box.dart';
import '../../../../share/widget/app_btn.dart';
import '../../../../share/widget/map_with_polygon.dart';
import '../../../select_area/select_area_page.dart';
import '../controllers/overview_controller.dart';
import 'components/list_certificates.dart';
import 'components/list_staff.dart';

class OtherTab extends StatefulWidget {
  @override
  _OtherTabState createState() => _OtherTabState();
}

class _OtherTabState extends State<OtherTab> {
  OverviewController _controller;
  MapWithPolygonDelegate _mapDelegate;

  @override
  void didChangeDependencies() {
    if (_controller == null) {
      _controller = Provider.of<OverviewController>(context);
      _controller.getRegionInfo();
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const ClampingScrollPhysics(),
      child: Column(
        children: [
          const ListCertificates(),
          const SpacingBox(height: 3),
          const ListStaff(),
          const SizedBox(
            height: 20,
          ),
          if (_controller.polygon != null)
            Container(
              padding: const EdgeInsets.all(13),
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Khu vực canh tác',
                        style: ptHeadline(context).copyWith(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 0.2,
                            color: Colors.black87.withOpacity(0.7)),
                      ),
                      // Icon(Icons.info_outline, color: Colors.blue, size: 22),
                    ],
                  ),
                  const SpacingBox(height: 2),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: AppBtn(
                      'Cập nhật',
                      onPressed: () {
                        SelectAreaPage.go(
                          context,
                          strokeWidth: _controller.polygon.strokeWeight,
                          strokeColor: _controller.polygon.strokeColor,
                          fillColor: _controller.polygon.fillColor,
                          strokeOpacity: _controller.polygon.strokeOpacity,
                          fillOpacity: _controller.polygon.fillOpacity,
                          initialPolygon: _controller.polygon.paths,
                          onPolygon: (newPolygon) {
                            _controller.updatePolygon(newPolygon, _mapDelegate);
                          },
                        );
                      },
                    ),
                  ),
                  const SpacingBox(height: 1),
                  const Divider(),
                  const SpacingBox(height: 2),
                  if (_controller.polygon != null && _controller.polygon.paths.isNotEmpty)
                    FutureBuilder(
                        future: Future.delayed(const Duration(milliseconds: 1000)),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState == ConnectionState.waiting)
                            return Center(
                              child: kLoadingSpinner,
                            );
                          return SizedBox(
                            width: double.infinity,
                            height: 300,
                            child: MapWithPolygon(
                              polygon: _controller.polygon,
                              onDelegate: (mapDelegate) {
                                _mapDelegate = mapDelegate;
                              },
                            ),
                          );
                        }),
                ],
              ),
            ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
