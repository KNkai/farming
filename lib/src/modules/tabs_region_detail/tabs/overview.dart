import 'package:flutter/material.dart';
import 'package:mi_smart/base_widget/src/dialog.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/modules/tabs_region_detail/tabs/controllers/overview_controller.dart';
import 'package:mi_smart/src/share/repo/farmingType_repo.dart';
import 'package:mi_smart/src/share/repo/material_repo.dart';
import 'package:mi_smart/src/share/widget/address_vn.dart';
import 'package:mi_smart/src/share/widget/app_btn.dart';
import 'package:mi_smart/src/share/widget/app_form.dart';
import 'package:mi_smart/src/share/widget/drop_down.dart';
import 'package:mi_smart/src/share/widget/list_img_picker.dart';
import 'package:mi_smart/src/share/widget/text_form.dart';
import 'package:provider/provider.dart';

class OverView extends StatefulWidget {
  @override
  _OverViewState createState() => _OverViewState();
}

class _OverViewState extends State<OverView> {
  OverviewController _controller;

  @override
  void didChangeDependencies() {
    if (_controller == null) {
      _controller = Provider.of<OverviewController>(context);
      _controller.getRegionInfo();
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const ClampingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextForm(
            showCursor: false,
            hint: 'Người sỡ hữu',
            keyboardType: TextInputType.text,
            shouldValidate: true,
            controller: _controller.name,
          ),
          TextForm(
            showCursor: false,
            hint: 'Số điện thoại người sở hữu',
            keyboardType: TextInputType.number,
            shouldValidate: true,
            controller: _controller.phone,
          ),
          TextForm(
            showCursor: false,
            hint: 'Địa chỉ người sỡ hữu',
            keyboardType: TextInputType.text,
            shouldValidate: true,
            controller: _controller.address,
          ),
          TextForm(
            showCursor: false,
            hint: 'Tên Địa Điểm Canh Tác',
            keyboardType: TextInputType.text,
            shouldValidate: true,
            controller: _controller.nameRegion,
          ),
          TextForm(
            showCursor: false,
            hint: 'Diện tích canh tác',
            keyboardType: TextInputType.number,
            shouldValidate: true,
            controller: _controller.area,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 8),
            padding: const EdgeInsets.symmetric(horizontal: 15),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: AddressVn(
              controller: _controller.ctrlAddress,
              hasAddressText: false,
              onData: (detail, p, d, w) {
                setState(() {
                  _controller.ctrlAddress.value = AddressVnValues(detail, p, d, w);
                });
              },
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text('Hình ảnh điểm canh tác'),
          ),
          const SpacingBox(height: 1),
          SizedBox(
            height: 70,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: ImageRowPicker(_controller.images.cast<String>(), onUpdateListImg: (listImg) {
                setState(() {
                  _controller.images = listImg;
                });
              }),
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: FutureBuilder<List>(
                initialData: const [],
                future: MaterialRepo().getAllMaterial(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData || snapshot.data.length == 0) {
                    return const LinearProgressIndicator();
                  }
                  return AppForm(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: CustomListDropDown(
                      hint: 'Nguyên liệu',
                      values: snapshot.data,
                      initialValue: _controller.materialId,
                      onSelect: (value) {
                        _controller.materialId = value;
                      },
                      fieldDisplay: "name",
                      fieldValue: "id",
                      enable: false,
                    ),
                  );
                }),
          ),
          const SizedBox(
            height: 25,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: FutureBuilder<List>(
                initialData: const [],
                future: FarmingTypeRepo().getAllFarmingType(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData || snapshot.data.length == 0) {
                    return const LinearProgressIndicator();
                  }
                  return AppForm(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: CustomListDropDown(
                      initialValue: _controller.farmingTypeId,
                      hint: 'Hình thức canh tác',
                      values: snapshot.data,
                      onSelect: (value) {
                        setState(() {
                          _controller.farmingTypeId = value;
                        });
                      },
                      fieldDisplay: "name",
                      fieldValue: "id",
                    ),
                  );
                }),
          ),
          const SizedBox(
            height: 10,
          ),
          TextForm(
            showCursor: false,
            hint: 'Ghi chú',
            keyboardType: TextInputType.text,
            shouldValidate: true,
            controller: _controller.note,
          ),
          Center(
            child: GestureDetector(
              onTap: () {
                showConfirmDialog(context, 'Xác nhận xóa?', navigatorKey: navigatorKey, confirmTap: () {
                  _controller.deleteRegion().then((value) {
                    if (value)
                      Future.delayed(const Duration(milliseconds: 300), () => navigatorKey.currentState.maybePop());
                  });
                });
              },
              child: const Text(
                'Xoá địa điểm',
                style: TextStyle(color: Colors.orange, fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
              padding: EdgeInsets.only(
                  left: 12,
                  right: 12,
                  top: 20,
                  bottom: MediaQuery.of(context).padding.bottom == 0 ? 20 : MediaQuery.of(context).padding.bottom),
              decoration: const BoxDecoration(color: Colors.transparent),
              child: AppBtn(
                'Lưu',
                onPressed: () => _controller.updateRegion(),
              )),
        ],
      ),
    );
  }
}
