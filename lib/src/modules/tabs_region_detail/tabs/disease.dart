import 'package:flutter/material.dart';
import 'package:mi_smart/navigator.dart';
import 'package:mi_smart/src/model/disease_situation.model.dart';
import 'package:mi_smart/src/modules/tabs_region_detail/components/situation_sheet.dart';
import 'package:provider/provider.dart';

import '../../../../base_config/base_config.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../share/components/situation_card.dart';
import '../../../share/widget/app_btn.dart';
import 'controllers/disease_controller.dart';

class Disease extends StatefulWidget {
  @override
  _DiseaseState createState() => _DiseaseState();
}

class _DiseaseState extends State<Disease> {
  DiseaseController _controller;

  @override
  void didChangeDependencies() {
    if (_controller == null) {
      _controller = Provider.of<DiseaseController>(context);
      _controller.getListDisease();
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      onNotification: (ScrollNotification scrollInfo) {
        if (scrollInfo is ScrollEndNotification && scrollInfo.metrics.extentAfter == 0) {
          _controller.loadMoreSituations();
        }
        return true;
      },
      child: SingleChildScrollView(
        physics: const ClampingScrollPhysics(),
        child: Column(
          children: [
            AppBtn(
              'Đăng tình hình dịch bệnh',
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    useRootNavigator: true,
                    isScrollControlled: true,
                    builder: (context) => SituationSheet(
                          situation: DiseaseSituation(regionId: _controller.idRegion),
                          isCreate: true,
                          onCreated: (situation) {
                            _controller.listDiseaseNotifier.value.insert(0, situation.toJson());
                          },
                        )).then((value) {
                  _controller.getListDisease();
                });
              },
            ),
            const SpacingBox(
              height: 4,
            ),
            ValueListenableBuilder<List>(
                valueListenable: _controller.listDiseaseNotifier,
                builder: (_, items, loadMoreBtn) {
                  return Column(
                    children: [
                      ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: items.length,
                        itemBuilder: (context, index) => buildSituationCard(items[index], index),
                        separatorBuilder: (context, index) => const SpacingBox(height: 2),
                      ),
                      const SpacingBox(height: 4)
                    ],
                  );
                }),
          ],
        ),
      ),
    );
  }

  SituationCard buildSituationCard(item, int index) {
    return SituationCard(
        key: ObjectKey(item["id"]),
        situation: DiseaseSituation.fromJson(item),
        onTap: () {
          navigatorKey.currentState.pushNamed(View.diseaseDetail, arguments: item["id"]).then((value) {
            if (value != null) {
              _controller.listDiseaseNotifier.value[index] = (value as DiseaseSituation).toJson();
            } else {
              _controller.listDiseaseNotifier.value.removeAt(index);
            }
          });
        });
  }
}
