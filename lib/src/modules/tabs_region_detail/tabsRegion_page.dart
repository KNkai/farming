import 'package:provider/provider.dart';

import '../../../base_config/base_config.dart';
import '../../../navigator.dart';
import '../../share/repo/diaryPharse_repo.dart';
import '../../share/repo/diary_repo.dart';
import '../../share/widget/app_scaffold.dart';
import 'components/image_slider_and_dot.dart';
import 'components/region_detail_header.dart';
import 'tabs/controllers/diary_controller.dart';
import 'tabs/controllers/disease_controller.dart';
import 'tabs/controllers/overview_controller.dart';
import 'tabs/diary.dart';
import 'tabs/disease.dart';
import 'tabs/other/other_tab.dart';
import 'tabs/overview/overview_tab.dart';
import 'tabs_region_controller.dart';

class TabsRegionPage extends StatefulWidget {
  final dynamic region;
  final intialIndex;
  const TabsRegionPage(this.region, {this.intialIndex});

  static void goDisease(BuildContext context, dynamic region) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => TabsRegionPage(
              region,
              intialIndex: 2,
            )));
  }

  @override
  _TabsRegionPageState createState() => _TabsRegionPageState();
}

class _TabsRegionPageState extends State<TabsRegionPage> with SingleTickerProviderStateMixin {
  TabController _tabController;
  bool fixedScroll;
  List<String> tabLabels = ["Tổng quan", "Nhật ký", "Tình hình dịch bệnh", "Thông tin khác"];
  GlobalKey tabKey = new GlobalKey();
  TabsRegionController tabsRegionController;

  @override
  void initState() {
    tabsRegionController = TabsRegionController(region: widget.region);
    _tabController = TabController(length: 4, vsync: this, initialIndex: widget.intialIndex ?? 0);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScafold(
      padding: EdgeInsets.zero,
      title: 'Địa điểm canh tác',
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider<DiaryController>(create: (_) => DiaryController(widget.region["id"])),
          ChangeNotifierProvider<DiseaseController>(create: (_) => DiseaseController(widget.region["id"])),
          ChangeNotifierProvider<OverviewController>(create: (_) => OverviewController(widget.region["id"])),
          Provider<TabsRegionController>(
            create: (context) => tabsRegionController,
            dispose: (context, value) {
              value.dispose();
            },
          )
        ],
        child: NestedScrollView(
          headerSliverBuilder: buildHeader,
          body: buildTabs(),
        ),
      ),
      //titleBottom: _tabController.index == 0 ? 'Lưu' : null,
      //onBottomTap: _tabController.index == 0 ? () {} : null,
    );
  }

  Container buildTabs() {
    return Container(
      color: kBackgroundColor,
      padding: const EdgeInsets.only(left: 16, right: 16, top: 15),
      child: TabBarView(
        controller: _tabController,
        children: [
          const OverviewTab(),
          Diary(),
          Disease(),
          OtherTab(),
        ],
      ),
    );
  }

  List<Widget> buildHeader(_, __) => [
        SliverToBoxAdapter(
          child: Container(
            color: Colors.white,
            child: Consumer<OverviewController>(
              builder: (context, controller, child) {
                var region = controller.region ?? widget.region;
                return Column(
                  children: [
                    ValueListenableBuilder(
                        valueListenable: tabsRegionController.stateImageHeader,
                        builder: (_, listImages, __) {
                          if (listImages != null && listImages.length > 0)
                            return ImageSliderAndDot(imageLinks: listImages.cast<String>());

                          return const SizedBox.shrink();
                        }),
                    const SizedBox(height: 10),
                    RegionDetailHeader(
                      name: region['name'],
                      address: parseRegionAddress(region),
                      diaryPhase: getOpeningDiaryName(region["openingDiary"]),
                      pressPhase: () => navigateToOpeningDiary(region),
                    )
                  ],
                );
              },
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Container(
            color: Colors.white,
            child: TabBar(
              key: tabKey,
              onTap: (value) {
                Scrollable.ensureVisible(tabKey.currentContext, duration: const Duration(milliseconds: 800));
                setState(() {});
              },
              labelColor: Colors.black,
              controller: _tabController,
              isScrollable: true,
              physics: const AlwaysScrollableScrollPhysics(),
              tabs: tabLabels.map((e) => Tab(text: e)).toList(),
            ),
          ),
        ),
      ];

  String parseRegionAddress(region) {
    return [region['ward'], region['district'], region['province']].where((element) => element != null).join(", ");
  }

  navigateToOpeningDiary(region) {
    if (region['openingDiary']['phase'] != null) {
      DiaryPharseSrv().getItem(region['openingDiary']['phase']['id']).then((res) {
        navigatorKey.currentState.pushNamed(View.phase, arguments: res);
      });
    } else if (region['openingDiary']['farmingDiary'] != null) {
      DiarySrv().getItem(region['openingDiary']['farmingDiary']['id']).then((res) {
        navigatorKey.currentState.pushNamed(View.detailDiaryPage, arguments: res);
      });
    }
  }

  String getOpeningDiaryName(dynamic openingDiary) {
    String diaryPhase;
    if (openingDiary != null) {
      if (openingDiary['farmingDiary'] != null) {
        diaryPhase = openingDiary['farmingDiary']['name'];
      }
      if (openingDiary['phase'] != null) {
        diaryPhase = diaryPhase + " - ${openingDiary['phase']['name']}";
      }
    }
    return diaryPhase;
  }
}
