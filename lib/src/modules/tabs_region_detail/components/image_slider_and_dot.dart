import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';
import 'package:mi_smart/base_config/src/utils/image_proxy.dart';
import 'package:mi_smart/base_config/src/utils/size_config.dart';
import 'package:mi_smart/src/share/components/animated_dot.dart';

class ImageSliderAndDot extends StatefulWidget {
  const ImageSliderAndDot({Key key, @required this.imageLinks}) : super(key: key);
  final List<String> imageLinks;
  @override
  _ImageSliderAndDotState createState() => _ImageSliderAndDotState();
}

class _ImageSliderAndDotState extends State<ImageSliderAndDot> {
  int currentSlide = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider(
          options: CarouselOptions(
            height: SizeConfig.setHeight(232),
            viewportFraction: 1,
            onPageChanged: (index, _) {
              setState(() {
                currentSlide = index;
              });
            },
            autoPlay: true,
            autoPlayInterval: const Duration(seconds: 10),
            autoPlayAnimationDuration: const Duration(milliseconds: 1200),
          ),
          items: widget.imageLinks
              .map((e) => ClipRRect(
                    child: Image.network(
                      e,
                      width: deviceWidth(context),
                      fit: BoxFit.cover,
                      loadingBuilder: (context, child, loadingProgress) {
                        if (loadingProgress == null) return child;
                        // return Center(child: kLoadingSpinner);
                        return Image.network(
                          resizeImageLink(e, w: deviceWidth(context) ~/ 2, quality: 20),
                          width: deviceWidth(context),
                          fit: BoxFit.cover,
                          loadingBuilder: kLoadingBuilder,
                        );
                      },
                    ),
                  ))
              .toList(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
            widget.imageLinks.length,
            (index) => AnimatedDot(actived: currentSlide == index),
          ),
        )
      ],
    );
  }
}
