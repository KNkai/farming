import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/constants.dart';
import 'package:mi_smart/src/share/components/icon_text.dart';
import 'package:mi_smart/src/share/utils/color.dart';

class RegionDetailHeader extends StatelessWidget {
  const RegionDetailHeader({
    Key key,
    @required this.name,
    @required this.address,
    this.diaryPhase,
    this.pressPhase,
  }) : super(key: key);

  final String name, address, diaryPhase;
  final GestureTapCallback pressPhase;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 23),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(name, style: kTextTitle),
          const SizedBox(height: 12),
          IconText(icon: Icons.location_on, text: address),
          const SizedBox(height: 12),
          ...diaryPhase != null
              ? [
                  const Text(
                    'Nhật ký:',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(height: 12),
                  GestureDetector(
                    onTap: pressPhase,
                    child: Text(
                      diaryPhase,
                      style: const TextStyle(
                          color: AppColor.primary, decoration: TextDecoration.underline, fontWeight: FontWeight.w600),
                    ),
                  ),
                ]
              : [],
          const SizedBox(height: 12),
        ],
      ),
    );
  }
}
