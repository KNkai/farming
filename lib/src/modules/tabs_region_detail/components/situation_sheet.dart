import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mi_smart/src/model/disease_situation.model.dart';
import 'package:mi_smart/src/modules/region/region_srv.dart';

import '../../../../base_config/src/utils/constants.dart';
import '../../../../base_config/src/utils/validate_builder.dart';
import '../../../../base_widget/src/spacing_box.dart';
import '../../../../base_widget/src/toast.dart';
import '../../../../navigator.dart';
import '../../../share/components/bottom_sheet_wrapper.dart';
import '../../../share/components/select_image_list_input.dart';
import '../../../share/components/select_input.dart';
import '../../../share/components/select_list_sheet.dart';
import '../../../share/components/textarea_input.dart';
import '../../../share/repo/disease_repo.dart';

class SituationSheet extends StatefulWidget {
  final bool isCreate;
  final DiseaseSituation situation;
  final Function(DiseaseSituation) onCreated, onUpdated;
  const SituationSheet({
    this.onCreated,
    this.onUpdated,
    this.situation,
    this.isCreate = true,
  });

  @override
  _SituationSheetState createState() => _SituationSheetState();
}

class _SituationSheetState extends State<SituationSheet> {
  String regionId, title, description, diseaseId;
  List<String> listImages = [];
  GlobalKey<FormState> diseaseSituationForm = new GlobalKey(debugLabel: "diseaseSituationForm");
  @override
  void initState() {
    super.initState();
    if (widget.situation != null) {
      regionId = widget.situation.regionId;
      diseaseId = widget.situation.diseaseId;
      title = widget.situation.title;
      description = widget.situation.description;
      listImages = widget.situation.listImages ?? [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheetWrapper(
      height: 600,
      appBar: buildSheetBar(context,
          title: "Tình hình dịch bệnh",
          btnTitle: widget.isCreate ? "Thêm mới" : "Lưu",
          onPress: updateDiseaseSituation),
      padding: const EdgeInsets.all(16),
      formKey: diseaseSituationForm,
      children: [
        if (regionId == null) ...[
          SelectListInput(
            labelText: "Địa điểm canh tác",
            hintText: "Chọn địa điểm",
            selectedId: diseaseId,
            sheetItems: buildRegionSheetItems(),
            onSaved: (value) => regionId = value,
            validator: ValidateBuilder().empty().build(),
          ),
          const SpacingBox(height: 1),
        ],
        SelectListInput(
          labelText: "Dịch bệnh",
          hintText: "Chọn dịch bệnh",
          selectedId: diseaseId,
          sheetItems: buildDiseaseSheetItems(),
          onSaved: (value) => diseaseId = value,
          validator: ValidateBuilder().empty().build(),
        ),
        const SpacingBox(height: 1),
        TextareaInput(
          labelText: "Tiêu đề",
          hintText: "Nhập tiêu đề",
          value: title,
          onSaved: (value) => title = value,
          validator: ValidateBuilder().empty().build(),
        ),
        const SpacingBox(height: 1),
        TextareaInput(
          labelText: "Mô tả",
          hintText: "Nhập mô tả",
          value: description,
          onSaved: (value) => description = value,
        ),
        const SpacingBox(height: 2),
        SelectImageListInput(
          label: "Hình ảnh dịch bệnh",
          initialValue: listImages,
          onChanged: (images) => listImages = images,
        ),
        const SpacingBox(height: 1),
      ],
    );
  }

  void updateDiseaseSituation() {
    if (diseaseSituationForm.currentState.validate()) {
      diseaseSituationForm.currentState.save();
      try {
        if (widget.isCreate) {
          DiseaseRepo()
              .createDiseaseSituation(title, description, listImages, regionId, DateTime.now().toString(), diseaseId)
              .then((res) {
            navigatorKey.currentState.pop();
            if (widget.onCreated != null) {
              widget.onCreated(DiseaseSituation.fromJson(res));
            }
          }).catchError(handleError);
        } else {
          DiseaseRepo()
              .editDiseaseSituation(widget.situation.id, title, description, listImages, diseaseId)
              .then((res) {
            navigatorKey.currentState.pop();
            if (widget.onUpdated != null) {
              widget.onUpdated(DiseaseSituation.fromJson(res));
            }
          }).catchError(handleError);
        }
      } catch (err) {
        showToast(err.toString(), flagColor: false);
      }
    }
  }

  handleError(err) {
    showToast("Có lỗi xảy ra: ${err.toString()}", flagColor: false);
  }

  Future<List<SelectListSheetItem>> buildDiseaseSheetItems() {
    return DiseaseSrv().getList(limit: 100, fragment: "name id").then((value) {
      return value["data"].map<SelectListSheetItem>((e) {
        return SelectListSheetItem(display: e['name'], id: e['id'], data: e);
      }).toList();
    });
  }

  Future<List<SelectListSheetItem>> buildRegionSheetItems() {
    return RegionSrv().getList(limit: 100, fragment: "name id").then((value) {
      return value["data"].map<SelectListSheetItem>((e) {
        return SelectListSheetItem(display: e['name'], id: e['id'], data: e);
      }).toList();
    });
  }
}
