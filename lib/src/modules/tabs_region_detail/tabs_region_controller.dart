import 'package:mi_smart/base_config/base_config.dart';

class TabsRegionController {
  final dynamic region;

  ValueNotifier<List<String>> stateImageHeader;

  TabsRegionController({@required this.region}) {
    stateImageHeader = ValueNotifier<List<String>>(region['listImages'].cast<String>());
  }

  void eventChangeHeaderImage(List<String> images) {
    stateImageHeader.value = []..addAll(images);
  }

  void dispose() {}
}
