import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mi_smart/src/modules/find_regions/find_region_marker_generator.dart';
import 'package:mi_smart/src/modules/search_place/search_place_repo.dart';
import 'package:mi_smart/src/modules/select_area/app_map_repo.dart';
import 'package:mi_smart/src/modules/select_area/model/map_model.dart';

class FindRegionsBloc {
  final _allRegionMarkers = List<MarkerData>();
  final _markerGenerator = FindRegionMarkerGenerator();
  final _repo = AppMapRepo();
  final statePolygons = StreamController<Set<Region>>();
  final stateMarkers = StreamController<List<MarkerData>>();
  final stateSide = StreamController<StateSide>();
  final stateShowSelect = StreamController<List<MarkerData>>();
  final stateLoadingRefreshBtn = ValueNotifier<bool>(false);

  bool _isRefresingRegion = false;

  void eventTapMarker(MarkerData markerData) {
    // replace new
    final list = List<MarkerData>();

    list.add(_allRegionMarkers[markerData.index]);
    list.addAll(_allRegionMarkers.where((e) => (e.region.id != markerData.region.id)));

    stateShowSelect.add(list);
    stateMarkers.add(_allRegionMarkers);
  }

  void eventLoadAllAreas({@required bool isDiseaseModule}) {
    stateLoadingRefreshBtn.value = true;
    _repo.getAllRegions(isDiseaseModule: isDiseaseModule).then((value) {
      try {
        if (value != null) {
          if (value is List<Region>) {
            _applyPolygonsAndMarkers(value);
          } else if (value is List<GetAllDiseaseSituationData>) {
            _applyPolygonsAndMarkersForDisease(value);
          }
          Future.delayed(Duration(seconds: 2)).then((value) {
            eventTapMarker(_allRegionMarkers[0]);
            stateSide.add(StateSideMoveToDirection(_allRegionMarkers[0].position));
          });
        }
      } finally {
        stateLoadingRefreshBtn.value = false;
      }
    });
  }

  void eventRefreshRegionData(MarkerData currentMarkerData) {
    if (!_isRefresingRegion) {
      _isRefresingRegion = true;
      _repo.getOneRegion(currentMarkerData.region.id).then((value) {
        if (value != null && value is Region) {
          final list = List<MarkerData>();
          _allRegionMarkers[currentMarkerData.index].region = value;
          list.add(_allRegionMarkers[currentMarkerData.index]);
          list.addAll(_allRegionMarkers.where((e) => (e.region.id != currentMarkerData.region.id)));
          stateShowSelect.add(list);
        }
        _isRefresingRegion = false;
      });
    }
  }

  void eventCloseDetail() {
    // remove
    stateShowSelect.add(null);
  }

  void eventMoveToPlace(AutoCompletePrediction prediction) {
    _repo.placeDetail(prediction.placeId).then((value) {
      if (value is LatLng) {
        stateSide.add(StateSideMoveToDirection(value));
      }
    });
  }

  void _applyPolygonsAndMarkers(List<Region> listRegion) async {
    _allRegionMarkers.clear();
    int regionCount = 0;
    for (int cf = 0; cf < listRegion.length; cf++) {
      final region = listRegion[cf];
      if (region.polygon != null && region.polygon.paths != null && region.polygon.paths.isNotEmpty) {
        final firstPosPolygon = region.polygon.paths[0];
        final bytesMarkerEnable = await _markerGenerator.genTitle(region.name, darkMode: true);
        _allRegionMarkers.add(MarkerData(
            index: regionCount++,
            region: region,
            diseaseRegion: null,
            iconEnable: BitmapDescriptor.fromBytes(bytesMarkerEnable),
            markerId: MarkerId(firstPosPolygon.lat.toString()),
            position: LatLng(firstPosPolygon.lat, firstPosPolygon.lng)));
      }
      stateMarkers.add(_allRegionMarkers);
      statePolygons.add(_allRegionMarkers.map((e) => e.region).toSet());
    }
  }

  void _applyPolygonsAndMarkersForDisease(List<GetAllDiseaseSituationData> listRegionDisease) async {
    _allRegionMarkers.clear();
    int regionCount = 0;

    for (int cf = 0; cf < listRegionDisease.length; cf++) {
      final region = listRegionDisease[cf].region;
      if (region?.polygon?.paths != null && region.polygon.paths.isNotEmpty) {
        final firstPosPolygon = region.polygon.paths[0];
        final bytesMarkerEnable = await _markerGenerator.genTitle(region.name, darkMode: true);
        _allRegionMarkers.add(MarkerData(
            index: regionCount++,
            region: region,
            diseaseRegion: listRegionDisease[cf],
            iconEnable: BitmapDescriptor.fromBytes(bytesMarkerEnable),
            markerId: MarkerId(firstPosPolygon.lat.toString()),
            position: LatLng(firstPosPolygon.lat, firstPosPolygon.lng)));
      }
      stateMarkers.add(_allRegionMarkers);
      statePolygons.add(_allRegionMarkers.map((e) => e.region).toSet());
    }
  }

  void dispose() {
    statePolygons.close();
    stateShowSelect.close();
    stateMarkers.close();
    stateSide.close();
  }
}

abstract class StateSide {}

class StateSideMoveToDirection extends StateSide {
  final LatLng position;
  StateSideMoveToDirection(this.position);
}

class StateSideGoBounds extends StateSide {
  final List<LatLng> positions;
  StateSideGoBounds(this.positions);
}

class PolygonPositions {
  final Set<Region> regions;
  PolygonPositions(this.regions);
}

class MarkerData {
  final index;
  Region region;
  final GetAllDiseaseSituationData diseaseRegion;
  final BitmapDescriptor _iconEnable;
  final MarkerId markerId;
  final LatLng position;

  BitmapDescriptor get icon => _iconEnable;

  MarkerData(
      {@required BitmapDescriptor iconEnable,
      @required this.region,
      @required this.diseaseRegion,
      @required this.markerId,
      @required this.position,
      @required this.index})
      : this._iconEnable = iconEnable;
}
