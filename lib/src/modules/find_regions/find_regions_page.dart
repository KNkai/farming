import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mi_smart/src/modules/tabs_region_detail/tabsRegion_page.dart';

import '../../../navigator.dart';
import '../../share/utils/color.dart';
import '../../share/utils/util.dart';
import '../search_place/search_place_page.dart';
import '../select_area/model/map_model.dart';
import 'find_regions_bloc.dart';

class FindRegionsPage extends StatefulWidget {
  final bool isDiseaseModule;

  const FindRegionsPage({bool isDiseaseModule}) : this.isDiseaseModule = isDiseaseModule ?? false;

  static bool _lockGoToMapPage = false;
  static void go(BuildContext context, {bool isDiseaseModule}) {
    if (!_lockGoToMapPage) {
      _lockGoToMapPage = true;
      onLocationWhenInUsePermissionRequest(onGranted: () {
        _lockGoToMapPage = false;
        Navigator.of(context)
            .push(MaterialPageRoute(
                builder: (_) => FindRegionsPage(
                      isDiseaseModule: isDiseaseModule,
                    )))
            .then((value) {});
      }, onAlreadyDenied: () {
        _lockGoToMapPage = false;
        WarningDialog.show(context, 'Vui lòng mở quyền truy cập địa điểm', 'Đóng');
      }, onAndroidPermanentDenied: () {
        _lockGoToMapPage = false;
        WarningDialog.show(context, 'Vui lòng mở quyền truy cập địa điểm', 'Đóng');
      }, onJustDeny: () {
        _lockGoToMapPage = false;
        WarningDialog.show(context, 'Vui lòng mở quyền truy cập địa điểm', 'Đóng');
      });
    }
  }

  @override
  _FindRegionsPagePageState createState() => _FindRegionsPagePageState();
}

class _FindRegionsPagePageState extends State<FindRegionsPage> {
  GoogleMapController _mapController;
  MapType _type = MapType.none;

  var _bloc = FindRegionsBloc();

  @override
  void initState() {
    _bloc.stateSide.stream.listen((state) {
      if (state is StateSideMoveToDirection)
        _mapController.moveCamera(CameraUpdate.newLatLng(state.position));
      else if (state is StateSideGoBounds)
        Future.delayed(const Duration(seconds: 1)).then((value) {
          // Have to delay to avoid malfunction (freeze)
          _mapController.moveCamera(CameraUpdate.newLatLngBounds(bounds(state.positions), 70.0));
        });
    });
    _bloc.eventLoadAllAreas(isDiseaseModule: widget.isDiseaseModule);
    super.initState();
  }

  @override
  void dispose() {
    _mapController.dispose();
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        StreamBuilder<Set<Region>>(
          stream: _bloc.statePolygons.stream,
          builder: (context, ss) {
            return StreamBuilder<List<MarkerData>>(
              stream: _bloc.stateMarkers.stream,
              builder: (context, ss) {
                return GoogleMap(
                    mapToolbarEnabled: false,
                    markers: ss?.data?.map((e) {
                      return Marker(
                        onTap: () {
                          _bloc.eventTapMarker(e);
                        },
                        consumeTapEvents: true,
                        markerId: e.markerId,
                        position: e.position,
                        icon: e.icon,
                      );
                    })?.toSet(),
                    mapType: _type,
                    polygons: ss?.data?.map((markerData) {
                      return Polygon(
                          consumeTapEvents: true,
                          onTap: () {
                            _bloc.eventTapMarker(markerData);
                          },
                          geodesic: true,
                          strokeWidth: 3,
                          strokeColor: markerData?.region?.polygon?.strokeColor
                                  ?.withOpacity(markerData.region.polygon?.strokeOpacity?.toDouble() ?? 1) ??
                              AppColor.primary,
                          fillColor: markerData.region.polygon?.fillColor
                                  ?.withOpacity(markerData.region.polygon.fillOpacity ?? 0.3) ??
                              AppColor.primary.withOpacity(0.3),
                          polygonId: PolygonId(markerData.region.polygon.paths[0].lat.toString()),
                          points: markerData.region?.polygon?.paths?.map((e) => LatLng(e.lat, e.lng))?.toList());
                    })?.toSet(),
                    myLocationButtonEnabled: false,
                    myLocationEnabled: true,
                    tiltGesturesEnabled: false,
                    onMapCreated: (mapController) {
                      _mapController = mapController;
                      _visibleMap();
                    },
                    initialCameraPosition: const CameraPosition(zoom: 14, target: LatLng(10.774296, 106.696959)));
              },
            ); // Dist 1
          },
        ),
        SafeArea(
          child: Stack(
            children: [
              Positioned(
                top: 6,
                left: 24,
                right: 24,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        _wrapShadow(
                            Container(
                              height: 40,
                              width: 40,
                              child: const Icon(Icons.close),
                              decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                              ),
                            ), onTap: () {
                          Navigator.of(context).pop();
                        }),
                        const SizedBox(
                          height: 10,
                        ),
                        _wrapShadow(
                            Container(
                              height: 40,
                              width: 40,
                              child: const Icon(Icons.location_searching),
                              decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                              ),
                            ), onTap: () {
                          _requestCurrentLocation(_mapController);
                        })
                      ],
                    ),
                    _wrapShadow(
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: const Text(
                            'Tìm kiếm',
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                        ), onTap: () {
                      SearchPage.goSelect(context, (prediction) {
                        _bloc.eventMoveToPlace(prediction);
                      });
                    })
                  ],
                ),
              ),
              Positioned(
                  bottom: 30,
                  left: 0,
                  right: 0,
                  child: StreamBuilder<List<MarkerData>>(
                      stream: _bloc.stateShowSelect.stream,
                      builder: (context, ss) {
                        if (ss?.data == null) return const SizedBox.shrink();
                        final data = ss.data;
                        return Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              _wrapShadow(
                                  Container(
                                    height: 40,
                                    width: 40,
                                    child: const Icon(Icons.close),
                                    decoration: const BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                    ),
                                  ), onTap: () {
                                _bloc.eventCloseDetail();
                              }),
                              const SizedBox(
                                height: 10,
                              ),
                              _itemShowSelect(markerDatas: data, isDiseaseModule: widget.isDiseaseModule)
                            ],
                          ),
                        );
                      }))
            ],
          ),
        ),
      ],
    ));
  }

  Widget _itemShowSelect({List<MarkerData> markerDatas, @required bool isDiseaseModule}) {
    Widget image;
    String firstRowTitle;
    String secondRowTitle;
    String thirdRowTitle;
    String firstRowValue;
    String secondRowValue;
    String thirdRowValue;
    DateTime time;

    final listImages = List<Widget>();
    final regions = List<Region>();
    final firstRowTitles = List<String>();
    final secondRowTitles = List<String>();
    final firstRowValues = List<String>();
    final secondRowValues = List<String>();
    final thirdRowValues = List<String>();
    markerDatas.forEach((element) {
      final region = element.region;
      regions.add(region);
      if (isDiseaseModule) {
        final diseaseRegion = element.diseaseRegion;
        image = diseaseRegion?.disease?.listImages != null && diseaseRegion.disease.listImages.isNotEmpty
            ? Image.network(
                diseaseRegion.disease.listImages[0],
                fit: BoxFit.cover,
              )
            : null;
        firstRowTitle = 'Người đăng';
        secondRowTitle = 'Nội dụng';
        thirdRowTitle = 'Loại bệnh';
        firstRowValue = diseaseRegion?.reporterName ?? '';
        secondRowValue = diseaseRegion?.title ?? '';
        thirdRowValue = diseaseRegion?.disease?.name ?? '';
        time = diseaseRegion?.updatedAt;
      } else {
        image = region?.listImages != null && region.listImages.isNotEmpty
            ? Image.network(
                region.listImages[0],
                fit: BoxFit.cover,
              )
            : null;
        firstRowTitle = 'Diện tích';
        secondRowTitle = 'Sản phẩm';
        thirdRowTitle = 'Khu vực';
        firstRowValue = '${region?.area} ha';
        secondRowValue = region?.materialName;
        thirdRowValue = region?.zone?.name;
        time = region?.updatedAt;
      }

      image = image == null
          ? const SizedBox.shrink()
          : Container(
              padding: const EdgeInsets.only(right: 10),
              height: 100,
              width: 100,
              child: image,
            );

      listImages.add(image);
      firstRowTitles.add(firstRowTitle);
      secondRowTitles.add(secondRowTitle);
      firstRowValues.add(firstRowValue);
      secondRowValues.add(secondRowValue);
      thirdRowValues.add(thirdRowValue);
    });

    return SizedBox(
      height: 170,
      width: double.infinity,
      child: PageView.builder(
          onPageChanged: (index) {
            _mapController.animateCamera(CameraUpdate.newLatLngZoom(markerDatas[index].position, 14));
          },
          itemCount: markerDatas.length,
          controller: PageController(viewportFraction: 0.81),
          itemBuilder: (_, index) {
            return GestureDetector(
                child: Container(
                  margin: EdgeInsets.only(right: 20),
                  constraints: const BoxConstraints(minWidth: 180),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white,
                  ),
                  padding: const EdgeInsets.all(15),
                  child: Row(
                    children: [
                      image,
                      Flexible(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                regions[index]?.name ?? '',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 0.2,
                                    color: Colors.black87.withOpacity(0.7)),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Text('$firstRowTitle: $firstRowValue'),
                              Text('$secondRowTitle: $secondRowValue'),
                              Text('$thirdRowTitle: $thirdRowValue'),
                              Text(
                                  'Ngày cập nhật: ${time == null ? '' : DateFormat("dd/MM/yyyy").format(time.toUtc())}'),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  if (widget.isDiseaseModule)
                    TabsRegionPage.goDisease(context, regions[index].toJson());
                  else
                    navigatorKey.currentState
                        .pushNamed(View.tabsRegion, arguments: regions[index].toJson())
                        .then((value) {
                      _bloc.eventRefreshRegionData(markerDatas[index]);
                    });
                });
          }),
    );
  }

  Widget _wrapShadow(Widget child, {Function onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey[400],
            spreadRadius: 1,
            blurRadius: 10,
            offset: const Offset(2, 4), // changes position of shadow
          ),
        ]),
        child: child,
      ),
    );
  }

  void _visibleMap() {
    // Delay to avoid drop frames
    Future.delayed(const Duration(milliseconds: 500)).then((_) {
      setState(() {
        _type = MapType.normal;
      });
    });
  }

  void _requestCurrentLocation(GoogleMapController controller) {
    getCurrentPosition().then((value) {
      controller.moveCamera(CameraUpdate.newLatLng(LatLng(value.latitude, value.longitude)));
    });
  }
}

class WrapReturnInheritanceSizeOnce extends StatelessWidget {
  final Widget child;
  final void Function(Size) onSize;
  const WrapReturnInheritanceSizeOnce(this.child, this.onSize);
  @override
  Widget build(BuildContext context) {
    onSize((context.findRenderObject() as RenderBox).size);
    return child;
  }
}
