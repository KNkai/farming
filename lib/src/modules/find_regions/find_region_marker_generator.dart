import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:mi_smart/base_config/base_config.dart';

class FindRegionMarkerGenerator {
  final _textPadding = const EdgeInsets.all(18);
  final _tp = TextPainter(textDirection: TextDirection.ltr, textAlign: TextAlign.center);

  Future<Uint8List> genTitle(String text, {bool darkMode = false}) async {
    final recorder = PictureRecorder();
    final canvas = Canvas(
      recorder,
    );
    _tp.text = TextSpan(style: TextStyle(color: darkMode ? Colors.white : Colors.black, fontSize: 28), text: text);
    _tp.layout(
      minWidth: 0,
      maxWidth: double.infinity,
    );
    final widthRRect = _tp.size.width + _textPadding.left + _textPadding.right;
    final heightRRect = _tp.size.height + _textPadding.top + _textPadding.bottom;
    final rrectGeneral =
        RRect.fromRectAndRadius(Rect.fromLTWH(0, 0, widthRRect, heightRRect), const Radius.circular(20));
    canvas.drawShadow(
        Path()
          ..addRRect(RRect.fromRectAndRadius(
              Rect.fromLTWH(1.7, 2, widthRRect + 1.7, heightRRect + 2), const Radius.circular(20))),
        Colors.grey[400],
        5,
        true);
    canvas.drawRRect(rrectGeneral, Paint()..color = darkMode ? Colors.black : Colors.white);
    _tp.paint(canvas, Offset(_textPadding.left, _textPadding.top));
    final picture = recorder.endRecording();
    return picture.toImage(_roundUp(widthRRect + 20), _roundUp(heightRRect + 20)) // radius plus
        .then((image) {
      return image.toByteData(format: ImageByteFormat.png).then((byteData) {
        picture.dispose();
        return byteData.buffer.asUint8List();
      });
    });
  }

  Future<Uint8List> genIndexMarker(int index) async {
    final recorder = PictureRecorder();
    final canvas = Canvas(
      recorder,
    );
    final imageByteData = await rootBundle.load('assets/gg_marker.png');
    final image = await decodeImageFromList(imageByteData.buffer.asUint8List());
    canvas.drawImage(image, const Offset(0, 0), Paint());
    _tp.text = TextSpan(
        style: const TextStyle(color: Colors.white, fontSize: 28, fontWeight: FontWeight.bold),
        text: index == null ? '' : index.toString());
    _tp.layout(
      minWidth: image.width.toDouble(),
      maxWidth: image.width.toDouble(),
    );
    _tp.paint(canvas, Offset(0, image.height.toDouble() * 0.165));
    final picture = recorder.endRecording();
    return picture.toImage(image.width, image.height).then((image) {
      return image.toByteData(format: ImageByteFormat.png).then((byteData) {
        return byteData.buffer.asUint8List();
      });
    });
  }

  int _roundUp(double value) {
    return value - value.round().toDouble() < 0 ? value.round() : (value + 1).round();
  }
}
