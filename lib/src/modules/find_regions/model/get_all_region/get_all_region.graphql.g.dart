// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_all_region.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetAllRegionPolygon _$GetAllRegion$Query$RegionPageData$Region$PolygonFromJson(Map<String, dynamic> json) {
  return GetAllRegionPolygon()
    ..paths = (json['paths'] as List)
        ?.map((e) => e == null ? null : GetAllRegion$Query$Polygon$Path.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..strokeColor = fromHex(json['strokeColor'] as String)
    ..strokeOpacity = (json['strokeOpacity'] as num)?.toDouble()
    ..fillColor = fromHex(json['fillColor'] as String)
    ..fillOpacity = (json['fillOpacity'] as num)?.toDouble();
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$Region$PolygonToJson(GetAllRegionPolygon instance) =>
    <String, dynamic>{
      'paths': instance.paths?.map((e) => e?.toJson())?.toList(),
      'strokeColor': instance.strokeColor,
      'strokeOpacity': instance.strokeOpacity,
      'fillColor': instance.fillColor,
      'fillOpacity': instance.fillOpacity,
    };

GetAllRegion$Query$RegionPageData$Region$FarmingType _$GetAllRegion$Query$RegionPageData$Region$FarmingTypeFromJson(
    Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData$Region$FarmingType()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..listImages = (json['listImages'] as List)?.map((e) => e as String)?.toList()
    ..createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String)
    ..regionCount = json['regionCount'] as int;
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$Region$FarmingTypeToJson(
        GetAllRegion$Query$RegionPageData$Region$FarmingType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'listImages': instance.listImages,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'regionCount': instance.regionCount,
    };

GetAllRegion$Query$RegionPageData$Region$Certificate _$GetAllRegion$Query$RegionPageData$Region$CertificateFromJson(
    Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData$Region$Certificate()
    ..title = json['title'] as String
    ..description = json['description'] as String
    ..listImages = (json['listImages'] as List)?.map((e) => e as String)?.toList()
    ..effectiveDate = json['effectiveDate'] == null ? null : DateTime.parse(json['effectiveDate'] as String)
    ..expiredDate = json['expiredDate'] == null ? null : DateTime.parse(json['expiredDate'] as String);
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$Region$CertificateToJson(
        GetAllRegion$Query$RegionPageData$Region$Certificate instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'listImages': instance.listImages,
      'effectiveDate': instance.effectiveDate?.toIso8601String(),
      'expiredDate': instance.expiredDate?.toIso8601String(),
    };

GetAllRegion$Query$RegionPageData$Region$User _$GetAllRegion$Query$RegionPageData$Region$UserFromJson(
    Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData$Region$User()
    ..id = json['id'] as String
    ..uid = json['uid'] as String
    ..email = json['email'] as String
    ..name = json['name'] as String
    ..phone = json['phone'] as String
    ..address = json['address'] as String
    ..avatar = json['avatar'] as String
    ..province = json['province'] as String
    ..district = json['district'] as String
    ..ward = json['ward'] as String
    ..provinceId = json['provinceId'] as String
    ..districtId = json['districtId'] as String
    ..wardId = json['wardId'] as String
    ..role = json['role'] as String
    ..createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String);
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$Region$UserToJson(
        GetAllRegion$Query$RegionPageData$Region$User instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'email': instance.email,
      'name': instance.name,
      'phone': instance.phone,
      'address': instance.address,
      'avatar': instance.avatar,
      'province': instance.province,
      'district': instance.district,
      'ward': instance.ward,
      'provinceId': instance.provinceId,
      'districtId': instance.districtId,
      'wardId': instance.wardId,
      'role': instance.role,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

GetAllRegion$Query$RegionPageData$Region$Zone$Polygon _$GetAllRegion$Query$RegionPageData$Region$Zone$PolygonFromJson(
    Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData$Region$Zone$Polygon()
    ..paths = (json['paths'] as List)
        ?.map((e) => e == null ? null : GetAllRegion$Query$Polygon$Path.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..strokeColor = fromHex(json['strokeColor'] as String)
    ..strokeOpacity = (json['strokeOpacity'] as num)?.toDouble()
    ..fillColor = fromHex(json['fillColor'] as String)
    ..fillOpacity = (json['fillOpacity'] as num)?.toDouble();
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$Region$Zone$PolygonToJson(
        GetAllRegion$Query$RegionPageData$Region$Zone$Polygon instance) =>
    <String, dynamic>{
      'paths': instance.paths?.map((e) => e?.toJson())?.toList(),
      'strokeColor': instance.strokeColor,
      'strokeOpacity': instance.strokeOpacity,
      'fillColor': instance.fillColor,
      'fillOpacity': instance.fillOpacity,
    };

GetAllRegion$Query$RegionPageData$Region$Zone _$GetAllRegion$Query$RegionPageData$Region$ZoneFromJson(
    Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData$Region$Zone()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..locationLat = (json['locationLat'] as num)?.toDouble()
    ..locationLng = (json['locationLng'] as num)?.toDouble()
    ..polygon = json['polygon'] == null
        ? null
        : GetAllRegion$Query$RegionPageData$Region$Zone$Polygon.fromJson(json['polygon'] as Map<String, dynamic>)
    ..createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String)
    ..regionCount = json['regionCount'] as int;
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$Region$ZoneToJson(
        GetAllRegion$Query$RegionPageData$Region$Zone instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'locationLat': instance.locationLat,
      'locationLng': instance.locationLng,
      'polygon': instance.polygon?.toJson(),
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'regionCount': instance.regionCount,
    };

GetAllRegion$Query$RegionPageData$Region$Staff _$GetAllRegion$Query$RegionPageData$Region$StaffFromJson(
    Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData$Region$Staff()
    ..id = json['id'] as String
    ..phone = json['phone'] as String
    ..email = json['email'] as String
    ..name = json['name'] as String
    ..avatar = json['avatar'] as String
    ..uid = json['uid'] as String
    ..isBlock = json['isBlock'] as bool
    ..address = json['address'] as String
    ..province = json['province'] as String
    ..district = json['district'] as String
    ..ward = json['ward'] as String
    ..provinceId = json['provinceId'] as String
    ..districtId = json['districtId'] as String
    ..wardId = json['wardId'] as String
    ..createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String);
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$Region$StaffToJson(
        GetAllRegion$Query$RegionPageData$Region$Staff instance) =>
    <String, dynamic>{
      'id': instance.id,
      'phone': instance.phone,
      'email': instance.email,
      'name': instance.name,
      'avatar': instance.avatar,
      'uid': instance.uid,
      'isBlock': instance.isBlock,
      'address': instance.address,
      'province': instance.province,
      'district': instance.district,
      'ward': instance.ward,
      'provinceId': instance.provinceId,
      'districtId': instance.districtId,
      'wardId': instance.wardId,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

GetAllRegion$Query$RegionPageData$Region$Material _$GetAllRegion$Query$RegionPageData$Region$MaterialFromJson(
    Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData$Region$Material()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..htmlContent = json['htmlContent'] as String
    ..listImages = (json['listImages'] as List)?.map((e) => e as String)?.toList()
    ..thumbnail = json['thumbnail'] as String
    ..createdAt = json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String)
    ..regionCount = json['regionCount'] as int;
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$Region$MaterialToJson(
        GetAllRegion$Query$RegionPageData$Region$Material instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'htmlContent': instance.htmlContent,
      'listImages': instance.listImages,
      'thumbnail': instance.thumbnail,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'regionCount': instance.regionCount,
    };

GetAllRegion$Query$RegionPageData$Pagination _$GetAllRegion$Query$RegionPageData$PaginationFromJson(
    Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData$Pagination()
    ..limit = json['limit'] as int
    ..offset = json['offset'] as int
    ..page = json['page'] as int
    ..total = json['total'] as int;
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageData$PaginationToJson(
        GetAllRegion$Query$RegionPageData$Pagination instance) =>
    <String, dynamic>{
      'limit': instance.limit,
      'offset': instance.offset,
      'page': instance.page,
      'total': instance.total,
    };

GetAllRegion$Query$RegionPageData _$GetAllRegion$Query$RegionPageDataFromJson(Map<String, dynamic> json) {
  return GetAllRegion$Query$RegionPageData()
    ..data = (json['data'] as List)?.map((e) => e == null ? null : Region.fromJson(e as Map<String, dynamic>))?.toList()
    ..total = json['total'] as int
    ..pagination = json['pagination'] == null
        ? null
        : GetAllRegion$Query$RegionPageData$Pagination.fromJson(json['pagination'] as Map<String, dynamic>);
}

Map<String, dynamic> _$GetAllRegion$Query$RegionPageDataToJson(GetAllRegion$Query$RegionPageData instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
      'total': instance.total,
      'pagination': instance.pagination?.toJson(),
    };

GetAllRegion$Query _$GetAllRegion$QueryFromJson(Map<String, dynamic> json) {
  return GetAllRegion$Query()
    ..getAllRegion = json['getAllRegion'] == null
        ? null
        : GetAllRegion$Query$RegionPageData.fromJson(json['getAllRegion'] as Map<String, dynamic>);
}

Map<String, dynamic> _$GetAllRegion$QueryToJson(GetAllRegion$Query instance) => <String, dynamic>{
      'getAllRegion': instance.getAllRegion?.toJson(),
    };

GetAllRegion$QueryGetListInput _$GetAllRegion$QueryGetListInputFromJson(Map<String, dynamic> json) {
  return GetAllRegion$QueryGetListInput(
    limit: json['limit'] as int,
    offset: json['offset'] as int,
    page: json['page'] as int,
    order: json['order'],
    filter: json['filter'],
    search: json['search'] as String,
  );
}

Map<String, dynamic> _$GetAllRegion$QueryGetListInputToJson(GetAllRegion$QueryGetListInput instance) {
  final map = Map<String, dynamic>();
  if (instance.limit != null) map['limit'] = instance.limit;
  if (instance.offset != null) map['offset'] = instance.offset;
  if (instance.page != null) map['page'] = instance.page;
  if (instance.order != null) map['order'] = instance.order;
  if (instance.filter != null) map['filter'] = instance.filter;
  if (instance.search != null) map['search'] = instance.search;
  return map;
}

GetAllRegion$Query$Polygon$Path _$GetAllRegion$Query$Polygon$PathFromJson(Map<String, dynamic> json) {
  return GetAllRegion$Query$Polygon$Path()
    ..lat = (json['lat'] as num)?.toDouble()
    ..lng = (json['lng'] as num)?.toDouble();
}

Map<String, dynamic> _$GetAllRegion$Query$Polygon$PathToJson(GetAllRegion$Query$Polygon$Path instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lng': instance.lng,
    };

GetAllRegionArguments _$GetAllRegionArgumentsFromJson(Map<String, dynamic> json) {
  return GetAllRegionArguments(
    q: json['q'] == null ? null : GetAllRegion$QueryGetListInput.fromJson(json['q'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$GetAllRegionArgumentsToJson(GetAllRegionArguments instance) => <String, dynamic>{
      'q': instance.q?.toJson(),
    };
