// GENERATED CODE - DO NOT MODIFY BY HAND
import 'dart:ui';

import 'package:artemis/artemis.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mi_smart/src/modules/select_area/model/map_model.dart';
import 'package:mi_smart/src/share/utils/util.dart';

part 'get_all_region.graphql.g.dart';

mixin GetAllRegion$PolygonFragMixin {
  List<GetAllRegion$Query$Polygon$Path> paths;
  Color strokeColor;
  double strokeOpacity;
  Color fillColor;
  double fillOpacity;
}

@JsonSerializable(explicitToJson: true)
class GetAllRegionPolygon with EquatableMixin, GetAllRegion$PolygonFragMixin {
  GetAllRegionPolygon();
  factory GetAllRegionPolygon.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$Region$PolygonFromJson(json);

  @override
  List<Object> get props => [paths, strokeColor, strokeOpacity, fillColor, fillOpacity];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$Region$PolygonToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData$Region$FarmingType with EquatableMixin {
  GetAllRegion$Query$RegionPageData$Region$FarmingType();

  factory GetAllRegion$Query$RegionPageData$Region$FarmingType.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$Region$FarmingTypeFromJson(json);

  String id;

  String name;

  String description;

  List<String> listImages;

  DateTime createdAt;

  DateTime updatedAt;

  int regionCount;

  @override
  List<Object> get props => [id, name, description, listImages, createdAt, updatedAt, regionCount];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$Region$FarmingTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData$Region$Certificate with EquatableMixin {
  GetAllRegion$Query$RegionPageData$Region$Certificate();

  factory GetAllRegion$Query$RegionPageData$Region$Certificate.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$Region$CertificateFromJson(json);

  String title;

  String description;

  List<String> listImages;

  DateTime effectiveDate;

  DateTime expiredDate;

  @override
  List<Object> get props => [title, description, listImages, effectiveDate, expiredDate];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$Region$CertificateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData$Region$User with EquatableMixin {
  GetAllRegion$Query$RegionPageData$Region$User();

  factory GetAllRegion$Query$RegionPageData$Region$User.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$Region$UserFromJson(json);

  String id;

  String uid;

  String email;

  String name;

  String phone;

  String address;

  String avatar;

  String province;

  String district;

  String ward;

  String provinceId;

  String districtId;

  String wardId;

  String role;

  DateTime createdAt;

  DateTime updatedAt;

  @override
  List<Object> get props => [
        id,
        uid,
        email,
        name,
        phone,
        address,
        avatar,
        province,
        district,
        ward,
        provinceId,
        districtId,
        wardId,
        role,
        createdAt,
        updatedAt
      ];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$Region$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData$Region$Zone$Polygon with EquatableMixin, GetAllRegion$PolygonFragMixin {
  GetAllRegion$Query$RegionPageData$Region$Zone$Polygon();

  factory GetAllRegion$Query$RegionPageData$Region$Zone$Polygon.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$Region$Zone$PolygonFromJson(json);

  @override
  List<Object> get props => [paths, strokeColor, strokeOpacity, fillColor, fillOpacity];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$Region$Zone$PolygonToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData$Region$Zone with EquatableMixin {
  GetAllRegion$Query$RegionPageData$Region$Zone();

  factory GetAllRegion$Query$RegionPageData$Region$Zone.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$Region$ZoneFromJson(json);

  String id;

  String name;

  double locationLat;

  double locationLng;

  GetAllRegion$Query$RegionPageData$Region$Zone$Polygon polygon;

  DateTime createdAt;

  DateTime updatedAt;

  int regionCount;

  @override
  List<Object> get props => [id, name, locationLat, locationLng, polygon, createdAt, updatedAt, regionCount];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$Region$ZoneToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData$Region$Staff with EquatableMixin {
  GetAllRegion$Query$RegionPageData$Region$Staff();

  factory GetAllRegion$Query$RegionPageData$Region$Staff.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$Region$StaffFromJson(json);

  String id;

  String phone;

  String email;

  String name;

  String avatar;

  String uid;

  bool isBlock;

  String address;

  String province;

  String district;

  String ward;

  String provinceId;

  String districtId;

  String wardId;

  DateTime createdAt;

  DateTime updatedAt;

  @override
  List<Object> get props => [
        id,
        phone,
        email,
        name,
        avatar,
        uid,
        isBlock,
        address,
        province,
        district,
        ward,
        provinceId,
        districtId,
        wardId,
        createdAt,
        updatedAt
      ];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$Region$StaffToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData$Region$Material with EquatableMixin {
  GetAllRegion$Query$RegionPageData$Region$Material();

  factory GetAllRegion$Query$RegionPageData$Region$Material.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$Region$MaterialFromJson(json);

  String id;

  String name;

  String description;

  String htmlContent;

  List<String> listImages;

  String thumbnail;

  DateTime createdAt;

  DateTime updatedAt;

  int regionCount;

  @override
  List<Object> get props =>
      [id, name, description, htmlContent, listImages, thumbnail, createdAt, updatedAt, regionCount];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$Region$MaterialToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData$Pagination with EquatableMixin {
  GetAllRegion$Query$RegionPageData$Pagination();

  factory GetAllRegion$Query$RegionPageData$Pagination.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageData$PaginationFromJson(json);

  int limit;

  int offset;

  int page;

  int total;

  @override
  List<Object> get props => [limit, offset, page, total];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageData$PaginationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$RegionPageData with EquatableMixin {
  GetAllRegion$Query$RegionPageData();

  factory GetAllRegion$Query$RegionPageData.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$RegionPageDataFromJson(json);

  List<Region> data;

  int total;

  GetAllRegion$Query$RegionPageData$Pagination pagination;

  @override
  List<Object> get props => [data, total, pagination];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$RegionPageDataToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query with EquatableMixin {
  GetAllRegion$Query();

  factory GetAllRegion$Query.fromJson(Map<String, dynamic> json) => _$GetAllRegion$QueryFromJson(json);

  GetAllRegion$Query$RegionPageData getAllRegion;

  @override
  List<Object> get props => [getAllRegion];
  Map<String, dynamic> toJson() => _$GetAllRegion$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$QueryGetListInput with EquatableMixin {
  GetAllRegion$QueryGetListInput({this.limit, this.offset, this.page, this.order, this.filter, this.search});

  factory GetAllRegion$QueryGetListInput.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$QueryGetListInputFromJson(json);

  int limit;

  int offset;

  int page;

  dynamic order;

  dynamic filter;

  String search;

  @override
  List<Object> get props => [limit, offset, page, order, filter, search];
  Map<String, dynamic> toJson() => _$GetAllRegion$QueryGetListInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegion$Query$Polygon$Path with EquatableMixin {
  GetAllRegion$Query$Polygon$Path();

  factory GetAllRegion$Query$Polygon$Path.fromJson(Map<String, dynamic> json) =>
      _$GetAllRegion$Query$Polygon$PathFromJson(json);

  double lat;

  double lng;

  @override
  List<Object> get props => [lat, lng];
  Map<String, dynamic> toJson() => _$GetAllRegion$Query$Polygon$PathToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GetAllRegionArguments extends JsonSerializable with EquatableMixin {
  GetAllRegionArguments({this.q});

  factory GetAllRegionArguments.fromJson(Map<String, dynamic> json) => _$GetAllRegionArgumentsFromJson(json);

  final GetAllRegion$QueryGetListInput q;

  @override
  List<Object> get props => [q];
  Map<String, dynamic> toJson() => _$GetAllRegionArgumentsToJson(this);
}

class GetAllRegionQuery extends GraphQLQuery<GetAllRegion$Query, GetAllRegionArguments> {
  GetAllRegionQuery({this.variables});

  @override
  final DocumentNode document = const DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'getAllRegion'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'q')),
              type: NamedTypeNode(name: NameNode(value: 'QueryGetListInput'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'getAllRegion'),
              alias: null,
              arguments: [ArgumentNode(name: NameNode(value: 'q'), value: VariableNode(name: NameNode(value: 'q')))],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'data'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'), alias: null, arguments: [], directives: [], selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'province'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'note'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'listImages'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'district'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'ward'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'provinceId'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'districtId'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'wardId'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'locationLat'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'locationLng'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'polygon'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(
                              selections: [FragmentSpreadNode(name: NameNode(value: 'polygonFrag'), directives: [])])),
                      FieldNode(
                          name: NameNode(value: 'location'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'area'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'managerId'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'zoneId'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'farmingType'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'description'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'listImages'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'createdAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'updatedAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'regionCount'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ])),
                      FieldNode(
                          name: NameNode(value: 'ownerName'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'ownerPhone'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'ownerAddress'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'assigneeIds'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'materialId'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'materialName'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'htmlContent'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'farmingTypeId'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'certificates'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'title'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'description'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'listImages'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'effectiveDate'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'expiredDate'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ])),
                      FieldNode(
                          name: NameNode(value: 'createdAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'updatedAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'manager'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'uid'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'email'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'phone'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'address'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'avatar'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'province'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'district'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'ward'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'provinceId'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'districtId'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'wardId'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'role'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'createdAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'updatedAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ])),
                      FieldNode(
                          name: NameNode(value: 'zone'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'locationLat'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'locationLng'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'polygon'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: SelectionSetNode(selections: [
                                  FragmentSpreadNode(name: NameNode(value: 'polygonFrag'), directives: [])
                                ])),
                            FieldNode(
                                name: NameNode(value: 'createdAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'updatedAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'regionCount'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ])),
                      FieldNode(
                          name: NameNode(value: 'assignees'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'phone'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'email'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'avatar'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'uid'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'isBlock'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'address'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'province'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'district'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'ward'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'provinceId'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'districtId'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'wardId'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'createdAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'updatedAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ])),
                      FieldNode(
                          name: NameNode(value: 'material'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: SelectionSetNode(selections: [
                            FieldNode(
                                name: NameNode(value: 'id'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'name'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'description'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'htmlContent'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'listImages'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'thumbnail'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'createdAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'updatedAt'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null),
                            FieldNode(
                                name: NameNode(value: 'regionCount'),
                                alias: null,
                                arguments: [],
                                directives: [],
                                selectionSet: null)
                          ]))
                    ])),
                FieldNode(
                    name: NameNode(value: 'total'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'pagination'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'limit'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'offset'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'page'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'total'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ])),
    FragmentDefinitionNode(
        name: NameNode(value: 'polygonFrag'),
        typeCondition: TypeConditionNode(on: NamedTypeNode(name: NameNode(value: 'Polygon'), isNonNull: false)),
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'paths'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(name: NameNode(value: 'lat'), alias: null, arguments: [], directives: [], selectionSet: null),
                FieldNode(name: NameNode(value: 'lng'), alias: null, arguments: [], directives: [], selectionSet: null)
              ])),
          FieldNode(
              name: NameNode(value: 'strokeColor'), alias: null, arguments: [], directives: [], selectionSet: null),
          FieldNode(
              name: NameNode(value: 'strokeOpacity'), alias: null, arguments: [], directives: [], selectionSet: null),
          FieldNode(name: NameNode(value: 'fillColor'), alias: null, arguments: [], directives: [], selectionSet: null),
          FieldNode(
              name: NameNode(value: 'fillOpacity'), alias: null, arguments: [], directives: [], selectionSet: null)
        ]))
  ]);

  @override
  final String operationName = 'getAllRegion';

  @override
  final GetAllRegionArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  GetAllRegion$Query parse(Map<String, dynamic> json) => GetAllRegion$Query.fromJson(json);
}
