import 'package:flutter/cupertino.dart';
import 'package:mi_smart/base_config/base_config.dart';

class LoadingWhileFilter extends StatelessWidget {
  final VoidCallback onTap;
  final bool loading;

  const LoadingWhileFilter({this.onTap, this.loading = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minWidth: 120, minHeight: 40),
      decoration:
          BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(10)), color: Colors.white, boxShadow: [
        BoxShadow(
          color: Colors.grey[400],
          spreadRadius: 1,
          blurRadius: 10,
          offset: const Offset(2, 4), // changes position of shadow
        ),
      ]),
      child: Visibility(
        visible: !loading,
        replacement: const CupertinoActivityIndicator(),
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            if (onTap != null) onTap();
          },
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: const [
                Icon(Icons.refresh),
                SizedBox(
                  width: 10,
                ),
                Text('Tìm khu vực này', style: TextStyle(fontWeight: FontWeight.w800))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
