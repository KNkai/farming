import 'dart:io';

import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';

class DashboardPageController {
  final stateInitLoad = ValueNotifier<StateInitDashboard>(StateInitLoading());

  void eventLoadMetaData({bool loading = false}) async {
    if (loading) stateInitLoad.value = StateInitLoading();
    Future.doWhile(() async {
      await Future.delayed(Duration(seconds: 2));
      if (HiveService.instnace.releaseTheKraken) {
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            try {
              await HiveService.instnace.installData();
              stateInitLoad.value = StateInitLoaded();
            } catch (_) {
              stateInitLoad.value = StateInitLoadFailed();
            }
          } else {
            stateInitLoad.value = StateInitLoaded();
          }
        } catch (_) {
          stateInitLoad.value = StateInitLoaded();
        }
        return false;
      }
      return true;
    });
  }

  void dispose() {
    stateInitLoad.dispose();
  }
}

abstract class StateInitDashboard {}

class StateInitLoaded extends StateInitDashboard {}

class StateInitLoadFailed extends StateInitDashboard {}

class StateInitLoading extends StateInitDashboard {}
