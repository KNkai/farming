import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mi_smart/src/modules/dashboard/dashboard_page_controller.dart';
import 'package:mi_smart/src/modules/weather/weather_widget.dart';
import 'package:mi_smart/src/page/qr_action_page.dart';
import 'package:mi_smart/src/page/refresh.dart';
import 'package:package_info/package_info.dart';

import '../../../base_config/base_config.dart';
import '../../../base_widget/src/spacing_box.dart';
import '../../../navigator.dart';
import '../../share/utils/color.dart';
import '../notification/notification_page.dart';
import '../profile/model/profile_model.dart';
import '../profile/profile_repo.dart';
import '../tabs_region_detail/components/image_slider_and_dot.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> with WidgetsBindingObserver, RouteAware, Refresh {
  final _notifierNotify = ValueNotifier<int>(null);
  final _profile = ProfileRepo();
  final bannerImages = [
    'https://mcom-agri.mcom.app/api/setting/redirect/banner1',
    'https://mcom-agri.mcom.app/api/setting/redirect/banner2',
    'https://mcom-agri.mcom.app/api/setting/redirect/banner3'
  ];
  int current = 0;
  final _controller = DashboardPageController();

  @override
  void didChangeDependencies() {
    _getNotifiyCount();
    super.didChangeDependencies();
  }

  @override
  void initState() {
    _controller.eventLoadMetaData();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void pleaseRefresh() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        child: ValueListenableBuilder<StateInitDashboard>(
          valueListenable: _controller.stateInitLoad,
          builder: (_, state, loadedWidget) {
            if (state is StateInitLoading) {
              return Center(
                child: kLoadingSpinner,
              );
            }

            if (state is StateInitLoadFailed) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/logo.png',
                      width: SizeConfig.setWidth(255),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                    GestureDetector(
                      onTap: () {
                        _controller.eventLoadMetaData(loading: true);
                      },
                      child: Text(
                        'Tải lại',
                        style: TextStyle(color: AppColor.primary),
                      ),
                    )
                  ],
                ),
              );
            }

            return loadedWidget;
          },
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ImageSliderAndDot(imageLinks: bannerImages),
              const SpacingBox(height: 1),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 22),
                child: WeatherWidget(),
              ),
              Divider(color: Colors.grey[100], thickness: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SpacingBox(height: SizeConfig.setHeight(27)),
                    Row(
                      children: [
                        Expanded(
                            child: _buildBtn(
                                'Quét mã QR', 'dashboard_qr.png', () => QrActionPage.goFromDashboard(context))),
                        Expanded(
                            child: _buildBtn('Địa điểm\ncanh tác', 'btn1.png',
                                () => navigatorKey.currentState.pushNamed(View.region))),
                        // () => NewDiaryPage.go(context: context))),
                        Expanded(
                          child: _buildBtn('Nhật ký\ncanh tác', 'btn2.png', () {
                            navigatorKey.currentState.pushNamed(View.diary);
                          }),
                        ),
                      ],
                    ),
                    SpacingBox(height: SizeConfig.setHeight(50)),
                    Row(
                      children: [
                        Expanded(
                            child: _buildBtn('Tình hình\ndịch bệnh', 'btn3.png',
                                () => navigatorKey.currentState.pushNamed(View.listDisease))),
                        Expanded(
                            child: ValueListenableBuilder(
                          valueListenable: _notifierNotify,
                          builder: (context, value, child) => _buildBtn(
                              'Thông báo',
                              'btn4.png',
                              () => NotificationPage.go(
                                  context: context,
                                  onBack: () {
                                    _getNotifiyCount();
                                  }),
                              notifyCount: value),
                        )),
                        Expanded(
                            child: _buildBtn(
                                'Thống kê', 'btn5.png', () => navigatorKey.currentState.pushNamed(View.chart))),
                      ],
                    ),
                    SpacingBox(height: SizeConfig.setHeight(50)),
                    Row(
                      children: [
                        Expanded(
                          child: _buildBtn(
                              'Tài khoản', 'btn6.png', () => navigatorKey.currentState.pushNamed(View.profile)),
                        ),
                        Expanded(
                          child: SizedBox.shrink(),
                        ),
                        Expanded(
                          child: SizedBox.shrink(),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              const SpacingBox(height: 2),
              SizedBox(
                width: deviceWidth(context),
                child: Center(
                  child: Material(
                    color: Colors.transparent,
                    child: FutureBuilder<PackageInfo>(
                      initialData: null,
                      future: PackageInfo.fromPlatform(),
                      builder: (_, builder) {
                        if (builder.data != null) {
                          return Text(
                            'Phiên bản ${builder.data.version}',
                            style: const TextStyle(color: Colors.black54, fontSize: 12),
                          );
                        }

                        return const SizedBox.shrink();
                      },
                    ),
                  ),
                ),
              ),
              const SpacingBox(height: 1),
            ],
          ),
        ),
      ),
    );
  }

  void _getNotifiyCount() {
    _profile.getMe().then((value) {
      if (value is ProfileModel) {
        _notifierNotify.value = value.unseenNotify;
      }
    });
  }

  _buildBtn(String text, String image, Function onTap, {int notifyCount}) {
    return InkWell(
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            height: SizeConfig.setWidth(33),
            width: SizeConfig.setWidth(33),
            child: Stack(
              children: [
                Image.asset('assets/$image'),
                if (notifyCount != null && notifyCount > 0)
                  Positioned(
                    right: 2,
                    top: 0,
                    child: Container(
                        height: 15,
                        width: 15,
                        padding: const EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          color: AppColor.warning,
                          borderRadius: BorderRadius.circular(15),
                        )),
                  )
              ],
            ),
          ),
          SizedBox(
            height: SizeConfig.setHeight(15),
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14, color: Colors.black),
            ),
          ),
        ],
      ),
    );
  }
}
