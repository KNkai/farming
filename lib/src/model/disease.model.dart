// To parse this JSON data, do
//
//     final disease = diseaseFromJson(jsonString);

import 'dart:convert';

class Disease {
  Disease({
    this.id,
    this.name,
    this.description,
    this.htmlContent,
    this.listImages,
  });

  String id;
  String name;
  String description;
  String htmlContent;
  List<String> listImages;

  factory Disease.fromRawJson(String str) => Disease.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Disease.fromJson(Map<String, dynamic> json) => Disease(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        htmlContent: json["htmlContent"] == null ? null : json["htmlContent"],
        listImages: json["listImages"] == null ? null : List<String>.from(json["listImages"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "htmlContent": htmlContent == null ? null : htmlContent,
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
      };
}
