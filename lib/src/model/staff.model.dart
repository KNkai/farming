// To parse this JSON data, do
//
//     final staff = staffFromJson(jsonString);

import 'dart:convert';

class Staff {
  Staff({
    this.id,
    this.phone,
    this.email,
    this.name,
    this.avatar,
    this.uid,
    this.isBlock,
    this.address,
    this.province,
    this.district,
    this.ward,
    this.provinceId,
    this.districtId,
    this.wardId,
    this.createdAt,
    this.updatedAt,
  });

  String id;
  String phone;
  String email;
  String name;
  String avatar;
  String uid;
  dynamic isBlock;
  String address;
  String province;
  String district;
  String ward;
  String provinceId;
  String districtId;
  String wardId;
  DateTime createdAt;
  DateTime updatedAt;

  factory Staff.fromRawJson(String str) => Staff.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Staff.fromJson(Map<String, dynamic> json) => Staff(
        id: json["id"] == null ? null : json["id"],
        phone: json["phone"] == null ? null : json["phone"],
        email: json["email"] == null ? null : json["email"],
        name: json["name"] == null ? null : json["name"],
        avatar: json["avatar"] == null ? null : json["avatar"],
        uid: json["uid"] == null ? null : json["uid"],
        isBlock: json["isBlock"],
        address: json["address"] == null ? null : json["address"],
        province: json["province"] == null ? null : json["province"],
        district: json["district"] == null ? null : json["district"],
        ward: json["ward"] == null ? null : json["ward"],
        provinceId: json["provinceId"] == null ? null : json["provinceId"],
        districtId: json["districtId"] == null ? null : json["districtId"],
        wardId: json["wardId"] == null ? null : json["wardId"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "phone": phone == null ? null : phone,
        "email": email == null ? null : email,
        "name": name == null ? null : name,
        "avatar": avatar == null ? null : avatar,
        "uid": uid == null ? null : uid,
        "isBlock": isBlock,
        "address": address == null ? null : address,
        "province": province == null ? null : province,
        "district": district == null ? null : district,
        "ward": ward == null ? null : ward,
        "provinceId": provinceId == null ? null : provinceId,
        "districtId": districtId == null ? null : districtId,
        "wardId": wardId == null ? null : wardId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}
