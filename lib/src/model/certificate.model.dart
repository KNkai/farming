import 'dart:convert';

import 'dart:math';

class Certificate {
  Certificate({
    this.id,
    this.title,
    this.description,
    this.listImages,
    this.effectiveDate,
    this.expiredDate,
  });

  String id;
  String title;
  String description;
  List<String> listImages;
  DateTime effectiveDate;
  DateTime expiredDate;

  factory Certificate.fromRawJson(String str) => Certificate.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Certificate.fromJson(Map<String, dynamic> json) => Certificate(
        id: Random().nextInt(100000000).toString(),
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        listImages: json["listImages"] == null ? null : List<String>.from(json["listImages"].map((x) => x)),
        effectiveDate: json["effectiveDate"] == null ? null : DateTime.parse(json["effectiveDate"]),
        expiredDate: json["expiredDate"] == null ? null : DateTime.parse(json["expiredDate"]),
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
        "effectiveDate": effectiveDate == null ? null : effectiveDate.toIso8601String(),
        "expiredDate": expiredDate == null ? null : expiredDate.toIso8601String(),
      };
}
