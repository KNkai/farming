import 'dart:convert';

import 'package:mi_smart/src/model/attribute.model.dart';

class PhaseLogAttribute {
  PhaseLogAttribute({this.key, this.type, this.display, this.value, this.supplies, this.min, this.max});

  String key;
  String type;
  String display;
  dynamic value;
  Supplies supplies;
  int min;
  int max;

  factory PhaseLogAttribute.fromRawJson(String str) => PhaseLogAttribute.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PhaseLogAttribute.fromJson(Map<String, dynamic> json) {
    return PhaseLogAttribute(
      key: json["key"] == null ? null : json["key"],
      type: json["type"] == null ? null : json["type"],
      display: json["display"] == null ? null : json["display"],
      min: json["min"] == null ? null : json["min"],
      max: json["max"] == null ? null : json["max"],
      value: json["value"] == null ? null : json["value"],
      supplies: json["supplies"] == null ? null : Supplies.fromMap(json["supplies"]),
    );
  }

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "type": type == null ? null : type,
        "display": display == null ? null : display,
        "value": value == null ? null : value,
        "suppliesId": supplies == null ? null : supplies?.id
      };
}
