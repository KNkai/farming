import 'dart:convert';

class Region {
  Region({
    this.id,
    this.name,
    this.province,
    this.district,
    this.ward,
    this.provinceId,
    this.districtId,
    this.wardId,
    this.locationLat,
    this.locationLng,
    this.polygon,
    this.area,
    this.managerId,
    this.zoneId,
    this.ownerName,
    this.ownerPhone,
    this.ownerAddress,
    this.assigneeIds,
    this.materialId,
    this.materialName,
    this.htmlContent,
    this.certificates,
    this.farmingTypeId,
    this.createdAt,
    this.updatedAt,
  });

  String id;
  String name;
  String province;
  String district;
  String ward;
  String provinceId;
  String districtId;
  String wardId;
  double locationLat;
  double locationLng;
  Polygon polygon;
  int area;
  String managerId;
  String zoneId;
  String ownerName;
  String ownerPhone;
  String ownerAddress;
  List<String> assigneeIds;
  String materialId;
  String materialName;
  String htmlContent;
  List<Certificate> certificates;
  String farmingTypeId;
  DateTime createdAt;
  DateTime updatedAt;

  factory Region.fromRawJson(String str) => Region.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Region.fromJson(Map<String, dynamic> json) => Region(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        province: json["province"] == null ? null : json["province"],
        district: json["district"] == null ? null : json["district"],
        ward: json["ward"] == null ? null : json["ward"],
        provinceId: json["provinceId"] == null ? null : json["provinceId"],
        districtId: json["districtId"] == null ? null : json["districtId"],
        wardId: json["wardId"] == null ? null : json["wardId"],
        locationLat: json["locationLat"] == null ? null : json["locationLat"].toDouble(),
        locationLng: json["locationLng"] == null ? null : json["locationLng"].toDouble(),
        polygon: json["polygon"] == null ? null : Polygon.fromJson(json["polygon"]),
        area: json["area"] == null ? null : json["area"],
        managerId: json["managerId"] == null ? null : json["managerId"],
        zoneId: json["zoneId"] == null ? null : json["zoneId"],
        ownerName: json["ownerName"] == null ? null : json["ownerName"],
        ownerPhone: json["ownerPhone"] == null ? null : json["ownerPhone"],
        ownerAddress: json["ownerAddress"] == null ? null : json["ownerAddress"],
        assigneeIds: json["assigneeIds"] == null ? null : List<String>.from(json["assigneeIds"].map((x) => x)),
        materialId: json["materialId"] == null ? null : json["materialId"],
        materialName: json["materialName"] == null ? null : json["materialName"],
        htmlContent: json["htmlContent"] == null ? null : json["htmlContent"],
        certificates: json["certificates"] == null
            ? null
            : List<Certificate>.from(json["certificates"].map((x) => Certificate.fromJson(x))),
        farmingTypeId: json["farmingTypeId"] == null ? null : json["farmingTypeId"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "province": province == null ? null : province,
        "district": district == null ? null : district,
        "ward": ward == null ? null : ward,
        "provinceId": provinceId == null ? null : provinceId,
        "districtId": districtId == null ? null : districtId,
        "wardId": wardId == null ? null : wardId,
        "locationLat": locationLat == null ? null : locationLat,
        "locationLng": locationLng == null ? null : locationLng,
        "polygon": polygon == null ? null : polygon.toJson(),
        "area": area == null ? null : area,
        "managerId": managerId == null ? null : managerId,
        "zoneId": zoneId == null ? null : zoneId,
        "ownerName": ownerName == null ? null : ownerName,
        "ownerPhone": ownerPhone == null ? null : ownerPhone,
        "ownerAddress": ownerAddress == null ? null : ownerAddress,
        "assigneeIds": assigneeIds == null ? null : List<dynamic>.from(assigneeIds.map((x) => x)),
        "materialId": materialId == null ? null : materialId,
        "materialName": materialName == null ? null : materialName,
        "htmlContent": htmlContent == null ? null : htmlContent,
        "certificates": certificates == null ? null : List<dynamic>.from(certificates.map((x) => x.toJson())),
        "farmingTypeId": farmingTypeId == null ? null : farmingTypeId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}

class Certificate {
  Certificate({
    this.title,
    this.description,
    this.listImages,
    this.effectiveDate,
    this.expiredDate,
  });

  String title;
  String description;
  List<String> listImages;
  DateTime effectiveDate;
  DateTime expiredDate;

  factory Certificate.fromRawJson(String str) => Certificate.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Certificate.fromJson(Map<String, dynamic> json) => Certificate(
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        listImages: json["listImages"] == null ? null : List<String>.from(json["listImages"].map((x) => x)),
        effectiveDate: json["effectiveDate"] == null ? null : DateTime.parse(json["effectiveDate"]),
        expiredDate: json["expiredDate"] == null ? null : DateTime.parse(json["expiredDate"]),
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
        "effectiveDate": effectiveDate == null ? null : effectiveDate.toIso8601String(),
        "expiredDate": expiredDate == null ? null : expiredDate.toIso8601String(),
      };
}

class Polygon {
  Polygon({
    this.paths,
    this.strokeColor,
    this.strokeOpacity,
    this.strokeWeight,
    this.fillColor,
    this.fillOpacity,
  });

  List<Path> paths;
  String strokeColor;
  int strokeOpacity;
  int strokeWeight;
  String fillColor;
  double fillOpacity;

  factory Polygon.fromRawJson(String str) => Polygon.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Polygon.fromJson(Map<String, dynamic> json) => Polygon(
        paths: json["paths"] == null ? null : List<Path>.from(json["paths"].map((x) => Path.fromJson(x))),
        strokeColor: json["strokeColor"] == null ? null : json["strokeColor"],
        strokeOpacity: json["strokeOpacity"] == null ? null : json["strokeOpacity"],
        strokeWeight: json["strokeWeight"] == null ? null : json["strokeWeight"],
        fillColor: json["fillColor"] == null ? null : json["fillColor"],
        fillOpacity: json["fillOpacity"] == null ? null : json["fillOpacity"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "paths": paths == null ? null : List<dynamic>.from(paths.map((x) => x.toJson())),
        "strokeColor": strokeColor == null ? null : strokeColor,
        "strokeOpacity": strokeOpacity == null ? null : strokeOpacity,
        "strokeWeight": strokeWeight == null ? null : strokeWeight,
        "fillColor": fillColor == null ? null : fillColor,
        "fillOpacity": fillOpacity == null ? null : fillOpacity,
      };
}

class Path {
  Path({
    this.lat,
    this.lng,
  });

  double lat;
  double lng;

  factory Path.fromRawJson(String str) => Path.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Path.fromJson(Map<String, dynamic> json) => Path(
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lng: json["lng"] == null ? null : json["lng"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
      };
}
