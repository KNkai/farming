// To parse this JSON data, do
//
//     final diseaseSituation = diseaseSituationFromJson(jsonString);

import 'dart:convert';

import 'disease.model.dart';
import 'staff.model.dart';
import 'user.model.dart';

class DiseaseSituation {
  DiseaseSituation(
      {this.id,
      this.regionName,
      this.createdAt,
      this.title,
      this.description,
      this.listImages,
      this.reportedAt,
      this.status,
      this.disease,
      this.commentCount,
      this.reporterName,
      this.reporterAvatar,
      this.reporterId,
      this.user,
      this.staff,
      this.regionId,
      this.diseaseId,
      this.ward,
      this.district,
      this.province});

  String id;
  DateTime createdAt;
  String regionName;
  String title;
  String description;
  List<String> listImages;
  DateTime reportedAt;
  String status;
  Disease disease;
  int commentCount;
  String reporterName;
  String reporterAvatar;
  String reporterId;
  User user;
  Staff staff;
  String regionId;
  String diseaseId;
  String ward;
  String district;
  String province;

  factory DiseaseSituation.fromRawJson(String str) => DiseaseSituation.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DiseaseSituation.fromJson(Map<String, dynamic> json) => DiseaseSituation(
        id: json["id"] == null ? null : json["id"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        listImages: json["listImages"] == null
            ? null
            : List<String>.from(json["listImages"].map((x) {
                final uri = Uri.parse(x);
                if (uri.host.contains('imgur')) {
                  final legoB = uri.pathSegments[uri.pathSegments.length - 1].replaceFirst('.', 'm.');
                  return uri.origin + '/' + legoB;
                }
                return x;
              })),
        reportedAt: json["reportedAt"] == null ? null : DateTime.parse(json["reportedAt"]),
        status: json["status"] == null ? null : json["status"],
        disease: json["disease"] == null ? null : Disease.fromJson(json["disease"]),
        commentCount: json["commentCount"] == null ? null : json["commentCount"],
        reporterName: json["reporterName"] == null ? null : json["reporterName"],
        reporterAvatar: json["reporterAvatar"] == null ? null : json["reporterAvatar"],
        reporterId: json["reporterId"] == null ? null : json["reporterId"],
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        staff: json["staff"] == null ? null : Staff.fromJson(json["staff"]),
        regionId: json["regionId"] == null ? null : json["regionId"],
        diseaseId: json["diseaseId"] == null ? null : json["diseaseId"],
        regionName: json['region']['name'],
        ward: json['region']['ward'],
        district: json['region']['district'],
        province: json['region']['province'],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
        "reportedAt": reportedAt == null ? null : reportedAt.toIso8601String(),
        "status": status == null ? null : status,
        "disease": disease == null ? null : disease.toJson(),
        "commentCount": commentCount == null ? null : commentCount,
        "reporterName": reporterName == null ? null : reporterName,
        "reporterAvatar": reporterAvatar == null ? null : reporterAvatar,
        "reporterId": reporterId == null ? null : reporterId,
        "user": user == null ? null : user.toJson(),
        "staff": staff == null ? null : staff.toJson(),
        "regionId": regionId == null ? null : regionId,
        "diseaseId": diseaseId == null ? null : diseaseId,
        "region": {
          "name": regionName,
          "ward": ward,
          "district": district,
          "province": province,
        }
      };
  String getReporterName() {
    return user != null
        ? user.name
        : staff != null
            ? staff.name
            : reporterName ?? "Chuyên viên";
  }

  String getReporterAvatar() {
    return user != null
        ? user.avatar
        : staff != null
            ? staff.avatar
            : reporterAvatar ?? "https://i.ibb.co/9qCgj4W/default-profile.jpg";
  }
}
