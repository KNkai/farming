// To parse this JSON data, do
//
//     final diseaseComment = diseaseCommentFromJson(jsonString);

import 'dart:convert';

import 'disease.model.dart';
import 'disease_situation.model.dart';
import 'staff.model.dart';
import 'user.model.dart';

class DiseaseComment {
  DiseaseComment({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.message,
    this.listImages,
    this.commenterId,
    this.commenterName,
    this.commenterAvatar,
    this.situationId,
    this.diseaseId,
    this.situation,
    this.disease,
    this.staff,
    this.user,
  });

  String id;
  DateTime createdAt;
  DateTime updatedAt;
  String message;
  List<String> listImages;
  String commenterId;
  String commenterName;
  String commenterAvatar;
  String situationId;
  String diseaseId;
  DiseaseSituation situation;
  Disease disease;
  Staff staff;
  User user;

  factory DiseaseComment.fromRawJson(String str) => DiseaseComment.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DiseaseComment.fromJson(Map<String, dynamic> json) => DiseaseComment(
        id: json["id"] == null ? null : json["id"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        message: json["message"] == null ? null : json["message"],
        listImages: json["listImages"] == null ? null : List<String>.from(json["listImages"].map((x) => x)),
        commenterId: json["commenterId"] == null ? null : json["commenterId"],
        commenterName: json["commenterName"] == null ? null : json["commenterName"],
        commenterAvatar: json["commenterAvatar"] == null ? null : json["commenterAvatar"],
        situationId: json["situationId"] == null ? null : json["situationId"],
        diseaseId: json["diseaseId"] == null ? null : json["diseaseId"],
        situation: json["situation"] == null ? null : DiseaseSituation.fromJson(json["situation"]),
        disease: json["disease"] == null ? null : Disease.fromJson(json["disease"]),
        staff: json["staff"] == null ? null : Staff.fromJson(json["staff"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "message": message == null ? null : message,
        "listImages": listImages == null ? null : List<dynamic>.from(listImages.map((x) => x)),
        "commenterId": commenterId == null ? null : commenterId,
        "commenterName": commenterName == null ? null : commenterName,
        "commenterAvatar": commenterAvatar == null ? null : commenterAvatar,
        "situationId": situationId == null ? null : situationId,
        "diseaseId": diseaseId == null ? null : diseaseId,
        "situation": situation == null ? null : situation.toJson(),
        "disease": disease == null ? null : disease.toJson(),
        "staff": staff == null ? null : staff.toJson(),
        "user": staff == null ? null : user.toJson(),
      };
}
