// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

class User {
  User({
    this.id,
    this.uid,
    this.email,
    this.name,
    this.phone,
    this.address,
    this.avatar,
    this.province,
    this.district,
    this.ward,
  });

  String id;
  String uid;
  String email;
  String name;
  String phone;
  String address;
  String avatar;
  String province;
  String district;
  String ward;

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        uid: json["uid"] == null ? null : json["uid"],
        email: json["email"] == null ? null : json["email"],
        name: json["name"] == null ? null : json["name"],
        phone: json["phone"] == null ? null : json["phone"],
        address: json["address"] == null ? null : json["address"],
        avatar: json["avatar"] == null ? null : json["avatar"],
        province: json["province"] == null ? null : json["province"],
        district: json["district"] == null ? null : json["district"],
        ward: json["ward"] == null ? null : json["ward"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "uid": uid == null ? null : uid,
        "email": email == null ? null : email,
        "name": name == null ? null : name,
        "phone": phone == null ? null : phone,
        "address": address == null ? null : address,
        "avatar": avatar == null ? null : avatar,
        "province": province == null ? null : province,
        "district": district == null ? null : district,
        "ward": ward == null ? null : ward,
      };
}
