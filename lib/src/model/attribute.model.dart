import 'dart:convert';

class Attribute {
  Attribute({this.key, this.type, this.display, this.options, this.min, this.max, this.required, this.supplies});

  String key;
  String type;
  String display;
  List<dynamic> options;
  int min;
  int max;
  bool required;
  final Supplies supplies;

  // factory Attribute.fromRawJson(String str) => Attribute.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Attribute.fromJson(Map<String, dynamic> json) => Attribute(
      key: json["key"] == null ? null : json["key"],
      type: json["type"] == null ? null : json["type"],
      display: json["display"] == null ? null : json["display"],
      options: json["options"] == null ? null : List<dynamic>.from(json["options"].map((x) => x)),
      min: json["min"] == null ? null : json["min"],
      max: json["max"] == null ? null : json["max"],
      supplies: json["supplies"] == null ? null : Supplies.fromMap(json["supplies"]),
      required: json["required"] == null ? false : json["required"]);

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "type": type == null ? null : type,
        "display": display == null ? null : display,
        "options": options == null ? null : List<dynamic>.from(options.map((x) => x)),
        "min": min == null ? null : min,
        "max": max == null ? null : max,
        "required": required == null ? false : required,
      };
}

class Supplies {
  Supplies({
    this.id,
    this.name,
    this.unit,
  });

  final String id;
  final String name;
  final Unit unit;

  factory Supplies.fromMap(Map<String, dynamic> json) => Supplies(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        unit: json["unit"] == null ? null : Unit.fromMap(json["unit"]),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "unit": unit == null ? null : unit.toMap(),
      };
}

class Unit {
  Unit({
    this.id,
    this.name,
  });

  final String id;
  final String name;

  factory Unit.fromMap(Map<String, dynamic> json) => Unit(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
      };
}
