// To parse this JSON data, do
//
//     final phase = phaseFromJson(jsonString);
//     final region = regionFromJson(jsonString);
//     final situation = situationFromJson(jsonString);

import 'dart:convert';

import 'attribute.model.dart';

class Phase {
  Phase({
    this.id,
    this.name,
    this.status,
    this.startAt,
    this.endAt,
    this.updatedAt,
    this.attributes,
  });

  ///id phase
  String id;

  ///name phase
  String name;

  ///Trạng thái phase
  String status;

  ///Ngày bắt đầu phase
  dynamic startAt;

  ///ngày kết thúc phase
  dynamic endAt;

  ///update lần cuối của phase
  DateTime updatedAt;

  ///các giai đoạn của phase
  List<Attribute> attributes;

  factory Phase.fromRawJson(String str) => Phase.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Phase.fromJson(Map<String, dynamic> json) {
    return Phase(
      id: json["id"] == null ? null : json["id"],
      name: json["name"] == null ? null : json["name"],
      status: json["status"] == null ? null : json["status"],
      startAt: json["startAt"] == null ? null : DateTime.parse(json["startAt"]),
      endAt: json["endAt"] == null ? null : DateTime.parse(json["endAt"]),
      updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
      attributes: json["attributes"] == null
          ? null
          : List<Attribute>.from(json["attributes"].map((x) => Attribute.fromJson(x))),
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "status": status == null ? null : status,
        "startAt": startAt == null ? null : startAt.toIso8601String(),
        "endAt": endAt == null ? null : endAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "attributes": attributes == null ? null : List<dynamic>.from(attributes.map((x) => x.toJson())),
      };
}
