import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/src/utils/formart.dart';
import 'package:mi_smart/src/share/utils/color.dart';
import 'package:mi_smart/src/share/utils/hex_color.dart';

import 'phase_log_attribute.model.dart';
import 'staff.model.dart';
import 'user.model.dart';

class PhaseLog {
  PhaseLog({
    this.id,
    this.name,
    this.staffId,
    this.phaseId,
    this.attributes,
    this.color,
    this.time,
    this.createdBy,
    this.createrId,
    this.createrName,
    this.createrAvatar,
    this.createdAt,
    this.updatedAt,
    this.staff,
    this.user,
  });

  String id;
  String name;
  dynamic staffId;
  String phaseId;
  List<PhaseLogAttribute> attributes;
  dynamic color;
  DateTime time;
  String createdBy;
  String createrId;
  String createrName;
  String createrAvatar;
  DateTime createdAt;
  DateTime updatedAt;
  Staff staff;
  User user;

  factory PhaseLog.fromRawJson(String str) => PhaseLog.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PhaseLog.fromJson(Map<String, dynamic> json) => PhaseLog(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        staffId: json["staffId"] == null ? null : json["staffId"],
        phaseId: json["phaseId"] == null ? null : json["phaseId"],
        attributes: json["attributes"] == null
            ? null
            : List<PhaseLogAttribute>.from(json["attributes"].map((x) => PhaseLogAttribute.fromJson(x))),
        color: json["color"] == null ? null : json["color"],
        time: json["time"] == null ? null : DateTime.parse(json["time"]),
        createdBy: json["createdBy"] == null ? null : json["createdBy"],
        createrId: json["createrId"] == null ? null : json["createrId"],
        createrName: json["createrName"] == null ? null : json["createrName"],
        createrAvatar: json["createrAvatar"] == null ? null : json["createrAvatar"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        staff: json["staff"] == null ? null : Staff.fromJson(json["staff"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "staffId": staffId == null ? null : staffId,
        "phaseId": phaseId == null ? null : phaseId,
        "attributes": attributes == null ? null : List<dynamic>.from(attributes.map((x) => x.toJson())),
        "color": color == null ? null : color,
        "time": time == null ? null : time.toIso8601String(),
        "createdBy": createdBy == null ? null : createdBy,
        "createrId": createrId == null ? null : createrId,
        "createrName": createrName == null ? null : createrName,
        "createrAvatar": createrAvatar == null ? null : createrAvatar,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "staff": staff == null ? null : staff.toJson(),
        "user": user == null ? null : user.toJson(),
      };
  String getCreaterName() {
    return user != null
        ? user.name
        : staff != null
            ? staff.name
            : createrName ?? "Chuyên viên";
  }

  String getCreaterAvatar() {
    return user != null
        ? user.avatar
        : staff != null
            ? staff.avatar
            : createrAvatar ?? "https://i.ibb.co/9qCgj4W/default-profile.jpg";
  }

  String updatedAtToString() {
    return updatedAt.isAfter(DateTime.now().subtract(const Duration(days: 7)))
        ? Format.timeAgo(updatedAt)
        : Format.ddsmmsyyyyhhmm(updatedAt);
  }

  Color getColor() {
    return color != null ? Hexcolor.fromHex(color) : AppColor.primary;
  }
}
