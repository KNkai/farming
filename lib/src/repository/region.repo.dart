import 'package:mi_smart/core/graph-repository.dart';
import 'package:mi_smart/src/model/region.model.dart';

class RegionRepository extends GraphRepository<Region> {
  final String shortFragment = "id name";
  final String fullFragment = '''
  id name province provinceId district districtId ward wardId locationLat locationLng area updatedAt ownerName
  polygon { paths { lat lng } strokeColor strokeOpacity strokeWeight fillColor fillOpacity }
  ownerPhone ownerAddress name note
  certificates { title description listImages effectiveDate expiredDate }
  assignees { id phone email name avatar createdAt }
  zoneId zone { name }
  farmingTypeId
  farmingType { id name listImages }
  materialId
  material { id name listImages }
  listImages
  openingDiary { farmingDiaryId phaseId farmingDiary { id name status } phase { id name status } }
  ''';
  final String apiName = "Region";

  Region fromJson(Map<String, dynamic> json) => Region.fromJson(json);
}
