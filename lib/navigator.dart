import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'base_config/base_config.dart';
import 'src/modules/chart/chart_ui.dart';
import 'src/modules/dashboard/dashboard_page.dart';
import 'src/modules/diary/detailDiary_page.dart';
import 'src/modules/diary/listDiary_page.dart';
import 'src/modules/diary_phase/page/phase_page.dart';
// import 'src/modules/diary_phase/page/phase_log_edit_page.dart';
import 'src/modules/disease/disease_situation_page.dart';
import 'src/modules/disease/list_disease_page.dart';
import 'src/modules/intro/intro_page.dart';
import 'src/modules/login/confirm_otp_page.dart';
import 'src/modules/login/login_bloc.dart';
import 'src/modules/login/login_ui.dart';
import 'src/modules/login/reset_password_page.dart';
import 'src/modules/notification/notification_page.dart';
import 'src/modules/profile/profile_page.dart';
import 'src/modules/region/listRegion_page.dart';
import 'src/modules/splash/splash.dart';
import 'src/modules/tabs_region_detail/tabsRegion_page.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class View {
  static const String splash = '1';
  static const String login = '2';
  static const String dashboard = '3';
  static const String intro = '4';
  static const String otp = '5';
  static const String resetPass = '6';
  static const String profile = '7';
  static const String region = '8';
  static const String tabsRegion = '9';
  static const String diary = '10';
  static const String phase = '11';
  static const String detailDiaryPage = '12';
  static const String editPhaseLog = '13';
  static const String notification = '14';
  static const String diseaseDetail = '15';
  static const String listDisease = '16';
  static const String chart = '17';
}

Route<dynamic> onGenerateRoute(RouteSettings settings) {
  switch (settings.name) {
    case View.splash:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => Splash(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.login:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => Login(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.dashboard:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => DashboardPage(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.intro:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => IntroPage(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.otp:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => ConfirmOtpPage(settings.arguments as LoginBloc),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.resetPass:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => ResetPasswordPage(settings.arguments as LoginBloc),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.profile:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => ProfilePage(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.region:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => LisRegionPage(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.tabsRegion:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => TabsRegionPage(settings.arguments as dynamic),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.phase:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => PhasePage(settings.arguments as dynamic),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.diary:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => DiaryPage(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.detailDiaryPage:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => DetailDiaryPage(settings.arguments as dynamic),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    // case View.editPhaseLog:
    //   final arg = settings.arguments as Map;
    //   return PageRouteBuilder(
    //       pageBuilder: (context, animation, secondaryAnimation) =>
    //           PhaseLogEditPage(arg["phaseLog"], arg["isCreate"], phase: arg["phase"]));
    case View.notification:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => const NotificationPage(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.diseaseDetail:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) =>
              DiseaseSituationPage(situationId: settings.arguments),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.listDisease:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => ListDiseasePage(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    case View.chart:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => const ChartUi(),
          transitionsBuilder: _transitionBuilder,
          settings: settings);
    default:
      return MaterialPageRoute(
        builder: (context) => Scaffold(
          body: Center(
            child: Text('Đường dẫn đến ${settings.name} không tồn tại'),
          ),
        ),
      );
  }
}

Widget _transitionBuilder(BuildContext context, Animation<double> start, Animation<double> end, Widget child) {
  var begin = const Offset(0.0, 1.0);
  var end = Offset.zero;
  var curve = Curves.ease;

  var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

  return SlideTransition(
    position: start.drive(tween),
    child: child,
  );
}
