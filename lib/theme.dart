import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mi_smart/src/share/utils/color.dart';

ThemeData buildThemeData() {
  return ThemeData(
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
        selectedItemColor: AppColor.primary, unselectedItemColor: Color(0xffBDC1BB), showSelectedLabels: true),
    primarySwatch: Colors.green,
    textTheme: TextTheme(
      bodyText2: GoogleFonts.openSans(letterSpacing: 0.05, fontSize: 17),
    ),
  );
}
