import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';

class SpacingBox extends StatelessWidget {
  final double height;
  final double width;

  const SpacingBox({this.height = 0, this.width = 0});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: SizeConfig.setHeight(height),
      width: SizeConfig.setWidth(width),
    );
  }
}
