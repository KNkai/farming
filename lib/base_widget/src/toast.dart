import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mi_smart/src/share/utils/color.dart';

Future showToast(String mes, {bool flagColor = true}) async {
  await Fluttertoast.cancel();
  return Fluttertoast.showToast(
      fontSize: 13,
      msg: mes,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 2,
      backgroundColor: flagColor ? AppColor.primary.withOpacity(0.6) : Colors.red.withOpacity(0.75),
      textColor: Colors.white);
}

class ToastInstance {
  static bool _status;

  static bool get isToastOn => _status;

  static turnOff() {
    Fluttertoast.cancel();
    _status = false;
  }

  static turnOn() {
    _status = true;
  }
}
