import 'package:flutter/material.dart';
import 'package:mi_smart/base_config/base_config.dart';
import 'package:mi_smart/base_widget/src/spacing_box.dart';

class EmptyDataWidget extends StatelessWidget {
  final title;
  final bool showImage;
  const EmptyDataWidget(this.title, {this.showImage = true});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SpacingBox(height: 1),
          if (showImage) ...[
            Image.asset(
              'assets/list.png',
              height: 40,
            ),
            const SpacingBox(height: 1),
          ],
          const SpacingBox(height: 1),
          Opacity(
            child: Text(
              title,
              style: TextStyle(
                color: Colors.black54,
              ),
            ),
            opacity: .5,
          ),
        ],
      ),
    );
  }
}
