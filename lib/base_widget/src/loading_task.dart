import 'package:flutter/material.dart';

import '../../base_config/base_config.dart';
import '../../base_config/src/base/base_bloc.dart';

class LoadingTask extends StatelessWidget {
  final Widget child;
  final BaseBloc bloc;
  final Color color;

  const LoadingTask({
    @required this.child,
    @required this.bloc,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: bloc.loadingStream,
      initialData: false,
      builder: (context, snapshot) {
        return Stack(
          children: <Widget>[
            IgnorePointer(
              ignoring: snapshot.data,
              child: child,
            ),
            snapshot.data
                ? Center(child: kLoadingSpinner)
                // Center(
                //     child: ScaleAnimation(
                //     child: Container(
                //       width: SizeConfig.widthMultiplier * 30,
                //       height: SizeConfig.widthMultiplier * 30,
                //       decoration: BoxDecoration(
                //         color: Colors.white70,
                //         borderRadius: BorderRadius.all(
                //           Radius.circular(8),
                //         ),
                //       ),
                //       child: SizedBox(
                //         height: SizeConfig.widthMultiplier * 10,
                //         width: SizeConfig.widthMultiplier * 10,
                //         child: CircularProgressIndicator(),
                //       ),
                //     ),
                //   ))
                : const SizedBox.shrink()
          ],
        );
      },
    );
  }
}
