import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mi_smart/src/page/hive_service/hive_service.dart';
import 'package:mi_smart/src/share/utils/util.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'base_config/base_config.dart';
import 'base_config/src/firebase/firebase_service.dart';
import 'navigator.dart';
import 'theme.dart';

void main() async {
  await HiveService.instnace.init();
  timeago.setLocaleMessages('vi', timeago.ViMessages());
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(MyApp());
  });
  // Permissions
  FirebaseService.instance.init();
  onLocationWhenInUsePermissionRequest();
}

Image logo = Image.asset('assets/logo.png');
final routeObserver = RouteObserver<PageRoute>();

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    HiveService.instnace.runService();
    SizeConfig.init(
        // iPhone 11
        designHeight: 896.0,
        designWidth: 414.0);
    precacheImage(logo.image, context);
    return MaterialApp(
      localizationsDelegates: const [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en'),
        Locale('vi'),
      ],
      theme: buildThemeData(),
      navigatorKey: navigatorKey,
      onGenerateRoute: onGenerateRoute,
      initialRoute: View.splash,
      debugShowCheckedModeBanner: false,
      navigatorObservers: [routeObserver],
    );
  }
}
