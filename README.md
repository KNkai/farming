# Hướng dẫn build để deploy App Distribute & TestFlight

### App Distribute (Android)

* Mở terminal với đường dẫn trỏ tới thử mục thuộc project.
* Chạy các lệnh sau:
```sh
flutter clean
flutter build apk
```
* Terminal sẽ trả về 1 link dẫn tới file  APK  release:
```sh
✓ Built build/app/outputs/flutter-apk/app-release.apk (26.6MB).
```

### TestFlight (iOS)
* Yêu cầu: Xcode Version 11.7 (11E801a) (hoặc mới nhất)
* Cài provisioning profile dev vào máy (agri_dev.mobileprovision).
* Cài provisioning distribution vào máy (agri_prod.mobileprovision).
* Cài file p12 developer vào máy (p12_dev.p12).
* Cài file p12 distribution vào máy (p12_distribution.p12).
* Đổi cấu hình build
![](https://i.imgur.com/8tcecAo.gif)
* Nâng số build, version
![](https://i.imgur.com/zf7uwr7.png)
* Nhấn Archive (note: Nếu build failed, chạy debug trên một iphone device trước)
![](https://i.imgur.com/klhiWZ9.png)
* Chọn bản build mới nhất và bấm ‘Disribute App’, rồi next cho tới bước tiếp theo
![](https://i.imgur.com/a89039N.png)
* Chọn certificate và provisioning file phù hợp và next cho tới khi hiện ra  nút 'Upload'
![](https://i.imgur.com/25TwEjN.png)
